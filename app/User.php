<?php

namespace App;

use App\Models\Blog;
use App\Models\ContactUs;
use App\Models\Product;
use App\Models\Profile;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded=[];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function profile()
    {
        return $this->hasOne(Profile::class,'user_id');
    }

    public function products()
    {
        return $this->belongsToMany(Product::class,'user_product', 'user_id', 'product_id');
    }

    public function blogs()
    {
        return $this->belongsToMany(Blog::class);
    }

    public function readings()
    {
        return $this->hasMany('App\Reading', 'user_id');
    }

    public function ContactUs()
    {
        return $this->hasMany(ContactUs::class);
    }
}
