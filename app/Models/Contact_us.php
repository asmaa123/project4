<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contact_us extends Model 
{

    protected $table = 'contact_us';
    public $timestamps = true;
    protected $fillable = array('email', 'content', 'name','user_id');

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}