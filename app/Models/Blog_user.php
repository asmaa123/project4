<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog_user extends Model 
{

    protected $table = 'blog_user';
    public $timestamps = true;
    protected $fillable = array('blog_id', 'user_id');

}