<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Measurement_times extends Model 
{

    protected $table = 'measurement_times';
    public $timestamps = true;
    protected $fillable = array('name','status');

    public function readings()
    {
        return $this->hasMany('App\Models\Reading');
    }

}