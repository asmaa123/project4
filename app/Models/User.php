<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    protected $table = 'users';
    public $timestamps = true;
    protected $guarded = array('remember_token');
    protected $fillable = array('name', 'email', 'password');

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function products()
    {
        return $this->belongsToMany('App\Product');
    }

    public function blogs()
    {
        return $this->belongsToMany('App\Blog');
    }

    public function readings()
    {
        return $this->hasMany('App\Reading', 'user_id');
    }

    public function contact_uss()
    {
        return $this->hasMany('App\Contact_us');
    }

}