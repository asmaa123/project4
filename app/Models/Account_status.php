<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Account_status extends Model 
{

    protected $table = 'account_status';
    public $timestamps = true;
    protected $fillable = array('name');

    public function profiles()
    {
        return $this->hasMany('App\Profile');
    }

}