<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_product extends Model 
{

    protected $table = 'user_product';
    public $timestamps = true;
    protected $fillable = array('user_id', 'product_id');


}