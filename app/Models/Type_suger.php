<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_suger extends Model 
{

    protected $table = 'type_sugers';
    public $timestamps = true;
    protected $fillable = array('name');

    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }

}