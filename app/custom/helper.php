<?php

if (!function_exists('svgToPng')) {
    function svgToPng($svg_as_str, $img_name, $folder = "countries")
    {
        $path = public_path('/uploads/' . $folder . '/') . $img_name . '.png';
        if (!file_exists($path)) {
            $image = new Imagick();

            $image->setBackgroundColor(new ImagickPixel('transparent'));
            $image->readImageBlob($svg_as_str);
            $image->setImageFormat("png32");
            // check if request directory is exists & create it
            if (!\File::exists(public_path('/uploads/' . $folder))) {
                \File::makeDirectory(public_path('/uploads/' . $folder), $mode = 0777, true, true);
            }
            // save image in path
            $image->writeImage($path);
        }
        return  '/uploads/' . $folder . '/' . $img_name . '.png';
    }
}

function image_upload($cntrl,$imageRequest,$post_id="",$path = '/upload',$folder = 'products',$thumps = false,$wMark = true,$width = '250',$heigth = '250'){
    if($imageRequest[0] != null){
        $imgnum = 1;
        $exten  = array('jpeg','bmp','png','jpg','jpe','gif','svg','svgz');
        $result = ['names' => [],'imgExt' => []];
        require('I18N/Arabic.php');
        $Arabic = new I18N_Arabic('Glyphs');
        foreach ($imageRequest as $image) {
            if(in_array_ci($image->getClientOriginalExtension(),$exten)){
                $destinationPath = public_path().$path."/".$folder;
                if(in_array("settings",Request::segments())){
                    $imageName = 'waterMarkTest.png';
                }else{
                    $totalNum = time() + $imgnum;
                    $imageName = 'image_'.$post_id.'_'.$totalNum.'.'.$image->getClientOriginalExtension();
                    \App\UpImage::create([
                        'post_id' => $post_id,
                        'img_name' => $imageName,
                        'type' => $cntrl->getTable()
                    ]);
                }
                $image->move($destinationPath,$imageName);
                #======================== water mark ========================
                if($wMark == true){
                    if(getSettings('WM_active') == 1){

                        if(getSettings('WM_type') == 1){

                            $oldWMStr = public_path().'/upload/logo/waterMarkMake.png';
                            $newWMStr = public_path().'/upload/logo/WM_img.png';
                            \Illuminate\Support\Facades\File::delete($oldWMStr);
                            \Illuminate\Support\Facades\File::copy($newWMStr,$oldWMStr);

                            $wmImage = \Intervention\Image\Facades\Image::make(public_path('upload/logo/waterMarkMake.png'))
                                ->resize(getSettings('WM_imgWidth'), getSettings('WM_imgHeight'))
                                ->opacity(getSettings('WM_opacity'))
                                ->save();

                            \Intervention\Image\Facades\Image::make($destinationPath.'/'.$imageName)
                                ->insert($wmImage,getSettings('WM_position'), 10, 10)
                                ->save();

                        }else{

                            $oldWMStr = public_path().'/upload/images/WMStr.png';
                            $newWMStr = public_path().'/upload/images/WM-copied.png';
                            \Illuminate\Support\Facades\File::delete($oldWMStr);
                            \Illuminate\Support\Facades\File::copy($newWMStr,$oldWMStr);

                            $opacity = getSettings('WM_opacity') / 100;

                            $text = getSettings('WM_str');
                            $text = $Arabic->utf8Glyphs($text);

                            $draw = new ImagickDraw();
                            $draw->setFillColor(getSettings('WM_strColor'));
                            $draw->setFont(public_path().'/upload/fonts/DroidKufi-Regular.ttf');
                            $draw->setFontSize(getSettings('WM_strSize')); //use a large font-size
                            $draw->setStrokeColor(getSettings('WM_setStrokeColor'));
                            $draw->setStrokeWidth(1);
                            $draw->setStrokeAntialias(true);  //try with and without
                            $draw->setTextAntialias(true);  //try with and without
                            $draw->setFillOpacity($opacity);

                            $outputImage = new Imagick();
                            $outputImage->newImage(1400,1400, "transparent");  //transparent canvas
                            $outputImage->annotateImage($draw, 100, 100, getSettings('WM_strAngle'), $text);
                            $outputImage->trimImage(0); //Cut off transparent border
                            $outputImage->setsize(1000,1000); //Sets the size of the Gmagick object

                            \Intervention\Image\Facades\Image::make($destinationPath.'/'.$imageName)
                                ->insert($outputImage,getSettings('WM_position'),10, 10)
                                ->save();
                        }
                    }
                }
                #============================================================
                $result['names'][] =  $imageName;
                $imgnum++;
            }else{
                $result['imgExt'][] = $image->getClientOriginalName();
            }
        }
        if($thumps === true){
            foreach ($result['names'] as $name) {
                $thumpsPath = public_path().'/upload/'.$folder.'/thumps/thumps_'.$name;
                \Intervention\Image\Facades\Image::make($destinationPath.'/'.$name)->resize($width,$heigth)->save($thumpsPath,100);
            }
        }
        return $result;
    }
}

function image_check($imageName,$folder = 'posts'){
    $path = public_path().'/upload/'.$folder.'/'.$imageName;
    if($imageName == ''){
        return Request::root().'/public/upload/logo/no_image.png';
    }
    if(file_exists($path)){
        return Request::root().'/public/upload/'.$folder.'/'.$imageName;
    }else{
        return Request::root().'/public/upload/logo/no_image.png';
    }
}
function image_check_thumps($imageName,$folder = 'posts'){
    $path = public_path().'/upload/'.$folder.'/thumps/thumps_'.$imageName;
    if($imageName == ''){
        return Request::root().'/public/upload/logo/thumps_empty.png';
    }
    if(file_exists($path)){
        return Request::root().'/public/upload/'.$folder.'/thumps/thumps_'.$imageName;
    }else{
        return Request::root().'/public/upload/logo/thumps_empty.png';
    }
}

function singleImg($image,$folder,$id=""){
    $exten  = array('jpeg','bmp','png','jpg','jpe','gif','svg','svgz');
    if(in_array_ci($image->getClientOriginalExtension(),$exten)){
        $destinationPath = public_path().'/upload/'.$folder;
        $imageName = $id.'-'.time().'.'.$image->getClientOriginalExtension();
        $image->move($destinationPath,$imageName);
        return $imageName;
    }else{
        return 'no_image.png';
    }
}
#================ get current lang Function ========================================

function getJsonData($jsonData, $curLang='ar'){
    return json_decode($jsonData)->$curLang;
}


function getApiJsonData($jsonData, $curLang){
    return json_decode($jsonData)->$curLang;
}
#================ Site Settings Function ========================================

function getSettings($set = 'SiteName'){
    return \App\SiteSetting::where('name',$set)->first()->value;
}

#====================== time to ar srting ==========================================

function timeToStr($timestamp, $num_times = 2) {
    if(App::isLocale('ar')){
        $times = array(
            31536000 => 'سنة',
            2592000 => 'شهر',
            604800 => 'اسبوع',
            86400 => 'يوم',
            3600 => 'س',
            60 => 'د',
            1 => 'ث'
        );
    }else if(App::isLocale('en')){
        $times = array(
            31536000 => 'year',
            2592000 => 'month',
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'min',
            1 => 'sec'
        );
    }

    $now = time() - 3600;
    $timestamp -= 3600;
    $secs = $now - $timestamp;

// Fix so that something is always displayed
    if ($secs == 0) {
        $secs = 1;
    }

    $count = 0;
    $time = '';

    foreach ($times as $key => $value) {
        if ($secs >= $key) {
// time found
            $s = '';
            $time .= floor($secs / $key);

            if ((floor($secs / $key) != 1))
                $s = ' ';

            $time .= ' ' . $value . $s;
            $count ++;
            $secs = $secs % $key;

            if ($count > $num_times - 1 || $secs == 0)
                break;
            else
                $time .= ' ، ';
        }
    }
    $st =  $time;
    return $st;
}