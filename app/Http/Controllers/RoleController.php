<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use DB;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct()
    {
         $this->middleware('permission:role-list', ['only' => ['index','show']]);
         $this->middleware('permission:role-create', ['only' => ['create','store']]);
         $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $permission = Permission::where('type','admin')->orderBy('orderBy')->orderBy('name')->get();
        $roles = Role::all();
        return view('admin.roles.index',compact('roles','permission'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $permission = Permission::where('type','admin')->orderBy('orderBy')->orderBy('name')->get();
        return view('admin.roles.add',compact('permission'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'slug' => 'required|unique:roles,slug',
            'permission' => 'required'
        ],[
            'name.required' => 'فضلا أكتب إسم الصلاحيه',         
            'slug.required' => 'فضلا أكتب إسم الصلاحيه',         
            'permission.required' => 'فضلا أختر الصلاحيات'
        ]);
        $role = Role::create([
            'guard_name' => 'admin',
            'name' => $request->input('name'),
            'slug' => $request->slug
            ]);
        $role->syncPermissions($request->input('permission'));


        return redirect()->route('roles.index')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم إنشاء المجموعه بنجاح']));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $rolePermissions = Permission::join("role_has_permissions","role_has_permissions.permission_id","=","permissions.id")
            ->where("role_has_permissions.role_id",$id)
            ->orderBy('orderBy')->orderBy('name')->get();


        return view('admin.roles.show',compact('role','rolePermissions'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::where('type','admin')->orderBy('orderBy')->orderBy('name')->get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();


        return view('admin.roles.edit',compact('role','permission','rolePermissions'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'slug' => 'required',
            'permission' => 'required',
        ],[
            'slug.required' => 'فضلا أكتب إسم الصلاحيه',         
            'permission.required' => 'فضلا أختر الصلاحيات'          
        ]);

        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->slug = $request->slug;
        $role->save();

        $role->syncPermissions($request->input('permission'));

        return redirect()->route('roles.index')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم التعديل بنجاح']));
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(in_array($id,[1])){
            Session()->flash('error_flash_message','لا يمكن حذف صلاحيات المدير العام');
            return back();
        }
        DB::table("roles")->where('id',$id)->delete();
        DB::table("model_has_roles")->where('role_id',$id)->delete();
        return redirect()->route('roles.index')->withFlashMessage(json_encode(['success'=>true,'msg'=>'تم الحذف بنجاح']));
    }

    public function UsersHasRole ($roleName)
    {
        $users = User::role($roleName)->get();
        return view('admin.roles.users',compact('users','roleName'));
    }
}
