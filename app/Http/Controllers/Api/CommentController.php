<?php

namespace App\Http\Controllers\Api;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    use  apiResponseTrait;

    public function addcomment(Request $request){
        $user = Auth::user();

        $rule=[
            'comment'=>'required',
            'blog_id'=>'required',
        ];

        $validator=validator()->make($request->all(),$rule);

        if($validator->fails()){

            return $this->apiResponse($validator);
        }


        $comment=new Comment();
        $comment->user_id=Auth::id();
        $comment->blog_id=$request->blog_id;
        $comment->content=$request->comment;
        $comment->save();
        return $this->apiResponse($comment);

    }

    public function showcomment($id){
//        $user = Auth::user();
//
//        $rule=[
//            'blog_id'=>'required',
//
//        ];
//
//        $validator=validator()->make($request->all(),$rule);
//
//        if($validator->fails()){
//
//            return $this->apiResponse(['error'=>$validator->errors()], 401);
//        }
        $coments=Comment::where('blog_id',$id)->with(['User','User.profile'])->orderby('created_at','DESC')->get();

        if (isset($coments) && count($coments)>0) {
            $comments=$this->paginate($coments);
            return $this->apiResponse($comments);
        }else{
            return $this->apiResponse([],404);

        }


    }
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
