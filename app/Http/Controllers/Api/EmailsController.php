<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailsController extends Controller
{
    use apiResponseTrait;
    public $email = '';

    public function send($email,$str)
    {
        $title = 'العنوان';
        $content = 'العنوان';
        $this->email = $email;

        Mail::send('email', ['password'=>$str], function ($message)
        {

            $message->from('info@cicrapp.maakaweb.com');

            $message->to($this->email);
            $message->subject('password reset');
            $message->priority(999);

        });


        return $this->apiResponse(['من فضلك افحص بريدك الالكتروني','password'=>$str]);
    }}
