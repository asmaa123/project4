<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AccountStatusResource;
use App\Models\Account_status;
use Illuminate\Http\Request;

class AccountStatusController extends Controller
{
    use apiResponseTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $accout_status = Account_status::all();
        $accoutStatus= AccountStatusResource::collection($accout_status);
        return $this->apiResponse($accoutStatus);
    }

}
