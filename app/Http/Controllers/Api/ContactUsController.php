<?php

namespace App\Http\Controllers\Api;

use App\Models\ContactUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ContactUsController extends Controller
{
use apiResponseTrait;

    public function addcontact(Request $request){

        $user_id=Auth::id();
        $rule=[
            'name'=>'required',
            'email'=>'required',
            'message'=>'required',

        ];

        $validator=validator()->make($request->all(),$rule);

        if($validator->fails()){

            return $this->apiResponse($validator);
        }

        $contact_us=new ContactUs();
        $contact_us->name=$request->name;
        $contact_us->email=$request->email;
        $contact_us->content=$request->message;
        $contact_us->user_id=$user_id;
        $contact_us->save();

            return $this->apiResponse($contact_us);







    }

}
