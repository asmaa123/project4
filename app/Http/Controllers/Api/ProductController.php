<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\ProductResource;
use App\Models\User_product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{

    use  apiResponseTrait;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $user = Auth::user();
        $product=Product::where(function($query) use($id) {


            if(isset($id)){

                $query->where('catogery_id',$id);
            }

        })->get();

        $products= ProductResource::collection($product);
        $items = $this->paginate($products);

        return  $this->apiResponse($items);

    }

    public function searchPro(Request $request){

        $user = Auth::user();
        $product=Product::where('name', 'LIKE', "%{$request->search_pro}%")->whereHas('catogery',function($q)use($request){
            $q->where('id',$request->category_id);
        })->with('catogery')->get();
         if (isset($product) && count($product)>0) {
            $products= ProductResource::collection($product);
            return $this->apiResponse($products);
        }else{
            return $this->apiResponse([],404);

        }
    }

    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

    public function detailProduct(Request $request)
    {

        $pro=Product::where('id',$request->product_id)->pluck('name')->first();

        $product=Product::select('weight','carbonydrate','protine','calories','fiber')->where('id',$request->product_id)->first();

        $product->setAttribute('name', getApiJsonData($pro,request()->header('accept-language')));

        return $this->apiResponse($product);
    }

    public function post_like(Request $request)
    {
        $user = Auth::user();
        $validator = Validator::make($request->all(), [
            'product_id'=>'required',
        ]);

        if ($validator->fails()) {
            return $this->apiResponse($validator);
        }
        $id = $request->product_id;
        $product = Product::find($id);
        $use_pro=User_product::where('user_id',Auth::id())->where('product_id',$id)->first();
        if ($request->type=='add'){
            if (isset($use_pro)){
                return $this->apiResponse(['count_likes'=>User_product::where('product_id',$id)->count(),'likes_status'=>2]);

            }else {
                $user->products()->attach($product);
            }
        }
        elseif ($request->type=='delete'){
            $user->products()->detach($product);
        }
//        $user->products()->toggle($id);
        return $this->apiResponse(['count_likes'=>User_product::where('product_id',$id)->count(),'likes_status'=>User_product::where('user_id',Auth::id())->where('product_id',$id)->count()]);

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
