<?php

namespace App\Http\Controllers\Api;

use App\Models\Type_suger;
use App\Models\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Mail;


class UserController extends Controller
{
    use apiResponseTrait;
    public $successStatus = 200;


    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        $messages= [];
        if ($request->lang == 'ar'){
            $messages = [

                'email.required' => 'من فضلك ادخل البريد الالكترونى',

                'lang.required' => 'اللغة مطلوبة',
                'password.required' => 'من فضلك ادخل الرقم السرى',


            ];
        }
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required',

        ],$messages);


        if ($validator->fails()) {
            return $this->apiResponse(['error'=>$validator->errors()], 401);
        }
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();
            $token =  $user->createToken('MyApp')->accessToken;
            return $this->apiResponse(['token' => $token,'user' => $user, 'profile' => $user->profile], $this->successStatus);
        }
        else{
            return $this->apiResponse(['error'=>'Unauthorised','msg'=>'لقد أدخلت بيانات غير صحيحة'], 401);
        }
    }


    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */

    public function register(Request $request)
    {
        $messages= [];
        if ($request->lang == 'ar'){
            $messages = [
                'name.required' => 'من فضلك ادخل الاسم',
                'name.min' => 'من فضلك ادخل الاسم لا يقل عن3  ',
                'name.max' => 'من فضلك ادخل الاسم لا يزيد عن 15 ',
                'email.required' => 'من فضلك ادخل البريد الالكترونى',
                'email.unique' => 'البريد الالكتروني موجود',
                'lang.required' => 'اللغة مطلوبة',
                'password.required' => 'من فضلك ادخل الرقم السرى',
                'gender.required' => 'من فضلك ادخل الجنس',
                'weight.required' => 'من فضلك ادخل الوزن',
                'c_password.required' => 'من فضلك ادخل الرقم السرى',
                'c_password.same' => 'من فضلك ادخل الرقم السرى متطابق',
                'acount_status_id.required' => 'من فضلك ادخل حاله حساب صحيحه',
                'type_sugers_id.required' => 'من فضلك ادخل نوع السكر صحيحه',
                'age.required' => 'من فضلك ادخل العمر صحيح',
                'date_injury.required' => 'من فضلك ادخل تاريخ الاصابه',
                'height.required' => 'من فضلك ادخل طول صحيح',

            ];
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:15',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'phone' => 'required|min:13|numeric',
            'c_password' => 'required|same:password',
            'gender' => 'required',
            'weight' => 'required',
            'acount_status_id' => 'required',
            'type_sugers_id' => 'required',
        ],$messages);


        if ($validator->fails()) {
            return $this->apiResponse(['error'=>$validator->errors()], 401);
        }
        $input = array_except($request->all(),'c_password');
        $input['password'] = bcrypt($input['password']);
//        $user = User::create($input);
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = $input['password'];
        $user->remember_token = Str::random(60);
        $user->save();

        $profile= Profile::create($request->except('name', 'email','password','c_password'));
        $user->profile()->save($profile);
        $token =  $user->createToken('MyApp')->accessToken;

        return $this->apiResponse(['token' => $token,'user' => $user,$user->profile],$this->successStatus);
    }


    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }

    public function updateProfile(Request $request){
        $token = str_replace('Bearer ', '', $request->header('Authorization'));

        $validator = Validator::make($request->all(), [

            'email' => 'email|unique:users',
            'password' => '',
            'phone' => 'min:11|numeric',
            'c_password' => 'same:password',
        ]);

        if ($validator->fails()) {
            return $this->apiResponse(['error'=>$validator->errors()], 401);
        }



        $user = User::findorfail(Auth::id());
        $user->name = isset($request->name)?$request->name:$user->name;
        $user->email = isset($request->email)?$request->email:$user->email;
        $user->remember_token = Str::random(60);
        $user->save();


        $profile=Profile::where('user_id',Auth::id())->first();
        if ($request->hasFile('photo')) {
            $picture_name = 'uploads'.'/'.time().str_shuffle('abcdef').'.'.$request->file('photo')->getClientOriginalExtension();
//            $picture_name = explode("'\'",$picture_name);
//            unset($picture_name);
            Image::make($request->file('photo'))->save(public_path("$picture_name"));
            $request->request->set('image', $picture_name);
            $profile->image=$picture_name;
        }//end if

        $profile->fill($request->except('photo', 'email','password','c_password'));

        $profile->save();
        $user->token = $token;


        return $this->apiResponse(['user' => $user, 'profile' => $user->profile],$this->successStatus);

    }

    public function resetPassword(Request $request){
        if (isset($request->email)&&count(User::where('email',$request->email)->get())){
            $user = User::where('email',$request->email)->first();
            //generate random string
            $str = "";
            $characters = array_merge( range('0','9'));
            $max = count($characters) - 1;
            for ($i = 0; $i < 4; $i++) {
                $rand = mt_rand(0, $max);
                $str .= $characters[$rand];
            }
            $user->password = bcrypt($str);
            $user->save();
            

            return app('App\Http\Controllers\Api\EmailsController')->send($request->email,$str);

        }else{


            return $this->apiResponse('Email does not exist',404);
        }
    }//end resetPassword


//reset password
    public function changePassword(Request $request){
        $firstTenChar = mb_substr($request->word, 0,10);
        if ($firstTenChar=='RrAlDyi02Q') {
            $validator = Validator::make($request->all(), [
                'password' => 'required',
            ]);
            if ($validator->fails()) {

                return $this->apiResponse(['error' => $validator->errors()], 401);
            }

            $input = array_except($request->all(), 'c_password');
            $input['password'] = bcrypt($input['password']);

            $user = User::where('email', $request->email)->first();
            $user->password = $input['password'];
            $user->save();

            $token = $user->createToken('MyApp')->accessToken;

            return $this->apiResponse(['token' => $token, 'user' => $user, 'profile' => $user->profile], $this->successStatus);
        }
        else{
            return $this->apiResponse(['word'=>'غير صحيحه']);
        }
    }//end update_password

//    when login
    public function update_password(Request $request){


        $validator = Validator::make($request->all(), [

            'old_password' => 'required',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {

            return $this->apiResponse(['error'=>$validator->errors()], 401);
        }

        if(Auth::attempt([ 'email' => Auth::user()->email, 'password' => request('old_password')])){
            $input = array_except($request->all(),'c_password');
            $input['password'] = bcrypt($input['password']);

            $user=Auth::user();
            $user->password=$input['password'];
            $user->save();

            $token =  $user->createToken('MyApp')->accessToken;

            return $this->apiResponse(['token' => $token,'user' => $user, 'profile' => $user->profile],$this->successStatus);
        }else{
            return $this->apiResponse('كلمة المرور القديمة غير صحيحية',404);

        }

    }//end update_password





}
