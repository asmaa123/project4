<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\MeasurementTimeResource;
use App\Models\Measurement_times;
use App\Models\Reading;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ReadingSugarController extends Controller
{
    use  apiResponseTrait;

    //

    public function Measurement_time(){
        $user = Auth::user();

        $Measurement_time=Measurement_times::all();
        $Measurement_times= MeasurementTimeResource::collection($Measurement_time);

        return $this->apiResponse($Measurement_times);

    }

    public function readingSugar(Request $request){
        $user = Auth::user();


        $validator = Validator::make($request->all(), [
            'measurement_id'=>'required',
        ]);

        if ($validator->fails()) {
            return $this->apiResponse($validator);


        }

        $reading=new Reading();
        $reading->user_id=Auth::id();
        $reading->measurement_id=$request->measurement_id;
        $reading->time=$request->date;
        $reading->count=$request->qty_sugar;
        $reading->save();

        return $this->apiResponse($reading);


    }

     public function recordReading($id){
        $user = Auth::user();

//
//        $validator = Validator::make($request->all(), [
//            'measurement_id'=>'required',
//        ]);
//
//        if ($validator->fails()) {
//            return $this->apiResponse($validator);
//
//
//        }

        $readings=Reading::where('user_id',Auth::id())->whereHas('measurement_times',function($query) use($id) {
            $query->where('measurement_id', $id);
        })->get();

        if (isset($readings) && count($readings)>0) {
            $items = $this->paginate($readings);

            return $this->apiResponse($items);
        }else{
            return $this->apiResponse([]);

        }
    }

    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
}
