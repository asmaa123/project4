<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use App\Models\Measurement_times;

class MeasurmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $measurement=Measurement_times::paginate(30);

        return view('admin.measurement.index',compact('measurement'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.measurement.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'name.ar'=>'required',
             'name.en'=>'required',
            'status'=>'required'

        ];

        $message=[

            'name.requied'=>'this is required',
            'status.required'=>'this status is required'

        ];

        $this->validate($request,$rule,$message);


        $cat=Measurement_times::create(['name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),
        
                                       'status' => $request->status,
        
                                             ]);
        
        
     if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
        };


        flash()->message('تم إضافة الوحده بنجاح');

        return redirect(route('measurements.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prods=Measurement_times::find($id);

        return view('admin.measurement.edit',compact('prods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $prods=Measurement_times::find($id);
        $prods->update(['name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),
       'status' => $request->status
        ]);

 if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $prods->image = $filename;
            $prods->save();
        };


        flash()->message('تم تعديل  الوحده بنجاح');

        return redirect(route('measurements.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat=Measurement_times::find($id);
        $cat->delete();
        flash()->message('تم حذف الوحده بنجاح');
        return back();
    }
}
