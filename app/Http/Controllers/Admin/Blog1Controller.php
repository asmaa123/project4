<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use Intervention\Image\Facades\Image;

class Blog1Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs=Blog::where('type',1)->get();

        return view('admin.blog1.index',compact('blogs'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.blog1.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
  $rule=[
            
            'text.ar'=>'required',
            'title.ar'=>'required',
               'text.en'=>'required',
            'title.en'=>'required',
            'image'=>'required'
            
        ];

        $message=[

            
            'text.required'=>'this is required',
            'image.required'=>'this image is required',
            'title.required'=>'this is required'
          
        ];

        $this->validate($request,$rule,$message);

        $cat=Blog::create(['text'=> json_encode($request->text, JSON_UNESCAPED_UNICODE),
                            'title'=> json_encode($request->title, JSON_UNESCAPED_UNICODE),
                            'type'=>1,


                          ]);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
        };


        flash()->message('تم إضافة المدونه بنجاح');

        return redirect(route('disbates.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $cat=Blog::find($id);

        return view('admin.blog1.edit',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat=Blog::find($id);
        $cat->update([
            'text'=> json_encode($request->text, JSON_UNESCAPED_UNICODE),
            'title'=> json_encode($request->title, JSON_UNESCAPED_UNICODE),
            'type'=>1,

        ]);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
        };


        flash()->message('تم تعديل المدونه بنجاح');

        return redirect(route('disbates.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog=Blog::find($id);
        $blog->delete();
        flash()->message('تم حذف المدونه بنجاح');
        return back();
    }
}
