<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Medicine;
use Intervention\Image\Facades\Image;

class MedicineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $mids=Medicine::all();

       return view('admin.medicine.index',compact('mids'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.medicine.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'name.ar'=>'required',
            'name.en'=>'required',
            'image'=>'required',
            'text.ar'=>'required',
            'text.en'=>'required'


        ];

        $message=[

            'name.requied'=>'this is required',
            'image.requied'=>'this is required',
            'text.requied'=>'this is required'
        ];

        $this->validate($request,$rule,$message);


        $cat=Medicine::create(['name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),
                               'text'=> json_encode($request->text, JSON_UNESCAPED_UNICODE)]
            );

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
        };

        flash()->message('تم إضافة الدواء بنجاح');

        return redirect(route('medicines.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat=Medicine::find($id);

        return view('admin.medicine.edit',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat=Medicine::find($id);
        $cat->update(['name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),
                'text'=> json_encode($request->text, JSON_UNESCAPED_UNICODE)]
        );
        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
        };


        flash()->message('تم تعديل الدواء بنجاح');

        return redirect(route('medicines.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prods=Medicine::find($id);
        $prods->delete();
        flash()->message('تم حذف الدواء بنجاح');
        return back();
    }
}
