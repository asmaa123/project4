<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Type_suger;

class TypesugerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $sugers=Type_suger::all();

       return view('admin.sugers.index',compact('sugers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.sugers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rule=[
            'name.ar'=>'required|max:191',
             'name.en'=>'required|max:191'

        ];

        $message=[

            'name.requied'=>'this is required'
        ];

        $this->validate($request,$rule,$message);

        $cat=Type_suger::create(['name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE)]);



        flash()->message('تم إضافة النوع بنجاح');

        return redirect(route('sugers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat=Type_suger::find($id);

        return view('admin.sugers.edit',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat=Type_suger::find($id);


        $cat->update(['name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE)]);



        flash()->message('تم التعديل بنجاح');

        return redirect(route('sugers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat=Type_suger::find($id);
        $cat->delete();
        flash()->message('تم حذف النوع بنجاح');
        return back();
    }
}
