<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Advertisement;

use Intervention\Image\Facades\Image;

class AdvertisementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $advertisements=Advertisement::all();

        return view('admin.advertisements.index',compact('advertisements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.advertisements.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[


            'image'=>'required'

        ];

        $message=[


            'image.required'=>'this image is required'
        ];

        $this->validate($request,$rule,$message);

        $advertisement=Advertisement::create($request->all());


        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $advertisement->image = $filename;
            $advertisement->save();
        };


        flash()->message('تم إضافة الاعلان بنجاح');

        return redirect(route('advertisements.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $advertisement=Advertisement::find($id);

        return view('admin.advertisements.edit',compact('advertisement'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $advertisement=Advertisement::find($id);
        $advertisement->update($request->all());



        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $advertisement->image = $filename;
            $advertisement->save();
        };


        flash()->message('تم تعديل الاعلان بنجاح');

        return redirect(route('advertisements.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $advertisdement=Advertisement::find($id);
        $advertisdement->delete();

        flash()->message('تم الحزف بنجاح');

        return back();

    }
}
