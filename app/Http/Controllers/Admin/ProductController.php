<?php

namespace App\Http\Controllers\Admin;


use App\Models\Catogery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Intervention\Image\Facades\Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $prods=Product::with('catogery')->get();

       return view('admin.product.index',compact('prods'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
     return view('admin.product.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'name.ar'=>'required',
             'name.en'=>'required',
            'image'=>'required',
            'weight'=>'required|numeric',
            'carbonydrate'=>'required|numeric',
            'protine'=>'required|numeric',
            'calories'=>'required|numeric',
             'fiber'=>'required|numeric',
            


        ];

        $message=[

            'name.requied'=>'this is required|max:191',
            'image.requied'=>'this is required',
             'weight.requied'=>'this is required',
              'carbonydrate.requied'=>'this is required',
               'protine.requied'=>'this is required',
                'calories.requied'=>'this is required',
               'fiber.requied'=>'this is required'
            
        ];

        $this->validate($request,$rule,$message);


        $cat=Product::create([
                              'name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),
                              'weight'=> $request->weight,
                              'carbonydrate'=>$request->carbonydrate,
                              'protine'=>$request->protine,
                             'calories'=>$request->protine,
                              'fiber'=> $request->fiber,
                             'catogery_id'=> $request->catogery_id,

                             ]);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
        };



        flash()->message('تم إضافة المنتج بنجاح');

        return redirect(route('products.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prods=Product::find($id);

        return view('admin.product.edit',compact('prods'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $prods=Product::find($id);
        $prods->update([        'name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),
                              'weight'=> $request->weight,
                              'carbonydrate'=>$request->carbonydrate,
                              'protine'=>$request->protine,
                             'calories'=>$request->protine,
                              'fiber'=> $request->fiber,
                             'catogery_id'=> $request->catogery_id,

                             ]);
        
        
        
        
        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('/uploads/' . $filename ) );
            $prods->image = $filename;
            $prods->save();
        };



        flash()->message('تم تعديل  المنتج بنجاح');

        return redirect(route('products.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $prods=Product::find($id);
        $prods->delete();
        flash()->message('تم حذف المنتج بنجاح');
        return back();
    }
}
