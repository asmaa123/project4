<?php

namespace App\Http\Resources;

use App\Models\Blog_user;
use App\Models\Comment;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,

            'image' => 'uploads'.'/'.$this->image,

            'title' => getApiJsonData($this->title,request()->header('accept-language')),

            'text' => getApiJsonData($this->text,request()->header('accept-language')),

            'created_at' => Carbon::createFromFormat('Y-m-d H:i:s',$this->created_at )->format('Y-m-d'),
            'count_likes' =>Blog_user::where('blog_id',$this->id)->count(),
            'like_status' =>Blog_user::where('user_id',Auth::id())->where('blog_id',$this->id)->count(),
            'count_comments' =>Comment::where('blog_id',$this->id)->count(),




        ];
    }


}
