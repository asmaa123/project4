<?php

namespace App\Http\Resources;

use App\Models\Product;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductFavResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product=Product::where('id',$this->product_id)->first();
        return [
            'id' => $product->id,

            'weight' => $product->weight,

            'name' => getApiJsonData($product->name,request()->header('accept-language')),

            'protine' => $product->protine,

            'calories' => $product->calories,

            'fiber' => $product->fiber,

            'carbonydrate' => $product->carbonydrate,
            'image' =>'uploads/'. $product->image,

            'created_at' => $product->created_at,

            'updated_at' => $product->updated_at



        ];
    }
}
