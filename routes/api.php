<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/art', function () {


    \Illuminate\Support\Facades\Artisan::call('config:cache');
    //  \Illuminate\Support\Facades\Artisan::call('make:model',['name'=>'Join_us']);
});

Route::post('login', 'UserController@login');
Route::post('register', 'UserController@register');
Route::get('typeSugars', 'TypeSugerController@index');
Route::get('AccountStatus', 'AccountStatusController@index');
Route::post('changePassword', 'UserController@changePassword');
Route::post('reset_password', 'UserController@resetPassword');
Route::post('getNotifications', 'NotificationsController@getNotifications');
Route::get('CancelNotifications', 'NotificationsController@update');

Route::group(['middleware' => 'auth:api'], function(){
Route::post('details', 'UserController@details');
    Route::get('showblogs/{type}', 'BlogController@index');
    Route::get('catogeries', 'CatogeryController@index');
    Route::get('products/{id}/', 'ProductController@index');
    Route::post('searchPro', 'ProductController@searchPro');
    Route::get('detailProduct', 'ProductController@detailProduct');
    Route::post('post_like', 'ProductController@post_like');
    Route::post('add_like', 'DiabeteseController@postLikeBlog');
    Route::get('MeasurementTime', 'ReadingSugarController@Measurement_time');
    Route::post('readingSugar', 'ReadingSugarController@readingSugar');
    Route::get('recordReading/{id}', 'ReadingSugarController@recordReading');
    Route::get('diabetesFed', 'DiabetesFedController@index');
    Route::get('terms_condition', 'DiabetesFedController@terms_condition');
    Route::get('about_app', 'DiabetesFedController@about_app');
    Route::get('Diabeteseducation/{type}', 'DiabeteseController@index');
    Route::Post('addcomment', 'CommentController@addcomment');
    Route::get('showComment/{id}', 'CommentController@showcomment');
    Route::post('postLikeBlog', 'DiabeteseController@postLikeBlog');
    Route::get('medicine', 'MedicineController@index');
    Route::post('updateProfile', 'UserController@updateProfile');
    Route::get('favProduct', 'FavouriteController@favProduct');
    Route::get('favBlog', 'FavouriteController@favBlog');
    Route::get('settings', 'SettingController@index');
    Route::Post('contactUs', 'ContactUsController@addcontact');
    Route::post('update_password', 'UserController@update_password');
    Route::get('logOut', 'UserController@logOut');
    Route::get('detailBlog', 'BlogController@detailBlog');
    Route::post('updatePassword', 'UserController@changePassword');
    Route::get('detail_notification/{id}', 'NotificationsController@detail_notification');




});



Route::group(['prefix'=>'v1'],function() {













});
