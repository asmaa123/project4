@extends('admin.layouts.app')
@inject('category','App\Models\Static_page')
@section('title')
تعديل الصفحات
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/pages')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالضفحات</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text"></span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						تعديل بيانات
					</h3>
				</div>
			</div>
		</div>
		<!--begin::Form-->

		{!!Form::model($prods,[
    'action'=>['Admin\PagesController@update',$prods->id],
    'method'=>'put',
    'files'=>true,
    'class'=>'m-form m-form--fit m-form--label-align-right'
    ]) !!}



		<div class="col-lg-12">
			<label class=" col-form-label">اسم الصفحه عربي</label>

			<div class="col-lg-9">


				{!!Form::text('name[ar]',getJsonData($prods->name,'ar'),[
                'class'=>'form-control m-input','placeholder'=>" اسم الصفحه عربي"
                ])!!}
			</div>
		</div>

		<div class="col-lg-12">
			<label class="col-form-label">  اسم الصفحه انجليزي </label>

			<div class="col-lg-9">


				{!!Form::text('name[en]',getJsonData($prods->name,'en'),[
                'class'=>'form-control m-input','placeholder'=>"اسم الصفحه انجليزي"
                ])!!}
			</div>
		</div>






				<div class="col-lg-12">
					<label class=" col-form-label">العنوان عربي</label>

					<div class="col-lg-9">


						{!!Form::textArea('title[ar]',getJsonData($prods->title,'ar'),[
                        'class'=>'form-control m-input','placeholder'=>"العنوان عربي"
                        ])!!}
					</div>
				</div>

				<div class="col-lg-12">
					<label class="col-form-label"> العنوان انجليزي </label>

					<div class="col-lg-9">



				{!!Form::textArea('title[en]',getJsonData($prods->title,'en'),[
                'class'=>'form-control m-input','placeholder'=>"العنوان انجليزي"
                ])!!}
			</div>
		</div>




		<div class="col-lg-12">
			<label class=" col-form-label">النص عربي</label>

			<div class="col-lg-9">


				{!!Form::textArea('text[ar]',getJsonData($prods->text,'ar'),[
                'class'=>'form-control m-input','placeholder'=>" النص عربي"
                ])!!}
			</div>
		</div>

		<div class="col-lg-12">
			<label class="col-form-label">  النص انجليزي </label>

			<div class="col-lg-9">


				{!!Form::textArea('text[en]',getJsonData($prods->text,'en'),[
                'class'=>'form-control m-input','placeholder'=>" النص انجليزي"
                ])!!}
			</div>

		</div>


		<div class="col-lg-12">
			<label class="col-lg-1 col-form-label">  الصوره</label>

			<div class="col-lg-9">


				{!!Form::file('image',null,[
                'class'=>'form-control m-input',
                ])!!}
			</div>


		</div>


		<br>
		<div class='form-group' >
			<br>
			<button class="btn btn-primary" type="submit"> تعديل</button>
		</div>


		<!--end::Form-->
	</div>

	{!! Form::close()!!}
	<!--end::Portlet-->
	</div>
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

