<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">إحصائيات عامة</span>
            </span>
        </span>
    </a>
</li>






<li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
    <a href="javascript:;" class="m-menu__link m-menu__toggle">
        <i class="m-menu__link-icon flaticon-layers"></i>
        <span class="m-menu__link-text">إعدادات الموقع</span>
        <i class="m-menu__ver-arrow la la-angle-right"></i>
    </a>
    <div class="m-menu__submenu ">
        <span class="m-menu__arrow"></span>
        <ul class="m-menu__subnav">
            <li class="m-menu__item " aria-haspopup="true">
                <a href="{{url('admincp/settings')}}" class="m-menu__link ">
                    <span class="m-menu__link-wrap flaticon-cogwheel-2 menu-drop-icon">
                    <span class="m-menu__link-text">الإعدادت العامه</span>
                    
                    </span>
                </a>
            </li>

        </ul>
    </div>
</li>





<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/catogeries')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاقسام</span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/medicines')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-squares-3"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الادويه السكري</span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/products')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-web"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المنتجات</span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/blogs')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المدونات</span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/disbates')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-responsive"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">التثقيفات</span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/settings')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-settings-1" ></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الاعدادات </span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/user')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-users "></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">المستخدمين </span>
            </span>
        </span>
    </a>
</li>




<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/admins')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-profile-1 "></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الادمن </span>
            </span>
        </span>
    </a>
</li>




<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/pages')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-grid-menu-v2"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الصفحات </span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/reading')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-notepad"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">قراءات السكر  </span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/sugers')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">النوعيه السكري </span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/measurements')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-line-graph"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">وحدات القياس  </span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/messagers')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-multimedia"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">الرسائل </span>
            </span>
        </span>
    </a>
</li>



<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/contact_us')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-mail-1"></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">تواصل معنا  </span>
            <span class="kt-menu__link-badge"><span class="kt-badge kt-badge--rounded kt-badge--brand btn-danger fa fa-2x">

                @php  $cout=App\Models\Contact_us::where('read_at',0)->count()

                @endphp
                    {{$cout}}

                </span></span>
            </span>
        </span>
    </a>
</li>


<li class="m-menu__item" aria-haspopup="true">
    <a href="{{url('admincp/account_status')}}" class="m-menu__link ">
        <i class="m-menu__link-icon flaticon-user-settings" ></i>
        <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">اعدادت الحساب</span>
            </span>
        </span>
    </a>
</li>












<div class="m-menu__submenu ">
    <span class="m-menu__arrow"></span>
    <ul class="m-menu__subnav">
        <li class="m-menu__item " aria-haspopup="true">
            <a href="#" class="m-menu__link ">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                    <span></span>
                </i>
                <span class="m-menu__link-text">قائمة أولى</span>
            </a>
        </li>


        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true" m-menu-submenu-toggle="hover">
            <a href="javascript:;" class="m-menu__link m-menu__toggle">
                <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                <span class="m-menu__link-text">قائمة فرعيه</span>
                <i class="m-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="m-menu__submenu ">
                <span class="m-menu__arrow"></span>
                <ul class="m-menu__subnav">
                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="#" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot">
                                <span></span>
                            </i>
                            <span class="m-menu__link-text">قائمة</span>
                        </a>
                    </li>



                    <li class="m-menu__item " aria-haspopup="true">
                        <a href="#" class="m-menu__link ">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">قائمة</span>
                        </a>
                    </li>
                </ul>
            </div>
        </li>

        <li class="m-menu__item" aria-haspopup="true">
            <a href="{{url('admincp/users')}}" class="m-menu__link ">
                <i class="m-menu__link-icon flaticon-file"></i>
                <span class="m-menu__link-title">
            <span class="m-menu__link-wrap">
                <span class="m-menu__link-text">user</span>
            </span>
        </span>
            </a>
        </li>



    </ul>
</div>
</li>
