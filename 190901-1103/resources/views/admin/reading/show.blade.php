@extends('admin.layouts.app')

@section('title')
    تفاصيل القراءات
@endsection
@section('topBar')
    <li class="m-menu__item">
        <a href="{{url('admincp')}}" class="m-menu__link">
            <span class="m-menu__link-text">الرئيسية</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item">
        <a href="{{url('admincp/reading')}}" class="m-menu__link">
            <span class="m-menu__link-text">التحكم بالقراءات</span>
            <i class="m-menu__hor-arrow la la-angle-left"></i>
        </a>
    </li>
    <li class="m-menu__item" style="background: #e0deff;">
        <a href="javascript:;" class="m-menu__link">
            <span class="m-menu__link-text">القراءات</span>
            <i class="m-menu__hor-arrow la la-angle-down"></i>
        </a>
    </li>
@endsection

@section('header')
@endsection

@section('content')
    <!--begin::Portlet-->
    <div class="m-portlet">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
                    <h3 class="m-portlet__head-text">
                        تفاصيل القراءات
                    </h3>
                </div>
            </div>
        </div>

        @if($readings->count())

            <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_users">
                <thead>

            <tr>
                <th>#</th>
                <th>الاسم </th>

                <th>التاريخ </th>

                <th>العدد</th>
                <th>الوقت</th>


            </tr>

        @foreach($readings as $read)
            <tr class="debTr">
                <th>{{$read->id}}</th>
                <th>{{getJsonData(optional($read->measurement_times)->name)}}</th>


                
                <th>{{$read->time}}</th>


                <th>{{$read->measurement_times->id}}</th>

                <th>{{optional($read->measurement_times)->created_at}}</th>

            </tr>
                </thead>

                @endforeach
                @else

                    <div class="alert alert-danger">
                        no data
                    </div>
            @endif
            </table>














    {!! Form::close()!!}
    <!--end::Form-->
    </div>
    <!--end::Portlet-->
@endsection
@section('footer')
    <script type="text/javascript">

    </script>
@endsection

