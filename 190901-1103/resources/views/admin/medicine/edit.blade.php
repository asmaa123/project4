@extends('admin.layouts.app')

@section('title')
	الادويه السكري
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/medicines')}}" class="m-menu__link">
			<span class="m-menu__link-text">الادويه السكري</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text">الادويه السكري</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">

					</h3>
				</div>
			</div>
		</div>

	{!!Form::model($cat,[
'action'=>['Admin\MedicineController@update',$cat->id],
'method'=>'put',
'files'=>true,
'class'=>'m-form m-form--fit m-form--label-align-right'
]) !!}



			<div class="col-lg-12">
			<label class="col-form-label">اسم الاداء عربي</label>

			<div class="col-lg-9">


			{!!Form::text('name[ar]',getJsonData($cat->name,'ar'),[
            'class'=>'form-control m-input','placeholder'=>"اسم الاداء عربي"
            ])!!}
			</div>
			</div>



		<div class="col-lg-12">
			<label class="col-form-label">اسم الاداء انجليزي</label>

			<div class="col-lg-9">


				{!!Form::text('name[en]',getJsonData($cat->name,'en'),[
                'class'=>'form-control m-input','placeholder'=>"اسم الاداء انجليزي"
                ])!!}
			</div>
		</div>


		<div class="col-lg-12">
			<label class=" col-form-label"> النص عربي </label>
			<div class="col-lg-6">


				{!!Form::textarea('text[ar]',getJsonData($cat->text,'ar'),[
                'class'=>'form-control m-input','placeholder'=>"النص عربي"
                ])!!}
			</div>
			<label class=" col-form-label"> النص انجليزي </label>
			<div class="col-lg-6">


				{!!Form::textarea('text[en]',getJsonData($cat->text,'en'),[
                'class'=>'form-control m-input','placeholder'=>"النص انجليزي"
                ])!!}
			</div>

		</div>

			<div class="col-lg-12">
				<label class="col-lg-1 col-form-label"> الصوره</label>


				<div class="col-lg-9">
					{!!Form::file('image',null,[
                'class'=>'form-control m-input'
                ])!!}
				</div>
			</div>
<br><br>
			<br>
			<div class='form-group' >
				<br>
				<button class="btn btn-primary" type="submit"> تعديل</button>
			</div>

		</div>




		{!! Form::close()!!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

