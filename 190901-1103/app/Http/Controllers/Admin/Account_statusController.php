<?php

namespace App\Http\Controllers\Admin;

use App\Models\Account_status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Account_statusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs=Account_status::paginate(30);

        return view('admin.account_status.index',compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.account_status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'name.ar'=>'required',
              'name.en'=>'required'

        ];

        $message=[

            'name.requied'=>'this is required',

        ];

        $this->validate($request,$rule,$message);

        $cat=Account_status::create(['name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),


        ]);




        flash()->message('تم إضافة بنجاح');

        return redirect(route('account_status.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat=Account_status::find($id);

        return view('admin.account_status.edit',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cat=Account_status::find($id);
        $cat->update(['name'=> json_encode($request->name, JSON_UNESCAPED_UNICODE),


        ]);


        flash()->message('تم تعديل  بنجاح');

        return redirect(route('account_status.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog=Account_status::find($id);

      
        $blog->delete();
        flash()->message('تم حذف  بنجاح');
        return back();
    }
}
