<?php

namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Admin;
use Intervention\Image\Facades\Image;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admin=Admin::all();

        return view('admin.admin.index',compact('admin'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.admin.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rule=[
            'name'=>'required|max:191',
            'email'=>'required',
            'password'=>'required'
        ];

        $message=[
            'name.requied'=>'this is required',
            'email.requied'=>'this is required',
             'password.requied'=>'this is required'
        ];

        $this->validate($request,$rule,$message);


       $input =Admin:: create($request->all());
        $input['password'] = bcrypt($input['password']);

     $admin=new Admin();
     $admin->name=$request->name;
     $admin->email=$request->email;
     $admin->password = $input['password'];


        flash()->message('تم إضافة  بنجاح');

        return redirect(route('admins.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat=Admin::find($id);



        return view('admin.admin.edit',compact('cat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
  {
        $cat=Admin::find($id);
        $cat->update($request->all());
          




        flash()->message('تم تعديل  بنجاح');

        return redirect(route('admins.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat=Admin::find($id);
        $cat->delete();
        flash()->message('تم حذف  بنجاح');
        return back();
    }
}
