<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Intervention\Image\Facades\Image;


use App\Http\Controllers\Controller;

use App\Models\Messager;

class MessagerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $messagers=Messager::with('User')->get();

        return view('admin.messager.index',compact('messagers'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.messager.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rule=[
            'title'=>'required',
            'message'=>'required',
            'image'=>'required'
        ];

        $message=[
            'title.required'=>'this is required',
            'message.required'=>'this is required',
            'image.required'=>'this is required'
        ];

        $this->validate($request,$rule,$message);


        $data = [
            'title'=>$request->title,
            'message'=>$request->message

        ];



        $cat=Messager::create($data);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
        };



        flash()->message('تم إضافة الرساله بنجاح');

        return redirect(route('messagers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $messager=Messager::find($id);

        return view('admin.messager.edit',compact('messager'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $cat=Messager::find($id);

        $cat->update([
            'title'=> $request->title,
            'message'=>$request->message
        ]);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(200, 200)->save( public_path('uploads/' . $filename ) );
            $cat->image = $filename;
            $cat->save();
        };

        flash()->message('تم تعديل الرساله بنجاح');

        return redirect(route('messagers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat=Messager::find($id);
        $cat->delete();
        flash()->message('تم حذف الرساله بنجاح');
        return back();
    }
}
