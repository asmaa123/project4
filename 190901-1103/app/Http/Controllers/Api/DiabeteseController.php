<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\BlogResource;
use App\Http\Resources\DiabeteseResource;
use App\Models\Blog;
use App\Models\Blog_user;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class DiabeteseController extends Controller
{
    use  apiResponseTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type)
    {
        $user = Auth::user();

        $types = Blog::all()->where('type',1);

        $blogs=BlogResource::collection($types);
        $items = $this->paginate($blogs);

        return $this->apiResponse($items);
    }
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    public function postLikeBlog(Request $request){
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'blog_id'=>'required',
        ]);

        if ($validator->fails()) {
        return $this->apiResponse($validator);


        }
        $id = $request->blog_id;
        $blog = Blog::find($id);

        $user->blogs()->toggle($id);

        return $this->apiResponse(['count_likes'=>Blog_user::where('blog_id',$id)->count(),'likes_status'=>Blog_user::where('user_id',Auth::id())->where('blog_id',$id)->count()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
