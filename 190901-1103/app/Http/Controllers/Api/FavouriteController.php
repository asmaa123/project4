<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductFavResource;
use App\Models\Blog_user;
use App\Models\Product;
use App\Models\User_product;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

class FavouriteController extends Controller
{
    use apiResponseTrait;
    //

    public function favProduct(){
        $user=Auth::user();
$favProduct=User_product::where('user_id',Auth::id())->get();

        $products= ProductFavResource::collection($favProduct);

        $items = $this->paginate($products);
        return $this->apiResponse($items);

    }
    public function paginate($items, $perPage = 10, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }
    public function favBlog(){
        $user=Auth::user();
$favBlog=Blog_user::where('user_id',Auth::id())->get();
        return $this->apiResponse($favBlog);

    }
}
