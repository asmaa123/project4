<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Static_page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DiabetesFedController extends Controller
{
    use  apiResponseTrait;

    //

    public function index(){
        $user = Auth::user();

//        $diabetes='التغديه السكريه';
        $diabetes_fed_name=Static_page::where('id', 5)->pluck('name')->first();
        $diabetes_fed_title=Static_page::where('id',5)->pluck('title')->first();
        $diabetes_fed_text=Static_page::where('id', 5)->pluck('text')->first();

        $diabetes_fed=Static_page::select('image')->where('id', 5)->first();
        $diabetes_fed->setAttribute('name', getApiJsonData($diabetes_fed_name,request()->header('accept-language')));
        $diabetes_fed->setAttribute('title', getApiJsonData($diabetes_fed_title,request()->header('accept-language')));
        $diabetes_fed->setAttribute('text', getApiJsonData($diabetes_fed_text,request()->header('accept-language')));
        return $this->apiResponse($diabetes_fed);

    }

    public function terms_condition(){
        $user = Auth::user();


        $diabetes_fed_name=Static_page::where('id', 3)->pluck('name')->first();
        $diabetes_fed_title=Static_page::where('id',3)->pluck('title')->first();
        $diabetes_fed_text=Static_page::where('id', 3)->pluck('text')->first();

        $diabetes_fed=Static_page::select('image')->where('id', 3)->first();
        $diabetes_fed->setAttribute('name', getApiJsonData($diabetes_fed_name,request()->header('accept-language')));
        $diabetes_fed->setAttribute('title', getApiJsonData($diabetes_fed_title,request()->header('accept-language')));
        $diabetes_fed->setAttribute('text', getApiJsonData($diabetes_fed_text,request()->header('accept-language')));
        return $this->apiResponse($diabetes_fed);

    }

    public function about_app(){
        $user = Auth::user();


        $diabetes_fed_name=Static_page::where('id', 4)->pluck('name')->first();
        $diabetes_fed_title=Static_page::where('id',4)->pluck('title')->first();
        $diabetes_fed_text=Static_page::where('id', 4)->pluck('text')->first();

        $diabetes_fed=Static_page::select('image')->where('id', 4)->first();
        $diabetes_fed->setAttribute('name', getApiJsonData($diabetes_fed_name,request()->header('accept-language')));
        $diabetes_fed->setAttribute('title', getApiJsonData($diabetes_fed_title,request()->header('accept-language')));
        $diabetes_fed->setAttribute('text', getApiJsonData($diabetes_fed_text,request()->header('accept-language')));
        return $this->apiResponse($diabetes_fed);

    }

}
