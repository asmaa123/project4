<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact_us;

class Contact_usController extends Controller
{
    use apiResponseTrait;

    public function addcontact(Request $request){

        $rule=[
            'name'=>'required',
            'email'=>'required',
            'content'=>'required',

        ];

        $message=[

            'name.requied'=>'this is required',
            'email.requied'=>'this is required',
            'content.requied'=>'this is required'

        ];

        $this->validate($request,$rule,$message);



        $con=Contact_us::create($request->all());

        return $this->apiResponse($con);



    }

}
