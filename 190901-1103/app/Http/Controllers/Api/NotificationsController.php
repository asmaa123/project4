<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationsResourse;
use App\Models\Messager;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

class NotificationsController extends Controller
{
    use  apiResponseTrait;

    //

    function sendMessage($title,$message,$image) {

        $content = array(
            "en" => $message,

        );


        $fields = array(
            'app_id' => "7f77880d-cf51-478e-a0ef-c7ccd725c2b8",
            'included_segments' => array(
                'All'
            ),
            'data' => array(
                "foo" => "bar"
            ),
            'big_picture'=>'http://cicrapp.maakaweb.com/uploads/'.$image,
            'larg_icon'=>'http://cicrapp.maakaweb.com/site/logo.jpg',
            'contents' => $content,
            'headings'=>array(
                "en" => $title,

            )
        );
        $fields = json_encode($fields);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic MmVlNzBkYjUtNmUyZi00MzNiLTgyMzMtMDFhNzUxNDBiYmI0'
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }


    public function getNotifications(Request $request){
        $rule=[
            'title'=>'required',
            'message'=>'required',
        ];

        $message=[
            'title.requied'=>'this is required',
            'message.required'=>'this is required'
        ];

        $this->validate($request,$rule,$message);


        $data = [
            'title'=> $request->title,
            'message'=>$request->message

        ];



        $message=Messager::create($data);
        $message=Messager::find($message->id);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $message->image = $filename;
            $message->save();
        };
        flash()->message('تم إضافة الرساله بنجاح');
        $response = $this->sendMessage($request->title,$request->message,$message->image) ;
        $return["allresponses"] = $response;
        $message=Messager::find($message->id);
        $message->notification_id=getJsonData($response,'id');
        $message->save();



        return redirect(route('messagers.index'));

    }

    public function update(Request $request){
        $message=Messager::find($request->message_id);

            $message->title= $request->title;
        $message->message=$request->message;
        $message->save();

        $message=Messager::find($request->message_id);

        if($request->hasFile('image')){
            $thumbnail = $request->file('image');
            $filename = time() . '.' . $thumbnail->getClientOriginalExtension();
            Image::make($thumbnail)->resize(300, 300)->save( public_path('/uploads/' . $filename ) );
            $message->image = $filename;
            $message->save();
        };
        flash()->message('تم تعديل الرساله بنجاح');
        $response = $this->sendMessage($request->title,$request->message,$message->image) ;
        $return["allresponses"] = $response;
        $message=Messager::find($message->id);
        $message->notification_id=getJsonData($response,'id');
        $message->save();

        return redirect(route('messagers.index'));
    }
    public function detail_notification($id){

$notification=Messager::where('user_id',Auth::id())->where('id',$id)->where('seen',0)->first();
        return $this->apiResponse([$notification]);

    }


}
