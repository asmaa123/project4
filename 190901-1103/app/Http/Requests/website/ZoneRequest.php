<?php

namespace App\Http\Requests\website;

use Illuminate\Foundation\Http\FormRequest;

class ZoneRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'zone_name_ar' => 'required|string',
            'zone_name_en' => 'required|string',
            'city_id' => 'required',
            'zone_location_name'=>'required'
        ];
    }
}
