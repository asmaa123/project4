<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model 
{

    protected $table = 'medicines';
    public $timestamps = true;
    protected $fillable = array('image', 'name', 'text');

}