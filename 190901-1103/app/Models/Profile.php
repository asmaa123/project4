<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class Profile extends Model
{
    protected $table = 'profiles';
//    public $timestamps = true;
  protected $fillable = ['acount_status_id', 'type_sugers_id', 'phone','user_id', 'age','gender', 'date', 'height', 'weight', 'date_injury'];

    public function type_suger()
    {
        return $this->belongsTo(Type_suger::class);
    }

    public function account_status()
    {
        return $this->belongsTo(Account_status::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class,'user_id','user_id');
    }

    public function user()
    {

        return $this->hasOne(\App\User::class);
    }





}