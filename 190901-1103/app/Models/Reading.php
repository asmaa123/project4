<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reading extends Model 
{

    protected $table = 'readings';
    public $timestamps = true;
    protected $fillable = array('user_id', 'measurement_id', 'read_at', 'time', 'count');

    public function measurement_times()
    {
        return $this->belongsTo(Measurement_times::class,'measurement_id','id');
    }

    public function User()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

}