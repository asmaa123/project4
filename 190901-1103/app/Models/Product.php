<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model 
{

    protected $table = 'products';
    public $timestamps = true;
    protected $fillable = array('catogery_id', 'image','name','weight','carbonydrate','protine','calories','fiber');

    public function catogery()
    {
        return $this->belongsTo('App\Models\Catogery');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

}