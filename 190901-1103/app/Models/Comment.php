<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model 
{

    protected $table = 'comments';
    public $timestamps = true;
    protected $fillable = array('user_id', 'blog_id', 'content');

    public function User()
    {
        return $this->belongsTo(\App\User::class,'user_id');
    }
    
    public function Profile()
    {
        return $this->belongsTo(Profile::class,'user_id');
    }

    public function blog()
    {
        return $this->belongsTo('App\Blog');
    }

}