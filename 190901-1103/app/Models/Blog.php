<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model 
{

    protected $table = 'blogs';
    public $timestamps = true;
    protected $fillable = array('image', 'title', 'text','type');

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function users()
    {
        return $this->belongsToMany('App\User');
    }

}