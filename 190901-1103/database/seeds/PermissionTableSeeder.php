<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $models = ['role'];
        $permissions = [
            '-list' => 'عرض ',
            '-create' => 'إضافة',
            '-edit' => 'التعديل ',
            '-delete' => 'حذف ',
        ];

        foreach ($models as $model) {
            foreach ($permissions as $key => $val) {
                if(!Permission::where('name',$model.$key)->count()){
                    Permission::create(['name' => $model.$key,'slug'=>$val,'orderBy'=>$model.'Model','type'=>'admin']);
                    echo $model.$key."\n";
                }
            }
        }
        // when you create custom new permission edit & unComment next line
        // Permission::create(['name' => 'permission name','slug'=>'اسم الصلاحيه بالعربى','orderBy'=>'Permission module','type'=>'admin']);
    }
}
