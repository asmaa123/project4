<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMeasurementTimesTable extends Migration {

	public function up()
	{
		Schema::create('measurement_times', function(Blueprint $table) {
			$table->increments('id');
			$table->text('name');
			$table-> VARCHAR('image');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('measurement_times');
	}
}