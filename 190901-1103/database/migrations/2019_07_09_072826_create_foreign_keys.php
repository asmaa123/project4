<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('profiles', function(Blueprint $table) {
			$table->foreign('acount_status_id')->references('id')->on('account_status')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('profiles', function(Blueprint $table) {
			$table->foreign('type_sugers_id')->references('id')->on('type_sugers')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('readings', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('readings', function(Blueprint $table) {
			$table->foreign('measurement_id')->references('id')->on('measurement_times')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('contact_us', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->foreign('blog_id')->references('id')->on('blogs')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('products', function(Blueprint $table) {
			$table->foreign('catogery_id')->references('id')->on('catogeries')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('user_product', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('user_product')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('user_product', function(Blueprint $table) {
			$table->foreign('product_id')->references('id')->on('user_product')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('blog_user', function(Blueprint $table) {
			$table->foreign('blog_id')->references('id')->on('blog_user')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
		Schema::table('blog_user', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('blog_user')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('profiles', function(Blueprint $table) {
			$table->dropForeign('profiles_acount_status_id_foreign');
		});
		Schema::table('profiles', function(Blueprint $table) {
			$table->dropForeign('profiles_type_sugers_id_foreign');
		});
		Schema::table('readings', function(Blueprint $table) {
			$table->dropForeign('readings_user_id_foreign');
		});
		Schema::table('readings', function(Blueprint $table) {
			$table->dropForeign('readings_measurement_id_foreign');
		});
		Schema::table('contact_us', function(Blueprint $table) {
			$table->dropForeign('contact_us_user_id_foreign');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->dropForeign('comments_user_id_foreign');
		});
		Schema::table('comments', function(Blueprint $table) {
			$table->dropForeign('comments_blog_id_foreign');
		});
		Schema::table('products', function(Blueprint $table) {
			$table->dropForeign('products_catogery_id_foreign');
		});
		Schema::table('user_product', function(Blueprint $table) {
			$table->dropForeign('user_product_user_id_foreign');
		});
		Schema::table('user_product', function(Blueprint $table) {
			$table->dropForeign('user_product_product_id_foreign');
		});
		Schema::table('blog_user', function(Blueprint $table) {
			$table->dropForeign('blog_user_blog_id_foreign');
		});
		Schema::table('blog_user', function(Blueprint $table) {
			$table->dropForeign('blog_user_user_id_foreign');
		});
	}
}