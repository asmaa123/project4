<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogUserTable extends Migration {

	public function up()
	{
		Schema::create('blog_user', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('blog_id')->unsigned();
			$table->integer('user_id')->unsigned();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('blog_user');
	}
}