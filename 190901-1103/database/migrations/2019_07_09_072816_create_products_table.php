<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateProductsTable extends Migration {

	public function up()
	{
		Schema::create('products', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('catogery_id')->unsigned();
			$table->string('image');
			$table->text('name');
			$table->string('weight');
			$table->string('carbonydrate');
			$table->string('protine');
			$table->string('calories');
			$table->string('fiber');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('products');
	}
}