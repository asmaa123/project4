<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStaticPagesTable extends Migration {

	public function up()
	{
		Schema::create('static_pages', function(Blueprint $table) {
			$table->increments('id');
			$table->text('name');
			$table->text('title');
			$table->text('text');
			$table->string('image');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('static_pages');
	}
}