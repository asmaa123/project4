<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReadingsTable extends Migration {

	public function up()
	{
		Schema::create('readings', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->integer('measurement_id')->unsigned();

			$table->string('time');
			$table->string('count');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('readings');
	}
}