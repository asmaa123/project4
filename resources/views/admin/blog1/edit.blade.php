@extends('admin.layouts.app')

@section('title')
	تعديل : التثقيفات
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/blogs')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالتثقيفات</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text"> تعديل : التثقيفات</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						تعديل : التثقيفات
					</h3>
				</div>
			</div>
		</div>

	{!!Form::model($cat,[
'action'=>['Admin\Blog1Controller@update',$cat->id],
'method'=>'put',
'files'=>true,
'class'=>'m-form m-form--fit m-form--label-align-right'
]) !!}



			<div class="col-lg-12">
			<label class=" col-form-label">العنوان عربي </label>

			<div class="col-lg-6">


			{!!Form::text('title[ar]',getJsonData($cat->title,'ar'),[
            'class'=>'form-control m-input','placeholder'=>"العنوان بالعربي"
            ])!!}
			</div>
			</div>
		<div class="col-lg-12">
				<label class=" col-form-label">العنوان انجليزي</label>

				<div class="col-lg-6">


					{!!Form::text('title[en]',getJsonData($cat->title,'en'),[
                    'class'=>'form-control m-input','placeholder'=>"العنوان بالانجليزي"
                    ])!!}
				</div>
			</div>

		<div class="form-group m-form__group row m-form__section--last">

			<div class="col-lg-12">
				<label class="col-lg-3 col-form-label"> النص عربي </label>
				<div class="col-lg-6">


					{!!Form::textarea('text[ar]',getJsonData($cat->text,'ar'),[
                    'class'=>'form-control m-input','placeholder'=>"النص"
                    ])!!}
				</div>
				<label class="col-lg-3 col-form-label"> النص انجليزي </label>
				<div class="col-lg-6">


					{!!Form::textarea('text[en]',getJsonData($cat->text,'en'),[
                    'class'=>'form-control m-input','placeholder'=>"النص انجليزي"
                    ])!!}
				</div>

			</div>
	

			<div class="col-lg-12">
				<label class="col-lg-1 col-form-label"> الصوره</label>


				<div class="col-lg-9">
					{!!Form::file('image',null,[
                'class'=>'form-control m-input','placeholder'=>"كلمة المرور الحاليه"
                ])!!}
				</div>
			</div>
<br><br>
			<br>
			<div class='form-group' >
				<br>
				<button class="btn btn-primary" type="submit"> تعديل</button>
			</div>

		</div>




		{!! Form::close()!!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

