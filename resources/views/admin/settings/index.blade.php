@extends('admin.layouts.app')
@section('title','إعدادت الموقع')

@section('header')
<!-- BEGIN PAGE LEVEL STYLES -->
{!! Html::style('public/admin/global/plugins/jquery-file-upload/blueimp-gallery/blueimp-gallery.min.css') !!}
{!! Html::style('public/admin/global/plugins/jquery-file-upload/css/jquery.fileupload.css') !!}
{!! Html::style('public/admin/global/plugins/jquery-file-upload/css/jquery.fileupload-ui.css') !!}
{!! Html::style('public/admin/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css') !!}
{!! Html::style('public/admin/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput-typeahead.css') !!}
<!-- END PAGE LEVEL STYLES -->

@endsection

@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item active-top-bar">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text">إعدادات الموقع</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						تعديل إعددات الموقع
					</h3>
				</div>
			</div>
		</div>
		<!--begin::Form-->
		<form class="m-form m-form--fit m-form--label-align-right" action="#" method="post">
			<div class="m-portlet__body">
				<form class="form-horizontal" role="form" method="POST" action="{{ url('/admincp/settings') }}" enctype="multipart/form-data">
					{{ csrf_field() }}
					@foreach($settings as $setting )
						<div class="form-group{{ $errors->has('$setting->name') ? ' has-error' : '' }}">
							<label class="col-md-2 control-label">{{$setting->slug}}</label>
							<div class="col-md-9">
								<div class="input-group">
		                          <span class="input-group-addon">
		                          <i class="icon-settings"></i>
		                          </span>
									@if($setting->type == 0)
										@if($setting->name == 'KeyWords')
											{!! Form::text($setting->name, $setting->value ,['class'=>'form-control','data-role'=>'tagsinput']) !!}
										@else
											{!! Form::text($setting->name, $setting->value ,['class'=>'form-control']) !!}
										@endif
									@elseif($setting->type == 1)
										<span class="btn default blue-stripe fileinput-button form-control">
			                        @if($setting->name != null)
												<img src="{{url('public/upload/logo/'.$setting->value)}}" width="100" height="20">
											@else
												<img src="{{url('public/upload/logo/no_image.png')}}" width="100" height="20">
											@endif
										<i class="fa fa-plus"></i>
										<span>أختر الملف ... </span>
			                          		{!! Form::file($setting->name, array('multiple'=>false)) !!}
										</span>
									@elseif($setting->type == 2)
										{!! Form::textarea($setting->name, $setting->value ,['class'=>'form-control']) !!}
									@elseif($setting->type == 3)
										{!! Form::select("$setting->name",['0'=>'مغلق','1'=>'مفتوح'],$setting->value,['class'=>'form-control']) !!}
									@endif
								</div>
								@if ($errors->has('$setting->name'))
									<span class="help-block">
		                              <strong>{{ $errors->first('$setting->name') }}</strong>
		                          </span>
								@endif
							</div>
						</div>
					@endforeach
					<div class="form-actions">
						<div class="row">
							<div class="col-md-offset-3 col-md-9">
								<button type="submit" class="btn green">حفظ الإعدادات
									<i class="fa fa-save"></i>
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
			<div class="m-portlet__foot m-portlet__no-border m-portlet__foot--fit">
				<div class="m-form__actions m-form__actions--solid">
					<div class="row">
						<div class="col-lg-2"></div>
						<div class="col-lg-6">
							<button type="submit" class="btn btn-success">إضافة جديد</button>
						</div>
					</div>
				</div>
			</div>
		</form>
		<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection

<!-- footer -->
@section('footer')
<!-- BEGIN PAGE LEVEL PLUGINS -->
{!! Html::script('public/admin/global/plugins/bootstrap-tagsinput/bootstrap-tagsinput.min.js') !!}
{!! Html::script('public/admin/global/plugins/typeahead/handlebars.min.js') !!}
{!! Html::script('public/admin/global/plugins/typeahead/typeahead.bundle.min.js') !!}
<!-- END PAGE LEVEL PLUGINS -->

<script type="text/javascript">

</script>
@endsection