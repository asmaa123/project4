<div class="form-group m-form__group row">
	<label class="col-lg-1 col-form-label">الإسم بالكامل: </label>
	<div class="col-lg-4{{ $errors->has('name') ? ' has-danger' : '' }}">
		{!! Form::text('name',null,['class'=>'form-control m-input','required','autofocus','placeholder'=>"الإسم بالكامل"]) !!}
        @if ($errors->has('name'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
	</div>
	<label class="col-lg-2 col-form-label">البريد الإلكترونى : </label>
	<div class="col-lg-5{{ $errors->has('email') ? ' has-danger' : '' }}">
    	{!! Form::email('email',null,['class'=>'form-control m-input','required','placeholder'=>"البريد الإلكترونى"]) !!}
        @if ($errors->has('email'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
	</div>
</div>

<div class="form-group m-form__group row">
	<label class="col-lg-1 col-form-label">رقم الهاتف : </label>
	<div class="col-lg-2{{ $errors->has('phone') ? ' has-danger' : '' }}">
    	{!! Form::number('phone',null,['class'=>'form-control m-input','required','placeholder'=>"رقم الهاتف"]) !!}
        @if ($errors->has('phone'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('phone') }}</strong>
            </span>
        @endif
	</div>
	<label class="col-lg-1 col-form-label">الصلاحيات : </label>
	<div class="col-lg-5{{ $errors->has('roles') ? ' has-danger' : '' }}">
		{!! FORM::select('roles', $roles,$userRole, ['class'=>'form-control m-select','required','placeholder'=>"أختر صلاحية العضو"]) !!}
        @if ($errors->has('roles'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('roles') }}</strong>
            </span>
        @endif
	</div>
</div>

<div class="m-form__seperator m-form__seperator--dashed"></div>

<div class="form-group m-form__group">
	<div class="m-form__heading">
		<h3 class="m-form__heading-title">تغيير كلمة المرور</h3>
	</div>
</div>

<div class="form-group m-form__group row m-form__section--last">
	<label class="col-lg-1 col-form-label">الحاليه : </label>
	<div class="col-lg-3{{ $errors->has('curPassword') ? ' has-danger' : '' }}">
    	{!! Form::password('curPassword',['class'=>'form-control m-input','placeholder'=>"كلمة المرور الحاليه"]) !!}
        @if ($errors->has('curPassword'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('curPassword') }}</strong>
            </span>
        @endif
	</div>
	<label class="col-lg-1 col-form-label">الجديده : </label>
	<div class="col-lg-3{{ $errors->has('password') ? ' has-danger' : '' }}">
    	{!! Form::password('password',['class'=>'form-control m-input','placeholder'=>"كلمة المرور الجديده"]) !!}
        @if ($errors->has('password'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
	</div>
	<label class="col-lg-1 col-form-label">التأكيد : </label>
	<div class="col-lg-3{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
    	{!! Form::password('password_confirmation',['class'=>'form-control m-input','placeholder'=>"تأكيد كلمة المرور"]) !!}
        @if ($errors->has('password_confirmation'))
            <span class="form-control-feedback" role="alert">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
	</div>
</div>
