@extends('admin.layouts.app')

@section('title')
	تعديل : الاعلانات
@endsection
@section('topBar')
	<li class="m-menu__item">
		<a href="{{url('admincp')}}" class="m-menu__link">
			<span class="m-menu__link-text">الرئيسية</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item">
		<a href="{{url('admincp/advertisements')}}" class="m-menu__link">
			<span class="m-menu__link-text">التحكم بالاعلانات</span>
			<i class="m-menu__hor-arrow la la-angle-left"></i>
		</a>
	</li>
	<li class="m-menu__item" style="background: #e0deff;">
		<a href="javascript:;" class="m-menu__link">
			<span class="m-menu__link-text">الاعلانات</span>
			<i class="m-menu__hor-arrow la la-angle-down"></i>
		</a>
	</li>
@endsection

@section('header')
@endsection

@section('content')
	<!--begin::Portlet-->
	<div class="m-portlet">
		<div class="m-portlet__head">
			<div class="m-portlet__head-caption">
				<div class="m-portlet__head-title">
					<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
					</span>
					<h3 class="m-portlet__head-text">
						تعديل الاعلانات
					</h3>
				</div>
			</div>
		</div>

	{!!Form::model($advertisement,[
'action'=>['Admin\AdvertisementController@update',$advertisement->id],
'method'=>'put',
'files'=>true,
'class'=>'m-form m-form--fit m-form--label-align-right'
]) !!}



		<div class="col-lg-12">
			<label class=" col-form-label">   النوع</label>

			<div class="col-lg-9">



				@php

					$advertisement=App\Models\Advertisement::all();


				@endphp


				<select name="type" class="form-control">
					<option value="1"> اقسام </option>
					<option value="0"> المنتجات </option>


				</select>




			</div>
		</div>


		<div class="form-group">
			<div class="col-lg-12">
				<label class="col-lg-1 col-form-label"> الصوره</label>


				<div class="col-lg-9">
					{!!Form::file('image',null,[
                'class'=>'form-control m-input'
                ])!!}
				</div>
			</div>
		</div>


		<br><br>
			<br>
			<div class='form-group' >
				<br>
				<button class="btn btn-primary" type="submit"> تعديل</button>
			</div>

		</div>




		{!! Form::close()!!}
	<!--end::Form-->
	</div>
	<!--end::Portlet-->
@endsection
@section('footer')
<script type="text/javascript">

</script>
@endsection

