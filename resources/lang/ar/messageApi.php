<?php

return [

    'login' => 'البريد الالكترونى او رقم الجوال مطلوب',
    'invalid_credentials' => 'البيانات المدخله غير صحيحة او الحساب غير مفعل',
    'could_not_create_token' => 'لا يستطيع انشاء توكن حاول مرة اخرى',
    'register type require' => 'نوع تسجيل الدخول مطلوب',
    'email or phone number require' => 'البريد الالكتروني او رقم الهاتف مطلوب',
    'invalid code' => 'الكود غير صحيح',
    'no_result'=>'لا يوجد نتائج',
    'no_product'=>'لا يوجد منتج بهذا الرقم',
    'success_add_like'=>'تم اضافه المنتج الي المفضله بنجاح',


];
