# Cicr Laravel Project.

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/5.7/installation#installation)

Clone the repository

    git clone git@gitlab.com:gadara/cicr.git

Switch to the repo folder

    cd cicr

Install all the dependencies using composer

    composer install

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env

Generate a new application key

    php artisan key:generate

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Generate a new JWT authentication secret key

    php artisan passport:install

Generate a First Admin

    php artisan db:seed --class=createFirstAdmin

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000
