-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 20, 2019 at 06:43 PM
-- Server version: 10.2.27-MariaDB
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cicrapp_cicr`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_status`
--

CREATE TABLE `account_status` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `account_status`
--

INSERT INTO `account_status` (`id`, `name`, `created_at`, `updated_at`) VALUES
(7, '{\"ar\":\"مثقف سكري\",\"en\":\"Cultured diabetes\"}', '2019-07-28 10:20:24', '2019-08-25 05:45:44'),
(8, '{\"ar\":\"أخصائي سكري\",\"en\":\"Diabetes specialist\"}', '2019-07-28 10:24:35', '2019-08-25 05:45:10'),
(9, '{\"ar\":\"أخ سكري\",\"en\":\"Diabetic brother\"}', '2019-08-25 05:46:09', '2019-08-25 05:46:09'),
(10, '{\"ar\":\"سكري\",\"en\":\"Diabetes\"}', '2019-08-25 05:46:45', '2019-08-25 05:46:45'),
(11, '{\"ar\":\"بدون\",\"en\":\"without\"}', '2019-08-25 05:47:27', '2019-08-25 05:47:27'),
(15, '{\"ar\":\"تثقيف سكري\",\"en\":\"discount\"}', '2019-08-27 11:25:54', '2019-08-29 04:09:49');

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'المدير العام ', 'admin@admin.com', NULL, '$2y$10$PwTja8Plxj7MhHbBPScx0OcHPtpCR/oCq2JZASBUcZs.d1oeW5qyy', 'Xxnmqb18sA1r4JMuv1tdqvmZW1yzHL438KVPTp498J0rPLRT2IasrjXV6WBQ', '2019-07-08 05:00:00', '2019-08-07 11:52:29'),
(3, 'asmaa', 'asmaa@abozied.com', NULL, '$2y$10$rxkod6Ulz3h5K.itGesW3emuivXER0jvHTjPzBdYlJC2LnZMe.kGu', NULL, '2019-08-07 11:46:47', '2019-08-27 08:46:30');

-- --------------------------------------------------------

--
-- Table structure for table `advertisements`
--

CREATE TABLE `advertisements` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `type` varchar(244) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `image`, `title`, `text`, `created_at`, `updated_at`, `type`) VALUES
(61, '1567062344.jpg', '{\"ar\":\"تمارين الإحماء\",\"en\":\"Warm-up exercises\"}', '{\"ar\":\"فوائد تمارين الإحماء\\r\\nفيما يلي بعض فوائد تمارين الإحماء:\\r\\n\\r\\nبالنسبة لدرجة حرارة الجسم والعضلات: تزيد تمارين الإحماء من درجة حرارة العضلات، مما يقوي انقباضها ويسرع من ارتخائها، ويسرع حركة الجسم وقوته، ويقلل ذلك من حدوث الإصابات الناتجة عن تمدد العضلات المفاجئ. في الوقت نفسه، تساعد تمارين الإحماء الجسم على تجنب حدوث ارتفاع كبير في درجة حرارته في بداية التدريب من خلال السماح للجسم بالتبريد بفعالية.\\r\\nبالنسبة للدم: تزيد تمارين الإحماء من درجة حرارة الدم، مما يزيد من وفرة الاكسجين للاستعمال من قبل العضلات، لأن الروابط بين الأكسجين والهيموغلوبين في الدم تضعف.\\r\\nبالنسبة للأوعية الدموية: تساهم تمارين الإحماء في توسّع الأوعية الدموية مما يزيد من تدفق الدّم ويقلل الجهد على القلب.\\r\\nبالنسبة للمفاصل الكبيرة مثل الركبة والكتف: تساعد تمارين الإحماء هذه المفاصل على الوصول إلى أقصى إمكاناتها من الحركة.\\r\\nبالنسبة للتغيرات الهرمونية في الجسم: تزيد تمارين الإحماء من إنتاج بعض الهرمونات في الجسم مثل هرمون الكورتيزول والإبينفرين المسؤولين عن توفير الكربوهيدرات والأحماض الدهنية اللازمة لإنتاج الطاقة.\\r\\nبالنسبة لإنتاج الطاقة في الجسم: عند القيام بالتمارين الرياضية قد تنخفض مستويات الأكسجين فيقوم الجسم بإنتاج الطاقة عن طريق عملية ينتج عنها مركب يسمى حمض اللاكتيك، ويؤدي تراكم هذا المركب إلى صعوبة قيام الجسم بمتابعة التمارين. تفيد تمارين الإحماء بتهيئة الجسم لاستخدام الطاقة وعدم تراكم حمض اللاكتيك وبالتالي يستطيع الشخص القيام بالرياضة لوقت أطول وبشكل أقوى.\\r\\nبالنسبة لمعدل ضربات القلب: تساعد تمارين الإحماء على زيادة معدل ضربات القلب بالتدريج حتى يصل المعدل الذي يحتاجه الجسم خلال التمرين الأساسي. وبالتالي يرتفع ضغط الدم أيضا بالتدريج ويتم تهيئة الدماغ أن الجسم سوف يحتاج ضخ دم أكثر خلال الفترة القادمة.\",\"en\":\"The benefits of warm-up exercises\\r\\nHere are some benefits of warm-up exercises:\\r\\n\\r\\nFor body and muscle temperature: Warm-up exercises increase the temperature of the muscles, which strengthens their contractility and accelerates their relaxation, accelerates the movement and strength of the body, and reduces the incidence of injuries caused by sudden muscle expansion. At the same time, warm-up exercises help the body to avoid a significant rise in temperature at the beginning of the training by allowing the body to cool effectively.\\r\\nFor blood: Warm-up exercises increase the blood temperature, which increases the abundance of oxygen for use by muscles, because the bonds between oxygen and hemoglobin in the blood weaken.\\r\\nFor blood vessels: Warm-up exercises contribute to the expansion of blood vessels, which increases blood flow and reduces effort on the heart.\\r\\nFor large joints such as the knee and shoulder: Warm-up exercises help these joints reach their full potential of movement.\\r\\nFor hormonal changes in the body: Warm-up exercises increase the production of certain hormones in the body, such as cortisol and epinephrine, which are responsible for providing the carbohydrates and fatty acids needed to produce energy.\\r\\nWith regard to energy production in the body: When you do exercise may decrease levels of oxygen, the body produces energy through a process that results in a compound called lactic acid, and the accumulation of this compound makes it difficult for the body to continue exercises. Warm-up exercises help prepare the body for energy use and do not accumulate lactic acid so a person can do sports longer and stronger.\\r\\nFor heart rate: Warm-up exercises help increase the heart rate gradually until it reaches the rate needed by the body during the basic exercise. Thus, blood pressure also rises gradually and the brain is initialized that the body will need to pump more blood in the coming period.\"}', '2019-08-25 04:24:32', '2019-08-29 04:05:44', '0'),
(62, '1566975743.jpg', '{\"ar\":\"الرياضة التجميلية\",\"en\":\"Cosmetic sports\"}', '{\"ar\":\"فوائد ومميزات تمارين الرياضة التجميلية\\r\\nتتضمن تمارين الرياضة التجميلية مجموعةً من الفوائد والمميزات، نورد تالياً بعضاً منها:\\r\\n\\r\\nبناء الكتلة العضلية وذلك دون رفع أوزان، حيث تستهدف مجاميع عضلية مختلفة في الجسم في نفس الوقت. يستثنى من ذلك لاعبو كمال الأجسام حيث لا تعطينا الرياضة التجميلية الحجم الهائل للعضلات الذي يرغبون به.\\r\\nزيادة قوة الجسم وقدرته على الاحتمال، حيث يتحتم على المتدرب تكرار التمرينات مراراً حتى يتعب ثم يريح عضلاته طوال الليل ليعيد الكرة اليوم التالي ولكنه يستغرق وقتاً أطول ليشعر بالإجهاد، وهكذا يكون قد طور قدرة عضلاته على الاحتمال بما فى ذلك عضلات جهازه الدوري.\\r\\nعدم حاجة المتدرب إلى أجهزة او معدات خاصة أو حتى أماكن مخصصة للتمرين كالملاعب، فكل ما يحتاجه هو وزن جسمه نفسه وأي أدوات متوفرة حوله فى المنزل، لذا لا يضطر المتدرب إلى إلغاء التمرين بسبب بعد المسافة بينه وبين صالة الرياضة أو قلة النقود، فلا قيود تعوقه عن بناء جسده، بالإمكان التدرب في أي مكان وأي وقت.\\r\\nالمساعدة على تخفيف الوزن، حيث ترفع تمارين الرياضة التجميلية من معدل الحرق حتى بأوقات الراحة كما تزيد من ضربات القلب وسرعة التنفس لما فيها من تمرينات هوائية. لا داع للإلتزام بروتين غذائي صارم، وإنما يلزم فقط المداومة على بعض التدريبات لتقليل الوزن باعتدال. للمزيد: كل ما تحتاج إلى معرفته عن تنزيل الوزن\\r\\nسرعة الشفاء من الإصابات التى يمكن أن تنجم عن ممارستها مقارنة بأنواع التمرينات الأخرى التي تعتمد على رفع الأوزان، فالمتدرب يختار التمارين المناسبة له، ويحدد درجة صعوبتها، ويريح جسده بالوقت المناسب، وأثناء الليل تتعافى العضلات لتبدأ بالاستجابة للتمارين مرةً أخرى.\\r\\nزيادة مرونة الجسم، إذ من الضروري البدء بتمارين الاستطالة قبل التدريب وبعده أيضاً.\\r\\nسهولة ممارستها من قبل المبتدئين، ويمكن للمتدرب تطويرها بما يلائمه، فعلى سبيل المثال: يمكنه زيادة صعوبة تمرين الضغط بأدائه بيد واحدة أو زيادة العدات، وهكذا في أي تمرين آخر.\\r\\nتطوير التواصل بين الجسم والعقل ليعملا بتناغم سوياً وبنفس الكفاءة لتحقيق القوة، والرشاقة، والمرونة، والسرعة اللازمة.\\r\\nتمكين الشخص الذي يمارسها من الاستمتاع والإبداع، حيث يستطيع المتدرب تنفيذ تمرينات مختلفة بأوضاع وتكرارات متنوعة تبعاً لمكان تواجده مستخدما ما حوله من البيئة سواء بالمنزل، أو المنتزه، أو الساحة.\",\"en\":\"Benefits and advantages of plastic sports exercises\\r\\nAesthetic exercise includes a number of benefits and advantages, some of which are listed below:\\r\\n\\r\\nBuild muscle mass without lifting weights, targeting different muscle groups in the body at the same time. The exception is bodybuilders where cosmetic exercise does not give us the sheer volume of muscles they desire.\\r\\nIncreased body strength and endurance, where the trainee must repeat the exercises repeatedly until he gets tired and then relax his muscles all night to restore the ball the next day but takes longer to feel fatigue, and thus have developed the ability of his muscles endurance, including muscles of the circulatory system.\\r\\nThe trainee does not need special equipment or equipment, or even places dedicated to exercise, such as playgrounds. All he needs is his own body weight and any tools available around him at home, so the trainee does not have to cancel the exercise because of the distance between him and the gym or lack of money. About building his body, you can train anywhere, anytime.\\r\\nHelp to lose weight, where the exercises increase the cosmetic exercise rate of burning even at rest and increase the heart beat and breathing speed because of the aerobic exercises. There is no need to adhere to a strict dietary protein, but only to maintain some exercises to reduce weight in moderation. More: Everything you need to know about weight loss\\r\\nThe speed of recovery from injuries that can result from exercise compared to other types of exercises that rely on lifting weights.The trainee selects the appropriate exercises for him, determines the degree of difficulty, and relaxes his body in time, and during the night the muscles recover to begin to respond to the exercises again.\\r\\nIncrease the flexibility of the body, it is necessary to start elongation exercises before and after training as well.\\r\\nIt can be easily practiced by beginners, and the trainee can develop it to suit him.\\r\\nDevelop communication between the body and mind to work in harmony together and with the same efficiency to achieve the strength, agility, flexibility and speed needed.\\r\\nEnable the person practicing it to enjoy and creativity, where the trainee can perform different exercises with different situations and repetitions depending on where he is using the environment around him, whether at home, park or arena.\"}', '2019-08-25 04:28:10', '2019-08-28 04:02:24', '0'),
(63, '1566975583.jpg', '{\"ar\":\"أهمية التمارين الرياضية وتأثيرها على الوزن\",\"en\":\"The importance of exercise and its impact on weight\"}', '{\"ar\":\"فوائد ممارسة التمارين الرياضية\\r\\nمن أهم فوائد ممارسة التمارين الرياضية:\\r\\n\\r\\nتحسين المزاج\\r\\nيحسن آداء التمارين الرياضية المزاج، ويخفف القلق والتوتر، ويقلل خطر الإصابة بالاكتئاب، إذ قد يزيد حساسية الدماغ لهرموني السيروتونين (بالإنجليزية: Serotonin)، والنورابينفرين (بالإنجليزية: Norepinephrine) والتي تحمي من الاكتئاب.\\r\\n\\r\\nالحفاظ على العضلات والعظام\\r\\nلأداء التمارين الرياضية دور رئيسي في بناء عضلات وعظام قوية والحفاظ عليها، إذ يحفز نمو العضلات عبر تشجيع الجسم على إفراز هرمونات تزيد قدرة العضلات على امتصاص الأحماض الأمينية، والتي تعتبر المكون الرئيسي لها، وهو أمر تزيد أهميته مع التقدم في العمر، حين تبدأ كتلة الجسم العضلية بالانخفاض، مما يضعف الجسم، بالإضافة إلى أن الرياضة تزيد كثافة العظام في الصغر، مما يقي من الإصابة بهشاشة العظام لاحقاً.\\r\\n\\r\\nتخفيض خطر الإصابة بالأمراض المزمنة\\r\\nتساهم ممارسة التمارين الرياضية يومياً و بانتظام في تخفيض خطر الإصابة بالعديد من الأمراض المزمنة، كمرض السكري من النوع الثاني عبر الحفاظ على حساسية الإنسولين، وأمراض القلب، والسمنة الناتجة عن تراكم الدهون، وارتفاع ضغط الدم، بالإضافة إلى الموت المبكر.\\r\\n\\r\\nالحفاظ على صحة البشرة\\r\\n\\r\\nتزيد التمارين الرياضية معتدلة الشدة من إنتاج الجسم لمضادات الأكسدة الطبيعية، مما يخفف الضغط التأكسدي ويحافظ على صحة الخلايا، كما تعزز الرياضة تدفق الدم لخلايا البشرة، مما قد يؤخر ظهور علامات الشيخوخة عليها.\\r\\n\\r\\nأنواع التمارين الرياضية\\r\\nالتمارين الرياضية الهوائية\\r\\nتهدف التمارين الهوائية إلى زيادة استهلاك الجسم للاكسجين، واستخدامه لمختلف عمليات الأيض، وغالباً ما تؤدى بشدة متوسطة وعلى فترات طويلة، مع الإحماء قبل البدء بها، ثم التمرن مدة 20 دقيقة، ثم الراحة وهكذا، وتستهدف هذه التمارين عضلات الجسم الكبيرة، ويعتبر المشي، والسباحة، والهرولة، وركوب الدراجة من أهم الأمثلة عليها.\\r\\n\\r\\nالتمارين الرياضية اللاهوائية\\r\\nتهدف التمارين الرياضية اللاهوائية إلى بناء العضلات وزيادة قوة الجسم وقدرة تحمله، وهي تمارين عالية الشدة تؤدى على فترات زمنية قصيرة لا تتجاوز الدقيقتين، وفي حين أن التمارين اللاهوائية تستهلك سعرات حرارية أقل من الهوائية، وفوائد التمارين الهوائية للقلب أكبر من فوائد اللاهوائية، إلا أن التمارين اللاهوائية أفضل لبناء العضلات والقوة، وكلما زادت كتلة الجسم العضلية، زاد حرقه للدهون حتى عند الراحة، إذ تحرق العضلات سعرات حرارية بكمية أكبر من باقي أنسجة الجسم، ومن أهم الأمثلة على هذه التمارين:\\r\\n\\r\\nرفع الأثقال.\\r\\nالركض السريع.\\r\\nالقفز السريع والشديد بالحبل.\\r\\nالتمارين المتقطعة عالية الشدة.\\r\\nللمزيد: تمارين رفع الأثقال للأطفال\\r\\n\\r\\nالتمارين الرياضية وإنقاص الوزن\\r\\nإن زيادة حرق الجسم للسعرات الحرارية هو الأساس لإنقاص الوزن، ويمكن تحقيق ذلك بممارسة التمارين الرياضية التي ينصح بممارسة مزيج من نوعيها بشدة ومدة معينة لإنقاص الوزن، وبالمتوسط يمكن لممارسة التمارين الرياضية متوسطة الشدة إلى الشديدة مدة (150-250) دقيقة أسبوعياً أن يؤدي إلى تخفيف الوزن، ويمكن توضيح ذلك في ما يأتي:\\r\\n\\r\\nممارسة التمارين الرياضية متوسطة الشدة مدة (22-35) يومياً، تؤدي إلى خسارة وزن خفيفة، وأي مدة تقل عن ذلك تؤدي إلى الحفاظ على الوزن الحالي وثباته.\\r\\nممارسة التمارين الرياضية متوسطة الشدة إلى شديدة مدة 35 دقيقة على الأقل يومياً، تؤدي إلى خسارة وزن أكبر.\\r\\nومن المهم التأكيد على المزج بين نوعي الرياضة الهوائية واللاهوائية لتحقيق خسارة الوزن والدهون الأفضل، والحفاظ على كتلة الجسم العضلية، مع الحرص على حرق سعرات حرارية أكثر مما يستهلك الجسم لضمان إنقاص الوزن، وتحقيق أفضل النتائج، والحفاظ على عقل وجسم سليم وقوي.\\r\\n\\r\\nاقرأ أيضاً: نصائح لتخفيف الوزن\\r\\n\\r\\nعادة ما يساهم وضع خطة للتمارين الرياضية تلائم لياقة الشخص وصحته، وتطبيقها في الالتزام بها، وبالتالي إنقاص وزن أكبر، عوضاً عن ممارسة تمارين عشوائية في أوقات عشوائية، وفي ما يلي أنماط تمارين رياضية سهلة التطبيق:\\r\\n\\r\\nخطة تمارين رياضية يومية\\r\\nخطة تمارين رياضية يومية للمبتدئين\\r\\nتفيد هذه الخطة الأشخاص غير المعتادين على ممارسة الرياضة، وهي مجموعة تمارين سهلة تحفز عمليات الأيض في الجسم،لا تحتاج إلى معدات أو ملابس خاصة، ومن المهم أن تكون مدة هذه التمارين قصيرة كي يسهل الالتزام بها، ويكون النمط كالآتي:\\r\\n\\r\\n7 دقائق من المشي السريع.\\r\\n7 دقائق من تمرين الاندفاع البسيط (بالإنجليزية: Lunges)، وتمارين الضغط الخفيفة (بالإنجليزية: Push ups).\\r\\n7 دقائق من المشي السريع.\\r\\nو لتطبيق هذه الخطة يجب ممارسة التمارين (1-3) مرات يومياً، ولكن لن يستغرق كل تمرين منك وقتاً كبيراً، ويمتاز هذا النمط بمرونته إذ يستطيع الشخص تنفيذه في استراحة العمل، أو في الحديقة، كما يمكنك ممارسة تمرين صعود الدرج في العمل، مع تمارين الاندفاع والضغط، وعلى الرغم من أن هذه التمارين قصيرة المدة، إلا أنها تحرق سعرات حرارية بمعدل (300-500) سعرة حرارية يومياً.\\r\\n\\r\\nخطة تمارين رياضية يومية لغير المبتدئين\\r\\nهذه الخطة مخصصة للأشخاص المعتادين على ممارسة التمارين الرياضية، ويمكنهم تطبيقها لكسر الروتين الذي اعتادت عليه أجسامهم لتسريع إنقاص الوزن، وتتم عن طريق إضافة (30-45) دقيقة من التمارين السهلة الممتعة لنمط تمارينك اليومي، كما هو موضح فيما يأتي:\\r\\n\\r\\nإن كنت معتاداً على أداء تمارينك المعتاد في الصباح، فيمكن عندها ممارسة المشي السريع (بالإنجليزية: Brisk walk) في المساء.\\r\\nإن كنت معتاداً على أداء تمارينك المعتادة في المساء، فيمكن عندها ركوب الدراجة أو المشي صباحاً عند الذهاب للعمل.\\r\\nولا بد من الإشارة إلى أن نمط التمارين هذا مهم جداً عند اعتياد الجسم على نمط معين، وثبات الوزن وقياسات الجسم، إذ سيزيد هذا النمط نشاط الشخص الحركي، دون مجهود إضافي أو ضغط على المفاصل، مما يحرق المزيد من السعرات الحرارية، وبالتالي إنقاص وزن أسرع.\",\"en\":\"The benefits of exercise\\r\\nThe most important benefits of exercise:\\r\\n\\r\\nImprove your mood\\r\\nExercise improves mood, relieves anxiety and stress, and reduces the risk of depression.It may increase brain sensitivity to serotonin and norepinephrine, which protects against depression.\\r\\n\\r\\nMaintain muscles and bones\\r\\nExercise plays a key role in building and maintaining strong muscles and bones.It stimulates muscle growth by encouraging the body to secrete hormones that increase the ability of the muscles to absorb amino acids, which are the main component, which increases importance with age, when the body mass begins Muscle decline, which weakens the body, in addition to sports increase bone density at a young age, which prevents the development of osteoporosis later.\\r\\n\\r\\nReduce the risk of chronic diseases\\r\\nRegular daily exercise reduces the risk of many chronic diseases, such as type 2 diabetes by maintaining insulin sensitivity, heart disease, fat accumulation, high blood pressure, and premature death.\\r\\n\\r\\nKeep skin healthy\\r\\n\\r\\nModerate exercise increases the body\'s production of natural antioxidants, relieving oxidative stress and maintaining healthy cells.It also promotes blood flow to skin cells, which may delay the appearance of signs of aging.\\r\\n\\r\\nTypes of exercise\\r\\nAerobic exercise\\r\\nAerobic exercise aims to increase the body\'s consumption of oxygen, and use of various metabolic processes, and often performed moderately strongly and at long periods, with warming up before starting, then exercise for 20 minutes, then rest and so on, these exercises target the body\'s large muscles, and is considered walking, swimming , Jogging, and bike riding are the most important examples.\\r\\n\\r\\nAnaerobic exercise\\r\\nAnaerobic exercise aims to build muscle and increase the body\'s strength and endurance, which are high intensity exercises performed at short intervals of not more than two minutes, while anaerobic exercise consumes fewer calories than aerobic, and the benefits of cardio aerobic exercise greater than the benefits of anaerobic, but exercises Anaerobic is better for building muscle and strength, and the greater the muscle mass, the greater the fat burning even at rest, as the muscle burns more calories than the rest of the body\'s tissues, the most important examples of these exercises:\\r\\n\\r\\nWeight lifting.\\r\\nJogging fast.\\r\\nJump fast and tight rope.\\r\\nHigh intensity intermittent exercises.\\r\\nMore: Weightlifting exercises for children\\r\\n\\r\\nExercise and lose weight\\r\\nIncreasing the body\'s calorie burning is the basis for weight loss.This can be achieved by exercising, which is recommended to exercise a combination of both types strongly and a certain time to lose weight.On average exercise moderate to severe intensity for (150-250) minutes a week can lead to relief Weight, it can be explained in the following:\\r\\n\\r\\nMedium-intensity exercise (22-35) daily, leading to light weight loss, and any less than that leads to maintain the current weight and stability.\\r\\nModerate to severe exercise for at least 35 minutes a day, resulting in greater weight loss.\\r\\nIt is important to emphasize the combination of aerobic and anaerobic sports to achieve better weight loss and fat, and maintain muscle mass, taking care to burn more calories than the body consumes to ensure weight loss, achieve the best results, and maintain a healthy and strong mind and body.\\r\\n\\r\\nRead also: Weight Loss Tips\\r\\n\\r\\nDeveloping an exercise plan that is appropriate to a person\'s fitness and health, and applying it, usually contributes to adherence to it, thereby reducing more weight, rather than exercising random exercises at random times.\\r\\n\\r\\nDaily exercise plan\\r\\nDaily exercise plan for beginners\\r\\nThis plan benefits people who are not accustomed to exercise, an easy exercise group that stimulates metabolic processes in the body, do not need special equipment or clothing, and it is important that the duration of these exercises to be short to be easy to adhere to, and the pattern is as follows:\\r\\n\\r\\n7 minutes of brisk walking.\\r\\n7 minutes of simple lunges and light push ups.\\r\\n7 minutes of brisk walking.\\r\\nTo apply this plan, you should exercise (1-3) times a day, but each exercise will not take much time, and this pattern is flexible as the person can implement it in the work break, or in the garden, and you can practice the exercise of climbing stairs at work, with Impulse and pressure exercises, although these exercises are short duration, but they burn calories at the rate of (300-500) calories per day.\\r\\n\\r\\nDaily exercise plan for non-beginners\\r\\nThis plan is intended for people who are used to exercise, and can be applied to break the routine used by their bodies to accelerate weight loss, and is done by adding (30-45) minutes of easy exercise fun to your daily exercise pattern, as shown below:\\r\\n\\r\\nIf you are accustomed to your usual exercise in the morning, then brisk walk can be done in the evening.\\r\\nIf you are used to doing your usual exercises in the evening, you can ride a bike or walk in the morning when you go to work.\\r\\nIt should be noted that this pattern of exercise is very important when the body is accustomed to a certain pattern, and the stability of weight and body measurements, as this pattern will increase the activity of the person\'s motor, without additional effort or pressure on the joints, which burns more calories, and thus lose weight faster .\"}', '2019-08-25 04:35:32', '2019-08-28 03:59:43', '0'),
(64, '1566975694.jpg', '{\"ar\":\"تمارين رفع الساقين\",\"en\":\"Leg lifting exercises\"}', '{\"ar\":\"فوائد تمارين رفع الساقين\\r\\nلتمارين رفع الساقين فوائد كبيرة في تقوية العضلات المختلفة في الجسم أهمها عضلات البطن، وأسفل الظهر، والوركين، وعضلات الحوض، من هذه الفوائد:\\r\\n\\r\\nتمارين رفع الساقين تقوي عضلات البطن والعضلات السفلية\\r\\nتستفيد عضلات البطن السفلية بشكل رئيسي من رفع الساق، إذ يساعد تكرار التمارين المختلفة لرفع الساقين على زيادة قوة عضلات البطن، كما تقوي تمارين رفع الساقين عضلات البطن العلوية أيضاً.\\r\\n\\r\\nتمارين رفع الساقين تقوي عضلات أسفل الظهر\\r\\nيساعد الأداء المنتظم والمتكرر لتمارين رفع الساقين على تقليل خطر إصابات الظهر وآلام الظهر والإجهاد أثناء أداء التمارين الأخرى أو الأنشطة الروتينية.\\r\\n\\r\\nعلى الرغم من أن عضلات الظهر ليست هي محور تمارين رفع الساقين ،إلا أن تمارين البطن والساقين تعمل على توفير دعم متزايد للظهر.\\r\\n\\r\\nتمارين رفع الساقين تقوي العضلات القابضة في الفخذ\\r\\nجميع أنواع تمارين الساقين تزيد من مرونة وخفة حركة وقوة عضلات الفخذ، إذ يتطلب كل من ثني الركبة واستقامتها تحريك مفصل الورك.\\r\\n\\r\\nإذا كان لديك مشاكل في الورك أو التهاب المفاصل تحدث إلى طبيبك واحصل على إرشادات حول أفضل طريقة للقيام بهذه التمارين.\\r\\n\\r\\nتمارين رفع الساقين تقوي قبضة اليد\\r\\nعند للقيام برفع ساقيك في بعض تمارين الساقين فإنك تقوي قبضتك أيضاً حيث تتطلب بعض التمارين دعم وزن جسمك بالكامل باستخدام يديك أثناء التمرين.\\r\\n\\r\\nالقيام بتمارين رفع الساقين كعلاج طبيعي\\r\\nفي حال التعرض لإصابة في أحد الأطراف  السفلية أو إجراء جراحة في الفخذ أو الركبة أو الكاحل فقد تستفيد من تمارين رفع الساقين كأحد إجراءات العلاج الطبيعي للمساعدة على التعافي تماماً وتحسين قوة الأطراف السفلية وبالتالي المساعدة على تحسين القدرة على المشي.\\r\\n\\r\\nتعتبر تمارين رفع الساقين تمارين ممتازة لتقوية عضلات الفخذ، حيث ينصح المريض بالقيام بها بعد جراحة استبدال الورك أو استبدال مفصل الركبة وذلك بتوجيه وإشراف من الطبيب المختص.\\r\\n\\r\\nانواع تمارين رفع الساقين\\r\\nعادةً ما تتم عمليات رفع الساق أثناء الاستلقاء على الأرض التعلق ،حيث يوفر كل تمرين فوائد مختلفة ولكن تشترك كل التمارين في زيادة قوة عضلات البطن السفلية.\\r\\n\\r\\nتمارين رفع الساقين المزدوجة\\r\\nويتم ذلك عن طريق الجلوس بزاوية 90 درجة والقيام برفع الساقين بنفس الوقت، للتمرين فوائد عديدة حيث يعتمد على إشراك عضلات القلب الأساسية والورك وأوتار الركبة لرفع الساقين إلى أقصى درجة ممكنة ،ثم خفض الساقين إلى الأسفل.\\r\\n\\r\\nيتطلب التمرين مرونة الورك وأوتار الركبة، لذلك يمكن للمبتدئين الذين يواجهون صعوبة في التمرين ثني الساقين قليلاً أو رفع إحدى الساقين في كل مرة.\\r\\n\\r\\nتمارين رفع الساقين الجانبية\\r\\nيتم القيام بتمرين الرفع الجانبي للساق عن طريق الاستلقاء على جانب الجسم ثم رفع ساق الجهة الأخرى بشكل مستقيم نحو الأعلى حتى تصبح على وضع قريب من الوضع العمودي.\\r\\n\\r\\nتمارين رفع الساقين الجانبية أكثر فاعلية في تقوية عضلات الفخذين كما أنها الأفضل لتحقيق التوازن في الجسم.\\r\\n\\r\\nتمارين رفع الساقين أثناء الوقوف\\r\\nيتم القيام بالتمرين عن طريق الوقوف المستقيم ثم رفع إحدى الساقين للأمام مباشرة ثم إلى الخلف أو جانب الجسم.\\r\\n\\r\\nتحسن تمارين رفع الساقين أثناء الوقوف حركة مفصل الورك وتزيد المرونة أكثر من معظم الطرق الأخرى من التمرين.\\r\\n\\r\\nتمارين رفع الساقين أثناء التعلق\\r\\nتعتبر تمارين رفع الساقين أثناء التعلق التمرين الأكثر صعوبة ولكنه الأكثر تأثيراً على الجزء العلوي من الجسم مع الجزء السفلي وزيادة مرونة الكتف.\\r\\n\\r\\nيتم القيام بالتمرين عن طريق التعلق بقضيب أو داعمة مع الحرص على المباعدة بين اليدين والذراعين ثم محاولة رفع الساقين إلى الأمام والخلف ببطء.\\r\\n\\r\\nخطوات إجراء تمارين رفع الساقين\\r\\nإستلقِ على ظهرك، ومد قدميك على الأرض.\\r\\nقم بثني ركبتيك بزاوبة 90 درجة، وأبداً برفع ساقيك معاً بشكل متوازي.\\r\\nتأكد أثناء رفع قدميك من شد عضلات بطنك للأسفل.\\r\\nقم برفع قدميك للأعلى وكأنهما يصلان السقف.\\r\\nأبق على هذا الوضع لمدة 3 ثوان.\\r\\nاخفض ساقيك بشكل تدريجي وببطء.\\r\\nاسترخي قليلاً ثم عاود التمرين مرة أخرى وكرره 10 مرات.\\r\\nقد يكون التمرين صعباً في البداية، ولكن يجب ألا يسبب ألماً في الفخذ أو الركبة، فإذا شعرت بألم توقف عن أداء التمربن، واستشر طبيبك في أقرب وقت.\",\"en\":\"Benefits of leg lift exercises\\r\\nExercises to raise the legs great benefits in strengthening various muscles in the body the most important abdominal muscles, lower back, hips, pelvic muscles, of these benefits:\\r\\n\\r\\nLeg lift exercises strengthen the abdominal and lower muscles\\r\\nLower abdominal muscles benefit mainly from leg lift.Repeated different leg lift exercises help increase abdominal muscles.Legging exercises also strengthen the upper abdominal muscles.\\r\\n\\r\\nLeg lift exercises strengthen your lower back muscles\\r\\nRegular and frequent performance of leg lift exercises helps reduce the risk of back injuries, back pain and stress while performing other exercises or routine activities.\\r\\n\\r\\nAlthough back muscles are not the focus of leg lift exercises, abdominal and leg exercises provide increased back support.\\r\\n\\r\\nLeg lift exercises strengthen the hip flexors\\r\\nAll types of leg exercises increase the flexibility, agility and strength of the thigh muscles, as both bending and straightening of the knee requires the movement of the hip joint.\\r\\n\\r\\nIf you have problems with your hip or arthritis, talk to your doctor and get instructions on how best to do these exercises.\\r\\n\\r\\nLeg lift exercises strengthen the hand grip\\r\\nWhen lifting your legs in some leg exercises, you also strengthen your grip as some exercises require the support of your entire body weight using your hands during exercise.\\r\\n\\r\\nDoing legs lift exercises as a natural remedy\\r\\nIn the event of an injury to the lower limbs or surgery in the thigh, knee or ankle, you may benefit from exercises to raise the legs as a physiotherapy procedure to help fully recover and improve the strength of the lower limbs and thus help to improve the ability to walk.\\r\\n\\r\\nLeg lift exercises are excellent exercises to strengthen the muscles of the thigh, where the patient is recommended to do after surgery hip replacement or knee joint replacement and under the guidance and supervision of a specialist doctor.\\r\\n\\r\\nTypes of legs lift exercises\\r\\nLeg lifting is usually done while lying on the ground, where each exercise provides different benefits but all exercises are involved in increasing the strength of the lower abdominal muscles.\\r\\n\\r\\nDouble leg lift exercises\\r\\nThis is done by sitting at an angle of 90 degrees and lifting the legs at the same time, the exercise has many benefits as it depends on the involvement of the core heart muscles, hip and hamstrings to raise the legs as much as possible, then lower the legs down.\\r\\n\\r\\nExercise requires flexibility of the hip and hamstrings, so beginners who have difficulty exercising can bend the legs slightly or lift one leg at a time.\\r\\n\\r\\nSide lifting exercises\\r\\nThe lateral leg lift is performed by lying on the side of the body and then lifting the other leg straight upward until you are close to the vertical position.\\r\\n\\r\\nLateral leg lifting exercises are more effective in strengthening the muscles of the thighs and are also better for balancing the body.\\r\\n\\r\\nLeg lifting exercises while standing\\r\\nExercise is done by standing upright, lifting one leg straight ahead and back or side of the body.\\r\\n\\r\\nLeg lift exercises improve hip movement and increase flexibility more than most other methods of exercise.\\r\\n\\r\\nExercises lift legs while attachment\\r\\nLeg lifting exercises during attachment are the most difficult but most effective exercise on the upper body with the lower part and increased shoulder flexibility.\\r\\n\\r\\nExercise is carried out by attaching to a rod or supportive with care for the spacing between the hands and arms and then try to raise the legs forward and back slowly.\\r\\n\\r\\nSteps to perform leg lift exercises\\r\\nLie on your back, and stretch your feet on the floor.\\r\\nBend your knees at a 90 degree angle, and never lift your legs together in parallel.\\r\\nBe sure to lift your belly muscles down.\\r\\nRaise your feet up as if they were reaching the ceiling.\\r\\nKeep this mode for 3 seconds.\\r\\nLower your legs gradually and slowly.\\r\\nRelax for a while and repeat the exercise again and repeat 10 times.\\r\\nExercise may be difficult at first, but it should not cause pain in your thigh or knee.If you feel pain stop exercising, consult your doctor as soon as possible.\"}', '2019-08-25 04:38:29', '2019-08-28 04:01:34', '0'),
(65, '1567062255.jpg', '{\"ar\":\"أهمية الرياضة للأطفال\",\"en\":\"The importance of sport for children\"}', '{\"ar\":\"فوائد الرياضة للأطفال\\r\\nيرغب معظم الآباء في تشجيع أطفالهم على ممارسة الرياضة لمساعدتهم على تحسين احترامهم لذواتهم، فيمكن لكل طفل أن ينجح في رياضة معينة أو أخرى، ومع ذلك، يستغرق الأمر بعض الوقت من الآباء للعثور على الرياضة المناسبة لطفلهم.\\r\\n\\r\\nلا يوجد شيء آخر في الحياة يتيح للأطفال فرصة لتطوير سمات شخصية إيجابية والحصول على العديد من القيم كما تفعل الرياضة، وفيما يلي بعض المزايا التي قد يحصل عليها الطفل نتيجة ممارسته للرياضة:\\r\\n\\r\\nمحافظة الطفل على وزن صحي وضمن الحدود الطبيعية، تحميه من العديد من الأمراض التي تعتبر من مضاعفات السمنة أو زيادة الوزن، مثل سكري الأطفال، وامراض القلب مستقبلاً، وغيرها الكثير. \\r\\nتكوين شخصية الطفل ومبادئه الأخلاقية من خلال اللعب السليم، كما يمكن للأطفال الذين يمارسون الرياضة أن يكونوا قدوة جيدة لأقرانهم من المدرسة أو الحي للبدء في ممارسة بعض الألعاب الرياضية أيضًا.\\r\\nيمكن للأطفال من خلال الرياضة أن يُشكّلوا صداقات جديدة قد تستمر مدى الحياة.\\r\\nتجمع الرياضة بين الناس من جميع أنحاء العالم، بغض النظر عن جنسيتهم أو ديانتهم أو ثقافاتهم أو لون بشرتهم.\\r\\nتعليم الأطفال روح العمل الجماعي وفوائد التفاعل الاجتماعي.\\r\\nيمكن للأطفال اعتبار المسابقات الرياضية فرص لتعلمهم من نجاحهم وفشلهم، بالإضافة إلى أن الخسارة غالبًا ما تحفز الأطفال على العمل بجهد أكبر في المرة القادمة.\\r\\nيتعلم الأطفال من خلال الرياضة احترام السلطة والقواعد وزملاء الفريق والفريق الخصم.\\r\\nتساعد التجارب الرياضية على بناء تقدير ذاتي إيجابي لدى الأطفال.\\r\\nيمكن أن تكون المشاركة في الألعاب الرياضية وسيلة مفيدة للحد من التوتر وزيادة الشعور بالرفاه البدني والعقلي، فضلًا عن مكافحة حدوث الصراعات والاندفاعات العنيفة.\\r\\nعندما يتعلم الأطفال دروسًا إيجابية في الحياة من خلال الرياضة، فليس هناك شك في أنهم سيصبحون أشخاصًا صادقين وموثوقين يحاولون مساعدة الآخرين المحتاجين في أي لحظة عندما يكبرون.\\r\\nللمزيد: الحمية الغذائية المناسبة لسكري الأطفال\\r\\n\\r\\nكيف يمكن اختيار الأنشطة الرياضية المناسبة للطفل؟\\r\\nفيما يلي بعض الإرشادات التي تساعد على اختيار أنشطة اللياقة البدنية المناسبة لعمر الطفل:\\r\\n\\r\\nالتمارين الرياضية المناسبة للأطفال بعمر ال5 سنوات أو أقل:\\r\\nيمكن للأطفال في مرحلة ما قبل المدرسة لعب رياضات جماعية مثل كرة القدم أو كرة السلة، كما ينبغي أن يكون هدف ممارسة الرياضة للأطفال بهذا العمر هو اللعب وليس المنافسة، بالإضافة إلى أن الأطفال بهذا العمر يميلون إلى حب الرياضات المائية، فمن الممكن تعويد الاطفال على الأنشطة الرياضية المائية المختلفة مثل نفخ الفقاعات واستكشاف ما هو تحت الماء قبل البدء بتعليمهم مهارات السباحة الرسمية، ويكون الأطفال في سن الرابعة أو الخامسة مستعدين لتعلم التحكم في النَفَس والعوم.\\r\\n\\r\\nالتمارين الرياضية المناسبة للأطفال بعمر 6-8 سنوات\\r\\nيتطور الأطفال بما فيه الكفاية في سن السادسة ليتمكنوا من رمي كرة بيسبول أو تمرير كرة قدم أو كرة سلة، ويمكنهم أيضًا أداء تمرينات الجمباز وقيادة دراجة ذات عجلتين.\\r\\n\\r\\nالتمارين الرياضية المناسبة للأطفال بعمر 9-11 سنة\\r\\nعادة ما يكون الأطفال في هذا العمر قادرين على ضرب ورمي كرة البيسبول بطريقة دقيقة، لا مانع حينها من تشجيع المنافسة، وفي حال كان الأطفال مهتمين بالمشاركة في أحداث مثل السباقات القصيرة أو الجري لمسافات طويلة، فهي آمنة طالما أن الأطفال قد تدربوا على المسابقة ويحافظون على ترطيب أجسادهم بشرب الماء والسوائل.\\r\\n\\r\\nالتمارين الرياضية المناسبة للأطفال بعمر 12-14 سنة\\r\\nقد يفقد الأطفال اهتمامهم بالرياضات المنظمة أثناء وصولهم إلى مرحلة المراهقة وقد يرغبون في التركيز بدلًا من ذلك على تمارين القوة وبناء العضلات، ولكن ما لم يصل الطفل إلى سن البلوغ، فيجب على والديه أن يمنعانه من رفع الأوزان الثقيلة.\\r\\n\\r\\nالتمارين الرياضية المناسبة للأطفال بعمر 15 سنة أو أكبر\\r\\nبمجرد أن يمر الطفل بمرحلة البلوغ ويكون مستعدًا لرفع الأثقال، فيجب على والديه أن يطلبوا منه أن يأخذ درسًا في رفع الأثقال مع خبير متخصص، حيث أن الأداء السيئ لرفع الأثقال قد يؤدي إلى حدوث كسور أو أضرار في العضلات.\\r\\n\\r\\nفوائد أنواع الرياضة المختلفة للأطفال\\r\\nرياضة كرة القدم\\r\\nتعلم هذه الرياضة الجماعية الطفل القدرة على التحمل والتعامل مع القدم بدقة من خلال القدرة على تغيير اتجاه الكرة والاستجابة السريعة والحفاظ على التوازن.\\r\\n\\r\\nرياضة البيسبول\\r\\nتساعد الطبيعة البطيئة لهذه الألعاب في تعليم الأطفال التركيز والاهتمام، كما تساعد على تطوير التنسيق بين اليد والعين.\\r\\n\\r\\nرياضة كرة السلة\\r\\nلعبة سريعة مثل كرة السلة مهمة لتعليم الطفل السيطرة على الجسم من خلال الدفاع والهجوم.\\r\\n\\r\\nرياضة التزلج على الجليد\\r\\nتشجّع هذه الرياضة الانفرادية الأطفال على التطلع للأمام وتخطيط مسارهم في وقت مسبق.\\r\\n\\r\\nرياضة السباحة\\r\\nتعلم السباحة الأطفال التحكم في النَفَس والانتباه إلى التفاصيل كما تعلمهم أهمية الوقت من خلال تتويج الفائزين لمجرد أنهم كانوا أسرع لمدة أعشار من الثانية.\\r\\n\\r\\nرياضة التنس\\r\\nتُحسّن التفكير السريع، كما تُطوّر قوّة الذراع ودقته.\",\"en\":\"The benefits of sports for children\\r\\nMost parents want to encourage their children to exercise to help them improve their self-esteem.Each child can succeed in one sport or another, however, it takes some time from parents to find the right sport for their child.\\r\\n\\r\\nThere is nothing else in life that gives children the opportunity to develop positive personality traits and get as many values ​​as sport does. Here are some of the advantages a child may get as a result of exercising:\\r\\n\\r\\nMaintain a healthy weight and within the normal limits, protect it from many diseases that are considered complications of obesity or weight gain, such as diabetes, children, heart disease in the future, and many others.\\r\\nForming a child\'s personality and moral principles through proper play.Children who play sports can be good role models for their peers from school or neighborhood to start playing some sports as well.\\r\\nThrough sport, children can form new friendships that can last a lifetime.\\r\\nSports bring people from all over the world, regardless of their nationality, religion, cultures or skin color.\\r\\nTeach children the spirit of teamwork and the benefits of social interaction.\\r\\nChildren can consider sports competitions as opportunities to learn from their success and failure, and the loss often motivates children to work harder next time.\\r\\nChildren learn through sport respecting power, rules, teammates and opponent team.\\r\\nSports experiments help build positive self-esteem in children.\\r\\nParticipating in sports can be a useful means of reducing stress and increasing a sense of physical and mental well-being, as well as fighting violent conflicts and eruptions.\\r\\nWhen children learn positive lessons in life through sport, there is no doubt that they will become honest and reliable people who try to help others in need at any moment when they grow up.\\r\\nFor more: Diets suitable for children diabetes\\r\\n\\r\\nHow do you choose the right sports activities for the child?\\r\\nHere are some guidelines to help you choose fitness activities that are appropriate for your child\'s age:\\r\\n\\r\\nExercise appropriate for children 5 years old or younger:\\r\\nPreschoolers can play team sports such as football or basketball, and the goal of playing sports for children of this age should be to play, not to compete.In addition, children of this age tend to love water sports, and children can be accustomed to activities. Water sports such as blowing bubbles and exploring underwater before you start teaching them formal swimming skills, and children aged 4 or 5 are ready to learn to control breathing and floating.\\r\\n\\r\\nExercise suitable for children aged 6-8 years\\r\\nChildren develop enough at the age of six to be able to throw a baseball ball, pass a football or a basketball, and can also perform gymnastics and drive a two-wheeled bicycle.\\r\\n\\r\\nExercise suitable for children aged 9-11 years\\r\\nChildren of this age are usually able to hit and throw baseball in a precise way, then don\'t mind encouraging competition.If children are interested in participating in events such as short races or long-distance running, it is safe as long as the children have been trained in the competition and keep Moisturize their bodies by drinking water and fluids.\\r\\n\\r\\nExercise suitable for children aged 12-14 years\\r\\nChildren may lose interest in structured sports as they reach adolescence and may want to focus instead on strength training and muscle building, but unless a child reaches puberty, his or her parents must prevent him from lifting heavy weights.\\r\\n\\r\\nExercise appropriate for children 15 years of age or older\\r\\nOnce a child is in adulthood and ready to lift weights, his or her parents should ask them to take a weight lifting lesson with a specialist, as poor weight lifting performance can lead to fractures or muscle damage.\\r\\n\\r\\nThe benefits of different sports types for children\\r\\nfootball sport\\r\\nThis collective sport teaches the child\'s stamina and handling the foot accurately through the ability to change the direction of the ball, quick response and maintain balance.\\r\\n\\r\\nBaseball\\r\\nThe slow nature of these games helps to teach children to focus and pay attention, and also helps develop hand-eye coordination.\\r\\n\\r\\nBasketball Sports\\r\\nA quick game like basketball is important to teach the child to control the body through defense and attack.\\r\\n\\r\\nSnowboarding\\r\\nThis unilateral sport encourages children to look ahead and plan their path ahead of time.\\r\\n\\r\\nswimming sport\\r\\nSwimming teaches children to control their breath and attention to detail, as well as the importance of time by crowning the winners simply because they have been faster for tenths of a second.\\r\\n\\r\\nTennis Sport\\r\\nIt improves quick thinking and improves arm strength and accuracy.\"}', '2019-08-25 04:46:02', '2019-08-29 04:04:15', '0'),
(66, '1567062163.jpg', '{\"ar\":\"تمارين شد البطن\",\"en\":\"Tighten the abdominal exercises\"}', '{\"ar\":\"فوائد تمارين شد البطن\\r\\nبالإضافة إلى الحصول على مظهر مشدود، هناك فوائد عديدة لتمارين شد البطن:\\r\\n\\r\\nتقليل آلام الظهر\\r\\nتحسين استقامة وتوازن الجسم\\r\\nتقوية منطقة الجذع\\r\\nرفع الأداء الرياضي\\r\\nنصائح للقيام بتمارين شد البطن\\r\\nقم باستخدام السجادة المخصصة للتمارين الرياضية\\r\\nاحرص على القيام بتمارين الإحماء\\r\\nتمرّن بشكل منتظم ومستمر للحصول على الفائدة القصوى\\r\\nلا تهمل شعور الألم أثناء التمرين أو تستمر في التمرّن بالرغم من الألم الشديد\\r\\nجرّب التنويعات المختلفة لتمارين البطن واختر الأنسب لك\\r\\nقم بالتدرج في صعوبة و مدّة التمارين\\r\\nللمزيد: نصائح للحامل عند ممارسة الرياضة\\r\\n\\r\\nأنواع تمارين شد البطن\\r\\nتمرين البلانك\\r\\nيعد هذا التمرين من اشهر تمارين شد عضلات البطن والجذع عموماً، حيث يعمل على تقوية مختلف العضلات في هذه المنطقة مثل عضلات البطن والورك والمؤخرة وجانبي العمود الفقري. يضمن التمرين، بالتالي،الحصول على الفوائد الصحية بشكل كامل وعدم إهمال العضلات المحيطة بالبطن.\\r\\n\\r\\nطريقة أداء التمرين:\\r\\n\\r\\nقم بالاستلقاء على بطنك.\\r\\nارفع جسدك عن الأرض مع التمركز ووضع ثقل الجسم على الذراعين والقدمين.\\r\\nاجعل الأكواع على مستوى الكتفين وباعد بين ذراعيك مسافة الكتفين كذلك.\\r\\nابق جسمك مستقيماً وابق بهذه الوضعية حوالي عشر ثواني.\\r\\nيمكنك أيضاً فرد يديك أمام الكتفين مسافة عشرين سم مما يوسع نطاق عمل عضلات البطن.\\r\\nتمرين البلانك الجانبي\\r\\nهذا التمرين هو إحدى التنويعات على تمرين البلانك الكلاسيكي. يستهدف التمرين عضلات الجذع ويتميز بعمله على العضلات الخلفية الجانبية أسفل الظهر.\\r\\n\\r\\nطريقة أداء التمرين:\\r\\n\\r\\nقم بالاستلقاء بشكل جانبي مع شد عضلات البطن والحرص على استقامة الجسم.\\r\\nارفع جسدك للأعلى ودع ثقله يتمركز على ذراعك المثنية أسفلك.\\r\\nابق في هذه الوضعية لمدة 10 ثواني إلى دقيقة.\\r\\nكرر التمرين عدة مرات مع التبديل بين الجانبين.\\r\\nتمرين ضغط المعدة\\r\\nيعد هذا التمرين أحد أسهل تمارين شد عضلات البطن وأكثرها فعالية. يعمل التمرين بشكل أساسي على العضلة البطنية المستقيمة ويستهدف كذلك العضلات الجانبية المائلة للبطن.\\r\\n\\r\\nطريقة أداء التمرين:\\r\\n\\r\\nقم بالاستلقاء على الظهر مع ثني الركبتين.\\r\\nارفع الجزء العلوي من جسدك ( الرأس والكتفين) للأعلى نحو عضلات الحوض مع إبقاء اليدين خلف الرأس أو بشكل متقاطع على الصدر.\\r\\nابق في هذه الوضعية عدة ثواني ثم قم بتكرار التمرين اثني عشر مرة.\\r\\nبدلاً من وضع اليدين خلف الرأس يمكنك فردهما للأعلى مما يشكل ضغطاً إضافياً على عضلات المعدة.\",\"en\":\"The benefits of abdominoplasty exercises\\r\\nIn addition to having a taut appearance, there are several benefits to abdominoplasty:\\r\\n\\r\\nReduce back pain\\r\\nImprove the straightness and balance of the body\\r\\nStrengthen the trunk area\\r\\nIncrease athletic performance\\r\\nTips for Tummy Tuck Exercises\\r\\nUse an exercise mat\\r\\nBe sure to do warm-up exercises\\r\\nExercise regularly and for maximum benefit\\r\\nDo not neglect the pain during exercise or continue to work out despite severe pain\\r\\nTry different variations of your abdominal exercises and choose what works best for you\\r\\nGradually increase the difficulty and duration of the exercise\\r\\nMore: Tips for pregnant women when exercising\\r\\n\\r\\nTypes of abdominoplasty exercises\\r\\nPlank exercise\\r\\nThis exercise is one of the most famous exercises to tighten the abdominal muscles and the trunk in general, as it works to strengthen the various muscles in this region such as the muscles of the abdomen, hip, butt and sides of the spine. Exercise, therefore, ensures full health benefits and does not neglect the muscles around the abdomen.\\r\\n\\r\\nExercise Method:\\r\\n\\r\\nLie on your belly.\\r\\nLift your body off the ground with positioning and placing the weight of your body on your arms and feet.\\r\\nKeep the elbows at the shoulders level and distance your arms as well.\\r\\nKeep your body straight and stay in this position for about ten seconds.\\r\\nYou can also straighten your hands in front of your shoulders by a distance of 20 centimeters, extending your abdominal muscles.\\r\\nSide plank exercise\\r\\nThis exercise is one of the variants of the classic Plank exercise. The exercise targets the muscles of the trunk and is characterized by its work on the posterior lateral muscles of the lower back.\\r\\n\\r\\nExercise Method:\\r\\n\\r\\nLie down sideways, tighten your abdominal muscles, and make sure your body is straight.\\r\\nLift your body up and let its weight rest on your bent arm underneath.\\r\\nStay in this position for 10 seconds to a minute.\\r\\nRepeat the exercise several times with the switch between the sides.\\r\\nStomach pressure exercise\\r\\nThis is one of the easiest and most effective exercises to tighten the abdominal muscles. Exercise works primarily on the rectus abdominal muscle and also targets the lateral oblique muscles of the abdomen.\\r\\n\\r\\nExercise Method:\\r\\n\\r\\nLie on the back with your knees bent.\\r\\nLift your upper body (head and shoulders) up toward your pelvic muscles while keeping your hands behind the head or crossed the chest.\\r\\nStay in this position for several seconds and repeat the exercise twelve times.\\r\\nInstead of putting your hands behind your head, you can straighten them up, putting extra strain on your stomach muscles.\"}', '2019-08-25 06:00:12', '2019-08-29 04:02:43', '0'),
(67, '1566975509.jpg', '{\"ar\":\"تمارين تقوية عضلات الظهر\",\"en\":\"Exercises strengthen the muscles of the back\"}', '{\"ar\":\"قبل تمارين تقوية عضلات الظهر\\r\\nقم دائما باستشارة الطبيب قبل البدء في برنامج تمرين جديد.\\r\\nإذا كنت تعاني من إصابة مؤلمة مثل الناجمة عن التعرض للسقوط أو لحادث، فحاول دائماً الحصول على المساعدة الطبية والتقييم الإضافي لاستبعاد الحالات الخطيرة.\\r\\nإذا تسببت هذه التمارين في زيادة ألم ظهرك، فتوقف واطلب المساعدة الطبية.\\r\\nتذكر أنه يتوجب عليك ممارسة التمارين فقط داخل حدودك قدراتك الجسدية.\\r\\nإن القيام بالكثير من التمارين وبسرعة عالية قد يزيد من آلام الظهر ويبطئ عملية الشفاء.\\r\\nما هي التمارين التي يمكن لها أن تقوي من عضلات الظهر؟\\r\\nجرب هذه التمارين البسيطة، والتي لا تحتاج إلى معدات، لتقوية العضلات التي تدعم العمود الفقري. يمكن أن يؤدي اكتساب القوة إلى تقليل الألم والخلل الوظيفي. استشر طبيبك أو المعالج قبل البدء في هذه التمارين للتأكد من أنها مناسبة لحالتك.\\r\\n\\r\\nالأقواس لتقوية عضلات الظهر\\r\\nالعضلة الألوية الكبيرة‏ هي العضلة الكبيرة التي تقع في الأرداف.\\r\\nإنها واحدة من أقوى العضلات في الجسم ومسؤولة عن الحركة في الورك، بما في ذلك أنشطة تمديد الورك مثل القرفصاء.\\r\\nيمكن أن يساهم أي ضعف في العضلة الألوية في حدوث آلام الظهر. هذا لأنها تعد من المثبتات المهمة لمفاصل الورك وأسفل الظهر أثناء الحركات مثل المشي.\\r\\nالعضلة التي سيتم تمرينها: العضلة الألوية الكبيرة\\r\\n\\r\\nاستلق على الأرض مع قدميك مسطحة على الأرض، وباعد بين الوركين بالعرض.\\r\\nباستخدام يديك على جانبيك، اضغط قدميك على الأرض أثناء رفع الأرداف ببطء عن الأرض حتى يصل جسمك إلى خط مستقيم واحد. حافظ على كتفيك على الأرض.\\r\\nأخفض ظهرك واسترح لمدة 1 دقيقة.\\r\\nكرر التمرين 15 مرة.\\r\\nقم بأداء 3 مجموعات.\\r\\nالسحب إلى المناورة لتقوية عضلات الظهر\\r\\nالبطنية المستعرضة هي العضلات التي تلتف حول خط الوسط، وهي تساعد على دعم العمود الفقري والبطن وهي مهمة لتثبيت مفاصل العمود الفقري ومنع الإصابة أثناء الحركة.\\r\\n\\r\\nالعضلة التي سيتم تمرينها: البطنية المستعرضة\\r\\n\\r\\nاستلق على الأرض مع قدميك مسطحة على الأرض، وباعد بين الوركين بالعرض.\\r\\nاسترخ يديك من جانبك.\\r\\nخذ نفساً عميقاً إلى الداخل، ثم قم بالزفير واسحب السرة إلى العمود الفقري، استعن بعضلات البطن دون إمالة الوركين.\\r\\nتمسك بهذه الوضعية لمدة 5 ثواني.\\r\\nكرر التمرين 5 مرات.\",\"en\":\"Before exercises strengthen the muscles of the back\\r\\nAlways consult your doctor before starting a new exercise program.\\r\\nIf you have a traumatic injury such as a fall or accident, always seek medical help and further evaluation to rule out serious cases.\\r\\nIf these exercises increase your back pain, stop and seek medical help.\\r\\nRemember that you should only exercise within your physical limits.\\r\\nDoing too much exercise at high speed can increase back pain and slow down the healing process.\\r\\nWhat exercises can strengthen your back muscles?\\r\\nTry these simple exercises, which do not require equipment, to strengthen the muscles that support the spine. Gaining strength can reduce pain and dysfunction. Consult your doctor or therapist before starting these exercises to make sure they are appropriate for your condition.\\r\\n\\r\\nBraces to strengthen the back muscles\\r\\nThe large gluteus muscle is the large muscle located in the buttocks.\\r\\nIt is one of the strongest muscles in the body and is responsible for movement in the hip, including hip extension activities such as squat.\\r\\nAny weakness in the gluteus muscle can contribute to back pain. This is because they are important stabilizers for hip and lower back joints during movements such as walking.\\r\\nMuscle to be exercised: Large gluteus muscle\\r\\n\\r\\nLie on the floor with your feet flat on the floor, and spread across your hips across.\\r\\nWith your hands on your sides, press your feet on the floor while slowly lifting the buttocks off the ground until your body reaches one straight line. Keep your shoulders on the floor.\\r\\nLower your back and rest for 1 minute.\\r\\nRepeat 15 times.\\r\\nPerform 3 sets.\\r\\nDrag to maneuver to strengthen the back muscles\\r\\nTransverse abdominal muscles that wrap around the midline, help support the spine and abdomen and are important for stabilizing the joints of the spine and preventing injury during movement.\\r\\n\\r\\nMuscle to be exercised: transverse abdominal\\r\\n\\r\\nLie on the floor with your feet flat on the floor, and spread across your hips across.\\r\\nRelax your hands.\\r\\nTake a deep breath in, exhale and drag the navel to the spine.Use the abdominal muscles without tilting your hips.\\r\\nHold this position for 5 seconds.\\r\\nRepeat the exercise 5 times.\"}', '2019-08-25 06:03:54', '2019-08-28 03:58:29', '0');
INSERT INTO `blogs` (`id`, `image`, `title`, `text`, `created_at`, `updated_at`, `type`) VALUES
(68, '1567062052.jpg', '{\"ar\":\"فوائد رياضة المشي وكيفية الاستفادة القصوى منها\",\"en\":\"The benefits of walking and how to make the most of them\"}', '{\"ar\":\"فوائد رياضة المشي\\r\\nلرياضة المشي فوائد كثيرة تجعلها الخيار الأفضل للمحافظة على وزن صحي:\\r\\n\\r\\nتعمل على تنظيم عمل الجهاز الهضمي وتمنع الإمساك\\r\\nتساعد على حرق الدهون في جميع أجزاء الجسم مما يمنح الجسم مع الوقت شكل مقبول ومتناسق\\r\\nرياضة المشي تمنع ترهلات الجلد وتقوي العضلات\\r\\nرياضة المشي تمنح ليونة مفاصل الركبتين والقدمين\\r\\nرياضة المشي تفيد في علاج مضاعفات ضغط الدم ودوالي الساقين\\r\\nرياضة المشي تشط الدورة الدموية وتزيد كمية الدم الوارد إلى شرايين وأوردة الجسم\\r\\nإيصال أكبر نسبة من الأكسجين إلى كافة أنحاء الجسم وحرقها\\r\\nإعطاء طاقة كافية من النشاط والحيوية لأداء الأعمال اليومية بتركيز أفضل\\r\\nيخلص من الشعور بالكسل والإحباط\\r\\nتخليص الجسم من الأملاح الزائدة عن حاجته نتيجة العرق والإفرازات\\r\\nتزيد من قوة العضلات وتحمل التعب والإرهاق\\r\\nتقوية عضلة القلب وزيادة قدرتها على ضخ الدم ووقايتها من مرض تصلب الشرايين وانسدادها\\r\\nمنع زيادة الكولسترول السيء في الشرايين وزيادة الكولسترول الجيد HDL\\r\\nالمحافظة على جسم سليم متناسق ورشيق\\r\\nكيفية الاستفادة القصوى من رياضة المشي\\r\\nاختيار الوقت المناسب للمشي حيث لا يوجد ضغط عمل أو مشاغل\\r\\nاختيار المكان المناسب حيث يكون ممتع وغير خطير لكي لا يتسبب بالأذى، كالمشي في الطبيعة، على شاطئ البحر، في الحقول والحدائق العامة\\r\\nالانتظام في موعد ثابت لكي يتحول المشي إلى روتين؛ كالمشي صباحًا أو مساءً حيث يكون الجو لطيف ومريح\\r\\nعدم المشي في الجو الحار أو الرياح الشديدة\\r\\nالمشي بملابس مريحة وفضفاضة لكي لا يعيق الحركة\\r\\nأخذ زجاجة مياه للشرب لتجنب الجفاف\\r\\nتجنب أداء رياضة المشي بعد الأكل مباشرة لتجنب ثقل الحركة\\r\\nالبدء في رياضة المشي ببطئ لتحمية الجسم ثم زيادة الحركة والسرعة تدريجيًا\\r\\nيجب الانتباه أن يكون التنفس شهيق وزفير لتجنب دخول الغبار إلى الفم\",\"en\":\"The benefits of walking\\r\\nWalking has many benefits that make it the best option for maintaining a healthy weight:\\r\\n\\r\\nIt regulates the work of the digestive system and prevents constipation\\r\\nHelps burn fat in all parts of the body giving the body an acceptable and consistent time\\r\\nWalking prevents skin sagging and strengthens muscles\\r\\nWalking gives the flexibility of the knees and feet joints\\r\\nWalking is useful in the treatment of complications of blood pressure and varicose veins\\r\\nWalking combs blood circulation and increases the amount of blood contained in the arteries and veins of the body\\r\\nDeliver the largest amount of oxygen to all parts of the body and burn\\r\\nGiving enough energy and vitality to perform daily work in a better concentration\\r\\nRelieves laziness and frustration\\r\\nRid the body of excess salts due to sweat and secretions\\r\\nIncreases muscle strength and endures fatigue and fatigue\\r\\nStrengthening the heart muscle and increasing its ability to pump blood and prevent it from atherosclerosis and blockage\\r\\nPrevent the increase of bad cholesterol in the arteries and increase the good cholesterol HDL\\r\\nMaintain a healthy, harmonious and slim body\\r\\nHow to make the most of the sport of walking\\r\\nChoose the right time for walking where there is no work pressure or concerns\\r\\nChoose the right place where it is fun and not dangerous so as not to cause harm, such as walking in nature, on the beach, in fields and parks\\r\\nRegularly schedule your walk to become a routine, such as a morning or evening walk where the weather is pleasant and comfortable\\r\\nDo not walk in hot weather or strong winds\\r\\nWalking in loose and comfortable clothes so as not to hinder movement\\r\\nTake a bottle of water to drink to avoid dehydration\\r\\nAvoid walking immediately after eating to avoid heavy movement\\r\\nStart walking slowly to protect your body and gradually increase movement and speed\\r\\nAttention should be breathing inhale and exhalation to avoid dust entering the mouth\"}', '2019-08-25 06:08:00', '2019-08-29 04:00:52', '0'),
(70, '1567061963.jpg', '{\"ar\":\"ما هي فوائد تمارين الإطالة؟\",\"en\":\"What are the benefits of stretching exercises?\"}', '{\"ar\":\"فوائد ممارسة تمارين الإطالة\\r\\nيساعدك الانتظام على تمارين الإطالة فيما يلي:\\r\\n\\r\\nالحركة بسهولة وأريحية.\\r\\nزيادة معدل سرعة الحركة.\\r\\nمنع الإصابات الناتجة عن الإجهاد والتمزق العضلي.\\r\\nالتخفيف من إصابتك بالشد العضلي.\\r\\nتجعل هذه التمارين عضلاتك مؤهلة للقيام بمجهود كبير كالجري، والتزحلق، والتنفس، والسباحة، وغيرها من الرياضات التي تحتاج إلى مجهود عضلي كبير.\\r\\nشعور الجسد بالمزيد من الاسترخاء.\\r\\nتنشيط الدورة الدموية.\\r\\nالشعور بالرضا والتحسن التام لحالتك الجسدية.\\r\\nكيفية ممارسة تمارين الإطالة\\r\\nليست تمارين الإطالة صعبة التطبيق، لكن يجب ممارستها بكل صحيح لأن بعض الناس قد تخطئ في ممارستها وربما تؤذي جسدها أو لا تحصل على الفائدة المرجوة.\\r\\nنشدّ العضلات باستمرار ورخاوة مع التركيز على إطالة العضلات. هذه هي الطريق الصحيحة لأداء التمارين.\\r\\nأما الطريقة الخاطئة تكمن في الوثب لأعلى والضغط إلى أسفل أو الاستمرار في شد العضلات إلى مرحلة الألم؛ مما يسبب ضرراً كبيراً للعضلات ويتلفها.\\r\\nأمثلة أوضاع تمارين الإطالة\\r\\nاجلس على الأرض برجلين مفرودتين وذراعين مشدودتين متوجهتين إلى الأسفل لتلامسا قدميك.\\r\\nاستمر على هذا الوضع لمدة 10-20 ثانية إلى أن تشعر باسترخاء.\\r\\nإذا استمر شعورك بالشد العضلي في رجليك، استرخ تدريجياً محاولاً الوصول إلى النقطة التي تشعر بالراحة معها دون ألم الشد العضلي؛ فهذا يتيح للأنسجة الإطالة التدريجية.\\r\\nستلاحظ بعد فترة من ممارستك لتمارين الإطالة بشكل صحيح أنّ حركتك أصبحت أسهل وأسرع، وأنّ فك عضلاتك المشدودة لا يستغرق وقتاً كبيراً.\\r\\nبعض النصائح العامّة عند ممارسة تمارين الإطالة\\r\\nقم بالإحماء أو التسخين قبل ممارسة تمارين الإطالة؛ أي قم بممارسة بعض التمارين الرياضية البسيطة الأخرى كالمشي الخفيف، أو الركض، أو ركوب الدراجة لمدة قصيرة. ثم مارس تمارين الإطالة بعد ذلك.\\r\\nركز على عضلات جسدك الرئيسية كعضلات الفخذين وأسفل الظهر والرقبة والكتفين، وكذلك المفاصل التي تستعملها كثيراً أثناء اللعب أو العمل.\\r\\nتأكد أنك تمارس هذه التمارين على جانبي جسدك، فلو كنت تمرن عضلات فخذك الأيمن مثلاً، قم بنفس التمرين على عضلات فخذك الأيسر.\\r\\nلا تمارس تمارين الإطالة قبل الخوض في أي نشاط مكثف كالعدو السريع والمشي والأنشطة الميدانية الأخرى؛ فهذا من شأنه أن يضعف من أدائك فيها.\\r\\nمارس التمارين بشكل سلس دون عنف أو سرعة زائدة.\\r\\nلا يجب أن تشعر بالألم أثناء ممارستك لهذه التمارين، فاحرص على الوصول إلى نقطة الاسترخاء لا الألم.\\r\\nمارس تمارين الإطالة بانتظام (مرتان إلى ثلاث مرات في الأسبوع على الأقل).\",\"en\":\"The benefits of stretching exercises\\r\\nRegularity helps you stretch:\\r\\n\\r\\nMovement is easy and convenient.\\r\\nIncrease the rate of movement speed.\\r\\nPrevent injuries from stress and muscle rupture.\\r\\nReduce your muscle strain.\\r\\nThese exercises make your muscles eligible for a lot of work, such as running, skiing, breathing, swimming, and other sports that require a lot of muscle effort.\\r\\nFeeling more relaxed.\\r\\nStimulate blood circulation.\\r\\nFeeling good and fully improving your physical condition.\\r\\nHow to practice stretching exercises\\r\\nStretching exercises are not difficult to apply, but they should be done properly because some people may mistake them and may harm their body or not get the desired benefit.\\r\\nConstantly tighten and loosen muscles with a focus on stretching muscles. This is the right way to do exercises.\\r\\nThe wrong way is to jump up and squeeze down or continue to stretch the muscles to the pain stage, causing great damage and damage to the muscles.\\r\\nExamples of stretches\\r\\nSit on the floor with two straight legs and tight arms facing down to touch your feet.\\r\\nHold for 10-20 seconds until you feel relaxed.\\r\\nIf you continue to feel cramps in your legs, gradually relax and try to reach the point you feel comfortable with without the pain of muscle strain;\\r\\nAfter a period of stretching correctly, you will notice that your movement has become easier and faster, and that pulling your tight muscles doesn\'t take much time.\\r\\nSome general tips for stretching\\r\\nWarm up or warm up before stretching; doing some other simple exercise such as light walking, jogging, or cycling for a short time. Then do stretching exercises afterwards.\\r\\nFocus on your body\'s main muscles such as the muscles of your thighs, lower back, neck and shoulders, as well as the joints you use most frequently during play or work.\\r\\nMake sure that you do these exercises on both sides of your body.If you exercise your right thigh muscles, for example, do the same exercise on the muscles of your left thigh.\\r\\nDo not do stretching exercises before engaging in any intense activity such as jogging, walking and other field activities; this will impair your performance.\\r\\nExercise smoothly without violence or excessive speed.\\r\\nYou shouldn\'t feel pain while doing these exercises.Make sure to reach the point of relaxation, not pain.\\r\\nStretch regularly (at least two to three times a week).\"}', '2019-08-25 06:16:05', '2019-08-29 03:59:23', '0'),
(72, '1566975205.jpg', '{\"ar\":\"كيفية الحصول على عضلات بطن مشدودة\",\"en\":\"How to Get Tight Belly Muscles\"}', '{\"ar\":\"اتباع نظام غذائي متوازن\\r\\nالتركيز على التمرين وحده لا يمكن أن يكون كافياً لإبراز عضلات البطن، وذلك لأن بناء العضلات يبدأ بتناول غذاء صحي وسليم قبل البدء بممارسة التمارين الشاقة التي لن تجدي نفعاً في حال تم إهمال تعليمات تناول الطعام المناسب وذلك لأن النظام الغذائي مسؤول بنسبة 90% عن النتائج المرجوة، وذلك من خلال:\\r\\n\\r\\nتناول كميات مناسبة من البروتينات، في ما يتعلق بالبروتين فيجب الإلتزام بكمية كافية وذلك لاحتوائه على أعلى تأثير حراري للجسم ومساعدته في بناء العضلات وحرق الدهون وتجدر الإشارة إلى أهمية تناول بروتين خالي من الدهون.\\r\\nتناول الكربوهيدرات بعد ممارسة التمارين، يسود الا عتقاد بأن الكربوهيدرات ضارة وتسبب زيادة الوزن ويجب الامتناع عنها عند الرغبة في بناء العضلات وتلك معلومات خاطئة ومضرة أيضاً، وذلك لأن الكربوهيدرات مهمة لتحقيق التوازن الغذائي للجسم عند تناول كميات مناسبة منها، فزيادة كمية أي مكون غذائي بما يزيد عن الحاجة يسبب الوزن الزائد، تجدر الإشارة أن الكربوهيدرات يمكن أن تساعد على بناء عضلات البطن المشدودة عند تناولها بعد ممارسة التمارين الرياضية وذلك لأن فرصة تحولها لدهون وتخزينها تكون أقل، ولذلك يمكن إضافة الخضار النشوية أو الرز البني أو الحبوب للوجبات ليحصل الجسم على الفيتامينات، المعادن، مضادات الأكسدة، والألياف لضمان البقاء بصحة جيدة.\\r\\nتناول الدهون الصحية، بعض الأنظمة الغذائية تشجع على الامتناع عن تناول الدهون بشكل تام وهو ما قد يتسبب بكارثة غذائية للجسم، الدهون الصحية عنصر أساسي في النظام الغذائي ولها دور في استقرار مستوى الإنسولين في الدم، شرط أن تكون دهون متعددة غير مشبعة ودهون أحادية والتي يمكن الحصول عليها من المكسرات النيئة، زيت السمك وزيت الزيتون.\\r\\nموازنة الكميات والنوعيات\\r\\nالحرص على احتواء النظام الغذائي على جميع مكوناته من مصادرها الصحية أمر مهم ولكن يجب الانتباه للحصص التي يتناولها الفرد وأن يحرص على أن تكون مناسبة لنشاطه ومعدل ما يحرق من سعرات حرارية أثناء ساعات التمرين.\\r\\nالرياضة المناسبة للحصول على عضلات بطن مشدودة\\r\\nيجب التوقف عن ممارسة تمارين الطحن (السحق) بشكل مبالغ به. ينصح بممارسة هذا النوع من التمارين لمدة 20 دقيقة ثلاث مرات في الأسبوع ولا داع لممارستها أكثر من ذلك وذلك لأن هنالك العديد من التمارين أكثر إنتاجية وتساعد على بناء العضلات بشكل أكبر وخسارة الدهون بكميات أكثر والتي تشمل:\\r\\n\\r\\nتمارين القرفصات مع رفع الأثقال.\\r\\nتمارين الدفع للأمام مع رفع الأثقال اليدوية (المفردة).\\r\\nتمرين الرفعة المميتة للأثقال.\\r\\nتمارين الضغط مع رفع لأثقال بوضعية الجلوس العسكرية.\\r\\nتمارين الإنخفاض للصدر.\\r\\nتمارين رفع الأثقال أثناء الجلوس على المقعد.\\r\\nتمارين رفع الجسم للأعلى مع التشبث بعمود حديدي.\\r\\nتمارين الضغط.\\r\\nممارسة التمارين القلبية\",\"en\":\"A balanced diet\\r\\nFocusing on exercise alone cannot be enough to highlight your abdominal muscles, because building muscle starts with a healthy and healthy diet before starting strenuous exercise that will not work if proper eating instructions are neglected because the diet is 90% responsible for the desired results , and that is through:\\r\\n\\r\\nEating adequate amounts of proteins, with regard to protein must be adhered to sufficient amount in order to contain the highest thermal effect of the body and help build muscle and burn fat. It should be noted the importance of eating a protein free of fat.\\r\\nCarbohydrate intake After exercise, it is believed that carbohydrates are harmful and cause weight gain and should be avoided when you want to build muscles and this information is wrong and harmful as well, because carbohydrates are important to achieve the nutritional balance of the body when eating adequate amounts of them, increasing the amount of any food component Because of the need to cause excess weight, it should be noted that carbohydrates can help to build tight abdominal muscles when taken after exercise because the chance of transformation of fat and storage is less, so you can add starchy vegetables or brown rice or grain to the meal The body gets vitamins, minerals, antioxidants, and fiber to ensure you stay healthy.\\r\\nEating healthy fats, some diets encourage to refrain from eating fat completely, which may cause a food disaster for the body, healthy fats are an essential component of the diet and have a role in the stability of the level of insulin in the blood, provided that the polyunsaturated fats and monounsaturated fats that can Get them from raw nuts, fish oil and olive oil.\\r\\nBalancing quantities and qualities\\r\\nEnsuring that the diet contains all its components from its health sources is important, but you should pay attention to the rations taken by the individual and be sure to be appropriate for his activity and the rate of calories burned during exercise hours.\\r\\nThe right sport to get tight abdominal muscles\\r\\nStop grinding exercises too much. This type of exercise is recommended for 20 minutes three times a week and is not necessary to do more because there are many more productive exercises and help to build muscle more and lose fat in more quantities, which include:\\r\\n\\r\\nSquat exercises with weight lifting.\\r\\nPush-ups with manual weightlifting.\\r\\nThe deadly exercise of lifting weights.\\r\\nPush-ups with lifting weights in the military sitting position.\\r\\nLower chest exercises.\\r\\nWeightlifting exercises while sitting on the seat.\\r\\nExercises lift the body up with clinging to an iron column.\\r\\nPush-ups.\\r\\nCardiovascular exercise\"}', '2019-08-25 06:41:30', '2019-08-28 03:53:25', '0'),
(73, '1566975140.jpg', '{\"ar\":\"هل تؤثر تمارين رفع الأثقال للأطفال على النمو؟\",\"en\":\"Do weight lifting exercises for children affect growth?\"}', '{\"ar\":\"ما هي فوائد رفع الأثقال للأطفال؟\\r\\nتمارين القوة التي تتضمن حملاً للأوزان قد تُسهم في تعزيز صحة العظام، ومن أبرز فوائدها أيضاً تحسين وقفة الجسم ووضعيته، وتعزيز قوة العضلات وحمايتها من التعرض للإصابات. تُسهم تمارين القوة أيضاً في تعزيز أداء الأطفال في رياضات أخرى.\\r\\n\\r\\nمن أهم الفوائد البدنية تعزيز الصحة الجسم بشكل عام والحفاظ على وزن صحي ومناسب.\\r\\n\\r\\nلا تقتصر التأثيرات الإيجابية لممارسة الأطفال لتمارين القوة بشكل عام على الصحة البدنية فقط، بل إنّ لها فوائد عديدة على الصحة النفسية أيضاً فهي تعزز ثقة الطفل بنفسه، وتعزز تقديره لذاته، وتُحسن نظرته إلى نفسه، وهذا أمر ينعكس على كافة جوانب حياته.\\r\\n\\r\\nما الذي يجب مراعاته قبل السماح للطفل برفع الأثقال؟\\r\\nخطر المنافسة\\r\\nيجب الانتباه أولاً إلى أن تمارين القوة وما تتضمنه من حمل للأوزان يختلف كلياً عن رياضات كمال الأجسام ورفع الأثقال، حيث تتضمن هذه الأخيرة المنافسة في حمل أثقال أكبر وأكبر ما قد يؤثر سلباً على صحة الأطفال إن انخرطوا بها. لهذا، يجب الحرص على ممارسة الأطفال للتمارين بأوزان خفيفة مناسبة لهم.\\r\\n\\r\\nالعمر المناسب\\r\\nلا يُفضّل بدء الأطفال بممارسة هذه التمارين قبل سن السابعة أو الثامنة، وذلك لضمان اكتمال قدرة الطفل على التوازن والوقوف بشكل سليم لتجنب تأثيرات سلبية للتعامل مع الأوزان بشكل خاطئ.\\r\\n\\r\\nتعليمات ممارسة رفع الأثقال\\r\\nلا يُنصح السماح بالطفل بالبدء بممارسة تمارين القوة قبل أن يكون الطفل قادراً على فهم واستيعاب التعليمات الموجهة إليه وتطبيقها أثناء ممارسة التمارين، فممارسة التمارين بشكلها الصحيح مهم جداً لتجنب تعرض الطفل للإصابة أثناء التمرين.\\r\\n\\r\\nفحص الطفل أولًا\\r\\nيُفضل إجراء فحص طبي شامل للطفل قبل شروعه بممارسة تمارين القوة للتحقق من قدرته وسلامته بدنياً، وأنّ هذه التمارين لن يكون لها تأثيرات سلبية على صحته.\\r\\n\\r\\nتدريب الطفل \\r\\nيُفضل وخاصة في البداية الاستعانة بمحترف يساعد على تدريب الطفل على أداء التمارين بشكل صحيح ليحقق الفائدة القصوى منها. وبعد ذلك، لا بد من الاستمرار بالإشراف على الطفل أثناء ممارسة تمارين القوة للتأكد من قيامه بها بالشكل الصحيح.\\r\\n\\r\\nراحة الطفل\\r\\nاحرص على راحة الطفل بين التمارين، وتأكّد أنه لا يضغط على نفسه أو يحمل أوزاناً أثقل من قدرته. وتأكد أيضاً أنه يمارس تمارين إحماء خفيفة قبل البدء بتمارين القوة، وتمارين مماثلة بعد الانتهاء.\",\"en\":\"What are the benefits of weight lifting for children?\\r\\nStrength exercises that include weight lifting may contribute to bone health. Strength training also contributes to children\'s performance in other sports.\\r\\n\\r\\nThe most important physical benefits are promoting overall body health and maintaining a healthy and appropriate weight.\\r\\n\\r\\nThe positive effects of children\'s exercise in general are not limited to physical health, but also have many benefits to mental health.It also enhances the child\'s self-confidence, self-esteem, and improves his \\/ her self-perception, which is reflected in all aspects of his or her life.\\r\\n\\r\\nWhat should be considered before allowing a child to lift weights?\\r\\nRisk of competition\\r\\nIt should be noted that strength training and weight-bearing exercises are completely different from bodybuilding and weightlifting, as they involve competition in carrying larger and larger weights that may adversely affect children\'s health. Therefore, care should be taken to exercise children with light weights suitable for them.\\r\\n\\r\\nThe right age\\r\\nIt is not preferable for children to start exercising before the age of seven or eight, to ensure that the child is able to balance and stand properly to avoid the negative effects of dealing with weights wrong.\\r\\n\\r\\nInstructions for weight lifting\\r\\nIt is not advisable to allow the child to start exercising strength exercises before the child is able to understand and understand the instructions given to him and apply them during exercise. Exercise properly is important to avoid injury to the child during exercise.\\r\\n\\r\\nCheck the baby first\\r\\nIt is preferable to have a thorough medical examination before the child begins to exercise strength exercises to verify his ability and physical integrity, and that these exercises will not have negative effects on his health.\\r\\n\\r\\nChild training\\r\\nEspecially at the outset it is preferable to have a professional who helps train the child to perform the exercises properly to achieve the maximum benefit from them. After that, it is important to continue to supervise the child while exercising strength exercises to make sure that they do it properly.\\r\\n\\r\\nChild comfort\\r\\nMake sure the child is comfortable between exercises, and make sure that he does not put pressure on himself or carrying weights heavier than his ability. Also make sure that he is doing light warm-up exercises before starting strength exercises, and similar exercises after completion.\"}', '2019-08-25 06:44:38', '2019-08-28 03:52:20', '0'),
(74, '1566975086.jpg', '{\"ar\":\"متى يظهر تأثير الرياضة على الجسم؟\",\"en\":\"When does the effect of sport on the body appear?\"}', '{\"ar\":\"تأثير الرياضة على الجسم\\r\\nمتى يظهر تأثير الرياضة على الجسم؟\\r\\nأهمية اختيار التمرين الصحيح لتحقيق النتائج\\r\\nبداية ظهور النتائج\\r\\nكيفية تسريع حدوث النتائج بطريقة آمنة؟\\r\\nزيادة كثافة تمرينك وتناول البروتين\\r\\nتناول الطعام لتغذية جسمك\\r\\nتحديد أولويات وتفضيل تدريب القوة على تمارين القلب المستقرة التقليدية\\r\\nقم بقضاء وقت للتعافي\\r\\nتبديل التمرينات بعد ستة إلى 12 أسبوعًا\\r\\n \\r\\n\\r\\nمتى يظهر تأثير الرياضة على الجسم؟\\r\\nإذا كنت تريد رؤية بعض التحولات والنتائج الايجابية في القوة العضلية وشكل الجسم، فمن المحتمل أن يستغرق الأمر حوالي ثمانية أسابيع.\\r\\n\\r\\nبطبيعة الحال، ثمانية أسابيع ليس رقماً صعباً وسريعاً ولا ينطبق على الجميع، حيث تختلف مدة ظهور النتائج باختلاف الأعمار والجنس وطريقة التمرين وشدته وعدد مرات تكرار التمرين في الأسبوع وطبيعة الغذاء المتناول أثناء فترة التمرين، ولكن يمكن اعتباره قاعدة بالنسبة لمعظم المتمرنين.\\r\\n\\r\\nللتأكد من أنك على الطريق الصحيح مع تحقيق أهدافك تأكد من تطوير عادات متناسقة تساعد جسمك على تحقيق أهدافه، مثل التمدد قبل التمرين وبعده، تناول وجبات متوازنة والسماح لنفسك بالراحة عند الحاجة إليها.\\r\\n\\r\\nأهمية اختيار التمرين الصحيح لتحقيق النتائج\\r\\nمن المهم أن تتذكر أهمية الشكل المناسب عند إجراء أي تمرين، خاصة عندما يتعلق الأمر بتدريب القوة.\\r\\nضمان شكل صحيح للتمرين يجعل جسمك يتحرك في الاتجاه الصحيح نحو النتائج التي تريدها، وسوف تمنع إصابة غير ضرورية على طول الطريق أيضًا.\\r\\nالحفاظ على تمرين صحيح وأوزان خفيفة قد يعني أنك لا تستطيع الحصول على النتائج بالسرعة التي تريدها، أو قد لا تتمكن من القيام بعدد كبير من التكرارات في التمرين الواحد.\\r\\nتحقيق أهدافك والحفاظ على سلامتك أمران أكثر أهمية من سرعة الإنجاز.\\r\\nبداية ظهور النتائج\\r\\nسترى النتائج في أقل من شهر، لاسيما إذا لم تكن قد تدربت بمثل هذه الطريقة (تمارين القوة) منذ فترة طويلة.\\r\\nمن حيث التدريب على القوة، يمكنك أن ترى زيادة بنسبة 10% فيما يمكنك رفعه من الأثقال كل شهر خلال الأشهر الأربعة إلى الستة الأولى، وعند هذه النقطة ستتباطأ التعديلات وسيتوجب عليك تغيير التدريبات للحفاظ على سلامتك.\\r\\nبالنسبة لتضخم العضلات، قد تستغرق النتائج الملحوظة وقتًا أطول قليلاً -من 4 إلى 8 أسابيع من التدريب المستهدف- مع نتائج قابلة للقياس بشكل كبير خلال أربعة إلى ستة أشهر.\\r\\nيمكن للمبتدئين أن يلاحظوا ما يصل إلى 2 ونصف باوند من العضلات شهريًا، لكن هذا سيزداد تدريجياً مع ازدياد الخبرة في التمرين، على الرغم من أن قياس الوزن قد لا يظهر ذلك، لأن الميزان سوف لا يظهر أي خسارة في الدهون يمكن أن تحصل.\\r\\nكما تحدث تغيرات كفاءة القلب بشكل سريع للغاية، خاصة في البداية.\\r\\nكيفية تسريع حدوث النتائج بطريقة آمنة؟\\r\\nاتبع هذه الخطوات الخمس لنتائج لياقة سريعة وآمنة وفعالة:\\r\\n\\r\\nزيادة كثافة تمرينك وتناول البروتين\\r\\nتظهر الأبحاث التي نشرت في المجلة الأمريكية للتغذية السريرية أن الجمع بين ممارسة عالية الكثافة وزيادة استهلاك البروتين يسمح للناس بفقدان المزيد من الدهون وبناء مزيد من العضلات مع خفض السعرات الحرارية، حيث يجب تقليل تنازل المزيد من السعرات الحرارية، وتناول المزيد من البروتين لتقليل خسائر كتلة العضلات أثناء فقدان الوزن.\\r\\n\\r\\nتناول الطعام لتغذية جسمك\\r\\nتختلف احتياجات السعرات الحرارية بشكل كبير بين الناس\\r\\nبينما يؤدي نقص السعرات الحرارية (حرق سعرات حرارية أكثر مما تستهلك) إلى فقدان الوزن\\r\\nالنقص الكبير في السعرات قد يؤدي إلى الاحتفاظ بالدهون\\r\\nفائض السعرات الحرارية (استهلاك سعرات حرارية أكثر مما تحرق) مثالي لبناء العضلات\\r\\nلذا بدلاً من التعلق والتفكير الكبير في حساب السعرات الحرارية، ركز على الطعام كوقود\\r\\nاستمع إلى إشارات جوعك وقم بتعبئتها بأطعمة معالجة بشكل كامل أو أقل، لأن هذا سيساعدك في الحصول على المزيد من الألياف، ومنع إفراز الأنسولين الزائد وتخزين الدهون، ومساعدتك على الوصول إلى أهدافك الخاصة بفقدان الدهون.\\r\\n\\r\\nتحديد أولويات وتفضيل تدريب القوة على تمارين القلب المستقرة التقليدية\\r\\nيمكن أن تزيد تدريبات القوة من حرق السعرات الحرارية، حتى أثناء عدم ممارسة الريضة لمدة تصل إلى 72 ساعة بعد مغادرة صالة الألعاب الرياضية وفقاً لبحث نشر في المجلة الأوروبية لعلم وظائف الأعضاء التطبيقي، إضافةً إلى ذلك، فإنه يبني عملية الأيض للعضلة ويزيد من قوة الأداء.\\r\\n\\r\\nقم بقضاء وقت للتعافي\\r\\nامنح نفسك يوم راحة كامل واحد على الأقل في الأسبوع ولا تتدرب بشكل مكثف لنفس المجموعة العضلية مرتين خلال فترة ثلاثة أيام.\\r\\nخلط التدريبات الخاصة بك وشدتها للسماح لجسمك بالتعافي.\\r\\nمن الطبيعي أن تشعر ببعض الألم بعد 24 إلى 48 ساعة من التدريبات، خاصة في البداية، لكن لا يجب أن تشعر بالوهن أو عدم القدرة على المشي.\\r\\nتبديل التمرينات بعد ستة إلى 12 أسبوعًا\\r\\nللحفاظ على جسمك يتكيف باستمرار، من المهم تغيير التمرينات الخاصة بك كل ستة إلى 12 أسبوعاً\\r\\nقد يعني ذلك تغيير عدد مرات التكرار في التمرين أو تجربة تمارين جديدة أو تجربة السباحة الأسبوعية وتغير نمط القيام بالتمارين\\r\\nبخلاف ذلك، يمكن أن يتأقلم جسمك بشكل جيد مع التمرين الاختياري الذي لم يعد يرى له فائدة على العضلات\",\"en\":\"The effect of sport on the body\\r\\nWhen does the effect of sport on the body appear?\\r\\nThe importance of choosing the right exercise to achieve results\\r\\nThe start of the results\\r\\nHow to speed up the occurrence of results in a safe manner?\\r\\nIncrease your exercise intensity and protein intake\\r\\nEat to nourish your body\\r\\nPrioritize and prefer strength training over traditional stable cardio exercises\\r\\nSpend time recovering\\r\\nSwitch exercise after six to 12 weeks\\r\\n \\r\\n\\r\\nWhen does the effect of sport on the body appear?\\r\\nIf you want to see some shifts and positive results in muscle strength and body shape, it is likely to take about eight weeks.\\r\\n\\r\\nOf course, eight weeks is not a difficult and fast number and does not apply to everyone, as the duration of the appearance of results varies depending on age, sex, exercise method and intensity and frequency of exercise per week and the nature of food intake during the exercise period, but can be considered a rule for most exercise.\\r\\n\\r\\nTo make sure you\'re on the right track with achieving your goals, make sure you develop consistent habits that help your body achieve its goals, such as stretching before and after exercise, eating balanced meals and allowing yourself to rest when you need them.\\r\\n\\r\\nThe importance of choosing the right exercise to achieve results\\r\\nIt is important to remember the importance of proper form when conducting any exercise, especially when it comes to strength training.\\r\\nEnsuring a proper form of exercise makes your body move in the right direction towards the results you want, and will prevent unnecessary injury along the way as well.\\r\\nMaintaining a correct exercise and light weights may mean that you cannot get results as fast as you want, or you may not be able to do as many repetitions in a single exercise.\\r\\nAchieving your goals and maintaining your safety are more important than speed of achievement.\\r\\nThe start of the results\\r\\nYou will see results in less than a month, especially if you have not been trained in such a way (strength training) for a long time.\\r\\nIn terms of strength training, you can see a 10% increase in weight lifting each month during the first four to six months, at which point adjustments will slow down and you\'ll have to change your workouts to stay safe.\\r\\nFor muscle enlargement, the observed results may take a little longer - 4 to 8 weeks of targeted training - with significantly measurable results within four to six months.\\r\\nBeginners can notice up to 2 and a half pounds of muscle per month, but this will gradually increase as exercise experience increases, although weight measurement may not show this, because the scale will not show any fat loss you can get.\\r\\nChanges in heart efficiency occur very quickly, especially in the beginning.\\r\\nHow to speed up the occurrence of results in a safe manner?\\r\\nFollow these five steps for fast, safe and effective fitness results:\\r\\n\\r\\nIncrease your exercise intensity and protein intake\\r\\nResearch published in the American Journal of Clinical Nutrition shows that combining high-intensity exercise and increased protein consumption allows people to lose more fat and build more muscle while lowering calories.You should reduce more calorie compromise, and eat more protein to reduce muscle mass losses During weight loss.\\r\\n\\r\\nEat to nourish your body\\r\\nCalorie needs vary greatly among people\\r\\nLack of calories (burning more calories than you consume) leads to weight loss\\r\\nA significant shortage of calories may lead to fat retention\\r\\nExcess calories (consuming more calories than you burn) are ideal for building muscle\\r\\nSo instead of getting stuck and thinking big about calculating calories, focus on food as fuel\\r\\nListen to your hunger signals and fill them with fully or less processed foods, as this will help you get more fiber, prevent excessive insulin secretion and fat storage, and help you reach your fat loss goals.\\r\\n\\r\\nPrioritize and prefer strength training over traditional stable cardio exercises\\r\\nStrength training can increase calorie burning, even while not exercising for up to 72 hours after leaving the gym, according to research published in the European Journal of Applied Physiology.In addition, it builds muscle metabolism and increases strength.\\r\\n\\r\\nSpend time recovering\\r\\nGive yourself at least one full day of rest per week and don\'t work out intensively for the same muscle group twice in a three-day period.\\r\\nMix your workouts and intensity to allow your body to recover.\\r\\nIt is normal to feel some pain after 24 to 48 hours of exercise, especially at the beginning, but you should not feel debilitated or unable to walk.\\r\\nSwitch exercise after six to 12 weeks\\r\\nTo keep your body constantly adjusting, it is important to change your workouts every six to 12 weeks\\r\\nThis can mean changing the frequency of your workout, trying out a new workout, or a weekly swim experience and changing your workout pattern\\r\\nOtherwise, your body can adapt well to an optional exercise that no longer sees benefit to the muscles\"}', '2019-08-25 06:47:39', '2019-08-28 03:51:26', '0'),
(75, '1566919386.jpg', '{\"ar\":\"أهمية الرياضة أثناء اتباع الحميات الغذائية\",\"en\":\"The importance of sports during dieting\"}', '{\"ar\":\"أهمية الرياضة أثناء اتباع الحميات الغذائية\\r\\nفوائد ممارسة الرياضة أثناء اتباع نظام غذائي\\r\\nالرياضة تهدئ الأعصاب وتخفف من الضغوط النفسية والاكتئاب\\r\\nأفضل أنواع الرياضة\\r\\nما هو أفضل وقت لممارسة الرياضة مع الحمية الغذائية؟\\r\\nتعامل الجسم مع الرياضة والطعام\\r\\n \\r\\n\\r\\nفوائد ممارسة الرياضة أثناء اتباع نظام غذائي\\r\\nتكسب الرياضة الشخص إحساسًا بالثقة مما يساعد على الالتزام بالحميات الغذائية\\r\\nتحسن الرياضة من كفاءة الدورة الدموية\\r\\nتزيد الرياضة من عملية التمثيل الغذائي في الجسم ومن عملية حرق السعرات الحرارية الموجودة في الأطعمة مما يساعد على  تخفيف الوزن\\r\\nتساعد الرياضة على شد الجسم لمنع ظهور التجاعيد والترهلات\\r\\nتساعد الرياضة على تخفيف آلام الظهر لأنها تقوي العضلات البطنية والظهرية\\r\\nالرياضة مفيدة لمرضى القلب والضغط والسكري\\r\\nتساعد الرياضة على تقوية مناعة الجسم وتساعد على النوم أيضًا\\r\\nالرياضة تهدئ الأعصاب وتخفف من الضغوط النفسية والاكتئاب\\r\\nتحفز الرياضة إفراز مادة كيميائية تدعى (بيتا اندروفين)\\r\\nهذه المادة تساهم قي رفع الروح المعنوية لدى الشخص خاصة بعد ممارسة الرياضة مدة 20 دقيقة\\r\\nكلما زاد وقت الرياضة زاد تأثير هذه المادة في رفع الروح المعنوية\\r\\nيجب مراعاة البدء بالتمارين الرياضية الخفيفة لفــترة 20-30 دقيقة وحوالي 3 مرات في الأسبوع مع محاولة زيادة وقت التمارين بنسبة 10% كل أسبوع\\r\\nمن المهم التوقف عن أي نوع تمارين عند الشعور بالتعب أو بسرعة فائقة في نبضات القلب\\r\\nأفضل أنواع الرياضة\\r\\nالمشي السريع\\r\\nالجري\\r\\nتمارين الإيروبيك\\r\\nالسباحة\\r\\nما هو أفضل وقت لممارسة الرياضة مع الحمية الغذائية؟\\r\\nإن أفضل وقت لممارسة الرياضة يكون في الصباح الباكر قبل الإفطار\\r\\nالرياضة الصباحية تساهم في حرق نسبة جيدة من الدهون وذلك لأن مستوى الجلايكوجين وهو الوقود المحرك للعضلات يكون منخفض بعد ليلة طويلة من النوم\\r\\nالرياضة الصباحية تعتمد في طاقتها على السعرات الحرارية المأخوذة من مخزون الدهون في الجسم\\r\\nتعامل الجسم مع الرياضة والطعام\\r\\nيفضل أن لا تقل المدة الزمنية بين تناول الطعام وبين الرياضة عن ساعتين كحد أدنى لتجنب إرهاق الجسم وعدم توزيع الدم بشكل جيد في الجسم\\r\\nعند ممارسة الرياضة يبدأ القلب بضخ الدم إلى العضلات للقيام بالمجهود العضلي\\r\\nعند تناول الطعام يبدأ القلب بضخ الدم إلى المعدة للقيام بعمليات الهضم\\r\\nبالتالي لا يستطيع الجسم القيام بمجهودين معاً مما يؤدي إلى عدم توزيع الدم بشكل جيد في الجسم لأداء الرياضة وهضم الطعام في نفس الوقت\\r\\nتصبح هناك دوخة وإرهاق أثناء القيام بالرياضة\\r\\nقد يحدث تقيؤ نتيجة الضغط على عضلات البطن أثناء الرياضة كون المعدة يوجد بها طعام لم يتم هضمه وإمتصاصه بعد\\r\\nعند ممارسة الرياضة تتحول الدهون إلى ثاني أكسيد الكربون وماء\\r\\nالعضو الذي يلعب الدور الأهم في حرق الدهون هو الرئتين\\r\\nحيث يغادر الماء الذي يتكون نتيجة احتراق الدهون الجسم عن طريق البول والعرق والتنفس والبراز وأشكال أخرى من سوائل الجسم\",\"en\":\"The importance of sports during dieting\\r\\nThe benefits of exercise during dieting\\r\\nSports calm nerves and relieve stress and depression\\r\\nThe best types of sport\\r\\nWhat is the best time to exercise with diet?\\r\\nThe body deals with sports and food\\r\\n \\r\\n\\r\\nThe benefits of exercise during dieting\\r\\nSports gain a person a sense of confidence, which helps to adhere to diets\\r\\nSports improve circulation efficiency\\r\\nExercise increases the body\'s metabolism and calories burned in foods, helping to lose weight\\r\\nSport helps tighten the body to prevent the appearance of wrinkles and slouching\\r\\nExercise helps relieve back pain because it strengthens the abdominal and dorsal muscles\\r\\nSport is useful for patients with heart, pressure and diabetes\\r\\nExercise helps strengthen your body\'s immunity and helps you sleep\\r\\nSports calm nerves and relieve stress and depression\\r\\nSport stimulates the release of a chemical called beta-endorphins\\r\\nThis article contributes to raising the morale of the person, especially after exercise for 20 minutes\\r\\nThe more sports time, the greater the effect of this substance in raising morale\\r\\nStart with light exercise for 20-30 minutes and about 3 times a week and try to increase your exercise time by 10% each week\\r\\nIt is important to stop any type of exercise when you feel tired or too fast in your heartbeat\\r\\nThe best types of sport\\r\\nThe fast walking\\r\\nRunning\\r\\nAerobic Exercises\\r\\nSwimming\\r\\nWhat is the best time to exercise with diet?\\r\\nThe best time to exercise is early in the morning before breakfast\\r\\nMorning sports contribute to the burning of a good proportion of fat because the level of glycogen, which is the fuel that drives the muscles is low after a long night of sleep\\r\\nMorning sports depends on its energy on the calories taken from the stock of body fat\\r\\nThe body deals with sports and food\\r\\nIt is recommended that the time between eating and sports should not be less than two hours minimum to avoid fatigue and not distribute blood well in the body.\\r\\nWhen you exercise, the heart begins to pump blood to the muscles for muscular effort\\r\\nWhen you eat, the heart begins to pump blood to the stomach to digest\\r\\nConsequently, the body cannot do two efforts together, which leads to the lack of good blood distribution in the body to perform sports and digest food at the same time\\r\\nThere is dizziness and fatigue while doing sports\\r\\nVomiting may occur as a result of pressure on the abdominal muscles during sports as the stomach has food that has not been digested and absorbed yet\\r\\nWhen you exercise, fat is converted to carbon dioxide and water\\r\\nThe organ that plays the most important role in fat burning is the lungs\\r\\nThe water that is formed by the combustion of fats leaves the body through urine, sweat, breathing, feces and other forms of body fluids.\"}', '2019-08-25 06:51:42', '2019-08-27 12:23:06', '1'),
(76, '1566918973.jpg', '{\"ar\":\"لماذا يصبح فقدان الوزن أكثر صعوبة بعد سن الأربعين؟\",\"en\":\"Why weight loss becomes more difficult after the age of forty?\"}', '{\"ar\":\"أسباب صعوبة فقدان الوزن بعد سن 40 سنة\\r\\nانخفاض عملية التمثيل الغذائي مع التقدم في السن.\\r\\nالتغيرات الهرمونية، خاصة لدى النساء اللواتي يمرن سن اليأس.\\r\\nتلعب العوامل المرتبطة بنمط الحياة دورًا أيضًا، فقد يكون الرجال والنساء الأكبر سنا أكثر توترا  أقل قدرة جسديا على ممارسة الرياضة بالطريقة التي اعتادوا عليها عندما كانوا أصغر سنا.\\r\\nبالرغم من ذلك، إذا أبقيت هذه الأسباب الأساسية في ذهنك فإن عملية فقدان الوزن ليست مستحيلة تمامًا.\\r\\n\\r\\nنصائح لفقدان الوزن بعد سن 40 سنة\\r\\nقد تكون الخطوة الأولى لفقدان الوزن هي تحديد ما إذا كان هناك شيء سهل الإصلاح.\\r\\nيمكن أن تؤثر التغيرات في نمط الأكل على وزنك،على سبيل المثال: إذا كنت معتادًا على تناول الطعام مع أشخاص آخرين فقد تتناول المزيد من الخضار، أما اذا كنت لوحدك فقد تتناول أطعمة صحية أقل.\\r\\nارتبط حدوث تغييرات في نمط النوم بزيادة الوزن، بالرغم من أنك قد لم تر أي مشاكل في الوزن بالرغم من سهرك لوقت متأخر عندما كنت في العشرينات من العمر، إلا أن قلة النوم بعد الأربعين سوف تسبب لك بعض المشاكل.\\r\\nما تأكله يهم كثيرا، أوصى الخبراء بتناول المزيد من البروتينات والنباتات وتقليل الدهون المشبعة.\\r\\nيوصى باستبدال تمارين الركض التي تؤثر سلبا على المفاصل بالقيام بتمارين المقاومة أو القوة للحفاظ على العضلات.\\r\\nإن الحصول على رفيق في تمارينك الرياضية قد يحفزك على الاستمرار و الالتزام بروتين التمرين الجديد، وقد يمنعك ذلك من الاكتئاب، كما يجنبك عادات الأكل السيئة\\r\\nقد يكون السبب الكبير وراء زيادة الوزن المرتبطة بالعمر هو أن مفاصلك لم تعد قادرة على تحمل قيامك ببعض الرياضات مثل ركوب الدراجات لفترة طويلة أو الهرولة بشكل يومي أو لعب كرة السلة أسبوعيا. ذلك في حد ذاته مخيب للآمال، لكن هناك خيارات أخرى مثل السباحة على وجه الخصوص.\\r\\n\\r\\nماذا يحدث للجسم بعد عمر 40 سنة؟\\r\\nعندما يصل الشخص عمر 40 يبدأ نقص النسيج العضلي الذي يحدث كجزء طبيعي من عملية الشيخوخة.\\r\\n40% من الكتلة العضلية سوف تختفي بين سن 40 و80، هذا يساهم في تباطؤ الأيض لأن العضلات تحرق سعرات حرارية أكثر من الدهون.\\r\\nهناك فائدة من رؤية أنك لست الوحيد الذي تتعامل مع هذه المشكلات في هذه المرحلة الجديدة من الحياة، ذلك قد يؤدي إلى تغيير توقعاتك وتقليل قلقك بشأن فقدان الوزن ويزيد من تقبلك لجسمك.\\r\\n\\r\\nزيادة بضعة أرطال من وزنك قد لا تكون سيئة طالما لم يسبب لك ذلك بمشكلة صحية، هذا جزء مما يحدث في دورة الحياة.\",\"en\":\"Causes of difficulty losing weight after the age of 40 years\\r\\nMetabolism decreases with age.\\r\\nHormonal changes, especially in women who are going through menopause.\\r\\nLifestyle factors also play a role, as older men and women may be more tense physically less able to exercise the way they used to be when they were younger.\\r\\nHowever, if you keep these underlying causes in mind, weight loss is not completely impossible.\\r\\n\\r\\nTips for weight loss after the age of 40 years\\r\\nThe first step to losing weight may be to determine if something is easy to fix.\\r\\nChanges in your eating style can affect your weight.For example, if you are used to eating with other people, you may eat more vegetables, but if you are alone, you may eat less healthy foods.\\r\\nChanges in your sleep pattern have been associated with weight gain, although you may not have experienced any weight problems.While you\'re late when you\'re in your 20s, lack of sleep after forty will cause you some problems.\\r\\nWhat you eat matters a lot, experts recommend eating more proteins and plants and reducing saturated fat.\\r\\nIt is recommended to replace jogging exercises that adversely affect the joints by doing resistance or strength exercises to maintain muscle.\\r\\nHaving a companion in your workouts can motivate you to stick to your new exercise routine, prevent you from being depressed, and avoid bad eating habits.\\r\\nThe big reason for age-related weight gain may be that your joints are no longer able to withstand sports such as long cycling, jogging on a daily basis or playing basketball weekly. That in itself is disappointing, but there are other options like swimming in particular.\\r\\n\\r\\nWhat happens to the body after the age of 40 years?\\r\\nWhen a person reaches the age of 40 begins muscle deficiency that occurs as a natural part of the aging process.\\r\\n40% of muscle mass will disappear between the ages of 40 and 80, this contributes to a slowing of metabolism because the muscles burn more calories than fat.\\r\\nThere\'s a point in seeing that you\'re not the only one dealing with these problems in this new phase of life.This can change your expectations, reduce your anxiety about losing weight and increase your acceptance of your body.\\r\\n\\r\\nIncreasing a few pounds of weight may not be bad as long as it does not cause you a health problem, this is part of what happens in the life cycle.\"}', '2019-08-25 06:56:37', '2019-08-27 12:16:13', '0'),
(77, '1566919323.png', '{\"ar\":\"تمارين حرق دهون الجسم\",\"en\":\"Exercises to burn body fat\"}', '{\"ar\":\"تمارين حرق دهون الجسم\\r\\nما هي أفضل تمارين حرق دهون الجسم؟\\r\\nتمرين القفز لحرق دهون الجسم\\r\\nتمرين الجري في المكان لحرق دهون الجسم\\r\\nالقفز بالحبل لحرق دهون الجسم\\r\\nتمرين العُقلة لحرق دهون الجسم\\r\\nالمشي لحرق دهون الجسم\\r\\nممارسة التمارين هو مجرد قطعة واحدة من اللغز\\r\\nما الوقت اللازم لممارسة تمارين حرق دهون الجسم؟\\r\\nنصائح لحرق دهون الجسم\\r\\n \\r\\n\\r\\nما هي أفضل تمارين حرق دهون الجسم؟\\r\\nتمرين القفز لحرق دهون الجسم\\r\\nالوقوف منفرج القدمين قليلاً والذراعين مفرودين إلى الجانبين مع شد عضلات البطن والحفاظ على الظهر منفرداً.\\r\\nالقفز للأعلى مع رفع الذراعين المفرودين لأعلى الرأس وملامسة الكفين\\r\\nثم ضرب الكفين بالفذخين عند العودة للوضع الأصلي\\r\\nثم القفز مرة أخرى\\r\\nوهكذا مع تكرار التمرين أكثر من مرة\\r\\nتمرين الجري في المكان لحرق دهون الجسم\\r\\nالوقوف مع شد عضلات البطن والحفاظ على الظهر منفرداً.\\r\\nالقفز بالرجل اليسرى مع رفع الركبة اليمنى إلى مستوى الفخذ\\r\\nثم القفز بالرجل اليمنى مع رفع الركبة اليسرى إلى مستوى الفخذ، وهكذا بسرعة.\\r\\nالقفز بالحبل لحرق دهون الجسم\\r\\nيؤدي هذا التمرين إلى رفع عدد نبضات القلب بشكل سريع\\r\\nهذا يزيد من معدل حرق الدهون والتخلص من السعرات الحرارية في الجسم كله وخاصةً البطن\\r\\nتمرين العُقلة لحرق دهون الجسم\\r\\nباستخدام مسند حديد أو عقلة مُعلقة على الجدار تمسك بيديك بها على الجانبين ثم تبدأ برفع جسمك لأعلى حتى تكون الرقبة بمستوى العُقلة\\r\\nثم تنزل بجسمك لأسفل حتى تكون ذراعيك مفرودتان\\r\\nثم تعيد الرفع مرة أخرى\\r\\nوهكذا مع تكرار التمرين 10 أو 20 مرة حسب القدرة\\r\\nالمشي لحرق دهون الجسم\\r\\nالمشي لمدة نصف ساعة يومياً مع الزيادة في مدة المشي بالتدريج من التمارين المهمة والمفيدة جداً.\\r\\n\\r\\n \\r\\n\\r\\nممارسة التمارين هو مجرد قطعة واحدة من اللغز\\r\\nلِنَضع في اعتبارنا أن ممارسة التمارين الرياضية هو مجرد جزء واحد من البرنامج الناجح لإنقاص الوزن، يقول تشورش: \\\"تناول الطعام وممارسة الرياضة ليست قضايا منفصلة\\\". وأضاف \\\"إنهما على اتصال وثيق، فالكثير من الناس يعتقدون أن ممارسة الكثير من التمارين يترك لك الحرية لتناول ما تشاء\\\".\\r\\n\\r\\nيقول كويست: \\\"من الأسهل بكثير عدم تناول السعرات الحرارية بدلاً من محاولة حرقها\\\".\\r\\n\\r\\nونضع في اعتبارنا أيضاً أن تعريف فقدان الوزن الناجح هو الحفاظ على خسارة الوزن، كما يقول تشورش: «ليس من الصعب أن نفقد الوزن». \\\"أي شخص يمكن أن يفقد الوزن، الصعب حقاً هو الحفاظ على تلك الخسارة، والذي يكون من خلال كلٍ من النظام الغذائي وممارسة الرياضة معاً\\\".\\r\\n\\r\\n \\r\\n\\r\\nما الوقت اللازم لممارسة تمارين حرق دهون الجسم؟\\r\\nيقول تشورش: \\\"أنت لم تكسب وزن 20 رطلاً في الأشهر الستة الأخيرة، فلن تفقده أيضاً في الأشهر الستة القادمة\\\". \\\"الناس لا يريدون أن يسمعوا عن الصبر\\\"، كما يقول. \\\"إنهم يريدون إشباعاً فورياً، ولكن الواقع الصعب هو أنه إذا كنت ترغب في إنقاص وزنك والحفاظ على الخسارة، فهذا عمل شاق، ولا يفقد أي شخص الوزن ويحافظ عليه دون أن يحاول\\\".\\r\\n\\r\\n \\r\\n\\r\\nنصائح لحرق دهون الجسم\\r\\nلِيكن لديك صديق أو شريك يُشَجعك، فمن الأسهل بكثير أن تقول لنفسك لا من أن تقول لا لشخص آخر.\\r\\nجدولة التدريبات الخاصة بك مع الحفاظ على الأوقات المحددة للتدريبات الخاصة بك. فتحديد موعد ممارسة الرياضة في وقت مبكر لا يجعل لديك عذراً.\\r\\nوزن نفسك يومياً. وهذا من أفضل الأدوات لمعرفة ما إذا كنت تفقد الوزن فعلاً.\\r\\nلا تبدأ بالكثير من التمارين الشاقة، فأنت لست معتاد عليها. قال كويست إن رفع الأوزان الثقيلة جداً أو البدء بها في ستة أيام في الأسبوع الأول من التمارين الرياضية خطأ. ويقول: \\\"الناس في نهاية المطاف يضرون أنفسهم في الأسبوع الأول ثم يتخلون عن ذلك\\\".\\r\\nسجل خطواتك. فسوف يلهمك في نهاية الأسبوع عندما تنظر إلى الوراء وترى ما قد أنجزت.\",\"en\":\"Exercises to burn body fat\\r\\nWhat are the best exercises to burn body fat?\\r\\nJumping exercise to burn body fat\\r\\nRunning exercise in place to burn body fat\\r\\nJump rope to burn body fat\\r\\nMind exercise to burn body fat\\r\\nWalking to burn body fat\\r\\nPractice exercises is just one piece of the puzzle\\r\\nWhat is the time required to exercise fat burning exercises?\\r\\nTips for Burning Body Fat\\r\\n \\r\\n\\r\\nWhat are the best exercises to burn body fat?\\r\\nJumping exercise to burn body fat\\r\\nStand a little obtuse feet and arms straight to the sides while tightening the abdominal muscles and keeping the back alone.\\r\\nJump up with the arms straight up and touch your hands\\r\\nThen hit the hands with thighs when returning to the original position\\r\\nThen jump again\\r\\nThus repeat the exercise more than once\\r\\nRunning exercise in place to burn body fat\\r\\nStand with tightening your abdominal muscles and keeping your back alone.\\r\\nJump with the left leg with the right knee raised to the thigh level\\r\\nThen jump with the right leg with the left knee raised to the thigh level, and so quickly.\\r\\nJump rope to burn body fat\\r\\nThis exercise quickly increases the number of heartbeats\\r\\nThis increases the rate of fat burning and the elimination of calories in the whole body, especially the abdomen\\r\\nMind exercise to burn body fat\\r\\nUsing an iron arm or a wall bar that holds your hands on the sides, then you start lifting your body up until the neck is level\\r\\nThen lower your body down so that your arms are straight\\r\\nThen re-raise again\\r\\nThus repeat the exercise 10 or 20 times depending on ability\\r\\nWalking to burn body fat\\r\\nWalking for half an hour a day and gradually increasing the duration of the exercise is very important and useful.\\r\\n\\r\\n \\r\\n\\r\\nPractice exercises is just one piece of the puzzle\\r\\nKeep in mind that exercise is just one part of a successful weight loss program. “Eating and exercising are not separate issues,” says Church. \\\"They are in close contact. Many people think that doing a lot of exercise leaves you free to eat whatever you want.\\\"\\r\\n\\r\\n\\\"It\'s much easier not to eat calories than to try to burn them,\\\" says Quest.\\r\\n\\r\\nKeep in mind, too, that the definition of successful weight loss is to maintain weight loss, says Church. \\\"It\'s not hard to lose weight.\\\" \\\"Anyone who can lose weight is really hard to sustain that loss, which is through both diet and exercise.\\\"\\r\\n\\r\\n \\r\\n\\r\\nWhat is the time required to exercise fat burning exercises?\\r\\n\\\"You haven\'t gained 20 pounds in the last six months, you won\'t lose it either in the next six months,\\\" says Church. \\\"People don\'t want to hear about patience,\\\" he says. \\\"They want instant gratification, but the hard reality is that if you want to lose weight and keep losing, it\'s hard work, and no one loses weight and keeps it without trying.\\\"\\r\\n\\r\\n \\r\\n\\r\\nTips for Burning Body Fat\\r\\nTo have a friend or partner encourage you, it\'s much easier to say to yourself than to say no to someone else.\\r\\nSchedule your workouts while maintaining the set times for your workouts. Scheduling exercise early does not make you have an excuse.\\r\\nWeigh yourself daily. This is one of the best tools to see if you are really losing weight.\\r\\nDon\'t start with a lot of strenuous exercise, you are not used to it. Quest said lifting or starting heavy weights in six days in the first week of exercise was wrong. \\\"People end up hurting themselves in the first week and then giving up,\\\" he says.\\r\\nRecord your steps. You\'ll inspire you at the end of the week when you look back and see what has been accomplished.\"}', '2019-08-25 07:01:27', '2019-08-27 12:22:03', '1');
INSERT INTO `blogs` (`id`, `image`, `title`, `text`, `created_at`, `updated_at`, `type`) VALUES
(78, '1566919224.jpg', '{\"ar\":\"تمارين حرق دهون البطن\",\"en\":\"Exercises to burn belly fat\"}', '{\"ar\":\"ما هي أفضل تمارين حرق دهون البطن؟\\r\\nإن أفضل تمرين لإنقاص الوزن هو: \\\"التمرين الذي ستفعله\\\" كما يقول تيموثي تشورش، الأستاذ في مركز بينينجتون للبحوث الطبية الحيوية في باتون روج.\\r\\nويقول المعالج الطبيعي بن كويست: \\\"الأمران اللذان يمنعان الناس من فقدان الوزن من خلال التمارين الرياضية هما إما الملل أو الإصابة\\\".\\r\\nلذلك يجب عليك أن تبدأ بالتمرين الذي تحبه والذي تستطيع أن تفعله وتستمر عليه فعلاً، مثل المشي أو ركوب الدراجة أو أي رياضة أخرى تستمتع بها.\\r\\nتمرين القفز لحرق دهون البطن\\r\\nالوقوف بوضع الاستعداد للجري.\\r\\nالقفز للأعلى، وأثناء الوجود في الأعلى يجب تقديم إحدى الأرجل على الأخرى، مع الحرص على عدم إبعاد الأرجل عن بعضهما البعض أكثر من اللازم.\\r\\nثني الركبة قليلاً أثناء الهبوط للوقاية من حدوث أي إصابات.\\r\\nتكرار القفز مرةً أخرى، ولكن مع عكس الأرجل.\\r\\nهذا التمرين يُعتبر من التمارين المنشطة للقلب والعضلات، وبعض الخبراء يعتقدون بأنّه التمرين الأفضل لخسارة الوزن، وتحديداً من منطقتي البطن والأرداف.\\r\\n\\r\\nتمرين البطن لحرق دهون البطن\\r\\n\\r\\nترقد على ظهرك على الأرض وتشبك بين أصابع يديك مع وضعهما تحت الرأس.\\r\\nتحاول القيام من وضع الرقود لوضع الجلوس مع الانحناء للأمام قليلاً.\\r\\nحاول أن تستخدك قوتك للقيام من عضلات بطنك فقط وليس من عضلات الذراعين.\\r\\nتعود للوضع الأصلي مرة أخرى.\\r\\nكرر هذا التمرين وستشعر بحرقان أو رعشة في منطقة عضلات البطن، وهذا يعني أنك تقوم بالتمرين بصورة صحيحة، فلا تقلق.\\r\\nتمرين الدراجة لحرق دهون البطن\\r\\nارقد على الأرض مع رفع القدمين ثم البدء بتحريكهما حركةً دائرية للأمام وللخلف حركةً تشبه حركة البدال التي تكون على الدراجة.\\r\\nهذا التمرين مهم لعضلات البطن والفخذين.\\r\\nتمرين لمس القدمين لحرق دهون البطن\\r\\nارقد على الأرض مع رفع القدمين عمودياً على الحائط\\r\\nثم البدء بلمس القدم اليسرى باليد اليمنى\\r\\nثم العودة للوضع الأصلي\\r\\nثم تكرار الحركة باليد اليسرى للمس القدم اليمنى\\r\\nوهكذا مع تكرار التمرين 10 أو 20 مرة حسب القدرة\\r\\nهذا التمرين مهم لحرق دهون البطن وتقوية عضلاته.\\r\\n\\r\\nتمرين الالتفاف حول الوسط لحرق دهون البطن\\r\\nالوقوف في وضع مستقيم، مع ضم اليدين إلى بعضهما ووضعهما على الصدر.\\r\\nالالتفاف حول الوسط إلى جهة اليمين مرة، والعودة إلى الوضع الأصلي، ثم الالتفاف إلى جهة اليسار.\\r\\nهكذا، تكرار التمرين أكثر من مرة.\\r\\n \\r\\n\\r\\nنصائح للاستمرار في التمارين الرياضية\\r\\nاختر لنفسك صديقاً أو شريكاً يُشَجعك على الانتظام في ممارسة التمارين فإنه من الأسهل بكثير أن تقول لنفسك \\\"لا\\\" من أن تقول \\\"لا\\\" لشخص آخر.\\r\\nاجعل هناك موعداً خاصاً للتمارين مع الحفاظ على الأوقات المحددة للتدريبات الخاصة بك، ويُفضل تحديد موعد ممارسة الرياضة في وقت مبكر ليجعلك تداوم عليه بشكل أكبر ولا تكون لك حجة من خشية ضياع الوقت.\\r\\nقم بوزن نفسك يومياً، وهذا من أفضل الأدوات لمعرفة ما إذا كنت تفقد الوزن فعلاً ويعطيك دافعاً للاستمرار في التمارين والمداومة عليها.\\r\\nلا تبدأ بالكثير من التمارين الشاقة، فأنت لست معتاداً عليها. يقول الدكتور كويست: إن رفع الأوزان الثقيلة جداً أو البدء بها في ستة أيام من الأسبوع الأول من التمارين الرياضية خطأ، فالناس في نهاية المطاف يضرون أنفسهم في الأسبوع الأول ثم يتركون التمارين بالجملة لأنه لن يستطيع المداومة على هذه التمارين الشاقة، ولكن ابدأ دائماً بالقليل مع التدرج.\\r\\nسجل خطواتك اليومية، وفي نهاية الأسبوع عندما تنظر إلى ما أنجزت يعطيك هذا دافعاً قوياً للاستمرار.\",\"en\":\"What are the best exercises to burn belly fat?\\r\\nThe best weight-loss exercise is: “The workout you will do,” says Timothy Church, a professor at the Bennington Biomedical Research Center in Baton Rouge.\\r\\n\\\"The two things that prevent people from losing weight through exercise are either boredom or injury,\\\" says BenQuest, a physiotherapist.\\r\\nSo you should start with the exercise that you love and that you can actually do and continue on, such as walking, cycling or any other sport you enjoy.\\r\\nJumping exercise to burn belly fat\\r\\nStand-by running mode.\\r\\nJump up, and while you are up, one leg should be provided over the other, making sure not to keep the legs apart.\\r\\nSlightly bend the knee during landing to prevent injury.\\r\\nRepeat the jump again, but with the legs reversed.\\r\\nThis exercise is considered a stimulant exercise for the heart and muscles, and some experts believe it is the best exercise for weight loss, especially from the areas of the abdomen and buttocks.\\r\\n\\r\\nAbdominal exercise to burn belly fat\\r\\n\\r\\nLying on your back on the ground, clasping between your fingers with them under the head.\\r\\nTrying to do a lying position to a sitting position with a little bending forward.\\r\\nTry to use your strength to do just your abdominal muscles, not your arm muscles.\\r\\nRevert to original mode again.\\r\\nRepeat this exercise and you will feel a burning sensation or tremor in the area of ​​the abdominal muscles.This means that you are exercising properly, do not worry.\\r\\nExercise bike to burn belly fat\\r\\nLie on the floor with your feet raised and then start moving them in a circular motion forward and backward like a pedal movement on the bike.\\r\\nThis exercise is important for the abdominal muscles and thighs.\\r\\nExercise touching the feet to burn belly fat\\r\\nLie on the floor with your feet upright against the wall\\r\\nThen start touching the left foot with the right hand\\r\\nThen return to the original position\\r\\nThen repeat the movement with the left hand to touch the right foot\\r\\nThus repeat the exercise 10 or 20 times depending on ability\\r\\nThis exercise is important to burn belly fat and strengthen his muscles.\\r\\n\\r\\nWrap around the middle to burn belly fat\\r\\nStand upright, holding hands together and placing them on the chest.\\r\\nTurn around the center to the right once, return to the original position, then turn to the left.\\r\\nSo, repeat the exercise more than once.\\r\\n \\r\\n\\r\\nTips for continuing to exercise\\r\\nChoose a friend or partner who encourages you to get regular exercise.It is much easier to tell yourself \\\"no\\\" than to say \\\"no\\\" to someone else.\\r\\nMake a special schedule for your workout while keeping your workout times up.\\r\\nWeigh yourself every day.This is one of the best tools to see if you are really losing weight and giving you a motivation to keep up and keep exercising.\\r\\nDon\'t start with a lot of strenuous exercise, you are not used to it. `` Lifting too heavy weights or starting them in six days of the first week of exercise is a mistake, \'\' says Dr. Quest. pheasant.\\r\\nRecord your daily steps, and at the end of the week when you look at what you have accomplished this gives you a strong motivation to continue.\"}', '2019-08-25 07:05:01', '2019-08-27 12:20:24', '1'),
(79, '1566918840.jpg', '{\"ar\":\"ملتي فيتامين لكمال الاجسام\",\"en\":\"Multivitamin for bodybuilding\"}', '{\"ar\":\"ملتي فيتامين لكمال الاجسام\\r\\nما هو أفضل ملتي فيتامين لكمال الأجسام؟\\r\\nالمكملات الغذائية وملتي فيتامين\\r\\nأهم العناصر الضرورية في ملتي فيتامين\\r\\nما هي المكملات الغذائية الضرورية لكمال الأجسام؟\\r\\nالكرياتين لبناء العضلات\\r\\nألانين بيتا لبناء العضلات والجسم\\r\\nبروتين الحليب لبناء العضلات\\r\\nالحمض الأميني متعددة السلاسل (BCAA) لبناء العضلات\\r\\nالكافيين وبناء العضلات\\r\\n \\r\\n\\r\\nما هو أفضل ملتي فيتامين لكمال الأجسام؟\\r\\nالمكملات الغذائية وملتي فيتامين\\r\\nيساعد تناول الملتي فيتامين في الحصول على العناصر الغذائيّة التي لا يحصل عليها الجسم، أو لا يحصل عليها بكميات مناسبة، وهناك بعض العناصر التي يمكن الحصول على النسبة المناسبة منها وذلك بتناول كميات كبيرة من الطعام، ولذلك يعتبر تناول الفيتامينات والمكملات أمراً ضروريّاً.\\r\\n\\r\\nوعلى الرغم من تواجد عدد كبير من المكملات الغذائية للرجال، ينبغي للمرء أن يبحث عن الجرعة المناسبة والتركيز الأفضل للمواد الغذائية التي تتناسب وحاجة جسمه. وتأتي الفيتامينات في أشكال متعددة؛ مثل: الأقراص، والحبوب، وعلى شكل مسحوق.\\r\\n\\r\\nللمزيد: النساء بطبيعة الحال لائقات بدنيًا أكثر من الرجال\\r\\n\\r\\nأهم العناصر الضرورية في ملتي فيتامين\\r\\nعند الرغبة في شراء ملتي فيتامين لكمال الأجسام يجب الانتباه إلى وجود العناصر الغذائية الأساسية، وهي:\\r\\n\\r\\nالثيامين أو فيتامين B1.\\r\\nالريبوفلافين أو B2.\\r\\nالنياسين أو B3.\\r\\nفيتامين B6.\\r\\nفيتامين B12.\\r\\nفيتامين د.\\r\\nفيتامين هـ.\\r\\nحمض الفوليك.\\r\\nفيتامين E، وD، وC.\\r\\nللمزيد: التمارين الرياضية المكثفة قد تعزز الذاكرة\\r\\n \\r\\n\\r\\nما هي المكملات الغذائية الضرورية لكمال الأجسام؟\\r\\nعند الالتزام بالتدريب والتغذية المناسبة، هناك مكملات غذائية يمكن أن تساعد بالتأكيد في الحصول على اللياقة البدنية المطلوبة لكمال الأجسام جنباً إلى جنب مع ملتي فيتامين، وهي:\\r\\n\\r\\nالكرياتين لبناء العضلات\\r\\nمادة تنتج طبيعياً في الخلايا العضلية، وهي واحدة من المصادر الأولية للطاقة الخلوية ومن فوائد تناولها:\\r\\n\\r\\nزيادة كتلة العضلات.\\r\\nتحسين قوة العضلات.\\r\\nتقليل الألم الناشئ في العضلات.\\r\\nتحسين تدفق الدم أثناء التمرين.\\r\\nتحسين الأداء أثناء التدريب.\\r\\nالجرعة الموصى بها: تكون عبارة عن 5 غرامات، وتؤخذ على دفعتين: نصفها قبل التمرين، والنصف الآخر بعد التمرين.\\r\\n\\r\\nللمزيد: كيف اعرف نسبة الدهون في جسمي\\r\\n\\r\\nألانين بيتا لبناء العضلات\\r\\nحمض أميني يتم الحصول عليه عن طريق الأطعمة الغنية بالبروتين، ومن فوائده:\\r\\n\\r\\nتأخر التعب أثناء التمرين المكثف.\\r\\nزيادة نسبة التدريب الكلي.\\r\\nتحسين إنتاج الطاقة.\\r\\nزيادة بناء العضلات.\\r\\nزيادة القدرة على التحمل أثناء التمرين.\\r\\nتحسين فعالية الكرياتين.\\r\\nالجرعة الموصى بها: وتكون من 4 إلى 6 غرامات في اليوم، تؤخذ بانتظام.\\r\\n\\r\\nللمزيد: هل رجيم حرق الدهون بالدهون فعال؟\\r\\n\\r\\nبروتين الحليب لبناء العضلات\\r\\nمستخلص من الحليب، إذ أنّه يمكن أن يمد الجسم بكمية عالية من البروتين والأحماض الأمينية، ومن فوائده:\\r\\n\\r\\nسريع الهضم وسهل الامتصاص.\\r\\nزيادة كتلة العضلات.\\r\\nتحسين السيطرة على الشهية، وزيادة الشعور بالشبع.\\r\\nالجرعة الموصى بها: 20 إلى 30 غراماً، ويؤخذ على جرعة واحدة.\\r\\n\\r\\nللمزيد: هرمون التستوستيرون لكمال الاجسام\\r\\n\\r\\nالحمض الأميني متعددة السلاسل (BCAA) لبناء العضلات\\r\\nهناك ثلاث أحماض أمينية من 20 حمضاً أمينيّاً يكونون البروتين، وهي التي تحفز تكون البروتين وتساعد في عملية التمثيل الغذائي للبروتين، ومن فوائده:\\r\\n\\r\\nزيادة مستويات تكون البروتين العضلي.\\r\\nخفض مستويات تلف العضلات أثناء ممارسة التمرين.\\r\\nتحسين قدرة التحمل أثناء عملية التمرين.\\r\\nالجرعة الموصى بها: 3 إلى 6 غرامات قبل ممارسة التمرين وأثنائه.\\r\\n\\r\\nللمزيد: مخاطر المكملات الغذائية على صحة الرياضيين\",\"en\":\"Multivitamin for bodybuilding\\r\\nWhat is the best multivitamin for bodybuilding?\\r\\nSupplements and multivitamin\\r\\nThe most important elements in the multivitamin\\r\\nWhat supplements are necessary for bodybuilding?\\r\\nCreatine to build muscle\\r\\nBeta alanine for muscle building\\r\\nMilk protein to build muscle\\r\\nMulti-chain amino acid (BCAA) for muscle building\\r\\nCaffeine and muscle building\\r\\n \\r\\n\\r\\nWhat is the best multivitamin for bodybuilding?\\r\\nSupplements and multivitamin\\r\\nMultivitamins can help you get the nutrients that your body doesn\'t get, or don\'t get in the right amounts.\\r\\n\\r\\nAlthough there are a large number of supplements for men, one should look for the right dosage and the best concentration of nutrients that suit the needs of his body. Vitamins come in multiple forms, such as tablets, pills, and in powder form.\\r\\n\\r\\nMore: Women are naturally more physically fit than men\\r\\n\\r\\nThe most important elements in the multivitamin\\r\\nWhen you want to buy multivitamin for bodybuilding you should pay attention to the presence of essential nutrients, namely:\\r\\n\\r\\nThiamine or vitamin B1.\\r\\nRiboflavin or B2.\\r\\nNiacin or B3.\\r\\nVitamin B6.\\r\\nVitamin B12.\\r\\nVitamin D.\\r\\nVitamin E\\r\\nFolic acid.\\r\\nVitamin E, D, and C.\\r\\nMore: Intense exercise may boost memory\\r\\n \\r\\n\\r\\nWhat supplements are necessary for bodybuilding?\\r\\nWhen you adhere to proper training and nutrition, there are supplements that can certainly help in obtaining the required fitness for bodybuilding along with multivitamins, namely:\\r\\n\\r\\nCreatine to build muscle\\r\\nA substance produced naturally in muscle cells, which is one of the primary sources of cellular energy and the benefits of taking it:\\r\\n\\r\\nIncreased muscle mass.\\r\\nImprove muscle strength.\\r\\nReducing muscle pain.\\r\\nImprove blood flow during exercise.\\r\\nImprove performance during training.\\r\\nRecommended dosage: be 5 grams, taken in two batches: half before exercise, and the other half after exercise.\\r\\n\\r\\nLearn more: How do I know the percentage of fat in my body\\r\\n\\r\\nBeta alanine for muscle building\\r\\nAmino acid is obtained by foods rich in protein, and its benefits include:\\r\\n\\r\\nDelayed fatigue during intense exercise.\\r\\nIncrease the percentage of total training.\\r\\nImprove energy production.\\r\\nIncreased muscle building.\\r\\nIncreased stamina during exercise.\\r\\nImprove creatine efficacy.\\r\\nRecommended dosage: be 4 to 6 grams a day, taken regularly.\\r\\n\\r\\nRead More: Is Fat Burning Dieting Effective?\\r\\n\\r\\nMilk protein to build muscle\\r\\nExtracted from milk, as it can provide the body with a high amount of protein and amino acids, and its benefits:\\r\\n\\r\\nFast digestion and easy absorption.\\r\\nIncreased muscle mass.\\r\\nImprove appetite control, increase satiety.\\r\\nRecommended dosage: 20 to 30 grams, taken in one dose.\\r\\n\\r\\nMore: Testosterone for bodybuilding\\r\\n\\r\\nMulti-chain amino acid (BCAA) for muscle building\\r\\nThree amino acids from 20 amino acids make up protein, which stimulate protein formation and aid in protein metabolism.\\r\\n\\r\\nIncreased levels of muscle protein formation.\\r\\nReduce muscle damage levels during exercise.\\r\\nImprove endurance during exercise.\\r\\nRecommended dosage: 3 to 6 grams before and during exercise.\\r\\n\\r\\nMore: Dietary Supplements Risks to Athletes\' Health\"}', '2019-08-25 07:09:37', '2019-08-27 12:14:01', '0'),
(90, '1566918569.webp', '{\"ar\":\"كل ما يجب أن تعرفه عن سكر الدم\",\"en\":\"All you need to know about blood sugar\"}', '{\"ar\":\"إنتاج الجلوكوز\\r\\nبعد تناول الأطعمة الغنية بالكربوهيدرات مثل: الخبز الأبيض، والأرز، والمعكرونة، والبطاطس، والفواكه، والعسل. تبدأ الأحماض الموجودة في المعدة بتحليل الكربوهيدرات والسكريات الى الجلوكوز (أو ما يعرف بسكر الدم)، في الأمعاء يتم امتصاص الجلوكوز حتى يصل الى مجرى الدم، عند ذلك يتم إفراز الأنسولين من البنكرياس بكميات تتناسب مع كمية الجلوكوز في الدم. حيث يعمل هرمون الأنسولين على إدخال جزيئات الجلوكوز إلى خلايا الجسم بكافة أنواعها التي تعمل بدورها على استهلاك الجلوكوز لإنتاج الطاقة كما في خلايا الدم الحمراء أو تقوم بتخزينه (كخلايا الكبد) وتوزيعه لاحقاً على العضلات والخلايا العصبية للاستخدام لاحقاً.\\r\\n\\r\\nللمزيد: تفكك الكربوهيدرات\\r\\n\\r\\nكيفية قياس السكر في الدم\\r\\nيمكن قياس مستوى السكر في الدم من خلال فحص عينة من الدم مخبرياً، أو عن طريق استخدام أجهزة فحص السكر الفورية التي تعمل على قياس نسبة السكر في الدم من خلال قطرة دم تسحب من أحد الأصابع، وتظهر نتيجة الفحص خلال ثواني معدودة (وهذه الطريقة متعارف على استخدامها يومياً لدى مرضى السكري).\\r\\n\\r\\nيوجد عدة طرق لفحص مستوى السكر في الدم:\\r\\n\\r\\nأخذ عينة الدم لقياس الدم إما بوقت عشوائي بغض النظر عن الأكل.\\r\\nقياس السكر الصيامي (بالانجليزية:Fasting blood glucose test)، يتم من خلال قياس مستوى السكر في الدم بعد صيام الشخص عن الأكل لمدة 8 ساعات، تعطي هذه الطريقة نتائج أكثر دقة وأسهل للتحلل والتشخيص عادةً.\\r\\nاختبار تحمل السكر، بعد صيام المريض عن الطعام لمدة 8 ساعات يتم إعطائه 75 غ من السكر مذاب ب250مل من الماء ويتم قياس سكر الدم خلال فترة 30 إلى 60 دقيقة وقد تصل الى 3 ساعات.\\r\\nقياس السكر التراكمي (بالإنجليزية: Hemoglobin A1C)، يتم من خلاله قياس متوسط ​​مستوى السكر في خلايا الدم على مدار الثلاثة أشهر الماضية، حيث أنه يقيس مدى ارتباط جزيئات الجلوكوز في الهيموغلوبين الموجود في خلايا الدم الحمراء, ويتم الاستفادة منه في تشخيص مرض السكري.\\r\\nتفسير القياسات المخبرية\\r\\nتساعد القياسات المخبرية لمستوى السكر (الجلوكوز) في الدم، بالإضافة الى الاعتماد على عدة عوامل اخرى من الجنس، والعمر، والوزن، ومحيط الخصر، ووجود أمراض اخرى وغيرها من العوامل الأخرى على تصنيف معدلات السكر في الدم إلى عدة مستويات أو مراحل:\\r\\n\\r\\nمستويات السكر الطبيعية:\\r\\nبعد صيام 8 ساعات: أقل من 100 مغ\\/ ديسيلتر.\\r\\nبعد ساعتين من تناول وجبة تحتوي على 75 غ من الجلوكوز: أقل من 140 مغ \\/ديسيلتر.\\r\\nالسكر التراكمي أقل من 5.6 %.\\r\\n\\r\\nمرحلة مقدمات السكري (بالإنجليزية: Prediabetic):\\r\\nبعد صيام 8 ساعات: 100- 125 مغ\\/ديسيلتر.\\r\\nبعد ساعتين من تناول وجبة تحتوي على 75 مغ من الجلوكوز : 140-199 مغ\\/ديسيلتر. \\r\\nالسكر التراكمي : 5.6-6.4 %.\\r\\n\\r\\nمرحلة السكري (تعرف ب Diabetes):\\r\\nبعد صيام 8 ساعات: أعلى من 126 مغ\\/ديسيلتر.\\r\\nبعد ساعتين من تناول وجبة تحتوي على 75 مغ من الجلوكوز: أعلى من 200 مغ\\/ ديسيلتر.\\r\\nالسكر التراكمي: أعلى من 6.5%.\\r\\n\\r\\nارتفاع سكر الدم\\r\\nيعني ارتفاع مستوى الجلوكوز في الدم أكثر 130مغ\\/ديسيلتر بعد صيام 8 ساعات، أو أكثر من 180 مغ\\/ديسيلتر بعد تناول الوجبة بساعتين.\\r\\n\\r\\nمن الأعراض الدالة على حدوث ارتفاع في مستوى سكر الدم:\\r\\n\\r\\nزيادة العطش.\\r\\nالصداع.\\r\\nصعوبة في التركيز.\\r\\nعدم وضوح الرؤية.\",\"en\":\"Glucose production\\r\\nAfter eating foods rich in carbohydrates such as: white bread, rice, pasta, potatoes, fruits, honey. Acids in the stomach begin by analyzing carbohydrates and sugars into glucose (or glucose) .In the intestine, glucose is absorbed until it reaches the bloodstream, when insulin is released from the pancreas in amounts that are proportional to the amount of glucose in the blood. The hormone insulin introduces the molecules of glucose into the cells of all kinds of the body, which in turn to consume glucose to produce energy as in red blood cells or stored (such as liver cells) and distributed later to muscles and neurons for later use.\\r\\n\\r\\nMore: Disintegration of carbohydrates\\r\\n\\r\\nHow to measure blood sugar\\r\\nBlood glucose can be measured by testing a blood sample in the laboratory, or by using instant blood glucose monitoring devices that measure blood sugar through a drop of blood drawn from a finger, and the result of the test is seen in a matter of seconds. Daily use in diabetics).\\r\\n\\r\\nThere are several ways to check your blood sugar level:\\r\\n\\r\\nTake a blood sample to measure blood at either random time regardless of eating.\\r\\nFasting blood glucose test, measured by blood glucose after a person\'s 8-hour fasting, gives more accurate and easier results for degradation and diagnosis.\\r\\nThe sugar tolerance test, after the patient fasts for 8 hours, is given 75 g of sugar dissolved with 250 ml of water.\\r\\nHemoglobin A1C, which measures the average level of glucose in the blood cells over the past three months, as it measures the binding of glucose molecules in hemoglobin found in red blood cells, and is used in the diagnosis of diabetes .\\r\\nInterpretation of laboratory measurements\\r\\nLaboratory measurements of blood glucose in the blood, as well as dependence on several other factors of sex, age, weight, waist circumference, and the presence of other diseases and other factors help to classify blood sugar levels into several levels or stages:\\r\\n\\r\\nNatural sugar levels:\\r\\nAfter fasting 8 hours: Less than 100 mg \\/ dL.\\r\\n2 hours after a meal containing 75 g glucose: less than 140 mg \\/ dL.\\r\\nCumulative sugar is less than 5.6%.\\r\\n\\r\\nPrediabetic stage:\\r\\nAfter fasting 8 hours: 100-125 mg \\/ dL.\\r\\nTwo hours after a meal containing 75 mg of glucose: 140-199 mg \\/ dL.\\r\\nCumulative sugar: 5.6-6.4%.\\r\\n\\r\\nDiabetes (known as Diabetes):\\r\\nAfter fasting 8 hours: higher than 126 mg \\/ dL.\\r\\nTwo hours after a meal containing 75 mg of glucose: higher than 200 mg \\/ dL.\\r\\nCumulative sugar: higher than 6.5%.\\r\\n\\r\\nHigh blood sugar\\r\\nHigh blood glucose means more than 130 mg \\/ dL after an 8-hour fast, or more than 180 mg \\/ dL two hours after a meal.\\r\\n\\r\\nSymptoms of high blood sugar include:\\r\\n\\r\\nIncreased thirst.\\r\\nHeadache.\\r\\nDifficulty concentrating.\\r\\nBlurred vision.\"}', '2019-08-26 10:27:05', '2019-08-27 12:09:29', '0'),
(91, '1566918502.webp', '{\"ar\":\"تشخيص السكري\",\"en\":\"Diagnosis of diabetes\"}', '{\"ar\":\"ينصح بالتشخيص المبكر للسكري في الحالات التالية\\r\\nإذا واجهت أي من أعراض السكري المبكرة\\r\\nالعطش.\\r\\nالتعب طوال الوقت.\\r\\nالشعور بالجوع الشديد ، حتى بعد تناول الطعام.\\r\\nوجود رؤية ضبابية.\\r\\nالتبول في كثير من الأحيان.\\r\\nوجود تقرحات، أو جروح لا تلتئم.\\r\\nإذا كنت تعاني من زيادة الوزن (مؤشر كتلة الجسم أكبر من 25)\\r\\nإذا كنت تقع في أي من الفئات التالية\\r\\nلديك ارتفاع في ضغط الدم، أو أمراض في القلب، أو إرتفاع في مستوى الدهون الثلاثية.\\r\\nلديك تاريخ عائلي لمرض السكري.\\r\\nلديك تاريخ شخصي لمستويات سكر الدم عالية.\\r\\nليس لديك نشاط بدني منتظم.\\r\\nأنت امرأة لديها تاريخ من تكييس المبايض، أو سكري الحمل.\\r\\nإذا كنت فوق سن 45\\r\\nهذا يساعدك على تحديد خط الأساس لمستويات السكر في الدم. نظرًا لأن خطر الإصابة بمرض السكري يزداد مع تقدم العمر، يمكن أن يساعدك الاختبار في تحديد فرصك في تطويره.\\r\\n\\r\\nأنواع فحوصات تشخيص السكري\\r\\n\\r\\nقياس السكر الصيامي Fasting Blood Glucose\\r\\nيقيس هذا الاختبار السكر في الدم بعد مرور 8 ساعات على الأقل دون تناول الطعام.\\r\\n\\r\\nيستخدم هذا الاختبار للكشف عن مرض السكري أو أعراض ما قبل السكري. يفضل إجراء هذا الإختبار في الصباح.\\r\\n\\r\\nإذا كانت النتيجة تتراوح بين 100 و 125 ملغم \\/ ديسيلتر، فهذه النتيجة تعتبر للأشخاص المصابين بما يسمى \\\"ما قبل السكري\\\"، مما يعني أنك أكثر عرضة للإصابة بداء السكري من النوع 2 ولكنك لم تصب به بعد.\\r\\nإذا كانت النتيجة 126 ملغ \\/ ديسيلتر أو أعلى، يعني أن لديك مرض السكري. لكن يجب تأكيد هذه النتيجة من خلال إعادة الإختبار في يوم آخر.\\r\\nإيجابيات فحص السكر الصيامي\\r\\nمنخفض التكلفة.\\r\\nالفحص هو متاح على نطاق واسع.\\r\\nسلبيات قياس السكر الصيامي\\r\\nتتأثر بتغييرات نمط الحياة على المدى القصير: الإجهاد أو المرض.\\r\\nأقل ارتباطا وثيقا بمضاعفات مرض السكري.\\r\\nغير مناسب للمريض أو مقدم الرعاية الصحية: يتطلب الصيام، و جدولة موعد الصباح.\\r\\nالعديد من المختبرات يقيس مصل الدم ، وهو أمر غير مستحسن.\\r\\nاختبار تحمل السكر Glucose Tolerance Test\\r\\nيقيس هذا الاختبار نسبة السكر في الدم بعد مرور ما لا يقل عن ثماني ساعات دون تناول الطعام وبعد ساعتين من تناول مشروب يحتوي على 75 غ من السكر المذاب بالماء.\\r\\n\\r\\nيمكن استخدام هذا الاختبار لتشخيص مرض السكري، أو أعراض ما قبل السكري.\\r\\n\\r\\nإذا كانت النتيجة تتراوح بين 140 - 199 ملغم \\/ ديسيلتر، فهذه النتيجة تعتبر للأشخاص المصابين بما يسمى \\\"ما قبل السكري\\\"، مما يعني أنك أكثر عرضة للإصابة بداء السكري من النوع 2 ولكنك لم تصب به بعد.\\r\\nإذا كانت النتيجة 200 ملغ \\/ ديسيلتر أو أعلى، يعني أنك مصاب بالسكري. لكن يجب تأ\",\"en\":\"Early diagnosis of diabetes is recommended in the following cases\\r\\nIf you experience any of the early symptoms of diabetes\\r\\nThirst.\\r\\nFatigue all the time.\\r\\nFeeling very hungry, even after eating.\\r\\nHaving blurry vision.\\r\\nUrinate frequently.\\r\\nThe presence of ulcers, or wounds that do not heal.\\r\\nIf you are overweight (BMI greater than 25)\\r\\nIf you fall into any of the following categories\\r\\nYou have high blood pressure, heart disease, or high triglycerides.\\r\\nYou have a family history of diabetes.\\r\\nYou have a personal history of high blood sugar levels.\\r\\nYou have no regular physical activity.\\r\\nYou are a woman with a history of PCOS, or gestational diabetes.\\r\\nIf you are over the age of 45\\r\\nThis helps you determine the baseline for your blood sugar levels. Because the risk of developing diabetes increases with age, the test can help you determine your chances of developing it.\\r\\n\\r\\nTypes of diagnostic tests for diabetes\\r\\n\\r\\nFasting Blood Glucose\\r\\nThis test measures blood sugar after at least 8 hours without eating.\\r\\n\\r\\nThis test is used to detect diabetes or pre-diabetes symptoms. This test is recommended in the morning.\\r\\n\\r\\nIf the result is between 100 and 125 mg \\/ dL, this is considered for people with so-called \\\"pre-diabetes\\\", which means that you are more likely to develop type 2 diabetes, but you have not yet.\\r\\nIf the result is 126 mg \\/ dL or higher, means that you have diabetes. But this result must be confirmed by retesting another day.\\r\\nPros of fasting sugar\\r\\nLow cost.\\r\\nScreening is widely available.\\r\\nCons of fasting glucose measurement\\r\\nAffected by short-term lifestyle changes: stress or disease.\\r\\nLess closely related to diabetes complications.\\r\\nNot suitable for patient or healthcare provider: requires fasting, scheduling morning appointment.\\r\\nMany laboratories measure serum, which is not recommended.\\r\\nGlucose Tolerance Test\\r\\nThis test measures blood sugar at least eight hours after eating and two hours after a drink containing 75 g of sugar dissolved in water.\\r\\n\\r\\nThis test can be used to diagnose diabetes, or pre-diabetic symptoms.\\r\\n\\r\\nIf the result is between 140 and 199 mg \\/ dL, this result is considered for people with so-called \\\"pre-diabetes\\\", which means that you are more likely to develop type 2 diabetes but you have not yet.\\r\\nIf the result is 200 mg \\/ dL or higher, it means that you have diabetes. But it must come\"}', '2019-08-26 10:34:52', '2019-08-27 12:08:22', '0');

-- --------------------------------------------------------

--
-- Table structure for table `blog_user`
--

CREATE TABLE `blog_user` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog_user`
--

INSERT INTO `blog_user` (`id`, `blog_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, NULL, NULL),
(26, 36, 102, NULL, NULL),
(29, 37, 102, NULL, NULL),
(31, 1, 102, NULL, NULL),
(34, 39, 102, NULL, NULL),
(41, 40, 96, NULL, NULL),
(46, 59, 110, NULL, NULL),
(52, 56, 99, NULL, NULL),
(59, 39, 99, NULL, NULL),
(62, 39, 115, NULL, NULL),
(65, 39, 117, NULL, NULL),
(75, 75, 128, NULL, NULL),
(79, 62, 128, NULL, NULL),
(81, 63, 128, NULL, NULL),
(83, 77, 128, NULL, NULL),
(84, 64, 128, NULL, NULL),
(87, 61, 133, NULL, NULL),
(88, 61, 128, NULL, NULL),
(89, 78, 128, NULL, NULL),
(92, 62, 136, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `catogeries`
--

CREATE TABLE `catogeries` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `catogeries`
--

INSERT INTO `catogeries` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(19, '{\"ar\":\"الفاكهة\",\"en\":\"fruit\"}', '1564557834.png', '2019-07-31 04:23:54', '2019-07-31 04:23:54'),
(20, '{\"ar\":\"الخضروات\",\"en\":\"Vegetables\"}', '1564557895.png', '2019-07-31 04:24:55', '2019-07-31 04:24:55'),
(31, '{\"ar\":\"الحليب ومنتجاته\",\"en\":\"Milk and milk products\"}', '1566916912.PNG', '2019-08-22 09:06:55', '2019-08-27 11:41:52'),
(32, '{\"ar\":\"البيض\",\"en\":\"eggs\"}', '1566917895.PNG', '2019-08-22 09:09:12', '2019-08-27 11:58:15'),
(33, '{\"ar\":\"مجموعة اللحوم\",\"en\":\"Meat Group\"}', '1566917674.jpg', '2019-08-22 09:12:03', '2019-08-27 11:54:34'),
(34, '{\"ar\":\"الكريمات\",\"en\":\"Creams\"}', '1566917537.PNG', '2019-08-22 09:16:26', '2019-08-27 11:52:17'),
(35, '{\"ar\":\"مكسرات\",\"en\":\"Nuts\"}', '1566917371.PNG', '2019-08-22 09:22:26', '2019-08-27 11:49:31'),
(36, '{\"ar\":\"الأكلات الشعبيه\",\"en\":\"Popular food\"}', '1566917335.jpg', '2019-08-22 09:28:20', '2019-08-27 11:48:55'),
(37, '{\"ar\":\"المشروبات\",\"en\":\"Drinks\"}', '1566916886.jpg', '2019-08-22 09:36:53', '2019-08-27 11:41:26'),
(38, '{\"ar\":\"الخبز والحبوب\",\"en\":\"Bread and cereals\"}', '1566916733.PNG', '2019-08-22 09:42:04', '2019-08-27 11:38:53'),
(39, '{\"ar\":\"الوجبات الخفيفه\",\"en\":\"snacks\"}', '1566916711.PNG', '2019-08-22 09:46:14', '2019-08-27 11:38:31');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `blog_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `blog_id`, `content`, `created_at`, `updated_at`) VALUES
(60, 128, 61, 'للا', '2019-08-25 12:03:49', '2019-08-25 12:03:49'),
(61, 128, 63, 'dsd', '2019-08-26 06:01:22', '2019-08-26 06:01:22'),
(62, 128, 62, '45', '2019-08-26 06:52:22', '2019-08-26 06:52:22'),
(63, 128, 75, '22', '2019-08-26 06:53:13', '2019-08-26 06:53:13'),
(64, 128, 63, '45', '2019-08-26 07:00:17', '2019-08-26 07:00:17'),
(65, 128, 63, 'dsd', '2019-08-26 07:07:32', '2019-08-26 07:07:32'),
(66, 128, 77, 'sdds', '2019-08-26 07:07:53', '2019-08-26 07:07:53'),
(67, 128, 77, 'sd', '2019-08-26 07:07:57', '2019-08-26 07:07:57'),
(68, 128, 77, 'sdddddd', '2019-08-26 07:09:37', '2019-08-26 07:09:37'),
(69, 128, 64, 'لل', '2019-08-26 08:08:42', '2019-08-26 08:08:42'),
(70, 128, 78, 'تم', '2019-08-26 08:09:04', '2019-08-26 08:09:04'),
(71, 128, 78, 'زد', '2019-08-26 08:09:11', '2019-08-26 08:09:11'),
(72, 128, 78, '12', '2019-08-26 08:09:28', '2019-08-26 08:09:28'),
(73, 128, 78, '13', '2019-08-26 08:09:33', '2019-08-26 08:09:33'),
(74, 128, 78, '14', '2019-08-26 08:09:39', '2019-08-26 08:09:39'),
(75, 128, 78, '16', '2019-08-26 08:09:44', '2019-08-26 08:09:44'),
(76, 128, 78, '17', '2019-08-26 08:09:47', '2019-08-26 08:09:47'),
(77, 128, 78, '18', '2019-08-26 08:09:54', '2019-08-26 08:09:54'),
(78, 128, 78, '19', '2019-08-26 08:09:59', '2019-08-26 08:09:59'),
(79, 128, 78, '20', '2019-08-26 08:10:05', '2019-08-26 08:10:05'),
(80, 128, 78, '21', '2019-08-26 08:10:10', '2019-08-26 08:10:10'),
(81, 128, 61, '2', '2019-08-26 08:11:20', '2019-08-26 08:11:20'),
(82, 128, 61, '3', '2019-08-26 08:11:23', '2019-08-26 08:11:23'),
(83, 128, 61, '4', '2019-08-26 08:11:26', '2019-08-26 08:11:26'),
(84, 128, 61, '5', '2019-08-26 08:11:30', '2019-08-26 08:11:30'),
(85, 128, 61, '6', '2019-08-26 08:11:34', '2019-08-26 08:11:34'),
(86, 128, 61, '7', '2019-08-26 08:11:37', '2019-08-26 08:11:37'),
(87, 128, 61, '8', '2019-08-26 08:11:43', '2019-08-26 08:11:43'),
(88, 128, 61, '9', '2019-08-26 08:11:47', '2019-08-26 08:11:47'),
(89, 128, 61, '10', '2019-08-26 08:11:51', '2019-08-26 08:11:51'),
(90, 128, 61, '12', '2019-08-26 08:11:55', '2019-08-26 08:11:55'),
(91, 128, 61, '36', '2019-08-27 05:19:14', '2019-08-27 05:19:14'),
(92, 128, 78, '56', '2019-08-27 05:19:48', '2019-08-27 05:19:48'),
(93, 135, 61, '18', '2019-08-29 04:25:25', '2019-08-29 04:25:25');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

CREATE TABLE `contact_us` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `read_at` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `name`, `email`, `content`, `user_id`, `created_at`, `updated_at`, `read_at`) VALUES
(66, 'ahmed alfyjjjjj', 'the@was.com', 'fff', 136, '2019-09-02 13:46:05', '2019-09-09 11:34:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dir` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `measurement_times`
--

CREATE TABLE `measurement_times` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `measurement_times`
--

INSERT INTO `measurement_times` (`id`, `name`, `image`, `status`, `created_at`, `updated_at`) VALUES
(11, '{\"ar\":\"قبل الفطار\",\"en\":\"Before breakfast\"}', '1564656726.png', 'before', '2019-07-31 06:12:13', '2019-08-01 07:52:06'),
(12, '{\"ar\":\"بعد الفطار\",\"en\":\"after breakfast\"}', '1564656741.png', 'after', '2019-07-31 06:12:32', '2019-08-01 07:52:21'),
(13, '{\"ar\":\"قبل الغذاء\",\"en\":\"Before Launch\"}', '1564656758.png', 'before', '2019-07-31 06:13:20', '2019-08-01 07:52:38'),
(14, '{\"ar\":\"بعد الغذاء\",\"en\":\"After Launch\"}', '1564656775.png', 'after', '2019-07-31 06:13:47', '2019-08-01 07:52:55'),
(15, '{\"ar\":\"قبل العشاء\",\"en\":\"Before dinner\"}', '1564656786.png', 'before', '2019-07-31 06:15:55', '2019-08-01 07:53:06'),
(16, '{\"ar\":\"بعد العشاء\",\"en\":\"After dinner\"}', '1564656803.png', 'after', '2019-07-31 06:16:21', '2019-08-01 07:53:23'),
(17, '{\"ar\":\"قبل النوم\",\"en\":\"before sleep\"}', '1564656818.png', 'before', '2019-07-31 06:16:50', '2019-08-01 07:53:38'),
(18, '{\"ar\":\"قبل السحور\",\"en\":\"قبل السحور\"}', '1564656867.png', 'before', '2019-08-01 03:08:15', '2019-08-01 07:54:27');

-- --------------------------------------------------------

--
-- Table structure for table `medicines`
--

CREATE TABLE `medicines` (
  `id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medicines`
--

INSERT INTO `medicines` (`id`, `image`, `name`, `text`, `created_at`, `updated_at`) VALUES
(29, '1566920856.jpg', '{\"ar\":\"جليميبرايد\",\"en\":\"Glimepride\"}', '{\"ar\":\"إن جليميبيريد (Glimepiride)، هو دواء لعلاج مرض السكري، غير متعلق بالإنسولين (Insulin Independent). ويعتبر من \\\"الجيل الثاني\\\" من مجموعة أدوية السولفونيلورية (Sulphonylureas)(مع غليبيزيد - Glipizide وغليبوريد - Glyburide). من مميزات دواء غليميبيريد أنه يؤدي إلى خفض كمية السكر في الدم بفاعلية أكثر من الأدوية القديمة، التي تعتبر \\\"الجيل الاول\\\" (مثل: تولبوتاميد - Tolbutamide وكلوربروباميد - Chlorpropamide).\\r\\n\\r\\nيساعد غليميبيريد على إفراز الإنسولين من خلايا البنكرياس. ويساعد هذا الدواء على نقل السكر في الدم إلى داخل أنسجة الجسم. يجب دمج استعمال هذا الدواء مع حمية غذائية قليلة النشويات والدهون. يمكن تناول هذا الدواء بالتوازي مع تناول الإنسولين (Insulin) والميتافورمين (Metformin). يمتاز دواء (غليميبيريد) بطول مدة فعاليته الدوائية، والتي تمتد لِـ 24 ساعة، لذلك يتم تناوله مرة في اليوم. لا يستخدم هذا الدواء لعلاج مرض السكري المتعلق بالإنسولين (السكري اليَفْعِي - Juvenile Diabetes)\",\"en\":\"Glimepiride is a non-insulin-dependent diabetes medication. It is considered the \\\"second generation\\\" of the sulfonylureas group (with Glipizide and Glyburide). One of the advantages of glimepiride is that it reduces the amount of sugar in the blood more effectively than old drugs, which are considered \\\"first generation\\\" (such as: Tolbutamide - Tolbutamide and Chlorpropamide - Chlorpropamide).\\r\\n\\r\\nGlimepiride helps release insulin from pancreatic cells. This drug helps to transfer blood sugar into the tissues of the body. The use of this medicine should be combined with a diet low in carbohydrates and fats. This medicine can be taken in parallel with taking Insulin and Metformin. Glimepiride is characterized by the long duration of its pharmacological activity, which extends for 24 hours, so it is taken once a day. This medicine is not used to treat insulin-related diabetes (Juvenile Diabetes)\"}', '2019-08-22 06:35:25', '2019-08-27 12:47:36'),
(30, '1566920796.jpg', '{\"ar\":\"روزيغليتازون\",\"en\":\"Rosiglitazone\"}', '{\"ar\":\"يُستعمل  روزيغليتازون (Rosiglitazone) لمعالجة المصابين بمرض السكري غير المتعلق بالإنسولين (Insulin Independent) (سكري البالغين). يُستعمل هذا الدواء لمعالجة الأشخاص الذين لا يتحقق توازن تركيز السكر في الدم لديهم بواسطة الحمية الغذائية أو الرياضة البدنية. أحيانا، تدمج المعالجة بهذا الدواء مع أدوية أخرى تستعمل للسيطرة على مستوى السكر، مثل الميتفورمين (Metformin) أو أدوية من عائلة السلفونيل يوريا (Sulfonylurea). على عكس الأدوية الأخرى المعطاة عن طريق الفم والتي تزيد من إفراز الإنسولين، فإن الروزيغليتازون يزيد من حساسية مستقبلات الإنسولين, وبذلك يقلل من مقاومة الأنسجة لعمل الإنسولين.\",\"en\":\"Rosiglitazone is used to treat people with non-insulin-dependent diabetes mellitus (adult diabetes). This medicine is used to treat people whose blood sugar concentration is not balanced by diet or physical exercise. Occasionally, treatment with this medicine is combined with other drugs used to control glucose, such as metformin (Metformin) or drugs from the family of Sulfonylurea (Sulfonylurea). Unlike other oral medications that increase insulin secretion, rosiglitazone increases the sensitivity of insulin receptors, thereby reducing tissue resistance to insulin action.\"}', '2019-08-22 06:46:37', '2019-08-27 12:46:36'),
(31, '1566920515.jpg', '{\"ar\":\"الانسولين\",\"en\":\"Insulin\"}', '{\"ar\":\"الإنسولين (Insulin) هو هُرمون ينتجه البنكرياس. ويعتبر وجوده حيويا في عدد من عمليات الأيض (الاستقلاب \\/ تبادل المواد - Metabolism)، والتي أكثرها شهرة هي عملية مراقبة مستويات (تركيز) السكر في الدم. تم التعرف على عقار الإنسولين كدواء منذ سنوات الـ 20 في القرن الماضي، ويُعطى بالحَقـْن، كإضافة أو كبديل للإنسولين الطبيعي في إطار العلاج لمرض السكري.\\r\\n\\r\\nيعتبر الإنسولين العلاج الناجع الوحيد لسكّري اليافعين ( Juvenile - onset diabetes) (أو: السكري المُعتَمِد على الإنسولين - Insulin dependent diabetes)، كما يُعطى علاجا لمرض السكري الذي يصيب البالغين. يجب تعاطيه مع الحفاظ على تغذية متوازنة ومراقبة بشدّة، كما قد تكون هنالك حاجة لتغيير الجرعات في بعض حالات الإصابة بأمراض، التقيؤ، تغييرات في التغذية أو في مستويات النشاط الجسماني\",\"en\":\"Insulin is a hormone produced by the pancreas. Its presence is vital in a number of metabolic processes (metabolism \\/ metabolism), the most famous of which is the process of monitoring blood sugar levels. Insulin has been recognized as a drug since the 20 years of the last century, and is given by injection as an additive or alternative to natural insulin as part of the treatment for diabetes.\\r\\n\\r\\nInsulin is the only effective treatment for juvenile diabetes (or juvenile - onset diabetes) (or: insulin dependent diabetes - Insulin dependent diabetes), and is given treatment for diabetes in adults. It should be taken while maintaining balanced nutrition and strict monitoring, and dosing may be required in some cases of disease, vomiting, changes in nutrition or levels of physical activity\"}', '2019-08-22 06:50:47', '2019-08-27 12:41:55'),
(33, '1566920303.jpg', '{\"ar\":\"تولبوتاميد\",\"en\":\"Tolbutamide\"}', '{\"ar\":\"إن تولبوتاميد (Tolbutamide) هو دواء مضاد لمرض السكري الذي يعطى عن طريق الفم. يستخدم لعلاج مرضى السكري البالغين مع اتباع نظام غذائي منخفض الكربوهيدرات والدهون. يستخدم الدواء لخفض مستويات السكر في الدم من خلال تحفيز إنتاج وإفراز الإنسولين من البنكرياس، وتحفيز دخول السكر إلى خلايا الجسم.\\r\\n\\r\\nيتميز التولبوتاميد بنشاط قصير الأمد (6 - 12 ساعة)، وهو أضعف نسبيًّا من الأدوية المضادة لداء السكري الأخرى التي تعطى عن طريق الفم، ويتم تحلُّلها بشكل مكثف في الكبد لمواد غير نشطة. لذلك يمكن للتولبوتاميد أن يكون ملائمًا للمسنين والمرضى الذين يعانون من فشل في الكلى .\",\"en\":\"Tolbutamide is an oral anti-diabetic drug. It is used to treat adult diabetics with a low-carb and fat diet. The drug is used to lower blood sugar levels by stimulating the production and secretion of insulin from the pancreas, and stimulating the entry of sugar into the cells of the body.\\r\\n\\r\\nTolbutamide is characterized by short-term activity (6 - 12 hours), which is relatively weaker than other oral antidiabetic drugs, and is intensively decomposed in the liver for inactive substances. Tolbutamide can therefore be suitable for the elderly and patients with kidney failure.\"}', '2019-08-22 07:00:51', '2019-08-27 12:38:23'),
(34, '1566920238.jpg', '{\"ar\":\"جليبنكلاميد\",\"en\":\"Glibenclamide\"}', '{\"ar\":\"جليبنكلاميد (Glibenclamide) هو أحد الأدوية المضادة للسكري المعطاة عن طريق الفم. كما هو الأمر في الأدوية الأخرى من هذا النوع، فإن الغليبينكلاميد يحفز إفراز الإنسولين من جُزَيْراتِ لانغرهانس المتواجدة في البنكرياس، وَيُنَشِّطُ استقبال السكر لداخل خلايا الجسم، وبذلك يخفض مستوى السُّكر في الدم. يتم استعماله لعلاج سكري البالغين، بالدمج مع نظام غذائي قليل الكربوهيدرات والدهون. في حالات المرض الصعب، الإصابة أو الضغط، قد يَفقد الدواء من نجاعته في تحفيز الإنسولين من البنكرياس؛ في هذه الحالات يعطى الإنسولين بالحقن.\",\"en\":\"Glibenclamide is an oral antidiabetic drug. As with other drugs of this type, glibenclamide stimulates the secretion of insulin from Langerhans islets in the pancreas, and activates the reception of sugar inside the cells of the body, thus reducing the level of sugar in the blood. It is used to treat adult diabetes, in combination with a low-carbohydrate and fat diet. In cases of difficult illness, injury or pressure, the drug may lose its efficacy in stimulating insulin from the pancreas; in these cases insulin is given by injection.\"}', '2019-08-22 07:03:06', '2019-08-27 12:37:18'),
(35, '1566920158.jpg', '{\"ar\":\"ميتفورمين\",\"en\":\"Metformin\"}', '{\"ar\":\"ميتفورمين (Metformin)، دواء لمعالجة داء السكري ينتمي لمجموعة البيجوانيد (Biguanide). يستخدم الميتفورمين عند تناوله عن طريق الفم لعلاج داء سكري البالغين الذين لا تزال لديهم خلايا فعالة تفرز الانسولين في البنكرياس. يخفض الميتفورمين من مستوى السكر في الدم من خلال تقليل امتصاص الجلوكوز من جهاز الهضم إلى الدم، تخفيض إنتاج الجلوكوز على يد خلايا الكبد والكليتين وتعزيز حساسية الخلايا للانسولين، والتي تزيد من نجاعة استيعاب الجلوكوز من الدم.\\r\\n\\r\\nيصحب استخدام الميتفورمين، اتباع نظام غذائي، خاص بمرضى السكري، يحد من استهلاك السكريات والدهنيات. في معظم الحالات، إضافة الى الميتفورمين، يستخدم دواء اخر لمعالجة داء السكري، وظيفته تحفيز خلايا البنكرياس لتفرز انسولين.\",\"en\":\"Metformin, a drug for the treatment of diabetes belonging to the group of Biguanide (Biguanide). Metformin is used when taken orally to treat adult diabetes mellitus who still have effective insulin-secreting cells in the pancreas. Metformin lowers blood sugar by reducing glucose uptake from the digestive system to the blood, reducing glucose production by liver and kidney cells and enhancing insulin sensitivity of cells, which increase the efficiency of glucose absorption from the blood.\\r\\n\\r\\nThe use of metformin is accompanied by a diet, especially for diabetics, which limits the consumption of sugars and fats. In most cases, in addition to metformin, another drug is used to treat diabetes, the function of which is to stimulate pancreatic cells to secrete insulin.\"}', '2019-08-22 07:06:08', '2019-08-27 12:35:58'),
(36, '1566920115.jpg', '{\"ar\":\"سيتاجليبتين\",\"en\":\"Cytagliptin\"}', '{\"ar\":\"يساهم دواء سيتاجليبتين (Sitagliptin) في دمج الحمية والنشاط البدني، تنظيم نسب الجلوكوز (glucose) في الدم لدى المصابين بداء السكري من نوع 2 (Type 2 diabetes). يتم تناول هذا الدواء كعلاج وحيد أو بالإضافة مع أدوية أخرى مضادة للسكري مثل: الميتفورمين (Metformin) وأدوية أخرى من عائلة الثيزوليدنيديون (Thiazolidinediones). تعتمد آلية عمل هذا الدواء على التثبيط الانتقائي للإنزيم ديبيبتديل ببتيداز - 4 (4 - Dipeptidyl peptidase) الذي يقوم بتحليل هورمونات الإنكرتين (Incretins)، وهكذا ينجح الدواء بمضاعفة مستوى الإنكرتين الذي يعمل على البنكرياس ويقوم بإفراز الإنسولين (Insulin). يُمْنَعُ استخدام هذا الدواء على مرضى السكري من نوع 1 أو علاج الحامض الكيتوني (Ketoacidosis) السكري، لكون هذا العلاج غير فعال في علاج هذه الحالات.\",\"en\":\"Sitagliptin contributes to the integration of diet and physical activity, regulating blood glucose levels in people with type 2 diabetes. This medicine is taken as a single treatment or in combination with other antidiabetic drugs such as: Metformin (Metformin) and other medicines from the thiazolidinediones family. The mechanism of action of this drug is based on the selective inhibition of the enzyme dipeptidyl peptidase-4 (4 - Dipeptidyl peptidase), which analyzes the hormones of Incretin (Incretins), and thus succeeds the drug to double the level of denatin, which works on the pancreas and insulin secretion (Insulin). Do not use this medicine in patients with type 1 diabetes or treatment of diabetic ketoacidosis (Ketoacidosis), as this treatment is ineffective in the treatment of these conditions.\"}', '2019-08-22 07:10:14', '2019-08-27 12:35:15'),
(37, '1566920060.jpg', '{\"ar\":\"ريباجلينيد\",\"en\":\"Repaglinide\"}', '{\"ar\":\"ريباجلينيد (Repaglinide) هو دواء يستخدم لعلاج مرضى السكري غير المتعلق بالانسولين (سكري كبار السن، أي السكري من النمط الثاني). يمكن استخدام الريباجلينيد (Repaglinide) لوحده أو مع الميتفورمين (Metformin). بالإضافة إلى العلاج الدوائي، يجب إتباع نظام غذائي صحي ومتوازن وممارسة التمارين الرياضية.\\r\\n\\r\\nينتمي الريباجلينيد لعائلة جديدة من الأدوية تدعى (Meglitinides). تعمل هذه الأدوية بأسلوب مشابه لعمل الانسولين في الجسم. يتميز الريباجلينيد بكونه سريع التأثير، كما أنه يحفز إفراز الإنسولين فقط عندما يرتفع مستوى السكر في الدم. لذا يتم تناوله قبل الطعام. بالإضافة إلى ذلك، فإن للريباجلينيد (Repaglinide) تأثير قصير الأمد، بحيث يتم إفراز الأنسولين عند الحاجة فقط (أي عند تناول وجبات الطعام).\",\"en\":\"Repaglinide is a drug used to treat non-insulin-related diabetes (elderly diabetes, type 2 diabetes). Repaglinide can be used alone or in combination with metformin. In addition to drug therapy, a healthy and balanced diet and exercise should be followed.\\r\\n\\r\\nRibaglindide belongs to a new family of drugs called Meglitinides. These drugs work in a similar way to the work of insulin in the body. Ribaglindide is fast-acting and stimulates insulin secretion only when blood sugar levels rise. So it is taken before food. In addition, Repaglinide has a short-term effect, so that insulin is released only when needed (i.e. when eating meals).\"}', '2019-08-22 07:31:55', '2019-08-27 12:34:20'),
(38, '1566920008.jpg', '{\"ar\":\"بودسونيد ضبائب\",\"en\":\"Bodsonide foggers\"}', '{\"ar\":\"بودسونيد هو ستيرويد. يمنع من إطلاق المواد التي تسبب الالتهابات في الجسم مثل الهيستامين ، الليكوتريين.\\r\\n\\r\\nيقلل من التهاب خلايا القصبات الهوائية لذا يستعمل لعلاج التهاب خلايا القصبات\",\"en\":\"Bodsonide is an anabolic steroid. Prevents the release of substances that cause infections in the body such as histamine, leukotrienes.\\r\\n\\r\\nReduces inflammation of the bronchial cells so it is used to treat bronchial cell inflammation\"}', '2019-08-22 07:41:25', '2019-08-27 12:33:28'),
(39, '1566919955.jpg', '{\"ar\":\"روزيغليتازون\",\"en\":\"Rosiglitazone\"}', '{\"ar\":\"يُستعمل  روزيغليتازون (Rosiglitazone) لمعالجة المصابين بمرض السكري غير المتعلق بالإنسولين (Insulin Independent) (سكري البالغين). يُستعمل هذا الدواء لمعالجة الأشخاص الذين لا يتحقق توازن تركيز السكر في الدم لديهم بواسطة الحمية الغذائية أو الرياضة البدنية. أحيانا، تدمج المعالجة بهذا الدواء مع أدوية أخرى تستعمل للسيطرة على مستوى السكر، مثل الميتفورمين (Metformin) أو أدوية من عائلة السلفونيل يوريا (Sulfonylurea). على عكس الأدوية الأخرى المعطاة عن طريق الفم والتي تزيد من إفراز الإنسولين، فإن الروزيغليتازون يزيد من حساسية مستقبلات الإنسولين, وبذلك يقلل من مقاومة الأنسجة لعمل الإنسولين.\",\"en\":\"Rosiglitazone is used to treat people with non-insulin-dependent diabetes mellitus (adult diabetes). This medicine is used to treat people whose blood sugar concentration is not balanced by diet or physical exercise. Occasionally, treatment with this medicine is combined with other drugs used to control glucose, such as metformin (Metformin) or drugs from the family of Sulfonylurea (Sulfonylurea). Unlike other oral medications that increase insulin secretion, rosiglitazone increases the sensitivity of insulin receptors, thereby reducing tissue resistance to insulin action.\"}', '2019-08-22 07:50:50', '2019-08-27 12:32:35'),
(41, '1566919874.jpg', '{\"ar\":\"اكاربوز\",\"en\":\"Acarbose\"}', '{\"ar\":\"اكارْبوز (Acarbose) هو دواء لمعالجة مرض السكري، ويعطى عن طريق الفم، على العكس من الأدوية الأخرى التي تستعمل لعلاج مرض السكري، والتي تحفز إنتاج وإفراز الإنسولين من البنكرياس، يثبط الأكارْبوس امتصاص السُّكَّرِيات من الأمعاء، ويقلل من ارتفاع السكر في الدم بعد الأكل.\\r\\n\\r\\nيلائم أكارْبوس مرضى السكري غير المرتبطين بالإنسولين (NIDDM). عند المرضى الذين يتناولون أدوية محفزة لإنتاج الإنسولين من البنكرياس (مثل، جليبينكلاميد - Glibenclamide)، يزيد الأكارْبوس من تأثير خفض السكر للدواء الأول، وذلك لأنه يعمل حسب آلية مختلفة\",\"en\":\"Acarbose is a drug for the treatment of diabetes.It is administered orally.Unlike other drugs used to treat diabetes, which stimulate the production and secretion of insulin from the pancreas, acarbose inhibits the absorption of sugars from the intestines and reduces high blood sugar after eating.\\r\\n\\r\\nAcarbose is suitable for patients with non-insulin-related diabetes (NIDDM). In patients taking insulin-stimulating drugs from the pancreas (e.g., Glibenclamide), acarbose increases the sugar-lowering effect of the first drug, because it works according to a different mechanism\"}', '2019-08-22 07:54:23', '2019-08-27 12:31:15'),
(42, '1566919829.jpg', '{\"ar\":\"إكسيناتيد\",\"en\":\"Exenated\"}', '{\"ar\":\"إكْسيناتيد (Exenatide) هو دواء المحاكي لوظائف الهرمون الإنكريتيني (Incretins GLP - 1) في الجسم. وهو معد لعلاج حالات السكري من نوع 2، الذين يتناولون ميتفورمين (Metformin) أو أدوية أخرى من عائلة سولفونيلوريا (Sulphanylurea). يضاعف هذا الدواء إفراز الأنسولين (iInsulin) من البنكرياس نسبة لنسبة الجلوكوز في الدم،\",\"en\":\"Exenatide is a drug that mimics the functions of Incretins GLP-1 in the body. It is intended for the treatment of type 2 diabetes, who take Metformin or other drugs from the Sulphanylurea family. This drug doubles the insulin secretion (iInsulin) of the pancreas in proportion to blood glucose,\"}', '2019-08-22 07:58:49', '2019-08-27 12:30:29'),
(43, '1566919786.jpg', '{\"ar\":\"جليميبرايد\",\"en\":\"Glimepride\"}', '{\"ar\":\"إن جليميبيريد (Glimepiride)، هو دواء لعلاج مرض السكري، غير متعلق بالإنسولين (Insulin Independent). ويعتبر من \\\"الجيل الثاني\\\" من مجموعة أدوية السولفونيلورية (Sulphonylureas)(مع غليبيزيد - Glipizide وغليبوريد - Glyburide). من مميزات دواء غليميبيريد أنه يؤدي إلى خفض كمية السكر في الدم بفاعلية أكثر من الأدوية القديمة، التي تعتبر \\\"الجيل الاول\\\" (مثل: تولبوتاميد - Tolbutamide وكلوربروباميد - Chlorpropamide).\",\"en\":\"Glimepiride is a non-insulin-dependent diabetes medication. It is considered to be the \\\"second generation\\\" of the sulfonylureas group (with Glipizide and Glyburide). One of the advantages of glimepiride is that it reduces the amount of sugar in the blood more effectively than old drugs, which are considered \\\"first generation\\\" (such as: Tolbutamide - Tolbutamide and Chlorpropamide - Chlorpropamide).\"}', '2019-08-22 08:02:02', '2019-08-27 12:29:46'),
(44, '1566919744.jpg', '{\"ar\":\"الانسولين\",\"en\":\"Insulin\"}', '{\"ar\":\"الإنسولين (Insulin) هو هُرمون ينتجه البنكرياس. ويعتبر وجوده حيويا في عدد من عمليات الأيض (الاستقلاب \\/ تبادل المواد - Metabolism)، والتي أكثرها شهرة هي عملية مراقبة مستويات (تركيز) السكر في الدم. تم التعرف على عقار الإنسولين كدواء منذ سنوات الـ 20 في القرن الماضي، ويُعطى بالحَقـْن، كإضافة أو كبديل للإنسولين الطبيعي في إطار العلاج لمرض السكري.\\r\\n\\r\\nيعتبر الإنسولين العلاج الناجع الوحيد لسكّري اليافعين ( Juvenile - onset diabetes) (أو: السكري المُعتَمِد على الإنسولين - Insulin dependent diabetes)، كما يُعطى علاجا لمرض السكري الذي يصيب البالغين. يجب تعاطيه مع الحفاظ على تغذية متوازنة ومراقبة بشدّة، كما قد تكون هنالك حاجة لتغيير الجرعات في بعض حالات الإصابة بأمراض، التقيؤ، تغييرات في التغذية أو في مستويات النشاط الجسماني.\",\"en\":\"Insulin is a hormone produced by the pancreas. Its presence is vital in a number of metabolic processes (metabolism \\/ metabolism), the most famous of which is the process of monitoring blood sugar levels. Insulin has been recognized as a drug since the 20 years of the last century, and is given by injection as an additive or alternative to natural insulin as part of the treatment for diabetes.\\r\\n\\r\\nInsulin is the only effective treatment for juvenile diabetes (or juvenile - onset diabetes) (or: insulin dependent diabetes - Insulin dependent diabetes), and is given treatment for diabetes in adults. It should be taken while maintaining balanced nutrition and strict monitoring, and dosing may be required in some cases of disease, vomiting, changes in nutrition or levels of physical activity.\"}', '2019-08-22 08:05:48', '2019-08-27 12:29:04');

-- --------------------------------------------------------

--
-- Table structure for table `messagers`
--

CREATE TABLE `messagers` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `notification_id` text CHARACTER SET utf8 DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messagers`
--

INSERT INTO `messagers` (`id`, `title`, `message`, `image`, `notification_id`, `created_at`, `updated_at`) VALUES
(36, 'nnnnnnnn', 'hh', '1566738918.png', 'c8a37980-94d3-4a83-af0d-c741a9dbdf55', '2019-08-25 10:15:18', '2019-08-25 10:15:18'),
(37, 'nnnnnnnn', 'hh', '1566739039.png', 'e0578771-6bbe-4d2a-ba46-e68b8ed2273d', '2019-08-25 10:17:19', '2019-08-25 10:17:19'),
(38, 'تجربة', 'متوفر نسخة جديدة من التطبيق!!', '1566741628.jpg', '0bbcbda9-53c8-40fa-b50e-3bc931ad132d', '2019-08-25 11:00:28', '2019-08-25 11:00:28'),
(40, 'رسال10', 'انها رساله1', '1566810493.png', 'f47c8c3f-adc9-4631-9405-2ac993d85b2d', '2019-08-26 06:08:13', '2019-08-26 06:47:29'),
(41, 'fdgf', 'vgbg', '1566811212.png', '1825a433-b6a7-44be-91c4-124353b17494', '2019-08-26 06:20:12', '2019-08-26 06:20:12'),
(42, 'تجربة شركة جدارة لتقنية المعلومات', 'التجربة الاولي', '1566818461.png', '75c401a8-33e8-4959-8c54-536d031d2f23', '2019-08-26 08:21:01', '2019-08-26 08:21:01'),
(43, 'nnnnnnnn', 'hh', '1566819350.png', 'bae06185-0cc4-4a07-a290-f8f7dadf0369', '2019-08-26 08:35:50', '2019-08-26 08:35:50'),
(44, 'hjhi', 'kkkgytuyn', '1566820287.jpg', '639aabea-0270-4728-a9b0-5adfc3fb58c1', '2019-08-26 08:51:27', '2019-08-26 08:51:28'),
(45, 'hjhjppp', 'lllll', '1566820403.png', '377549cc-0b69-4f11-8e6f-2016398686a6', '2019-08-26 08:53:23', '2019-08-26 08:53:24'),
(46, 'sss', 'ssss', '1566821117.png', 'd85051b7-21a0-49f8-a6bf-a3b4a44d06da', '2019-08-26 09:05:17', '2019-08-26 09:05:17'),
(47, 'aaa', 'aaaa', '1566821655.jpg', 'fb247de7-d1d3-468e-b0d7-6c8d25494748', '2019-08-26 09:14:15', '2019-08-26 09:14:16'),
(48, 'lll', 'lllll', '1566821729.png', '4b8a78ba-2747-497d-9f66-609a9d84e29b', '2019-08-26 09:15:29', '2019-08-26 09:15:30'),
(49, 'gggg', 'ggggg', '1566822030.png', 'c8961e0c-2879-401e-ad0a-3f5ecda02228', '2019-08-26 09:20:30', '2019-08-26 09:20:30'),
(50, 'ds', 'sa', '1566822498.png', '1932cabc-ee7a-4cf5-8b6c-bbac4463c84d', '2019-08-26 09:28:18', '2019-08-26 09:28:18'),
(51, 'qwe', 'dsaaaaaaaaad ffdgsdf  fgdfgdf s fds', '1566823260.png', '9d52942f-e1af-435b-8de2-239dbe6c0dd3', '2019-08-26 09:41:00', '2019-08-26 09:41:00'),
(52, 'aaaaa', 'ssssss', '1566823435.jpg', '1d79fecb-3627-447b-b703-0b0c8fbf9204', '2019-08-26 09:43:55', '2019-08-26 09:43:56'),
(53, 'الخبز', 'الخبز شى اساسى فى حياتنا كلنا', '1566894161.png', '7a699d2e-10a9-4811-9aa9-b5171ee48340', '2019-08-27 05:22:41', '2019-08-27 05:22:41');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(2, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(3, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(4, '2016_06_01_000004_create_oauth_clients_table', 1),
(5, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(6, '2019_07_09_072816_create_account_status_table', 1),
(7, '2019_07_09_072816_create_blog_user_table', 1),
(8, '2019_07_09_072816_create_blogs_table', 1),
(9, '2019_07_09_072816_create_catogeries_table', 1),
(10, '2019_07_09_072816_create_comments_table', 1),
(11, '2019_07_09_072816_create_contact_us_table', 1),
(12, '2019_07_09_072816_create_languages_table', 1),
(13, '2019_07_09_072816_create_measurement_times_table', 1),
(14, '2019_07_09_072816_create_medicines_table', 1),
(15, '2019_07_09_072816_create_products_table', 1),
(16, '2019_07_09_072816_create_profiles_table', 1),
(17, '2019_07_09_072816_create_readings_table', 1),
(18, '2019_07_09_072816_create_settings_table', 1),
(19, '2019_07_09_072816_create_static_pages_table', 1),
(20, '2019_07_09_072816_create_type_sugers_table', 1),
(21, '2019_07_09_072816_create_user_product_table', 1),
(22, '2019_07_09_072816_create_users_table', 1),
(23, '2019_07_09_072826_create_foreign_keys', 1),
(24, '2019_02_13_131741_create_model_has_permissions_table', 2),
(25, '2019_02_13_131742_create_model_has_roles_table', 2),
(26, '2019_02_13_131743_create_permissions_table', 2),
(27, '2019_02_13_131743_create_role_has_permissions_table', 2),
(28, '2019_02_13_131744_create_roles_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0009ea61c4b2cb8755cc0489a4509cff3887716907d903e2eb34a0b32f76438a71c7d12850c9fd9c', 96, 1, 'MyApp', '[]', 0, '2019-07-30 09:57:58', '2019-07-30 09:57:58', '2020-07-30 12:57:58'),
('024d12e60ae679875f369a5c0aa58ef43eb03c07f56b9212daef61c48870387b7db7fde05ecf89c2', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:20:15', '2019-07-29 05:20:15', '2020-07-29 08:20:15'),
('026e32a871a5577f3208595f13a41bd31af804fdc95ee74294849c2bdac0fafc5a573e24089a3f9c', 46, 1, 'MyApp', '[]', 0, '2019-07-28 09:14:23', '2019-07-28 09:14:23', '2020-07-28 12:14:23'),
('02bdd08a25ac11d491ebc9f9ef59e28e98cda1b595aef094ae3dc3525eacd4a42004d1e68ba7f0be', 99, 1, 'MyApp', '[]', 0, '2019-08-20 03:29:25', '2019-08-20 03:29:25', '2020-08-20 06:29:25'),
('0353dd35d637d43ad6f40b5a7164e7be069314c5f3e437bc61b4299c98091719a5896af5e5cadbc6', 99, 1, 'MyApp', '[]', 0, '2019-08-08 08:13:47', '2019-08-08 08:13:47', '2020-08-08 11:13:47'),
('0365cfb605b5bbdd231c107f389b0bf59bd67cfc481db180373a58343890762444adca6ff9a0defe', 95, 1, 'MyApp', '[]', 0, '2019-07-29 07:09:24', '2019-07-29 07:09:24', '2020-07-29 10:09:24'),
('036d086a37d0a280a974210812d9b33158232ab73483b037c433b59e302b1bd96a6e0128f1816af0', 49, 1, 'MyApp', '[]', 0, '2019-07-29 04:56:09', '2019-07-29 04:56:09', '2020-07-29 07:56:09'),
('03964ce352e0eb2a156d0c6e3361b00da350d87dc52e0c4a0d61c962ba0c217c3a1efca543303f21', 28, 1, 'MyApp', '[]', 0, '2019-08-20 08:50:32', '2019-08-20 08:50:32', '2020-08-20 11:50:32'),
('03ce2bfeba3c2d003518651fbd2e034bae7d3106cb5a29148c5b019d5d1562ab53d9264dac07ca86', 88, 1, 'MyApp', '[]', 0, '2019-07-29 05:52:14', '2019-07-29 05:52:14', '2020-07-29 08:52:14'),
('03f88b81fcfdbf6bcc74747fc83d70b9a77262c569e981e8fc2c564bccae9299305364a39c01596a', 128, 1, 'MyApp', '[]', 0, '2019-08-25 12:02:12', '2019-08-25 12:02:12', '2020-08-25 15:02:12'),
('04f848c48b62a165c5cb0d26e4d644220498840adcc3ba5d16433c9a2aef59564f4b0c38add76a4c', 47, 1, 'MyApp', '[]', 0, '2019-07-28 12:25:13', '2019-07-28 12:25:13', '2020-07-28 15:25:13'),
('0715bcb2c0bc01be55a76c809c3223079092192f62c8c5f28f7caddac0143325cff39cc9fb063078', 128, 1, 'MyApp', '[]', 0, '2019-08-26 04:36:01', '2019-08-26 04:36:01', '2020-08-26 07:36:01'),
('071b6c653163dd4916cfb412f1d76a1ac6faf9f91a26d1a0ed8161bbde023f083982e114971c9580', 96, 1, 'MyApp', '[]', 0, '2019-08-22 03:48:31', '2019-08-22 03:48:31', '2020-08-22 06:48:31'),
('078f5afce374c5754d2864ef0e74f81aac42c12014c56875a0df27f85070abecb7947d446c250513', 96, 1, 'MyApp', '[]', 0, '2019-08-04 07:40:28', '2019-08-04 07:40:28', '2020-08-04 10:40:28'),
('08cf9a8acadc101b88f68644761f02dd682a7879ae503f71169e129fbce667abc6a5e40beab0b7b1', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:15:36', '2019-08-19 05:15:36', '2020-08-19 08:15:36'),
('09ea63425b2677b155d39f337d1075aafc24683be4bf5ed5da25bee312cd41a3ad2f0519061efb7d', 110, 1, 'MyApp', '[]', 0, '2019-08-06 11:06:04', '2019-08-06 11:06:04', '2020-08-06 14:06:04'),
('0a8ca8778385c013c2f946ccd437781ed03b9ccd140b1d28ac537ee2ec2425faf7a51dd96a6e9aae', 96, 1, 'MyApp', '[]', 0, '2019-08-08 09:39:53', '2019-08-08 09:39:53', '2020-08-08 12:39:53'),
('0ad4394465d13c911c90a936157b9ca33626092964986684658fc14035b5a943e3e974ba8cfd1003', 49, 1, 'MyApp', '[]', 0, '2019-07-29 04:33:49', '2019-07-29 04:33:49', '2020-07-29 07:33:49'),
('0ae08cb1d84fd6dc7684c8ec204669fb0bd10e4435423a3705824d2f7eaf393f5b5287b6771e3112', 96, 1, 'MyApp', '[]', 0, '2019-08-19 05:17:40', '2019-08-19 05:17:40', '2020-08-19 08:17:40'),
('0bceefd1ff586031a43568c26699de8e88d8cfa62156695a8bbc948d75374e22c23901e2a8dec41a', 28, 1, 'MyApp', '[]', 0, '2019-08-08 05:59:12', '2019-08-08 05:59:12', '2020-08-08 08:59:12'),
('0db543d5e7804b949fab081bfaf69e41dcebd88d14054b4890fbb34552b44401b296bc16ca71fdf2', 96, 1, 'MyApp', '[]', 0, '2019-07-30 10:01:58', '2019-07-30 10:01:58', '2020-07-30 13:01:58'),
('0de08c83eee5c6cbeeef69c7e554db0765024cc502c75a4a00ebba8ad4db66b45a7121a63149cf39', 120, 1, 'MyApp', '[]', 0, '2019-08-21 12:00:40', '2019-08-21 12:00:40', '2020-08-21 15:00:40'),
('0e207424f42a28f700fcd4178571fe607725b74492b856a3235839aeb6775a15b450f0ade96e8fe4', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:14:28', '2019-07-29 05:14:28', '2020-07-29 08:14:28'),
('0f7dec2a0d0210dfb5beb477e232b3981bf6b53e75e90e6ffd495a0441d1cc525532599631f2d492', 96, 1, 'MyApp', '[]', 0, '2019-08-05 08:21:57', '2019-08-05 08:21:57', '2020-08-05 11:21:57'),
('0fea31a17d680fbdf3818b0405470d38c6292d5e371cdfe0f5fba9d4e639cc6a477fff3b58fb2d3d', 81, 1, 'MyApp', '[]', 0, '2019-07-29 05:34:12', '2019-07-29 05:34:12', '2020-07-29 08:34:12'),
('109ebdf6dbfeec6c17d6b50a32fe81cf7bc43c79f86ccb91a4aa4ef91b4ab708a6b0253c72522d35', 96, 1, 'MyApp', '[]', 0, '2019-08-20 05:06:58', '2019-08-20 05:06:58', '2020-08-20 08:06:58'),
('10bca9ca48899501c5dda9a534c65aba20f215f46d7a192fd10c11bbb2c1dd162836b35141d7c1b5', 124, 1, 'MyApp', '[]', 0, '2019-08-22 13:50:26', '2019-08-22 13:50:26', '2020-08-22 16:50:26'),
('1103c71a016fb1aa2d66504ad41d1491e43ae707c40e83b979760efa2db7edf11165923833857a94', 99, 1, 'MyApp', '[]', 0, '2019-07-31 09:47:23', '2019-07-31 09:47:23', '2020-07-31 12:47:23'),
('115b203e0b6d84f12db0c7a894529252e850b0d87dffa147e89a9f837794fd717c9055bc72321819', 99, 1, 'MyApp', '[]', 0, '2019-07-31 08:49:38', '2019-07-31 08:49:38', '2020-07-31 11:49:38'),
('115cca3f3ae78bd8340a148cf2e274902cffe5eccd938221ea9e1a274f8bd4684f862e5e0bb2dba3', 28, 1, 'MyApp', '[]', 0, '2019-08-19 07:12:52', '2019-08-19 07:12:52', '2020-08-19 10:12:52'),
('11b3442d4f362c7dddb6ae020fea3f1a0d8e7b6267457d4aa7c5bf6ed4390d91edddcc3a2d70cffc', 46, 1, 'MyApp', '[]', 0, '2019-07-28 09:24:47', '2019-07-28 09:24:47', '2020-07-28 12:24:47'),
('11d3f05d2223bbd0e0a24fc9d9ec110dab6cbf51498b53e6aa9999b1c4a7fb3faf23b214ef4d75a1', 78, 1, 'MyApp', '[]', 0, '2019-07-29 05:39:23', '2019-07-29 05:39:23', '2020-07-29 08:39:23'),
('11e5e30cac35aa89fc2401f1058fe336ea6e4c2cefcc6065cb1abc1be46b981676256a9d6c5a1875', 95, 1, 'MyApp', '[]', 0, '2019-07-29 08:59:19', '2019-07-29 08:59:19', '2020-07-29 11:59:19'),
('12aa96996a6a247c58bde7d6dc2f40286745613c0a66d9e715f8f426eac982c377a795c8d81215af', 111, 1, 'MyApp', '[]', 0, '2019-08-19 08:28:01', '2019-08-19 08:28:01', '2020-08-19 11:28:01'),
('12c07f6c6868be3205830a8c914856a4d9118a1e93bb281d60c7b6479b0e55e9df6b877c4a5e60b9', 99, 1, 'MyApp', '[]', 0, '2019-07-31 04:12:31', '2019-07-31 04:12:31', '2020-07-31 07:12:31'),
('12d0a37b3a4898dda5ddbcf6f565756f7bd2eb7578ca32d3b08745386d067baefeb63b33655a330f', 28, 1, 'MyApp', '[]', 0, '2019-08-08 06:05:05', '2019-08-08 06:05:05', '2020-08-08 09:05:05'),
('1324ff3e1cce08c4eb3e65d2007152fcfb49dab3000cdee46328ccf80da386ff7ee97103d57ef140', 96, 1, 'MyApp', '[]', 0, '2019-07-30 04:00:59', '2019-07-30 04:00:59', '2020-07-30 07:00:59'),
('132590f1145a3d4a979145c486ce193039e0868fc3559b73a074e2b0649b7580237f3a85e9b808fc', 128, 1, 'MyApp', '[]', 0, '2019-08-27 12:34:35', '2019-08-27 12:34:35', '2020-08-27 15:34:35'),
('1396cae5547e5282228cf8f12cdbada105be393de26fa9bfa3b8d1a7888d739bf08fda0637a4988d', 99, 1, 'MyApp', '[]', 0, '2019-08-05 15:25:53', '2019-08-05 15:25:53', '2020-08-05 18:25:53'),
('141d170d221e1888f5a370b119e4767e8aa44c3ee07ec81922cb829ee11e0c28068b06cdb17c564c', 96, 1, 'MyApp', '[]', 0, '2019-08-15 04:43:22', '2019-08-15 04:43:22', '2020-08-15 07:43:22'),
('1464590b01ae4fc7bee1089362d5561a635befba39bc60aba4bc89f17cc19080639a3098fff1b36a', 128, 1, 'MyApp', '[]', 0, '2019-08-25 06:59:01', '2019-08-25 06:59:01', '2020-08-25 09:59:01'),
('146548d508e9aa713e335d7d550a4392346db8b010a02c66841baaab484a61c13d739bddda38e4b5', 96, 1, 'MyApp', '[]', 0, '2019-08-21 11:48:26', '2019-08-21 11:48:26', '2020-08-21 14:48:26'),
('149ae371a972cc48932d64532f5d4df34389f42e268fd26c44928e12cb9ca50e465012d643ca3fe7', 55, 1, 'MyApp', '[]', 0, '2019-07-29 04:34:53', '2019-07-29 04:34:53', '2020-07-29 07:34:53'),
('14b4950e0592f77f52bdbe0ba0d477106e0a83dbec72a9f05a09e594eb8f4a3de695d1dde63fc905', 67, 1, 'MyApp', '[]', 0, '2019-07-29 05:26:12', '2019-07-29 05:26:12', '2020-07-29 08:26:12'),
('156c3e31d9058ac3c5e76cf860745f4e0b950fbf713c9aff664b5f792fb50eb5762ff6c08f8e72bf', 96, 1, 'MyApp', '[]', 0, '2019-08-09 14:23:05', '2019-08-09 14:23:05', '2020-08-09 17:23:05'),
('1578fd58bc8072250d8135b8638f412d2f441d0a6398a9e94070d310768f61f1259c8543f43f4ad6', 47, 1, 'MyApp', '[]', 0, '2019-07-28 11:45:44', '2019-07-28 11:45:44', '2020-07-28 14:45:44'),
('15d7c117c6cab229e1e3eee94a40413233f7b48cd24715a18e5f249c92c97198d3a7a186a51a443f', 96, 1, 'MyApp', '[]', 0, '2019-07-29 10:15:06', '2019-07-29 10:15:06', '2020-07-29 13:15:06'),
('15ebbd7fa328deefc3181156cee8b9765860b4b18f77b6eebe58981859c31da8215a574f3555c385', 93, 1, 'MyApp', '[]', 0, '2019-07-29 10:06:07', '2019-07-29 10:06:07', '2020-07-29 13:06:07'),
('160f47f5df438499f7d56931a332703aa0bf654089fb370fee553fde59bdad04a207e4cfe4ce4d8a', 100, 1, 'MyApp', '[]', 0, '2019-07-30 07:05:46', '2019-07-30 07:05:46', '2020-07-30 10:05:46'),
('1657ae14fa6c6f4a8736f1671d0e6f36df3929ba8ea569f3fcf7b4edf9104a0c218d7e417bdf1df4', 128, 1, 'MyApp', '[]', 0, '2019-08-25 11:23:21', '2019-08-25 11:23:21', '2020-08-25 14:23:21'),
('16c73ae7944b93d11c64a0b28a40e98cdeb2b5d4bd9a7fc63fb2e53c44f5f1168d20e4e452da9912', 96, 1, 'MyApp', '[]', 0, '2019-08-19 11:32:49', '2019-08-19 11:32:49', '2020-08-19 14:32:49'),
('17a75233687c8a33586aa17b44a779e2e71496c0085906702a96b618293e1c6bf9a3e242d045cb26', 99, 1, 'MyApp', '[]', 0, '2019-08-04 08:26:29', '2019-08-04 08:26:29', '2020-08-04 11:26:29'),
('19364c19e6ab80482fd150b955356031d758f39e488541b5c673dc4c808c9746d0cf9ab35a28c180', 115, 1, 'MyApp', '[]', 0, '2019-08-19 12:33:12', '2019-08-19 12:33:12', '2020-08-19 15:33:12'),
('196cf9b84544fccd7e95aa76b2edc1dca65718a52a7363143e40d32b888420e24fcc87352fae2fca', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:10:13', '2019-07-29 05:10:13', '2020-07-29 08:10:13'),
('1998bf3b8e631f1c3c3213de574efe0f7424f50b2bc8aaa75afaa3337bfd2c4784319688ef193504', 99, 1, 'MyApp', '[]', 0, '2019-08-19 07:54:25', '2019-08-19 07:54:25', '2020-08-19 10:54:25'),
('199e59946c2c5080d1f8e8a3bfb9703a857bad11ae2130469699d596285d4f6a745b4fee68283030', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:14:30', '2019-07-29 05:14:30', '2020-07-29 08:14:30'),
('19e8641cc7c0c19c039b145e4005afcfb8c9e3a9b2871925a6a80a0f323b20a47083d9d4dace62ed', 78, 1, 'MyApp', '[]', 0, '2019-07-29 05:32:19', '2019-07-29 05:32:19', '2020-07-29 08:32:19'),
('1c1e581bb235a6ed5aeebd2295841ae2a767b4a4a549a9a68b4df32cc488e7956870aa9d10d132d4', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:23:02', '2019-07-29 05:23:02', '2020-07-29 08:23:02'),
('1c2ff3e08dd13782d3363109dee14dbed226e3e6ebf28973d28ffe8cc4be9335899a486fdd972b6c', 122, 1, 'MyApp', '[]', 0, '2019-08-22 10:25:58', '2019-08-22 10:25:58', '2020-08-22 13:25:58'),
('1c49e14f3601600f22368415e27b0410aab84a19376b4842ee0705fc100df637d3d5f21209692f9d', 28, 1, 'MyApp', '[]', 0, '2019-07-30 09:13:29', '2019-07-30 09:13:29', '2020-07-30 12:13:29'),
('1c777bf8a9f65383f60258836307476833a3311601e11fd3fed0bab70d1d0511d4cfb76b4726c04b', 111, 1, 'MyApp', '[]', 0, '2019-08-19 08:07:51', '2019-08-19 08:07:51', '2020-08-19 11:07:51'),
('1dc36149fc071b9785e4c0058a948c8a3d7eb08bc46faa48a01521478d039335dac72ebab2c2e92f', 99, 1, 'MyApp', '[]', 0, '2019-08-06 14:35:54', '2019-08-06 14:35:54', '2020-08-06 17:35:54'),
('1e9d17f2cc0d628d7fbf3b1970c1dc3fa3f31fdb31e0fe345725f090e364bdeced15503c0a664c6b', 99, 1, 'MyApp', '[]', 0, '2019-08-04 07:29:54', '2019-08-04 07:29:54', '2020-08-04 10:29:54'),
('1eb7ba924c2ee8af3fbaca3b802fdf364d2b90b9d077624b69ba8c1d84f42bde809f6c21eaaf1154', 99, 1, 'MyApp', '[]', 0, '2019-07-31 18:09:25', '2019-07-31 18:09:25', '2020-07-31 21:09:25'),
('1ec24637ddfcfe3f6b4351b6f0ac1ddb045ef0cf569cdc01cd15fde384ab4308602d8471306f0eda', 65, 1, 'MyApp', '[]', 0, '2019-07-29 05:22:15', '2019-07-29 05:22:15', '2020-07-29 08:22:15'),
('1f148b5e650a80d39dea437ec504376acd131a92008d9738e439c953eb447fa68bf4cd159251d40f', 96, 1, 'MyApp', '[]', 0, '2019-07-30 09:54:27', '2019-07-30 09:54:27', '2020-07-30 12:54:27'),
('1f27275003121992cdec44ea2b1e7a75465d4a2703bf8bc98ef6c124d762d82d25b5ae481fe54d11', 96, 1, 'MyApp', '[]', 0, '2019-08-21 06:19:19', '2019-08-21 06:19:19', '2020-08-21 09:19:19'),
('1fdba6980aec7dac10c1d9864520674d08a6a5f29e9e794a936fa1924caf08db3e33791c7410ad9e', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:15:15', '2019-07-29 05:15:15', '2020-07-29 08:15:15'),
('1ff957f6648f70e4db8275f5ab71b869812b2982a72fd36d4bd944430e2215667aae3a56d4b72578', 96, 1, 'MyApp', '[]', 0, '2019-07-30 04:27:15', '2019-07-30 04:27:15', '2020-07-30 07:27:15'),
('1ff98005574c5dca0db95e4471f1460a409e8d6361bfd426ae0add4f4f622971e2357dfcac41642f', 123, 1, 'MyApp', '[]', 0, '2019-08-22 13:19:54', '2019-08-22 13:19:54', '2020-08-22 16:19:54'),
('203198225a77193719741f78430a5a77fcb4fd6e7cadfcbcdfc0a74be78dc8af9a7a9fa3f803f00d', 96, 1, 'MyApp', '[]', 0, '2019-07-30 12:32:27', '2019-07-30 12:32:27', '2020-07-30 15:32:27'),
('204d6e668194b946fa21ecfc87e7446cc1debcbf0bd04fb12d5a3ebcf7f2b8058cef03849062b407', 50, 1, 'MyApp', '[]', 0, '2019-07-28 12:53:51', '2019-07-28 12:53:51', '2020-07-28 15:53:51'),
('209471ba261b2ea4bc00aa00d7bd914ffe0254e8c55c95fc4aa19d6f33f98cf2cac297b6cbaa8fa0', 99, 1, 'MyApp', '[]', 0, '2019-07-31 09:03:32', '2019-07-31 09:03:32', '2020-07-31 12:03:32'),
('209d33ef841c6b9114169dd69b144495efffd07a0993c09f3012726713ea716c021f35314f7534ae', 129, 1, 'MyApp', '[]', 0, '2019-08-25 07:37:11', '2019-08-25 07:37:11', '2020-08-25 10:37:11'),
('21afd09c85d1c7bb655d3e99abe3e478ad1abe20999d9521248a61ce15651317d00e0bc286bc704e', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:13:04', '2019-07-29 05:13:04', '2020-07-29 08:13:04'),
('21d465411830b7d8195547c42bfff85742653e34e1f6c0ad99566cf55383618e65d4a90fe2c4eef8', 133, 1, 'MyApp', '[]', 0, '2019-08-26 08:36:36', '2019-08-26 08:36:36', '2020-08-26 11:36:36'),
('21e89d8ea3f34e6d2408e89c0f378a135431ac08dc749c64d49909c40287e9b4b18fff9b8cc2d9a1', 96, 1, 'MyApp', '[]', 0, '2019-08-19 10:49:44', '2019-08-19 10:49:44', '2020-08-19 13:49:44'),
('222e31131b082130f6be59381127711da4e3a05d5dd6f271adfb7d5d37140ab010d78f70e6ad3ace', 99, 1, 'MyApp', '[]', 0, '2019-07-31 09:36:38', '2019-07-31 09:36:38', '2020-07-31 12:36:38'),
('23bf5043ccd0615e82c143ca8e030c7512d7297c0bb75adb5aa173e05b1c577fbe47cf21d172ae9d', 94, 1, 'MyApp', '[]', 0, '2019-07-29 06:04:52', '2019-07-29 06:04:52', '2020-07-29 09:04:52'),
('2433e1286e737065b77362e12ff8a97f9a946c36c7fc9f17e4d16544bf34751b7da86d02285d5047', 111, 1, 'MyApp', '[]', 0, '2019-08-19 08:33:22', '2019-08-19 08:33:22', '2020-08-19 11:33:22'),
('24d4195b6201e08f9120976e842780882d7886f755003058f2f1674fbaa2feb088664736a93e1b41', 101, 1, 'MyApp', '[]', 0, '2019-07-30 07:00:21', '2019-07-30 07:00:21', '2020-07-30 10:00:21'),
('25d5b6efcd98ef34dd22b2b48ae1618870675ba10fbcb431c36a938595e9202256060316589ef649', 96, 1, 'MyApp', '[]', 0, '2019-08-16 05:00:10', '2019-08-16 05:00:10', '2020-08-16 08:00:10'),
('265203512d73688caea1111acc7a4b7b89dcefe3f8d2091f1517f36ea8350e0f5ab7c0551a194cec', 96, 1, 'MyApp', '[]', 0, '2019-08-19 05:13:14', '2019-08-19 05:13:14', '2020-08-19 08:13:14'),
('27f9710eb932d9365fdf972da76d9f8f3fe3e82a81be5b0b5c29ac7792d5222ed3296d4068fc83c0', 114, 1, 'MyApp', '[]', 0, '2019-08-19 09:06:15', '2019-08-19 09:06:15', '2020-08-19 12:06:15'),
('28871eca812ee142b3a0e81ddd86c1a206d86fc086aa39ffb8df09f9aa8f0a68cb7571afa50c1447', 128, 1, 'MyApp', '[]', 0, '2019-08-27 12:27:50', '2019-08-27 12:27:50', '2020-08-27 15:27:50'),
('28cb7c57d3b1de4ab15f3a60113eb50273f6586c54f404f8b968ebc5de38d190703d0d605fd01070', 96, 1, 'MyApp', '[]', 0, '2019-08-21 06:54:17', '2019-08-21 06:54:17', '2020-08-21 09:54:17'),
('29d1949fe230702e07271743f165cf86c228d0f8a125e87239ef878c0728585cc04d1409379ebbb5', 28, 1, 'MyApp', '[]', 0, '2019-08-20 08:50:35', '2019-08-20 08:50:35', '2020-08-20 11:50:35'),
('2b2307524c10c58232aab5f143349289c391c688aa3dc07919759bc9f5f259401216a5effaa93c28', 111, 1, 'MyApp', '[]', 0, '2019-08-19 10:08:31', '2019-08-19 10:08:31', '2020-08-19 13:08:31'),
('2b96710d954c22cf44b39d73c4d9ec2485de5968920f4d92508943b4dd8ce015ff2a99fcaad58526', 111, 1, 'MyApp', '[]', 0, '2019-08-19 10:08:12', '2019-08-19 10:08:12', '2020-08-19 13:08:12'),
('2bf21553d82a6228dac812dd6fc8d776b410050317c6a00203088dabe0851c44c24e5a3376e8864f', 69, 1, 'MyApp', '[]', 0, '2019-07-29 05:25:28', '2019-07-29 05:25:28', '2020-07-29 08:25:28'),
('2d501abceaeba76e03b9048d6e0c9e368c0b1ca1d95e275d4424ea677a5758dffecc92782ace037e', 96, 1, 'MyApp', '[]', 0, '2019-07-30 10:00:51', '2019-07-30 10:00:51', '2020-07-30 13:00:51'),
('2d76ae7291c244de809372bc8d338d10e5d4e24f50f721132d80a707419a4b02ee9822af516b137f', 99, 1, 'MyApp', '[]', 0, '2019-07-30 05:30:22', '2019-07-30 05:30:22', '2020-07-30 08:30:22'),
('2fc57df57d0f71d47538e8408bed9e692253c7c0f8eca786087538d77b982cbe50572dfdc63079f3', 86, 1, 'MyApp', '[]', 0, '2019-07-29 05:43:15', '2019-07-29 05:43:15', '2020-07-29 08:43:15'),
('306f937f690808e52b2a71ee3fc54c28287089a1d26afc370208875987441484b15946eb89d4c765', 28, 1, 'MyApp', '[]', 0, '2019-07-31 10:00:15', '2019-07-31 10:00:15', '2020-07-31 13:00:15'),
('30938297e778a222e907a840b15a3ea0fa062fef5734414a2023f7267514ed13b7e3a3f93a158275', 46, 1, 'MyApp', '[]', 0, '2019-07-28 09:16:04', '2019-07-28 09:16:04', '2020-07-28 12:16:04'),
('30a9341b6d137c7f8f0073cf7692524bc32b0cd16b9e549bbea733450b26fa3cc48a04a8f069aabe', 112, 1, 'MyApp', '[]', 0, '2019-08-18 12:39:54', '2019-08-18 12:39:54', '2020-08-18 15:39:54'),
('312ededb5439c765dcd3ec2ddca4e4cc260b5f2bd19b56cb71fda025c4b7504e5655400fcef83bd0', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:21:16', '2019-07-29 05:21:16', '2020-07-29 08:21:16'),
('316ea059e63f5073cf1f0c0d5daa1d49cd73b629f2e3820ff4efd687dc0809ce5efaa2e2c3cad6d6', 95, 1, 'MyApp', '[]', 0, '2019-07-29 08:11:40', '2019-07-29 08:11:40', '2020-07-29 11:11:40'),
('31714cd5689aa4606713618095557c15c4ff78303de462cb613830fbf64b2aed3e45520d6821bee9', 28, 1, 'MyApp', '[]', 0, '2019-08-20 08:51:07', '2019-08-20 08:51:07', '2020-08-20 11:51:07'),
('32cd5d42726c53b1917f895b4c47d3ef35127016cd13f0045bb845755fbeba360ebaacfc36c355dd', 128, 1, 'MyApp', '[]', 0, '2019-08-25 08:48:18', '2019-08-25 08:48:18', '2020-08-25 11:48:18'),
('33c75db411df725afcccb8e4c954958bc4af2249b8942d84d29c0329dc352425083f30bd52c2858d', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:19:34', '2019-07-29 05:19:34', '2020-07-29 08:19:34'),
('33fd67a90860aa903c99b37c6db1d1805b7dbcdd46b9821f73cc79c72c39d7afa8f3f405657c16dd', 99, 1, 'MyApp', '[]', 0, '2019-08-04 08:25:51', '2019-08-04 08:25:51', '2020-08-04 11:25:51'),
('3437325773a67d50ae5c2844b35c084c02c4a45cff596cd029992992021ff8a6adafd834b444d535', 95, 1, 'MyApp', '[]', 0, '2019-07-29 07:59:18', '2019-07-29 07:59:18', '2020-07-29 10:59:18'),
('34df09931c3dc7a58e691e9d55d987fcc87571757e28c374f924d826307e6f3ad0dde84188015a04', 45, 1, 'MyApp', '[]', 0, '2019-07-28 08:34:10', '2019-07-28 08:34:10', '2020-07-28 11:34:10'),
('355a3af3ddbff0dae7193a8ba9a4b1f349378017406fd3c292c6077b11c2e888bf4ea2974d8a122f', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:13:14', '2019-07-29 05:13:14', '2020-07-29 08:13:14'),
('3608eb4817dfae50ae383fadaa58c596031c53770fb3d0fe4016176708f22ffb38f78177acbdd216', 111, 1, 'MyApp', '[]', 0, '2019-08-19 07:47:13', '2019-08-19 07:47:13', '2020-08-19 10:47:13'),
('360b5f97e1a55c4d4029dfc695650d1af0024379d9f1eccae763367867b296124c2a4539cde25c5c', 96, 1, 'MyApp', '[]', 0, '2019-08-21 12:22:08', '2019-08-21 12:22:08', '2020-08-21 15:22:08'),
('369aba949496a09b726179b807e03320aae8ed20a5474c32bc99af4315b8ffdd87392e69f6e2de5d', 96, 1, 'MyApp', '[]', 0, '2019-08-10 12:47:30', '2019-08-10 12:47:30', '2020-08-10 15:47:30'),
('36af211c2efc16d026de2be5aa21e767d3e0c96dc1d7b62f7fd7ecf4d4167e4ec4c52592b7fb16e0', 28, 1, 'MyApp', '[]', 0, '2019-08-08 05:59:16', '2019-08-08 05:59:16', '2020-08-08 08:59:16'),
('36e4a7b5ac8d6d326fd150421eafb7e44b45cbeb46c2b172c7dc67c0a5e8bb5a1eccc2af3cd53ae3', 138, 1, 'MyApp', '[]', 0, '2019-09-02 20:01:46', '2019-09-02 20:01:46', '2020-09-02 23:01:46'),
('372faa8fc8f7166a467a9ff8d126b9217727228541bff6bda1323991902c845bf1427f5a6349bbdf', 96, 1, 'MyApp', '[]', 0, '2019-08-25 05:43:38', '2019-08-25 05:43:38', '2020-08-25 08:43:38'),
('373eab25b7715bdfbf27b53c034277bd5be15bb3c28186bf6cf5391d16e470c155c0544cc8eef21b', 28, 1, 'MyApp', '[]', 0, '2019-07-30 09:05:06', '2019-07-30 09:05:06', '2020-07-30 12:05:06'),
('3759d18f1fc94bf22783f738af9dfd6f64c5c32b318f91d2d536c31eb2a7072524c6ae5976413271', 99, 1, 'MyApp', '[]', 0, '2019-07-30 09:14:29', '2019-07-30 09:14:29', '2020-07-30 12:14:29'),
('378833fa1844088e7eeafe72d3848a055deeacf84ebc9bba4a8cc999f7251eafd633305caba65bac', 99, 1, 'MyApp', '[]', 0, '2019-08-05 17:48:17', '2019-08-05 17:48:17', '2020-08-05 20:48:17'),
('3796ef4e23094353d2839823e049e67ca926538d584e27871edb641572fff8da2e4d2ad328586f2f', 93, 1, 'MyApp', '[]', 0, '2019-07-29 06:03:52', '2019-07-29 06:03:52', '2020-07-29 09:03:52'),
('37b81b04ad8fd8b42f4ee0cc65b10adba6f2ad8f9b24daaba7b3e6d1ff172a0bb2394edb902ee146', 128, 1, 'MyApp', '[]', 0, '2019-08-26 04:17:29', '2019-08-26 04:17:29', '2020-08-26 07:17:29'),
('3858451eec5f09e93312ae942b2436eb3bdbc737f283e1cb5007d32f90ad26a5df2b82ef1c027f8d', 96, 1, 'MyApp', '[]', 0, '2019-08-25 05:37:31', '2019-08-25 05:37:31', '2020-08-25 08:37:31'),
('39ba03a8f439984dd28e473a235256d758eaa0b2641558c90a2d49994778661514985866b2fbd540', 100, 1, 'MyApp', '[]', 0, '2019-07-30 07:36:36', '2019-07-30 07:36:36', '2020-07-30 10:36:36'),
('3ada4bb55afceec213ee890566de05ffe795b402e2be71e5d6f969ca4e38d58200f3f98b6a064109', 128, 1, 'MyApp', '[]', 0, '2019-08-27 04:15:01', '2019-08-27 04:15:01', '2020-08-27 07:15:01'),
('3c2bb6d99c94355be283f228b3878bf678f381f16d859242880e20d121d24ba13e34f2e8dadfeb75', 96, 1, 'MyApp', '[]', 0, '2019-08-18 12:44:39', '2019-08-18 12:44:39', '2020-08-18 15:44:39'),
('3c9395de66c46c7efffb994ab3800734e20710955129c3ed26ffd68307e8d43a6774db5ad908e913', 119, 1, 'MyApp', '[]', 0, '2019-08-21 05:50:07', '2019-08-21 05:50:07', '2020-08-21 08:50:07'),
('3cdb0a736e09cf448d51f2d993e2765a3d8f2b0ad7184c4e61fb805bf402aa2e4528e07d3016bc58', 96, 1, 'MyApp', '[]', 0, '2019-08-19 05:17:13', '2019-08-19 05:17:13', '2020-08-19 08:17:13'),
('3d501559879219f549bf2e41a26ddf129e71aa222388249e497248b73d5364c78fa6c159a6a9e18c', 96, 1, 'MyApp', '[]', 0, '2019-08-19 09:43:07', '2019-08-19 09:43:07', '2020-08-19 12:43:07'),
('3d878c2788df8663646ce77896cb47a6cdee716172bec98914699b9f9b4e9531da56d6f56698cc6e', 99, 1, 'MyApp', '[]', 0, '2019-08-02 19:13:39', '2019-08-02 19:13:39', '2020-08-02 22:13:39'),
('3e82a9725f230b39cfb920f99e71cc4a08e6013829e8c043f21e203584aa0e9d1218ad6650467561', 67, 1, 'MyApp', '[]', 0, '2019-07-29 05:26:33', '2019-07-29 05:26:33', '2020-07-29 08:26:33'),
('3edc06c921de441f5f2e8143799edfdae7d4fff163f76a0a1058283751ee8f8dddecc8c820c0b22f', 97, 1, 'MyApp', '[]', 0, '2019-07-30 05:10:03', '2019-07-30 05:10:03', '2020-07-30 08:10:03'),
('3f3cdd58ce57e007299f821b11db7cfe94a1a84f190c7f226e83f4365b75043098a86b547235e603', 99, 1, 'MyApp', '[]', 0, '2019-08-19 07:58:31', '2019-08-19 07:58:31', '2020-08-19 10:58:31'),
('40186aca7b2b4710c5e3ed2e8404d3bb31d9b6c4d88e6d7239966f10345ea26b8c8fca87a834e377', 106, 1, 'MyApp', '[]', 0, '2019-08-03 02:49:38', '2019-08-03 02:49:38', '2020-08-03 05:49:38'),
('403e64c3c005c2d57aca382a87bb43b0b37d657a74728a0659c36dfc05478bf2e1d103f64147b513', 137, 1, 'MyApp', '[]', 0, '2019-09-02 19:43:36', '2019-09-02 19:43:36', '2020-09-02 22:43:36'),
('408daed61b687edf96d45e93f9c6c45bb3b76a55798fbae648be166a9b0f9f5bbdd490b1c376077e', 44, 1, 'MyApp', '[]', 0, '2019-07-28 08:42:42', '2019-07-28 08:42:42', '2020-07-28 11:42:42'),
('40cc94049f88ce54d0984a5c349e8ab7b91cfea319117120beb24b519b1a7b88078d024191b8ca72', 67, 1, 'MyApp', '[]', 0, '2019-07-29 05:24:28', '2019-07-29 05:24:28', '2020-07-29 08:24:28'),
('40e243cc82a921c0cdc82b33b46e39387c5880b4bcdca64fa193eb471a8dd2cf6a4fa7eb4457de90', 96, 1, 'MyApp', '[]', 0, '2019-08-22 06:54:52', '2019-08-22 06:54:52', '2020-08-22 09:54:52'),
('40f2176575778dfd0a43954186ba79d500f97ccd034172eee71c08ae0829c579b542cec4e8b9f0d2', 128, 1, 'MyApp', '[]', 0, '2019-08-25 09:03:19', '2019-08-25 09:03:19', '2020-08-25 12:03:19'),
('4224971daf318d998a0807fb19526c28dbead2645b8c5f18a5feb4cf70b1b9e36e78e70b0e6018e6', 120, 1, 'MyApp', '[]', 0, '2019-08-21 12:00:36', '2019-08-21 12:00:36', '2020-08-21 15:00:36'),
('432717793757743b20f3947a9d1c99382a4d1598018bba6f720583af35f07dbec093af1ece1e05cb', 28, 1, 'MyApp', '[]', 0, '2019-07-31 10:00:25', '2019-07-31 10:00:25', '2020-07-31 13:00:25'),
('43660a50597909d0d6706b62d84d92e9a327428750dfbebfa55e0964a0823ef4ba176912ff10426d', 96, 1, 'MyApp', '[]', 0, '2019-08-15 04:49:03', '2019-08-15 04:49:03', '2020-08-15 07:49:03'),
('43c2fe94684104165032d80aa57aa0a7d7f50c663fd6681659bad064dddfb465be5380f0dc5dd5e8', 116, 1, 'MyApp', '[]', 0, '2019-08-20 08:51:25', '2019-08-20 08:51:25', '2020-08-20 11:51:25'),
('4535596afdb4762fd30979d8c12dcc847b952d0089a9a8689d7480c1ff61d948ebf247737d698574', 115, 1, 'MyApp', '[]', 0, '2019-08-19 11:17:00', '2019-08-19 11:17:00', '2020-08-19 14:17:00'),
('45a4feaca318f5cc0cea2691f13b459231c96829e93fcd779f7c35304b1ec3e2eb16dd407ec82570', 28, 1, 'MyApp', '[]', 0, '2019-07-30 09:07:52', '2019-07-30 09:07:52', '2020-07-30 12:07:52'),
('469b38675ceffeea2280bc8b7b3da7bfab22cf099d4705368e90d99cf4b9f550f4cc1d3a970c5299', 106, 1, 'MyApp', '[]', 0, '2019-08-22 05:12:56', '2019-08-22 05:12:56', '2020-08-22 08:12:56'),
('47602a106bc7baa0f036d4ad77b636f7cc0e2f743aaf64a9923331c4d06eb68af88c1d0979a064c9', 111, 1, 'MyApp', '[]', 0, '2019-08-19 08:28:11', '2019-08-19 08:28:11', '2020-08-19 11:28:11'),
('482c1154953ecd61d9ae90159d8d5b5da274e306af2de598907710862ed2f3b7bab71bae15aef134', 78, 1, 'MyApp', '[]', 0, '2019-07-29 05:53:45', '2019-07-29 05:53:45', '2020-07-29 08:53:45'),
('49ebda4619cfc804a1cdd31b4e5702db06bfc5d945541b4252536bee57425b339320335e8e2fc1ed', 96, 1, 'MyApp', '[]', 0, '2019-08-06 03:35:54', '2019-08-06 03:35:54', '2020-08-06 06:35:54'),
('4a854c39fa35e23e99241c6b00793b85bbbfe47d0a7bf02d751ff00ae47998a6954e3043741d2451', 96, 1, 'MyApp', '[]', 0, '2019-08-20 11:05:16', '2019-08-20 11:05:16', '2020-08-20 14:05:16'),
('4b80e96a2d7e1063ce5f4d2a420d995962909bc37590d3781a77c6496e5eb2714fc3a074b16aa9bd', 99, 1, 'MyApp', '[]', 0, '2019-07-30 06:50:59', '2019-07-30 06:50:59', '2020-07-30 09:50:59'),
('4e85f0109a8926235e0862c5c5d028db932ca84debe7ec2357f36d99e46a40e2e981d286650c221e', 96, 1, 'MyApp', '[]', 0, '2019-08-20 05:40:28', '2019-08-20 05:40:28', '2020-08-20 08:40:28'),
('4ff8c77068dc6d03866ef4b2d83d640b58b88ce26af957a00a7cb0539cc9992150a28cd35dad6e46', 129, 1, 'MyApp', '[]', 0, '2019-08-25 09:05:55', '2019-08-25 09:05:55', '2020-08-25 12:05:55'),
('507f8dd820f157ef646e748b93da4a214047bd23915e70ff349a6fb6c32966c785e048016f9df683', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:10:35', '2019-08-19 05:10:35', '2020-08-19 08:10:35'),
('50afe47b69c47743a564dc42f6123871707bf1d39c3112ba0dc704db064ae7353a8133d94c68b761', 83, 1, 'MyApp', '[]', 0, '2019-07-29 05:36:51', '2019-07-29 05:36:51', '2020-07-29 08:36:51'),
('5350daf4ff2d96e913980369b454692a6e1434075577edad8735466fac0768ed5a67b77f0ed7f03b', 99, 1, 'MyApp', '[]', 0, '2019-08-08 06:20:51', '2019-08-08 06:20:51', '2020-08-08 09:20:51'),
('53521d93444270951b024cefb653abc2361791f4bf384c9a44376474e983d8eacc4edba18c7031bc', 121, 1, 'MyApp', '[]', 0, '2019-08-22 05:13:41', '2019-08-22 05:13:41', '2020-08-22 08:13:41'),
('53538f7777b43cf52261c3faab373bf95b2216c32db5003527128715cee548ccd85f90e2d6a562c2', 128, 1, 'MyApp', '[]', 0, '2019-08-27 12:04:03', '2019-08-27 12:04:03', '2020-08-27 15:04:03'),
('53c3d5d089c31731d09ebbedca44e06da725510f238292ab0d52a2c3dc6d08c036df53846b50456e', 96, 1, 'MyApp', '[]', 0, '2019-07-29 10:05:19', '2019-07-29 10:05:19', '2020-07-29 13:05:19'),
('54f152abc62afe881b7316f7d8fa25a98962782ccee580e6bf83718644b985edd6290e7a43cddf78', 99, 1, 'MyApp', '[]', 0, '2019-07-30 14:38:41', '2019-07-30 14:38:41', '2020-07-30 17:38:41'),
('5613adcabf2bcb405261a2d741670721ba5f527def1b7eb26428b0d32b5b8288f97d1211dee15f11', 96, 1, 'MyApp', '[]', 0, '2019-07-30 04:19:22', '2019-07-30 04:19:22', '2020-07-30 07:19:22'),
('572dbf8ac08929209afc3ac75d78353000e11e137b40af545343baec147a31a538a4b1c73967ba86', 68, 1, 'MyApp', '[]', 0, '2019-07-29 05:25:06', '2019-07-29 05:25:06', '2020-07-29 08:25:06'),
('57393f73d6e8eea031e443170f11223230107a81b40a306486c716b3a01f2d13880a44113a8f7678', 99, 1, 'MyApp', '[]', 0, '2019-08-15 09:58:06', '2019-08-15 09:58:06', '2020-08-15 12:58:06'),
('582dd58bc83fac2c91c01bf941279b335ebb02910dd0771ac7ea8eb374b9bbe7123262101dfbbc63', 96, 1, 'MyApp', '[]', 0, '2019-08-22 06:19:15', '2019-08-22 06:19:15', '2020-08-22 09:19:15'),
('58965d1a4504492c5b592614956fa35a81704062442976df8c8e8c134fd7a548606954508ad66d2a', 99, 1, 'MyApp', '[]', 0, '2019-08-04 08:02:27', '2019-08-04 08:02:27', '2020-08-04 11:02:27'),
('59658301caed48a578fe37519a32d33a9e5b4dd82f455b00105f461a461b005376e4b429463fa4b7', 128, 1, 'MyApp', '[]', 0, '2019-08-27 04:18:37', '2019-08-27 04:18:37', '2020-08-27 07:18:37'),
('5a6b3c30dacb9147759dc558c3eefb6ef42c55ec3cf03b0a5b6fcd8ec4829266654e1f4d49833711', 28, 1, 'MyApp', '[]', 0, '2019-08-20 07:50:23', '2019-08-20 07:50:23', '2020-08-20 10:50:23'),
('5acea71b38c80ad3364549eb17effd3b51ed304321919559f8b082dd9cc662ad12c99af9b3e68b0e', 111, 1, 'MyApp', '[]', 0, '2019-08-19 08:31:52', '2019-08-19 08:31:52', '2020-08-19 11:31:52'),
('5c73bccccf3b5e2860f7ca34026f1fb2cf142161956d960c80c5eed71b15f3214d334fee0d4b47aa', 96, 1, 'MyApp', '[]', 0, '2019-08-06 08:18:14', '2019-08-06 08:18:14', '2020-08-06 11:18:14'),
('5ce594b74fc4bf51662ca9a2f418c4d369d01a256203b2ba2d1ac74a7bafbae651d73177bba323b6', 96, 1, 'MyApp', '[]', 0, '2019-08-09 13:18:09', '2019-08-09 13:18:09', '2020-08-09 16:18:09'),
('5d34310e4b9c679713025b5dc983e95fc37da122733f8e8c096cdd8526406a82fc3bbb4be5e23263', 73, 1, 'MyApp', '[]', 0, '2019-07-29 05:27:46', '2019-07-29 05:27:46', '2020-07-29 08:27:46'),
('5ee2256d7130186c38d298fd467d82f3787e7f5c87dc55cdc3ad0e0ee0509418abf52c69d153969f', 99, 1, 'MyApp', '[]', 0, '2019-08-04 09:04:29', '2019-08-04 09:04:29', '2020-08-04 12:04:29'),
('5f18a749bd35c9585556b38f4383cd66fb27aefd229f93e16a8c5a31e5210a7ee94bc0dd254526c2', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:10:41', '2019-07-29 05:10:41', '2020-07-29 08:10:41'),
('5f22e6fe8c43c7485f513d5b5fd09a1f2096dcd95cef8ce33f40d0243f36a49425d0febe53bfcc77', 99, 1, 'MyApp', '[]', 0, '2019-08-20 07:33:33', '2019-08-20 07:33:33', '2020-08-20 10:33:33'),
('5fc237bf9a58382e87c5b33b11b6482a7c50d40063e6c5f03e401e6bdf4bfd8bd768760bc568c121', 96, 1, 'MyApp', '[]', 0, '2019-08-18 12:43:53', '2019-08-18 12:43:53', '2020-08-18 15:43:53'),
('5fc7c7bcfc4926976f1983e004195a65ed295691461389c0fc5d8d10d04845a4eb5726e0b00f5f56', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:08:33', '2019-08-19 05:08:33', '2020-08-19 08:08:33'),
('608371929f5cddab225126b841f1f80cc41c55851b871aa077b70ed849b3f3e89f55c4c0e6893c93', 28, 1, 'MyApp', '[]', 0, '2019-08-20 08:50:12', '2019-08-20 08:50:12', '2020-08-20 11:50:12'),
('60ea5d1cfc8709e133d562120b0c77e9ca3ce752035923b119be2a6af6667dc662db10bfd9647a3b', 134, 1, 'MyApp', '[]', 0, '2019-08-26 10:07:41', '2019-08-26 10:07:41', '2020-08-26 13:07:41'),
('613dd9be58b9db4802e7fcfdf2995d13ede931174440b6ae10c3e8483892cd15a11be5740f570d40', 96, 1, 'MyApp', '[]', 0, '2019-08-22 04:33:45', '2019-08-22 04:33:45', '2020-08-22 07:33:45'),
('61d259eb16e9e8b03a56057900978f386069ed965fb4de8840289fe77efe909c428c4335378aca1c', 50, 1, 'MyApp', '[]', 0, '2019-07-28 13:00:52', '2019-07-28 13:00:52', '2020-07-28 16:00:52'),
('62618597e39863e022de970856a68e78596256e03ffe810ca5ae103fe8303de5122fa1c0af850f50', 99, 1, 'MyApp', '[]', 0, '2019-08-06 16:00:20', '2019-08-06 16:00:20', '2020-08-06 19:00:20'),
('6345aa36bf8c94b025890848681c0346aff2c81275d7461667473cb65bd71c1065fffb81b58ef47b', 99, 1, 'MyApp', '[]', 0, '2019-08-08 06:05:21', '2019-08-08 06:05:21', '2020-08-08 09:05:21'),
('635e7a34e9d45a78a04a553767282a633c78f61d6532e9e9bccb9d1d0691da8257ad2ff865b88b94', 96, 1, 'MyApp', '[]', 0, '2019-08-20 04:40:02', '2019-08-20 04:40:02', '2020-08-20 07:40:02'),
('645561c2ba8b8d62e92e21c44af275ddc8d9397431a8ccf7e3c56c038004965c6876926226aeba61', 96, 1, 'MyApp', '[]', 0, '2019-08-22 14:51:45', '2019-08-22 14:51:45', '2020-08-22 17:51:45'),
('648752cb68b8b66d66eccfb35e1026a87fae9425b5737df33c4e95295722a33a3cfcc709ffb64d07', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:14:22', '2019-07-29 05:14:22', '2020-07-29 08:14:22'),
('64f98aac352b20a5a46cd1d656799b4772a0a7b7853336f95eafad14292dc1030b3766a3204d119b', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:18:02', '2019-07-29 05:18:02', '2020-07-29 08:18:02'),
('650e6decf96bfdcb6b098e3b185f6306daf29f010b8c5373cffe0b9700751f66499a73dfd2add47f', 96, 1, 'MyApp', '[]', 0, '2019-08-05 08:22:09', '2019-08-05 08:22:09', '2020-08-05 11:22:09'),
('6624ca6c4091d17b53a2af39083724787a6fbcfc3a5516f0843eff1f0b7af49a9929ea69e25f29c0', 75, 1, 'MyApp', '[]', 0, '2019-07-29 05:28:47', '2019-07-29 05:28:47', '2020-07-29 08:28:47'),
('664d19285f89077d822682177c49bcdf23836025c3601c2c3dccc4f9eb09ede8813374fd9db954dc', 99, 1, 'MyApp', '[]', 0, '2019-07-31 19:44:13', '2019-07-31 19:44:13', '2020-07-31 22:44:13'),
('67b8ebd67a1ca37e61c76af21eb80d7ccd20945d5033dd3922ec185fa803c6364c2c1bbf3490e5fe', 96, 1, 'MyApp', '[]', 0, '2019-08-20 13:34:44', '2019-08-20 13:34:44', '2020-08-20 16:34:44'),
('67e209bade89ed36bf57e6ddd2c03716ff5793af1c7478b29648d2b28f900b417d17112afb0896d7', 96, 1, 'MyApp', '[]', 0, '2019-08-22 04:30:13', '2019-08-22 04:30:13', '2020-08-22 07:30:13'),
('67e748b78afa3025757b74afa1e3fcd8098896ff9d405652b964c971d5269b660e5c3a22f2969ee0', 128, 1, 'MyApp', '[]', 0, '2019-08-25 08:57:58', '2019-08-25 08:57:58', '2020-08-25 11:57:58'),
('68666737e2038906614c0f98d251dc5cd576718e29a2dce8e54db05ebac415748f877427d2c8766a', 49, 1, 'MyApp', '[]', 0, '2019-07-29 04:51:27', '2019-07-29 04:51:27', '2020-07-29 07:51:27'),
('68e796e25868b45eb0b231b50e36f8f610331bbda05d5de71a6a01ed8ff5bd3f095abc6c8f6b3fbd', 93, 1, 'MyApp', '[]', 0, '2019-07-30 05:23:24', '2019-07-30 05:23:24', '2020-07-30 08:23:24'),
('6961f6edfc397e7adb06af7f3e17a2d7702dc7cee674822342960f37cee276a2bb67f9d06db2ee1f', 105, 1, 'MyApp', '[]', 0, '2019-07-31 04:54:06', '2019-07-31 04:54:06', '2020-07-31 07:54:06'),
('696c3e1c482fdd68ac3057d9596b1a1149ec50ea9b13698e7e6cbbc8acab7fb9c546ffa39087d87b', 51, 1, 'MyApp', '[]', 0, '2019-07-28 13:01:55', '2019-07-28 13:01:55', '2020-07-28 16:01:55'),
('6ae5e91acbfec4d31d20ce7b5c604b6c7ba7c0984faaded7e98785b6c90b57f4b7498d7d7918d3f8', 96, 1, 'MyApp', '[]', 0, '2019-08-18 09:42:33', '2019-08-18 09:42:33', '2020-08-18 12:42:33'),
('6b4ecdb200287ab5c20f80d1ec8042173fb001a70062557825771c9a0f014c5882a5659b6f847873', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:17:38', '2019-08-19 05:17:38', '2020-08-19 08:17:38'),
('6b95478e1388a5c11ce7e5ca1e8de1131e47e610dd97fcbc3f0219732ff9ac17827351f72529ba2f', 128, 1, 'MyApp', '[]', 0, '2019-08-26 07:57:02', '2019-08-26 07:57:02', '2020-08-26 10:57:02'),
('6e8333552e274d0157744619fcd8b2aee4116b4aa28e11c26db905e055ba64d569b085e0a02f61d0', 54, 1, 'MyApp', '[]', 0, '2019-07-29 04:24:18', '2019-07-29 04:24:18', '2020-07-29 07:24:18'),
('6ea5f6fb2ac6ddcec85cc65b1a21a6182f1a4140afbb10e4d9f5ba9a02a76eb30b181dc18afa97b2', 96, 1, 'MyApp', '[]', 0, '2019-08-20 05:55:08', '2019-08-20 05:55:08', '2020-08-20 08:55:08'),
('6f7a770f0b58f40b569de930c6e113fd1526cfca2c5b7e0dd447c6ead6f5de8251ef16d29454c622', 99, 1, 'MyApp', '[]', 0, '2019-08-04 08:21:12', '2019-08-04 08:21:12', '2020-08-04 11:21:12'),
('700e4f55c06432931ad77e997f3ba4d26a79ffd6089e386a50c42ff1eda7ff89984c472c71c18c93', 99, 1, 'MyApp', '[]', 0, '2019-08-19 08:00:13', '2019-08-19 08:00:13', '2020-08-19 11:00:13'),
('71cd216e3137afcd62f3aefa965b6859d86561b3ba2e3a96a2d53d335c3ff15607fdf0697c1c9610', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:20:06', '2019-07-29 05:20:06', '2020-07-29 08:20:06'),
('729940bbab67687500ca39eb7ddb4e04c562566b5c6b0d0dfec5accea29ed9ecd235d2c52bdcc6c2', 98, 1, 'MyApp', '[]', 0, '2019-07-30 04:26:52', '2019-07-30 04:26:52', '2020-07-30 07:26:52'),
('7355c6c7439bc629a12c74779beaecd45c143bfb78d1ea4db027ae0103598396fe70a596eea2f85a', 28, 1, 'MyApp', '[]', 0, '2019-08-19 07:12:09', '2019-08-19 07:12:09', '2020-08-19 10:12:09'),
('74d26371ae839e6532319d6207896f1e3caa7eeec8e3b2ba643a1eea1bca46d592941bb6d7336169', 96, 1, 'MyApp', '[]', 0, '2019-07-30 09:45:38', '2019-07-30 09:45:38', '2020-07-30 12:45:38'),
('754bfe99a7e639265dab0b55aa5e1c62e0310e646d7a68b5753c9b650a297a0a4c7d10f953eaaffd', 96, 1, 'MyApp', '[]', 0, '2019-08-20 03:43:50', '2019-08-20 03:43:50', '2020-08-20 06:43:50'),
('7571cc1d8ff3506a6c83224ad9bb1a090d056eb875e74cf867f504056daf336d4f8d6f371e9ea6c7', 99, 1, 'MyApp', '[]', 0, '2019-08-19 08:49:50', '2019-08-19 08:49:50', '2020-08-19 11:49:50'),
('765511d9d7d63292469fd04e2bca658382a95936618c6d29a241cd403384d291eaadee777dc2e232', 96, 1, 'MyApp', '[]', 0, '2019-08-08 07:01:36', '2019-08-08 07:01:36', '2020-08-08 10:01:36'),
('76d69223a8753e6548f267e6060ee33a13efa2dee6fae4998bf247afdba9b53ff38167d1b134ee0b', 128, 1, 'MyApp', '[]', 0, '2019-08-26 08:08:01', '2019-08-26 08:08:01', '2020-08-26 11:08:01'),
('784e079ab80f186553dd589646d8cb0c597cbd7d17db9401c47bccd47a3d6510ca21612655162805', 73, 1, 'MyApp', '[]', 0, '2019-07-29 05:27:16', '2019-07-29 05:27:16', '2020-07-29 08:27:16'),
('7883de266c2cb388546686fc150f7c1b9518996534aa94d34dc3beeb9ee4d9a21cf21295ff01438a', 128, 1, 'MyApp', '[]', 0, '2019-08-27 04:10:31', '2019-08-27 04:10:31', '2020-08-27 07:10:31'),
('7a9e147baea4b52e9f25b0c36a4f5b24d2a56c77d6ccc01d39f429429f0023ea9d57ff4e4346f6d7', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:18:26', '2019-07-29 05:18:26', '2020-07-29 08:18:26'),
('7ae9432a0054952d26c8a54bfbdf78e7bc294ff61dde31ff539bcd7feb2c96adf31036cfa1d5bc0f', 96, 1, 'MyApp', '[]', 0, '2019-08-23 12:32:53', '2019-08-23 12:32:53', '2020-08-23 15:32:53'),
('7afaf1365ac6b0342e09dfe212e9c6029d7d16da535500f08227acba2f74efa5fb1f38017458878e', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:18:24', '2019-07-29 05:18:24', '2020-07-29 08:18:24'),
('7b0f822eab012d840f4d0ca848018b2d53a5f8fd412fa5ade960c5478f8b93dd80ec846b2951c1e6', 96, 1, 'MyApp', '[]', 0, '2019-08-21 11:53:43', '2019-08-21 11:53:43', '2020-08-21 14:53:43'),
('7b3600e455671570e690c284ad1879184bbd4cd58f4d2238a3fa8ed3609123897cc8d8a4d6fe03c0', 128, 1, 'MyApp', '[]', 0, '2019-08-26 07:36:23', '2019-08-26 07:36:23', '2020-08-26 10:36:23'),
('7c5f868368d9f84157a5fc83089cd77e3ea8f33b9f75505e5fa8d449c2d32ac5b981f930ab282fc5', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:20:21', '2019-07-29 05:20:21', '2020-07-29 08:20:21'),
('7c9d8192698e36271b94ae83ba9925b06f9cd0352f33dffed367b75aaf3e5cfe97f2b8beb86119b6', 99, 1, 'MyApp', '[]', 0, '2019-07-31 10:00:47', '2019-07-31 10:00:47', '2020-07-31 13:00:47'),
('7ccf7bee31885ef911380f3ae7dcfb8486ff29fbfaa36beace18d7ed29af786f4d8e417c902d5df3', 100, 1, 'MyApp', '[]', 0, '2019-07-30 07:35:58', '2019-07-30 07:35:58', '2020-07-30 10:35:58'),
('7d0fcd3132f6d154312e9541f8f7fb3fad2176a9bee146273184be1c179b47bb359fd458b6e7838e', 67, 1, 'MyApp', '[]', 0, '2019-07-29 05:27:11', '2019-07-29 05:27:11', '2020-07-29 08:27:11'),
('7d59809493a0445558373ca525d3c997986cd5c7b2027de6164c796bc024811453fcb86f26ca5cee', 96, 1, 'MyApp', '[]', 0, '2019-08-19 05:15:47', '2019-08-19 05:15:47', '2020-08-19 08:15:47'),
('7d5c2ec7b57a916d11560b7120fc86b6b244ba6e5fe3ed47b3f0faad38eb57c59f92ea20805b7cd3', 139, 1, 'MyApp', '[]', 0, '2019-09-03 13:38:50', '2019-09-03 13:38:50', '2020-09-03 16:38:50'),
('7ef75470c9bde85b1081871ba9fdc340c555259e82fba60ea8c5966597ad0b7294bbc7c1bff89c3d', 128, 1, 'MyApp', '[]', 0, '2019-08-26 05:37:00', '2019-08-26 05:37:00', '2020-08-26 08:37:00'),
('801f5e8f7bafd9340696245c064222eb79f610a12a947bee7cbbdfc554d1ac7d1462a0cfe1aad08b', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:12:23', '2019-08-19 05:12:23', '2020-08-19 08:12:23'),
('810f6e09ab8d291b92c4a7192a3573fc29d2504a4929e2f18dd6e3cea5d260b7e940e7a24d40760f', 96, 1, 'MyApp', '[]', 0, '2019-08-18 09:49:59', '2019-08-18 09:49:59', '2020-08-18 12:49:59'),
('8115c4800a5a55df3740a8eb624f4d8206172620ae32ee89d02515bb8857149ed116d3c9bd42b3ae', 113, 1, 'MyApp', '[]', 0, '2019-08-18 12:44:13', '2019-08-18 12:44:13', '2020-08-18 15:44:13'),
('812880ef107cc3fe6bda1c576d16f8fa6491677f93facd962f567572fb928b40fce36c485143d219', 128, 1, 'MyApp', '[]', 0, '2019-08-25 11:46:08', '2019-08-25 11:46:08', '2020-08-25 14:46:08'),
('819fab04de9363f43e65ea36acbaa27b606253207192a36f15c4d44244bf3ef802cb0498e4dc6021', 128, 1, 'MyApp', '[]', 0, '2019-08-25 09:48:29', '2019-08-25 09:48:29', '2020-08-25 12:48:29'),
('81e34670b92fc26cd691343f5869d7394677beb1b628afd32af9c77d7369205d44cb65860d330386', 53, 1, 'MyApp', '[]', 0, '2019-07-28 13:15:27', '2019-07-28 13:15:27', '2020-07-28 16:15:27'),
('828702dc7d8f41b4e29d1feae349e19ff292a4f1824c0daef84f05b41e209cdff596ce1cb00588a4', 78, 1, 'MyApp', '[]', 0, '2019-07-29 05:49:20', '2019-07-29 05:49:20', '2020-07-29 08:49:20'),
('82a1eb863ed97886605621222403252dbf17d5326e742cfe411674bd7c5170261cfdaa4d2e4bd667', 90, 1, 'MyApp', '[]', 0, '2019-07-29 06:01:17', '2019-07-29 06:01:17', '2020-07-29 09:01:17'),
('8300cff6d48e8af3ad229fc74c6bd9ed13801df13017498bab7876f84f3406c1bd6e9682ed6de774', 96, 1, 'MyApp', '[]', 0, '2019-07-30 10:02:15', '2019-07-30 10:02:15', '2020-07-30 13:02:15'),
('837f3ba2352882f995b716d2164ba6a5a4cae0904f017cd1a953d1a5306be9e0fbf1c9d5d49cb4e3', 96, 1, 'MyApp', '[]', 0, '2019-08-22 06:16:20', '2019-08-22 06:16:20', '2020-08-22 09:16:20'),
('839467f090623589d276317e6c620ef601ca9fe5e02c4c95e269b7d94665e2bae9c1ab4aacc3bc91', 57, 1, 'MyApp', '[]', 0, '2019-07-29 05:05:24', '2019-07-29 05:05:24', '2020-07-29 08:05:24'),
('83eb4ae2798fe66ca2ccbfdc59df24c9389220f2b53d6ff3e107853513a0b247056dea0e473b62cc', 89, 1, 'MyApp', '[]', 0, '2019-07-29 06:00:24', '2019-07-29 06:00:24', '2020-07-29 09:00:24'),
('84081824fbe173009f8a63dfb5a65dc72e0e26a86e0dd6d960d1270a6021dc09a932f070f9ece55e', 28, 1, 'MyApp', '[]', 0, '2019-07-30 09:10:40', '2019-07-30 09:10:40', '2020-07-30 12:10:40'),
('84b3ca2677c2c81cda1e43bbaaab10a12ac35ef382ef7c76f52d249da7cf0c221308d311b137f2ba', 111, 1, 'MyApp', '[]', 0, '2019-08-19 08:30:55', '2019-08-19 08:30:55', '2020-08-19 11:30:55'),
('84d1bcd2cf057853786ca13c689329fd334449ab5082f3c8cf08c1645be5e884114612bb3582c7d0', 99, 1, 'MyApp', '[]', 0, '2019-07-30 09:12:47', '2019-07-30 09:12:47', '2020-07-30 12:12:47'),
('85ec9eade95da27448d4eb88ad00379307f93d9ed8cdbe3d413bd8f6d43528879f7188ee893f9588', 96, 1, 'MyApp', '[]', 0, '2019-08-10 13:32:29', '2019-08-10 13:32:29', '2020-08-10 16:32:29'),
('8695ade70b09bac967d13de6176166246983e9700413de3fad4a17d4323678dc4d188a9136bbb29c', 96, 1, 'MyApp', '[]', 0, '2019-08-07 04:00:41', '2019-08-07 04:00:41', '2020-08-07 07:00:41'),
('87713d592d338733e5d1cf5c2e14d009af05a1d8e8ad7fafa187cc095d2bb2f01f032e0a968e7338', 129, 1, 'MyApp', '[]', 0, '2019-08-25 09:12:37', '2019-08-25 09:12:37', '2020-08-25 12:12:37'),
('87d2a822474bcd486e40dc0b7bfa52cb9b332e04fc9085e7d620c4d26ac3d4baf82b6fc7b7aa00a6', 67, 1, 'MyApp', '[]', 0, '2019-07-29 05:28:49', '2019-07-29 05:28:49', '2020-07-29 08:28:49'),
('88398c0fdf84288fdfd3540a405b9a7f40f6293ec5fe16763c5507177f3d2ef28dc24f0ef27a104a', 111, 1, 'MyApp', '[]', 0, '2019-08-19 07:47:42', '2019-08-19 07:47:42', '2020-08-19 10:47:42'),
('8882bbcc02423d0596c6085bbbf553eb314933cb4172b7309eabbeb9a187db2b6003e251fdd9ba5d', 99, 1, 'MyApp', '[]', 0, '2019-08-19 07:12:57', '2019-08-19 07:12:57', '2020-08-19 10:12:57'),
('88d4f979fc170e92dd5bdcf8cf6226075fa24475c0819b1360a34bb270b4a8331af8fd81d639e538', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:11:48', '2019-08-19 05:11:48', '2020-08-19 08:11:48'),
('895e6b92123d855fa87562bb654e00ed809a76039bea98b3a589cd81e887846ad833fe5d0e9436ae', 99, 1, 'MyApp', '[]', 0, '2019-08-08 04:34:44', '2019-08-08 04:34:44', '2020-08-08 07:34:44'),
('89ce51d947f3db026734c1a0ec4e28ce0fdd660d295dab89d684c437b7be9d90bbf385594c34573c', 93, 1, 'MyApp', '[]', 0, '2019-07-30 03:59:30', '2019-07-30 03:59:30', '2020-07-30 06:59:30'),
('8a1fdd822b1a9ceb5abd7dd580bb4a8a0dd0d0907ab1e0afbe78142517255c3cc37249f49fd1825f', 74, 1, 'MyApp', '[]', 0, '2019-07-29 05:28:40', '2019-07-29 05:28:40', '2020-07-29 08:28:40'),
('8b26ecf0c2336f1e7ee2b1bca6e88244daec506062ea38568c0841282d7025dbda73213cba953f0f', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:18:48', '2019-08-19 05:18:48', '2020-08-19 08:18:48'),
('8c3e2ec28ec7e9fe00e19b486f643c0628fa487de4dfa9b1a5db8086a49aa3eeec67d86d1389f319', 72, 1, 'MyApp', '[]', 0, '2019-07-29 05:27:05', '2019-07-29 05:27:05', '2020-07-29 08:27:05'),
('8c8832d8d32486bc1b19292bb55a436f90239a27f0cdd335299ad4e073270605b8c21aa42d173607', 91, 1, 'MyApp', '[]', 0, '2019-07-29 06:02:36', '2019-07-29 06:02:36', '2020-07-29 09:02:36'),
('8deea14aca1d82e70149ed33e3c2ea25901f36579156e7044599890080f09bbe9081b0944616809d', 92, 1, 'MyApp', '[]', 0, '2019-07-29 06:03:50', '2019-07-29 06:03:50', '2020-07-29 09:03:50'),
('8e008b5294deb0297e8c9e44ad508ab8a808c16e6a5164352e946a0d5491f371ff538bb099bf8f19', 118, 1, 'MyApp', '[]', 0, '2019-08-20 16:18:16', '2019-08-20 16:18:16', '2020-08-20 19:18:16'),
('8fbd9c24215977ab8de808a24461c9e99ca49d11ab3301b098eb996298d1a0f617b7f3dd06a6a6a6', 80, 1, 'MyApp', '[]', 0, '2019-07-29 05:33:20', '2019-07-29 05:33:20', '2020-07-29 08:33:20'),
('91743d460bb68caccdfd562b31fa1037d24848ec6214a87b2bde25a2106390e0052466e1b6a700eb', 96, 1, 'MyApp', '[]', 0, '2019-08-08 09:01:18', '2019-08-08 09:01:18', '2020-08-08 12:01:18'),
('929ec37d3fa2fddee03fbca0629248644ab22f43d0e4619bb5abcd4e3295000c6ea26fedf8ed306b', 111, 1, 'MyApp', '[]', 0, '2019-08-19 08:32:56', '2019-08-19 08:32:56', '2020-08-19 11:32:56'),
('9354f243e5460c881a843dd89ae62b5a132db64730f6142d8f5f04a69fd0f0e0ebd8e048d201b152', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:09:41', '2019-08-19 05:09:41', '2020-08-19 08:09:41'),
('93e37fdae04cb79676999288092126c856ed9074dfeae4c1804184a0280efc7665ac7b2d1f005043', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:13:27', '2019-07-29 05:13:27', '2020-07-29 08:13:27'),
('93e534ec163dab9767e53602257b92562293c3bcb59731fadba29fdade40acbaf3aae3e3e8bca9bd', 96, 1, 'MyApp', '[]', 0, '2019-07-29 10:12:57', '2019-07-29 10:12:57', '2020-07-29 13:12:57'),
('9436266c4c6694d7c50733ddf8c9389806a185c664d42e8a1d21ab8cc32cb1757dccaa4bcf073c16', 78, 1, 'MyApp', '[]', 0, '2019-07-29 06:01:31', '2019-07-29 06:01:31', '2020-07-29 09:01:31'),
('946ed063e8005d7eafa8d6a051001f1ee5f1b2ce9c6118c209b42da15ad996fdec1e5a4a15bac665', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:20:13', '2019-07-29 05:20:13', '2020-07-29 08:20:13'),
('95a9df972d1906fb91b2fea9b6e8b23c249cdb0172cba7984bd5e85a32376515e7976c323f7c8317', 96, 1, 'MyApp', '[]', 0, '2019-07-30 04:00:06', '2019-07-30 04:00:06', '2020-07-30 07:00:06'),
('962c752e7255ec2b0616e530ee3a10cf72ef2a448206357d302482f55d33cac038e8cf19a4cf1166', 76, 1, 'MyApp', '[]', 0, '2019-07-29 05:30:48', '2019-07-29 05:30:48', '2020-07-29 08:30:48'),
('97c96f9c5a892de68edde7dc69aadf8d8319cc13649cb1256d42c5066861661990d033936dda1040', 130, 1, 'MyApp', '[]', 0, '2019-08-25 12:30:41', '2019-08-25 12:30:41', '2020-08-25 15:30:41'),
('9884c9f16f3a58ec2b05257825d5e945712f4ef98c4feae2db99285e7979940c512df14609ccaf6d', 99, 1, 'MyApp', '[]', 0, '2019-08-08 05:59:29', '2019-08-08 05:59:29', '2020-08-08 08:59:29'),
('98ac4065538c4fd61b745a80a727b0abeefed811eca1bdc9809bac90afb84ddcbdb4afa7061ae3e2', 99, 1, 'MyApp', '[]', 0, '2019-08-08 09:43:39', '2019-08-08 09:43:39', '2020-08-08 12:43:39'),
('98da0e90efb4a858571dc98c640c7470f504c3eae1f1711c01a7354b40acc46e784b8cfcfe4c38b6', 96, 1, 'MyApp', '[]', 0, '2019-07-30 12:29:26', '2019-07-30 12:29:26', '2020-07-30 15:29:26'),
('9932c991c912ba454d1c0bca68148314300f97e14fe78f29a1ad6de01a9573f93e1b23e3c03de5fd', 128, 1, 'MyApp', '[]', 0, '2019-08-25 08:21:32', '2019-08-25 08:21:32', '2020-08-25 11:21:32'),
('99d546670ff12f58b1ce76a1ad689f409cdb82822a2e68497f7e551fded46a3b6478b779d4f0f7fb', 28, 1, 'MyApp', '[]', 0, '2019-07-31 19:44:05', '2019-07-31 19:44:05', '2020-07-31 22:44:05'),
('9a5f464501ebd9960ae1c646b82895e5a885e2e64258327987294884647a1bafac29c903c1bbecd0', 96, 1, 'MyApp', '[]', 0, '2019-08-08 02:25:06', '2019-08-08 02:25:06', '2020-08-08 05:25:06'),
('9b8ac1e53090bfd1e99c10ef718cffb6c1edf625bf66dc80e4ffe4593fd2cb33459c499df21368ff', 99, 1, 'MyApp', '[]', 0, '2019-08-06 14:33:55', '2019-08-06 14:33:55', '2020-08-06 17:33:55'),
('9b8e37f14bb776ae8325c0c53c656d2930cf1c9a2e553fdad601f0d8b9ee0290ee981875027934fc', 96, 1, 'MyApp', '[]', 0, '2019-08-22 04:54:51', '2019-08-22 04:54:51', '2020-08-22 07:54:51'),
('9c0ba94e0298909e76e0a4e0ec9921f6f357df0e14965a0ad155ed365740d0b37bb66e12cc39f39d', 96, 1, 'MyApp', '[]', 0, '2019-08-05 11:25:22', '2019-08-05 11:25:22', '2020-08-05 14:25:22'),
('9c2e110587c3db60c4fd6de3c1187f2ec846f6ea18aca12494b4ec7ed9d9b40e1406bd2fc1002870', 87, 1, 'MyApp', '[]', 0, '2019-07-29 05:47:09', '2019-07-29 05:47:09', '2020-07-29 08:47:09'),
('9e580b17fdb50f4b5437efe31c954b09aed5fb6b29b57832e6b64f0082a4ca26bc3f8045c8aca35c', 99, 1, 'MyApp', '[]', 0, '2019-07-30 09:06:32', '2019-07-30 09:06:32', '2020-07-30 12:06:32'),
('9ef254940a82bb4631f2392dd99975e237367645400553eab6b2d60f9067c154fef4d9f4b194b310', 61, 1, 'MyApp', '[]', 0, '2019-07-29 05:17:38', '2019-07-29 05:17:38', '2020-07-29 08:17:38'),
('9f365f85e40b55358fbf4d28eaba1326059b5a25dfb5ddf69c71ae86f2d0543c06b6f598675454a8', 117, 1, 'MyApp', '[]', 0, '2019-08-20 16:12:23', '2019-08-20 16:12:23', '2020-08-20 19:12:23'),
('a04ac5304c04be8af4248a2c2b73748ad9b4d9ea6f8d01c6b282bb71c68a6a4678352a47b6134355', 84, 1, 'MyApp', '[]', 0, '2019-07-29 05:37:11', '2019-07-29 05:37:11', '2020-07-29 08:37:11'),
('a1966efa2de578bb970070fa6c1f81ce9cf3f3806c86aea4b6cb28d7e11f54c838fcdea330013bef', 113, 1, 'MyApp', '[]', 0, '2019-08-18 12:43:36', '2019-08-18 12:43:36', '2020-08-18 15:43:36'),
('a556d649e5f6285b889a72213c0d5474f8df1a21e65d23dbc2a2e2fbafd83fc3875f78a65cd54287', 128, 1, 'MyApp', '[]', 0, '2019-08-27 12:37:47', '2019-08-27 12:37:47', '2020-08-27 15:37:47'),
('a58ef25564354e09498592b74beb057643841c4ebcbc305f83c9ed315451d481cea0ff82a46655be', 79, 1, 'MyApp', '[]', 0, '2019-07-29 05:32:19', '2019-07-29 05:32:19', '2020-07-29 08:32:19'),
('a639cd93f874c89be25f4de9a3f2619150a888bf652131b7b5556396736313660cf545401a8654a5', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:03:53', '2019-08-19 05:03:53', '2020-08-19 08:03:53'),
('a7ffb98e22d7ce5295f16cd4102a9d5fdde76290b4dd28525bd8d9a4626f4185348ee5245d15731c', 113, 1, 'MyApp', '[]', 0, '2019-08-18 12:42:30', '2019-08-18 12:42:30', '2020-08-18 15:42:30');
INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('a85e07f0fe15f916d1b1fa7d4f763f270fd7ce28ba9fbd70c20c2dfb79f5c4b3b872c801c35234f2', 99, 1, 'MyApp', '[]', 0, '2019-08-05 17:38:03', '2019-08-05 17:38:03', '2020-08-05 20:38:03'),
('a870b09b74f3487c454541a70195b271ebf58673077b3b6dd6e004c3d1971150b0573b8002f850e4', 28, 1, 'MyApp', '[]', 0, '2019-08-06 14:33:48', '2019-08-06 14:33:48', '2020-08-06 17:33:48'),
('aa433fc64b55da3a0d15e617a8e7e4d825311df1fa249de04226c7bae1fbc1781ca5ebecc6cb949f', 128, 1, 'MyApp', '[]', 0, '2019-08-29 03:15:34', '2019-08-29 03:15:34', '2020-08-29 06:15:34'),
('ac7de3de1b6587a6f691b8b5d19b40c18ca448faa0b9ad3c9eb02cf63a2807384e7e8824dcd39837', 136, 1, 'MyApp', '[]', 0, '2019-09-02 13:33:34', '2019-09-02 13:33:34', '2020-09-02 16:33:34'),
('acf02c08f2fd94ccf92fe3050406dd47d6d5e5efb43470e917d39525b63957215082669c62650bc1', 96, 1, 'MyApp', '[]', 0, '2019-08-04 07:50:37', '2019-08-04 07:50:37', '2020-08-04 10:50:37'),
('ad99fa37ec6da131d3a16a6c1c1b4f6ecc684f8f841f68ec5a69c98e04281c1989a87ba819e9cdf5', 96, 1, 'MyApp', '[]', 0, '2019-08-21 12:14:51', '2019-08-21 12:14:51', '2020-08-21 15:14:51'),
('adc012e92d441872345302073d4d3e7400d622622ff6e444fb36809b9211816c9265738ab647703a', 96, 1, 'MyApp', '[]', 0, '2019-07-30 12:27:01', '2019-07-30 12:27:01', '2020-07-30 15:27:01'),
('af495597c03f71e6f99ee4fc717068ec0a598957ea868900c82786506c9c42c7bd270432557db83f', 96, 1, 'MyApp', '[]', 0, '2019-07-31 13:34:23', '2019-07-31 13:34:23', '2020-07-31 16:34:23'),
('b08bb857436549d224214734a852b208282fe010ff0bf277f7905b68ec7db92f6ebc27119c436934', 96, 1, 'MyApp', '[]', 0, '2019-08-25 06:00:17', '2019-08-25 06:00:17', '2020-08-25 09:00:17'),
('b094caff94ae44794d56bab997602ae5cb830b7a32b58b935ef8fda1a688dde8d7434a5d8db60d58', 111, 1, 'MyApp', '[]', 0, '2019-08-18 12:12:07', '2019-08-18 12:12:07', '2020-08-18 15:12:07'),
('b0d29b7732c86fcfa5a9c09dd5002d6347b8a3e90be2178302a0ee66a4f3ac1fd70493b9d833ec1f', 96, 1, 'MyApp', '[]', 0, '2019-08-07 05:50:48', '2019-08-07 05:50:48', '2020-08-07 08:50:48'),
('b19160ad3dde9a2478febd2fdc1703ddcf9a5cea4eba52dd84ccb15c1429c6350dd0f6228666e6ba', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:15:33', '2019-08-19 05:15:33', '2020-08-19 08:15:33'),
('b197c5b205e300ba90d702d2807c351d0de5559f992208b34c59ccb9279155cc722a22cf5e94b125', 111, 1, 'MyApp', '[]', 0, '2019-08-19 08:07:02', '2019-08-19 08:07:02', '2020-08-19 11:07:02'),
('b22ad14b1bd58f8496dfcad197f7f37de5dd097746e3de1caedff8bfec86952052ff01ae0708659b', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:20:19', '2019-07-29 05:20:19', '2020-07-29 08:20:19'),
('b34f1098a013830834fc6bdbda98ebc270adb208e2cccdcbd3b5b084b8c0695d54138120a7562970', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:20:08', '2019-07-29 05:20:08', '2020-07-29 08:20:08'),
('b37fa558657d5e35db5348e69acaf58ba8f17e5d3514cb0a4162f5902b8a06c63af66b04bc3b5069', 129, 1, 'MyApp', '[]', 0, '2019-08-26 07:51:20', '2019-08-26 07:51:20', '2020-08-26 10:51:20'),
('b3956a27a132af6310873ea618717b134bdf62548fb065406fd2f692da25f49e05267cbf0ac3fda9', 99, 1, 'MyApp', '[]', 0, '2019-08-04 07:23:24', '2019-08-04 07:23:24', '2020-08-04 10:23:24'),
('b5d9b15d0a6da73db064340824c93716cd5de7ab7aa2942928cc65c327375a90e6c9a9cc47a8c8d0', 100, 1, 'MyApp', '[]', 0, '2019-07-30 06:56:10', '2019-07-30 06:56:10', '2020-07-30 09:56:10'),
('b6508487d377f60e9d55e4d8db0266d454e35838c66cf93eeeacd575d3e38bd605a281f79adbd0c6', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:23:05', '2019-07-29 05:23:05', '2020-07-29 08:23:05'),
('b6d2bafc1fda167870fe9325670c5dc94bb6be69d5283e265e8619d8a7a1e8481ddb82b5c21387de', 96, 1, 'MyApp', '[]', 0, '2019-08-22 04:43:15', '2019-08-22 04:43:15', '2020-08-22 07:43:15'),
('b7493d19659920b20d16840c59e3468e938373bcd7876d49d38af4d73aa0125ee42c6db416c399c4', 99, 1, 'MyApp', '[]', 0, '2019-07-30 08:09:10', '2019-07-30 08:09:10', '2020-07-30 11:09:10'),
('b97ed08ef7346cab74025a7ead0c703aef8fa32229071ab5ac586e15077e76e1c6ea08ff57532b73', 109, 1, 'MyApp', '[]', 0, '2019-08-05 17:37:30', '2019-08-05 17:37:30', '2020-08-05 20:37:30'),
('b9b8d022e73fd142a10f39a7545aa2d5b4c5b4988d55f8ccd5eff3bd2afb4dcbe233256b4e47501b', 53, 1, 'MyApp', '[]', 0, '2019-07-28 13:15:02', '2019-07-28 13:15:02', '2020-07-28 16:15:02'),
('b9dcdf71a00cc07ba9abd2c68b6666851e6971c3cbe6a8e02d014617b5469977fa310aae711331f2', 78, 1, 'MyApp', '[]', 0, '2019-07-29 05:58:48', '2019-07-29 05:58:48', '2020-07-29 08:58:48'),
('ba22cddb6229f55ce1c067a98d5687b9c2e91f739bea77b8da86878085547d9b6660ce8df3f359e2', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:09:03', '2019-08-19 05:09:03', '2020-08-19 08:09:03'),
('bb8f2291fe7fa11c3e9d338e59e034c10af94a425cf93a73a5547d479dd47df9db7b4f0ff4b1cd6c', 128, 1, 'MyApp', '[]', 0, '2019-08-25 09:03:19', '2019-08-25 09:03:19', '2020-08-25 12:03:19'),
('bbd2040586ba0512478e6b50b866d54cf3b7b31436f0359b099df3ac745559d2b27ebd3f87f476e0', 99, 1, 'MyApp', '[]', 0, '2019-08-05 15:35:42', '2019-08-05 15:35:42', '2020-08-05 18:35:42'),
('bc412a9ac2a5819cc7e6fb8dabc019022916b502ea2f714bab667177762666e218ccfd60a9e33baf', 85, 1, 'MyApp', '[]', 0, '2019-07-29 05:39:42', '2019-07-29 05:39:42', '2020-07-29 08:39:42'),
('bc877bbfe0b40f2bb0744c2428de982cb02f16fdcb4c5772d7d854c54af410c688b42ed71a3f7b0b', 96, 1, 'MyApp', '[]', 0, '2019-07-29 09:14:57', '2019-07-29 09:14:57', '2020-07-29 12:14:57'),
('be91758f8b9aba3dd359601d611779e10f87694c56ba051e15562ea8a30fd733ccc7a8635c521e2b', 96, 1, 'MyApp', '[]', 0, '2019-08-21 12:26:17', '2019-08-21 12:26:17', '2020-08-21 15:26:17'),
('bea42791eb9e820b2e573609b50f347110f9ca121dd58a2cd39e77e8552f275c6955195a221f75b3', 116, 1, 'MyApp', '[]', 0, '2019-08-20 08:28:14', '2019-08-20 08:28:14', '2020-08-20 11:28:14'),
('beaf4063797212581fe489a228ed5acfd645ab5f8f00306206b3bc7042bcabe0dea87b0edc06ad75', 93, 1, 'MyApp', '[]', 0, '2019-07-29 06:17:51', '2019-07-29 06:17:51', '2020-07-29 09:17:51'),
('bee2ac54fc4786dd2df0a3050102e4f2a3b93de3249a873ad0a0d2dabc03a81a0dff8ad803a680bc', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:11:24', '2019-07-29 05:11:24', '2020-07-29 08:11:24'),
('bee555b12a0d0b2b75188cb9601f861e540f2c1214feec340cbe80e9b8c953d63aa0b1a0f09485d4', 52, 1, 'MyApp', '[]', 0, '2019-07-28 13:12:30', '2019-07-28 13:12:30', '2020-07-28 16:12:30'),
('bfdcb660d881a5d4e8d956fbfbebda9537c92221856bc59cdc14da2748dfd1afafc0077d8bcb68a8', 96, 1, 'MyApp', '[]', 0, '2019-08-25 05:52:21', '2019-08-25 05:52:21', '2020-08-25 08:52:21'),
('c007587366c764b86eca9ea13a7c30ea5af76190f6066845f034cb25cc98e65b02ec6bb0717cbecc', 128, 1, 'MyApp', '[]', 0, '2019-08-26 14:29:08', '2019-08-26 14:29:08', '2020-08-26 17:29:08'),
('c01e7cbefded35e22682a263978ca477371d5afc90527848d3f24c24a3f786bff9b3c1ff4892af44', 96, 1, 'MyApp', '[]', 0, '2019-08-20 11:10:15', '2019-08-20 11:10:15', '2020-08-20 14:10:15'),
('c0391bb68bf00523429fea500bcf3ef7e700b254a27efd4f6940a824fbd4823c1340be0a7efb93f2', 96, 1, 'MyApp', '[]', 0, '2019-07-31 13:34:30', '2019-07-31 13:34:30', '2020-07-31 16:34:30'),
('c1b317ca89e73bf33e31e219f7135c80a677bcb83c175789a598652ea198334684e0c4a4d8ebbefd', 96, 1, 'MyApp', '[]', 0, '2019-08-20 12:25:26', '2019-08-20 12:25:26', '2020-08-20 15:25:26'),
('c35faa73a24a8a51387b2da40c92608ee9f531b2ae0ae1220e31e0b59407b7e7059f7a5487a5dec5', 53, 1, 'MyApp', '[]', 0, '2019-07-29 03:48:05', '2019-07-29 03:48:05', '2020-07-29 06:48:05'),
('c3bf3c71e5b1860740a0e3a5674bdb9c3ad59852a97d0e53d3142eb2fe4cac1df7fd84817df86157', 96, 1, 'MyApp', '[]', 0, '2019-08-20 06:22:06', '2019-08-20 06:22:06', '2020-08-20 09:22:06'),
('c55ee5f636614dd2d0f22fbb1ce69776354eaac00d626147b5b7fd140ca65c6280e591a5184182fd', 44, 1, 'MyApp', '[]', 0, '2019-07-28 08:32:50', '2019-07-28 08:32:50', '2020-07-28 11:32:50'),
('c7174415657b98ec49701232c2daa23f80c06bedbd42eae64dcb4ec914d166d6a7bf4d712f42c051', 48, 1, 'MyApp', '[]', 0, '2019-07-28 12:35:43', '2019-07-28 12:35:43', '2020-07-28 15:35:43'),
('c75767ee0262f7229d182d99504629cab9bf43b03279ef14aff7b5122639b7b3ef41660fc8642c7d', 96, 1, 'MyApp', '[]', 0, '2019-08-04 07:38:57', '2019-08-04 07:38:57', '2020-08-04 10:38:57'),
('c7d1decf1aadba379b1d52643b52d2250e3ef2123fc90a39cbdea97e98083b6e71ca59c85b774254', 45, 1, 'MyApp', '[]', 0, '2019-07-28 08:44:29', '2019-07-28 08:44:29', '2020-07-28 11:44:29'),
('c851c0be91da925c2deb3636892443c123b927fcc5cc7466a4c65656a52e248b7a1ebe00ecbaf2e3', 96, 1, 'MyApp', '[]', 0, '2019-08-22 05:49:33', '2019-08-22 05:49:33', '2020-08-22 08:49:33'),
('c863382c4078e5bdd54fdfbc14f4f5acea6bcb56fa09e708a0416b17b9b36c04f3c65fd8cdf32bb6', 99, 1, 'MyApp', '[]', 0, '2019-08-19 15:37:13', '2019-08-19 15:37:13', '2020-08-19 18:37:13'),
('c8a23a39c8a9b3ef2940718346a1f8818dac099c2f41e2f93cb031642522b4444e7969335e5e0db6', 97, 1, 'MyApp', '[]', 0, '2019-07-29 12:32:04', '2019-07-29 12:32:04', '2020-07-29 15:32:04'),
('c8c582fb867ca90b8e44585b8191dd053f50e5b8dadd66de1bd78ac674c0a70889996f012433892a', 28, 1, 'MyApp', '[]', 0, '2019-07-30 09:08:52', '2019-07-30 09:08:52', '2020-07-30 12:08:52'),
('cab676e20c7305c32e11b3c11363285496510f1d78c225b38ebd464037eb468e20285fd1692b2b83', 135, 1, 'MyApp', '[]', 0, '2019-08-29 05:36:56', '2019-08-29 05:36:56', '2020-08-29 08:36:56'),
('cd24ae5ddab97e48e3287cd25fc45f85c5d74d9695bb4e9c948f34e0ee14e0bf44dfe38809773897', 135, 1, 'MyApp', '[]', 0, '2019-08-29 04:05:58', '2019-08-29 04:05:58', '2020-08-29 07:05:58'),
('cd36cc8df61854d1aa3815913a765c7472105148c1a0db3aeaae8d73c1360d1521361f2ff0c53680', 96, 1, 'MyApp', '[]', 0, '2019-07-30 04:17:32', '2019-07-30 04:17:32', '2020-07-30 07:17:32'),
('cde3ebeb684ec2849af3cc4269974535f4521a76344270fecd16ce15725963a6b07ed442991137c3', 102, 1, 'MyApp', '[]', 0, '2019-07-31 09:15:47', '2019-07-31 09:15:47', '2020-07-31 12:15:47'),
('ce0c5cc7562b37374221ee4547f8ad3e8970eaa56191058dea86b87685a414d3234316017a4831bb', 62, 1, 'MyApp', '[]', 0, '2019-07-29 05:19:48', '2019-07-29 05:19:48', '2020-07-29 08:19:48'),
('cfa5965f01e163943969fceda92deb9bef1c276ef2c0be4b46918b41cde72747ad8a49f4ae731f21', 115, 1, 'MyApp', '[]', 0, '2019-08-19 11:26:00', '2019-08-19 11:26:00', '2020-08-19 14:26:00'),
('d0abc41b8cb222610a2c1b6970fc8123118d8fdf1f788cd2e52f6b58804fe5f97eebca59edd06167', 28, 1, 'MyApp', '[]', 0, '2019-08-19 07:12:44', '2019-08-19 07:12:44', '2020-08-19 10:12:44'),
('d18e7ccd7f8aee89a00ca2cf3cab1f3db4153da85650d2ad6de17525f5ab499fcea07a14862a49f6', 96, 1, 'MyApp', '[]', 0, '2019-08-19 05:12:03', '2019-08-19 05:12:03', '2020-08-19 08:12:03'),
('d29ba91c739839762e70bb14f4551292546165a58885190f0c51263f5115b27c8a804116dfd1892f', 95, 1, 'MyApp', '[]', 0, '2019-07-29 07:08:53', '2019-07-29 07:08:53', '2020-07-29 10:08:53'),
('d3099fc53b5512cbeeddf8d7de3917c63d0f1a7b8e85a8bb0b69845d86ab7124b7de454ee9b3cb57', 102, 1, 'MyApp', '[]', 0, '2019-07-30 10:06:45', '2019-07-30 10:06:45', '2020-07-30 13:06:45'),
('d41c6a174a036fcebe9347a3f74d68a2bc690eec7006cd396cde2b17cbfc22ce7620c385a790a09c', 135, 1, 'MyApp', '[]', 0, '2019-08-29 04:09:42', '2019-08-29 04:09:42', '2020-08-29 07:09:42'),
('d455def9c110ab2ab4815543f31e7d0790eb131da2becdd06cc952bd79932461a12453c96bc4e4f4', 128, 1, 'MyApp', '[]', 0, '2019-08-25 08:42:26', '2019-08-25 08:42:26', '2020-08-25 11:42:26'),
('d4cb3a1e0b5844b578807bb4f84e7f7a0a44c1fbc1861dd1bbb6314e5a3423d8d3edf6eee5395a30', 120, 1, 'MyApp', '[]', 0, '2019-08-22 06:30:03', '2019-08-22 06:30:03', '2020-08-22 09:30:03'),
('d8890ea78cee373f0334edcd89bc8d8f1deb78d37a3a40764433df27bae7b55b34741e4857b300c2', 115, 1, 'MyApp', '[]', 0, '2019-08-19 11:26:30', '2019-08-19 11:26:30', '2020-08-19 14:26:30'),
('d9e2bd5beb4023d1f778d9bf9ea55a7d1671235e4e2a0a6dc44f0a00c5cf65104311b226bf861581', 93, 1, 'MyApp', '[]', 0, '2019-07-30 04:00:25', '2019-07-30 04:00:25', '2020-07-30 07:00:25'),
('db579df5d64f8a6d8d3136af6e5036f30fe771adced1f41c6b1c85375aed80f250e56f7335f1c942', 96, 1, 'MyApp', '[]', 0, '2019-07-30 12:12:14', '2019-07-30 12:12:14', '2020-07-30 15:12:14'),
('dbf92e3be33953231a647b55b7d087fcc16577a21798a1db0438ca5e10ac2069889e4da922b6c448', 28, 1, 'MyApp', '[]', 0, '2019-08-19 07:12:50', '2019-08-19 07:12:50', '2020-08-19 10:12:50'),
('dd1fca9f8b6a34d71ec8d35cee562d0918b9a6b3788752a6decfd3d40bd88a35a0f2fa0145aa09f7', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:18:28', '2019-07-29 05:18:28', '2020-07-29 08:18:28'),
('dd35571f06b3e12ccafb407e9008983080a5a18f0d70fc2cde50240b8bdef3cdf0ae89ce05a502ab', 95, 1, 'MyApp', '[]', 0, '2019-07-29 07:37:46', '2019-07-29 07:37:46', '2020-07-29 10:37:46'),
('de1b344f22f2e9ca55876dadf6f4a3a2c67551b8d93858db7dbf8df6256fe53240934ad211ee61bf', 106, 1, 'MyApp', '[]', 0, '2019-08-03 02:49:59', '2019-08-03 02:49:59', '2020-08-03 05:49:59'),
('dfa38dee4387dcb8c322ea11cb8fe3b0e3051ad9f9a52155c4245318ae0c5bbc3c61bb8aa05fdfaf', 98, 1, 'MyApp', '[]', 0, '2019-07-30 06:52:11', '2019-07-30 06:52:11', '2020-07-30 09:52:11'),
('e1832f6b3e5167fcc8c974fc6575a119699e1d58584cb11ef762d26d1e922916c930d1543664fe1c', 56, 1, 'MyApp', '[]', 0, '2019-07-29 05:00:34', '2019-07-29 05:00:34', '2020-07-29 08:00:34'),
('e29c529428876b83a624c42937b7cbfd094a2d93f895091345017590424c57470ec9d0b9141442c2', 78, 1, 'MyApp', '[]', 0, '2019-07-29 05:42:29', '2019-07-29 05:42:29', '2020-07-29 08:42:29'),
('e36631775a2455aea9d92b0d90e44c20d3056bb5d213592772cb7882362e36205d0078ee87d1095b', 93, 1, 'MyApp', '[]', 0, '2019-07-29 06:18:03', '2019-07-29 06:18:03', '2020-07-29 09:18:03'),
('e39fe20f1efacc8310b605635e9935464cc7ab6519395ad303cc2b9297556708fcb7013c7d2cc3dd', 49, 1, 'MyApp', '[]', 0, '2019-07-28 12:50:42', '2019-07-28 12:50:42', '2020-07-28 15:50:42'),
('e4686c1f85b98a13b112f5dd2453a53f08807987587509605d85ceef436ae4bda38acf1af7b1ccdd', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:13:23', '2019-07-29 05:13:23', '2020-07-29 08:13:23'),
('e4fe977914677cfab88aa2035231b71431eba3eeea8ac73827ac7ba295302a1053ac775102a48a05', 96, 1, 'MyApp', '[]', 0, '2019-08-05 08:20:01', '2019-08-05 08:20:01', '2020-08-05 11:20:01'),
('e5105ec0b0e26a5aace08c7af095e057aa7b75df675c0a0cf0c2433bfee00554b6e21b6cf035815a', 106, 1, 'MyApp', '[]', 0, '2019-08-03 02:49:31', '2019-08-03 02:49:31', '2020-08-03 05:49:31'),
('e720e57b6235c6b7a3a0f68faa6b87d2a36040348fee07c6d05a22156dd4ca5eab1da6fa4aa9a80f', 96, 1, 'MyApp', '[]', 0, '2019-08-19 09:42:48', '2019-08-19 09:42:48', '2020-08-19 12:42:48'),
('e747c9748c9c4100e4ccd9d1d55a845baa55f2e6b727cf59dcabc1d41461d4470b9a4e3f929b15bd', 96, 1, 'MyApp', '[]', 0, '2019-08-08 05:57:05', '2019-08-08 05:57:05', '2020-08-08 08:57:05'),
('e78e92b82dd2b4d4170126a704c664ab1e79603b2573665b896a7d114df3f4057af491bb96d917e6', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:17:26', '2019-08-19 05:17:26', '2020-08-19 08:17:26'),
('e842eb32b65479b6fa78903e1a7dc8c650ae4de3ca9d3e5807e57d8dfbb91026e6a33aa3b1b289dd', 49, 1, 'MyApp', '[]', 0, '2019-07-29 05:20:17', '2019-07-29 05:20:17', '2020-07-29 08:20:17'),
('e8f493e65bd06277f10e19b87270ea8575a67af5240f1a7f3d17aff99312ab121c4303891210a972', 129, 1, 'MyApp', '[]', 0, '2019-08-25 08:49:58', '2019-08-25 08:49:58', '2020-08-25 11:49:58'),
('ea21e558f67f12c6149f93158d0b4c4b222dded555e8b447e3f37def1285701d5223be8313588c4c', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:14:19', '2019-07-29 05:14:19', '2020-07-29 08:14:19'),
('ea4b044cbe27ed0260198a52e3f19c05e1e72360c48d46567223014cecaaee902760edaa36563561', 94, 1, 'MyApp', '[]', 0, '2019-07-29 06:03:58', '2019-07-29 06:03:58', '2020-07-29 09:03:58'),
('eaa479e333436bd1f7d5835fa402c921809df45c8f3f780ffb3351cdf403b559c2edf1798f189708', 106, 1, 'MyApp', '[]', 0, '2019-08-03 02:49:27', '2019-08-03 02:49:27', '2020-08-03 05:49:27'),
('ebf4b006c9d39262ee746eee77774715cddefc5ee4311e5f33760494ab2e8ea04582c99bf723958a', 77, 1, 'MyApp', '[]', 0, '2019-07-29 05:31:23', '2019-07-29 05:31:23', '2020-07-29 08:31:23'),
('ec5b37f81997bd24b133f6df904d63316fa5c4d3ed333447c967a85c8a16307b55a12fbe101f6c9d', 96, 1, 'MyApp', '[]', 0, '2019-08-19 05:18:36', '2019-08-19 05:18:36', '2020-08-19 08:18:36'),
('ec61071bf46f40d399a7976d7a9b3dba66508debcd499eae4308a53523f43407b6949d2f6cfd15ab', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:23:42', '2019-08-19 05:23:42', '2020-08-19 08:23:42'),
('edc132b4aec5e991ed6504dfce16de2301c8881654ff396ecb8355d6481cfba0edff6d43a6f9193d', 99, 1, 'MyApp', '[]', 0, '2019-08-04 07:16:58', '2019-08-04 07:16:58', '2020-08-04 10:16:58'),
('f0954b18152c82e644e1edd6ece06fb79bf7367b8a51a68d34f71c2309d7c19f6ee13434aea45445', 96, 1, 'MyApp', '[]', 0, '2019-08-10 12:10:57', '2019-08-10 12:10:57', '2020-08-10 15:10:57'),
('f0e48a7cf8ff2961673aeea34cab0a46507d5d6023228aeb8dbe5c7ddf163880013a57ebea21b67d', 100, 1, 'MyApp', '[]', 0, '2019-07-30 07:37:36', '2019-07-30 07:37:36', '2020-07-30 10:37:36'),
('f45fcdf3507cba8adc19e2e1e681d6bb02615155a10a600d22f9666b30dec2a78075d32831230a52', 44, 1, 'MyApp', '[]', 0, '2019-07-28 09:50:33', '2019-07-28 09:50:33', '2020-07-28 12:50:33'),
('f4f1e2111703e41138296b47ba7aa60c78b2e34234eb48547bb892bb1d28ebb5b433166fcd671ff1', 99, 1, 'MyApp', '[]', 0, '2019-08-08 17:19:56', '2019-08-08 17:19:56', '2020-08-08 20:19:56'),
('f52952ebd74b381d6ed9f75fb6c340804254be2913085447d00a61f9b679df4facf0477648fc517f', 82, 1, 'MyApp', '[]', 0, '2019-07-29 05:35:38', '2019-07-29 05:35:38', '2020-07-29 08:35:38'),
('f56d541b9ab58f2d87a0c5f6708d763f0840455b7d5c1068cd7f5f1f14fc2923088d274a4e28e4cd', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:14:17', '2019-07-29 05:14:17', '2020-07-29 08:14:17'),
('f6b0a827806d98ae31f93321d330562fc515a10beb425212835b4e0823f017a7e452ac5d0744f502', 66, 1, 'MyApp', '[]', 0, '2019-07-29 05:23:36', '2019-07-29 05:23:36', '2020-07-29 08:23:36'),
('f80db91910fc3c0725a4748af81294042d55069055facdc6f2af19a0a0222079353d6d0640b7eab2', 95, 1, 'MyApp', '[]', 0, '2019-07-29 07:56:08', '2019-07-29 07:56:08', '2020-07-29 10:56:08'),
('f9b870aa66aa7f8338bc6177d188396bc3a0475a14140b1e7972ee41ffc4a97353c82f9dbca60f89', 93, 1, 'MyApp', '[]', 0, '2019-07-29 12:30:52', '2019-07-29 12:30:52', '2020-07-29 15:30:52'),
('f9e4c4d47166c0896bad260bf9979b320100252dfce4c39c1592729cdf63dea9a7df7a36332a08db', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:10:08', '2019-08-19 05:10:08', '2020-08-19 08:10:08'),
('f9ed89c84d5d55621fc39478a2e8e1279456dbfd37853a2a3d11b24cef93633ed12281f9cf5c69b3', 49, 1, 'MyApp', '[]', 0, '2019-07-28 12:52:45', '2019-07-28 12:52:45', '2020-07-28 15:52:45'),
('f9fcbce5e8d816fef1ddb8f00d8f32b3dc8b0fbf9c2c8f6cf9237ac842ac57bd90f63ac91d9a8c66', 99, 1, 'MyApp', '[]', 0, '2019-07-30 09:11:29', '2019-07-30 09:11:29', '2020-07-30 12:11:29'),
('fa74423861a27ab12f7f69b09259378bd154cb0a6f637e3128639b50ceb052ffb7525bc5efdf8e2e', 99, 1, 'MyApp', '[]', 0, '2019-08-08 07:56:57', '2019-08-08 07:56:57', '2020-08-08 10:56:57'),
('fb17070c70d0a2a9ad8431e54443e17ed01a42d111256795796a87e320a8922c262321a2f7d6ac71', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:14:05', '2019-07-29 05:14:05', '2020-07-29 08:14:05'),
('fb645dcf31705206a17379a48a2f0e0a2a68f25ebf90f4541b00854d5a398f203a8dde3ff710d3a9', 99, 1, 'MyApp', '[]', 0, '2019-08-19 05:11:27', '2019-08-19 05:11:27', '2020-08-19 08:11:27'),
('fbf8764a67623134387fce256a5e4ac5537dbfadac1e00f3833979246390c981af880771471ad835', 96, 1, 'MyApp', '[]', 0, '2019-08-04 07:43:47', '2019-08-04 07:43:47', '2020-08-04 10:43:47'),
('fc13d584edc314fa32de95de8d051274db244cff4b8d2087cc4ffc888a6aab6699a9506a435aa484', 96, 1, 'MyApp', '[]', 0, '2019-07-30 12:59:19', '2019-07-30 12:59:19', '2020-07-30 15:59:19'),
('fc7475875289b7915d1136a020552957e192ba5732aa31c934be032980033e2a5b087a54349c13ec', 96, 1, 'MyApp', '[]', 0, '2019-08-19 05:13:40', '2019-08-19 05:13:40', '2020-08-19 08:13:40'),
('fd84578b8bf50d4b10263ae63092604a43715c2de82ae04adc19fd625784e08a11f57c75a54d93b2', 28, 1, 'MyApp', '[]', 0, '2019-08-08 05:59:22', '2019-08-08 05:59:22', '2020-08-08 08:59:22'),
('fddae97a0f3a0b4eeac5379a9ae5fb9debeb8fcd5df6eb8b8d5553a722865b51aec50ea453e2faf8', 128, 1, 'MyApp', '[]', 0, '2019-08-25 07:49:45', '2019-08-25 07:49:45', '2020-08-25 10:49:45'),
('ff27a6cebdd1d14e2b7b4ae0f1a6cc18cc2ada3e386b592c3e5cb8f49cb5dfcdb3443b12a33be9fa', 53, 1, 'MyApp', '[]', 0, '2019-07-29 05:14:26', '2019-07-29 05:14:26', '2020-07-29 08:14:26'),
('ffafa240a31aac5ed2ab44eee2062fc27d83e8e40560005d334278f473172926aa84381966753602', 112, 1, 'MyApp', '[]', 0, '2019-08-18 12:40:55', '2019-08-18 12:40:55', '2020-08-18 15:40:55');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'yf64A7zd5wJff6rORWQHYgNxhrZs9crfjBDthbX4', 'http://localhost', 1, 0, 0, '2019-07-18 09:00:31', '2019-07-18 09:00:31'),
(2, NULL, 'Laravel Password Grant Client', 'WGOAb9A9cEJv49OaHpyOjO4S1gntm1q5KTqNTy4m', 'http://localhost', 0, 1, 0, '2019-07-18 09:00:31', '2019-07-18 09:00:31');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-07-18 09:00:31', '2019-07-18 09:00:31');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderBy` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `catogery_id` int(10) UNSIGNED NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carbonydrate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `protine` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calories` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fiber` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `catogery_id`, `image`, `name`, `weight`, `carbonydrate`, `protine`, `calories`, `fiber`, `created_at`, `updated_at`) VALUES
(123, 31, '1566916636.jpg', '{\"ar\":\"جبن بارميزان مبشور\",\"en\":\"Grated Parmesan Cheese\"}', '5', '1', '2', '2', '1', '2019-08-27 14:37:16', '2019-08-27 11:37:16'),
(124, 31, '1566916499.jpg', '{\"ar\":\"جبن تشيدر\",\"en\":\"cheddar cheese\"}', '28', '4', '7', '7', '9', '2019-08-27 14:34:59', '2019-08-27 11:34:59'),
(125, 31, '1566916381.jpg', '{\"ar\":\"جبن حلوم\",\"en\":\"Halloumi Cheese\"}', '100', '8', '16', '16', '29', '2019-08-27 14:33:01', '2019-08-27 11:33:01'),
(127, 31, '1566915946.jpg', '{\"ar\":\"جبن حليب الماعز لين\",\"en\":\"Soft goat milk cheese\"}', '28', '1', '28', '28', '75', '2019-08-27 14:25:46', '2019-08-27 11:25:46'),
(128, 31, '1566915805.jpg', '{\"ar\":\"جبن فيتا\",\"en\":\"Feta cheese\"}', '28', '6', '4', '4', '73', '2019-08-27 14:23:25', '2019-08-27 11:23:25'),
(129, 31, '1566915737.jpg', '{\"ar\":\"جبن كريم\",\"en\":\"Cream cheese\"}', '15', '5', '1', '1', '51', '2019-08-27 14:22:17', '2019-08-27 11:22:17'),
(130, 31, '1566915578.jpg', '{\"ar\":\"جبن كريم خالي الدسم\",\"en\":\"Cream cheese skim\"}', '28', '1', '5', '5', '29', '2019-08-27 14:19:38', '2019-08-27 11:19:38'),
(136, 31, '1566915516.jpg', '{\"ar\":\"جبن موزاريلا خالي الدسم\",\"en\":\"Skim Mozzarella\"}', '28', '0', '9', '9', '42', '2019-08-27 14:18:36', '2019-08-27 11:18:36'),
(137, 31, '1566915447.jpg', '{\"ar\":\"حليب بالفراولة\",\"en\":\"Strawberry Milk\"}', '28', '8', '7', '7', '29.4', '2019-08-27 14:17:27', '2019-08-27 11:17:27'),
(138, 31, '1566915289.jpg', '{\"ar\":\"حليب بودرة\",\"en\":\"Milk Powder\"}', '32', '8', '8', '8', '158', '2019-08-27 14:14:49', '2019-08-27 11:14:49'),
(139, 32, '1566915212.jpg', '{\"ar\":\"أومليت\",\"en\":\"Omelet\"}', '61', '7', '6', '6', '95', '2019-08-27 14:13:33', '2019-08-27 11:13:33'),
(140, 32, '1566915071.jpg', '{\"ar\":\"بياض البيض\",\"en\":\"Egg whites\"}', '33', '1', '4', '4', '15', '2019-08-27 14:11:11', '2019-08-27 11:11:11'),
(141, 32, '1566915010.jpg', '{\"ar\":\"بيض أوز\",\"en\":\"Eggs of goose\"}', '144', '19', '20', '20', '266', '2019-08-27 14:10:10', '2019-08-27 11:10:10'),
(142, 32, '1566914899.jpg', '{\"ar\":\"بيض الرومي\",\"en\":\"Eggs turkey\"}', '79', '9', '10', '10', '135', '2019-08-27 14:08:19', '2019-08-27 11:08:19'),
(143, 32, '1566913262.jpg', '{\"ar\":\"بيض السمن\",\"en\":\"Ghee eggs\"}', '9', '1', '1', '1', '14', '2019-08-27 13:41:02', '2019-08-27 10:41:02'),
(144, 32, '1566913183.jpg', '{\"ar\":\"بيض بط\",\"en\":\"Duck eggs\"}', '70', '9', '9', '9', '129', '2019-08-27 13:39:43', '2019-08-27 10:39:43'),
(145, 32, '1566913054.PNG', '{\"ar\":\"بيض دجاج\",\"en\":\"chicken egg\"}', '50', '5', '6', '6', '71', '2019-08-27 13:37:34', '2019-08-27 10:37:34'),
(146, 32, '1566912977.jpg', '{\"ar\":\"بيض مجفف\",\"en\":\"Dried eggs\"}', '5', '2', '2', '2', '30', '2019-08-27 13:36:17', '2019-08-27 10:36:17'),
(147, 32, '1566912888.jpg', '{\"ar\":\"بيض مخفوق مع الحليب\",\"en\":\"Scrambled eggs with milk\"}', '61', '7', '6', '6', '101', '2019-08-27 13:34:48', '2019-08-27 10:34:48'),
(148, 32, '1566912801.jpg', '{\"ar\":\"بيض مسلوق\",\"en\":\"Poached eggs\"}', '50', '5', '6', '6', '77', '2019-08-27 13:35:11', '2019-08-27 10:35:11'),
(149, 32, '1566912755.jpg', '{\"ar\":\"بيض مقلي\",\"en\":\"Fried Eggs\"}', '46', '7', '6', '6', '90', '2019-08-27 13:32:35', '2019-08-27 10:32:35'),
(150, 33, '1566912675.jpg', '{\"ar\":\"بط مشوي بدون الجلد\",\"en\":\"Grilled duck without skin\"}', '85', '10', '20', '20', '170', '2019-08-27 13:31:15', '2019-08-27 10:31:15'),
(151, 33, '1566912569.jpg', '{\"ar\":\"بط مشوي مع الجلد\",\"en\":\"Roast duck with skin\"}', '85', '24', '16', '16', '286', '2019-08-27 13:29:29', '2019-08-27 10:29:29'),
(152, 33, '1566912294.jpg', '{\"ar\":\"حمام\",\"en\":\"Pigeon\"}', '100', '4', '21', '21', '117', '2019-08-27 13:24:54', '2019-08-27 10:24:54'),
(153, 33, '1566912500.jpg', '{\"ar\":\"دجاج\",\"en\":\"chickens\"}', '85', '2', '8', '8', '101', '2019-08-27 13:28:20', '2019-08-27 10:28:20'),
(154, 33, '1566912145.jpg', '{\"ar\":\"دجاج معلب\",\"en\":\"Canned Chicken\"}', '142', '11', '30', '30', '234', '2019-08-27 13:22:25', '2019-08-27 10:22:25'),
(155, 33, '1566912012.jpg', '{\"ar\":\"ربيان\",\"en\":\"Shrimp\"}', '100', '1', '19', '19', '87', '2019-08-27 13:20:12', '2019-08-27 10:20:12'),
(156, 33, '1566911336.jpg', '{\"ar\":\"ربيان مقلي\",\"en\":\"Fried Shrimp\"}', '85', '10', '19', '19', '205', '2019-08-27 13:08:56', '2019-08-27 10:08:56'),
(157, 33, '1566911268.jpg', '{\"ar\":\"سجق\",\"en\":\"sausage\"}', '57', '16', '6', '6', '179', '2019-08-27 13:07:48', '2019-08-27 10:07:48'),
(158, 33, '1566911051.jpg', '{\"ar\":\"سلمون معلب\",\"en\":\"Canned salmon\"}', '85', '6', '17', '17', '130', '2019-08-27 13:04:11', '2019-08-27 10:04:11'),
(159, 33, '1566910953.jpg', '{\"ar\":\"سمان مطبوخ\",\"en\":\"Cooked quail\"}', '85', '12', '21', '21', '198', '2019-08-27 13:02:33', '2019-08-27 10:02:33'),
(160, 19, '1566910861.jpg', '{\"ar\":\"سمك الحمراء\",\"en\":\"Red thickness\"}', '85', '1', '22', '22', '108', '2019-08-27 13:01:01', '2019-08-27 10:01:01'),
(161, 33, '1566910765.jpg', '{\"ar\":\"سمك تونا معلب\",\"en\":\"Canned tuna\"}', '85', '1', '22', '22', '98', '2019-08-27 12:59:25', '2019-08-27 09:59:25'),
(162, 33, '1566910005.jpg', '{\"ar\":\"صدر دجاج بروستد\",\"en\":\"Broasted chicken breast\"}', '85', '3', '26', '26', '140', '2019-08-27 12:46:45', '2019-08-27 09:46:45'),
(163, 33, '1566909706.jpg', '{\"ar\":\"كبد دجاج\",\"en\":\"Chicken liver\"}', '85', '4', '20', '20', '133', '2019-08-27 12:41:46', '2019-08-27 09:41:46'),
(164, 33, '1566909333.jpg', '{\"ar\":\"لحم بقر مسلوق (مطحون)\",\"en\":\"Boiled Beef (Ground)\"}', '85', '17', '20', '20', '245', '2019-08-27 12:35:33', '2019-08-27 09:35:33'),
(166, 33, '1566909267.jpg', '{\"ar\":\"لحم بقر مقلي (مطحون)\",\"en\":\"Fried Beef (Ground)\"}', '85', '19.1', '20', '20', '260', '2019-08-27 12:34:27', '2019-08-27 09:34:27'),
(167, 35, '1566909184.jpg', '{\"ar\":\"جوز الهند مجفف\",\"en\":\"Dried coconut\"}', '45', '15', '1', '1', '159', '2019-08-27 12:33:04', '2019-08-27 09:33:04'),
(168, 35, '1566909118.jpg', '{\"ar\":\"فستق مجفف\",\"en\":\"Dried pistachios\"}', '28', '12', '5', '5', '156', '2019-08-27 12:31:58', '2019-08-27 09:31:58'),
(169, 35, '1566909031.jpg', '{\"ar\":\"فول الصويا محمص\",\"en\":\"Roasted soybeans\"}', '172', '43', '60', '60', '815', '2019-08-27 12:30:31', '2019-08-27 09:30:31'),
(170, 35, '1566908960.jpg', '{\"ar\":\"لوز (بيذان)\",\"en\":\"Almonds\"}', '28', '13', '5', '5', '161', '2019-08-27 12:29:20', '2019-08-27 09:29:20'),
(171, 35, '1566908877.jpg', '{\"ar\":\"مكسرات الذرة\",\"en\":\"Corn nuts\"}', '28', '4', '2', '2', '145', '2019-08-27 12:27:57', '2019-08-27 09:27:57'),
(172, 35, '1566908775.jpg', '{\"ar\":\"مكسرات مشكلة محمصة\",\"en\":\"Mixed roasted nuts\"}', '28', '14', '4', '4', '166', '2019-08-27 12:26:15', '2019-08-27 09:26:15'),
(173, 35, '1566908659.jpg', '{\"ar\":\"مكسرات مشكلة محمصة بالزيت\",\"en\":\"Mixed roasted nuts with oil\"}', '28', '15', '4', '4', '172', '2019-08-27 12:26:46', '2019-08-27 09:26:46'),
(174, 36, '1566908139.jpg', '{\"ar\":\"فول الصويا محمص مجفف\",\"en\":\"Roasted soybean dryer\"}', '28', '6', '11', '11', '126', '2019-08-27 12:15:39', '2019-08-27 09:15:39'),
(176, 36, '1566907559.jpg', '{\"ar\":\"إلبة\",\"en\":\"Elbe\"}', '100', '3', '4', '4', '118', '2019-08-27 12:05:59', '2019-08-27 09:05:59'),
(178, 36, '1566907463.jpg', '{\"ar\":\"برجر اللحم\",\"en\":\"Beef Burger\"}', '100', '4', '10', '10', '176', '2019-08-27 12:04:23', '2019-08-27 09:04:23'),
(179, 36, '1566907219.jpg', '{\"ar\":\"برياني دجاج\",\"en\":\"Chicken Biryani\"}', '100', '2', '15', '15', '141', '2019-08-27 12:00:19', '2019-08-27 09:00:19'),
(180, 36, '1566907029.jpg', '{\"ar\":\"برياني لحم\",\"en\":\"Beef Biryani\"}', '100', '2', '17', '17', '142', '2019-08-27 11:57:09', '2019-08-27 08:57:09'),
(181, 36, '1566906956.jpg', '{\"ar\":\"بطاطس مقلية\",\"en\":\"French fries\"}', '100', '5', '2', '2', '183', '2019-08-27 11:55:56', '2019-08-27 08:55:56'),
(182, 36, '1566903100.jpg', '{\"ar\":\"بلاليط\",\"en\":\"Balalit\"}', '100', '6', '5', '5', '199', '2019-08-27 10:51:40', '2019-08-27 07:51:40'),
(183, 36, '1566906295.jpg', '{\"ar\":\"شوارما الدجاج\",\"en\":\"Chicken Shawarma\"}', '100', '5', '20', '20', '224', '2019-08-27 11:44:55', '2019-08-27 08:44:55'),
(184, 36, '1566906452.jpg', '{\"ar\":\"شوارما اللحم\",\"en\":\"Beef Shawarma\"}', '100', '6', '20', '20', '227', '2019-08-27 11:47:32', '2019-08-27 08:47:32'),
(185, 36, '1566906781.jpg', '{\"ar\":\"كاري الدجاج\",\"en\":\"Chicken Curry\"}', '100', '1', '9', '9', '84', '2019-08-27 11:53:01', '2019-08-27 08:53:01'),
(186, 19, '1566904828.jpg', '{\"ar\":\"كباب\",\"en\":\"kebab\"}', '100', '12', '8', '8', '281', '2019-08-27 11:20:28', '2019-08-27 08:20:28'),
(187, 19, '1566902829.jpg', '{\"ar\":\"مشمش\",\"en\":\"apricot\"}', '100', '11.12', '2', '2', '2', '2019-09-02 22:59:53', '2019-09-02 19:59:53'),
(188, 19, '1566902764.jpg', '{\"ar\":\"مانجو\",\"en\":\"mango\"}', '100', '1', '1', '1', '143', '2019-08-27 10:46:04', '2019-08-27 07:46:04'),
(189, 19, '1566902445.jpg', '{\"ar\":\"موز\",\"en\":\"banana\"}', '118', '4', '4', '4', '105', '2019-08-27 10:40:45', '2019-08-27 07:40:45'),
(190, 19, '1566902392.jpg', '{\"ar\":\"تفاح\",\"en\":\"apple\"}', '138', '1', '1', '1', '71', '2019-08-27 10:39:52', '2019-08-27 07:39:52'),
(191, 19, '1566902326.jpg', '{\"ar\":\"تمر هندي\",\"en\":\"tamarind\"}', '120', '1', '3', '3', '286', '2019-08-27 10:38:46', '2019-08-27 07:38:46'),
(192, 19, '1566902068.jpg', '{\"ar\":\"توت\",\"en\":\"blueberry\"}', '123', '1', '1', '1', '64', '2019-08-27 10:34:28', '2019-08-27 07:34:28'),
(193, 19, '1566901968.jpg', '{\"ar\":\"جوافة\",\"en\":\"guava\"}', '90', '1', '2', '2', '61', '2019-08-27 10:32:48', '2019-08-27 07:32:48'),
(194, 19, '1566901680.jpg', '{\"ar\":\"فراولة\",\"en\":\"Strawberries\"}', '144', '1', '1', '1', '46', '2019-08-27 10:28:01', '2019-08-27 07:28:01'),
(195, 19, '1566899672.jpg', '{\"ar\":\"ليمون\",\"en\":\"Lemon\"}', '58', '1', '1', '1', '16', '2019-08-27 09:54:32', '2019-08-27 06:54:32'),
(196, 19, '1566899164.jpg', '{\"ar\":\"افوكادو\",\"en\":\"avocado\"}', '146', '21', '2', '2', '233', '2019-08-27 09:46:04', '2019-08-27 06:46:04'),
(197, 19, '1566898163.jpg', '{\"ar\":\"أناناس\",\"en\":\"Pineapple\"}', '155', '1', '1', '1', '77', '2019-08-27 09:29:23', '2019-08-27 06:29:23'),
(198, 19, '1566898075.jpg', '{\"ar\":\"البرتقال\",\"en\":\"Orange\"}', '131', '1', '1', '1', '61', '2019-08-27 09:27:55', '2019-08-27 06:27:55'),
(199, 34, '1566897971.jpg', '{\"ar\":\"كريم مبيض القهوة (خفيف)\",\"en\":\"Coffee Creamer (Light)\"}', '15', '2', '1', '1', '29', '2019-08-27 09:26:11', '2019-08-27 06:26:11'),
(200, 34, '1566897853.jpg', '{\"ar\":\"كريمة بودرة\",\"en\":\"Cream powder\"}', '6', '2', '1', '1', '32', '2019-08-27 09:24:13', '2019-08-27 06:24:13'),
(201, 34, '1566897679.jpg', '{\"ar\":\"كريمة حامضة خالية الدهن\",\"en\":\"Sour cream fat-free\"}', '30', '0', '1', '1', '22', '2019-08-27 09:21:19', '2019-08-27 06:21:19'),
(202, 34, '1566896337.jpg', '{\"ar\":\"كريمة سائلة (خفيفة)\",\"en\":\"Liquid cream (light)\"}', '30', '1', '1', '1', '21', '2019-08-27 08:58:57', '2019-08-27 05:58:57'),
(203, 34, '1566896198.jpg', '{\"ar\":\"كريمة حامضة قليلة الدهن\",\"en\":\"Low fat sour cream\"}', '30', '4', '2', '2', '54', '2019-08-27 08:56:38', '2019-08-27 05:56:38'),
(205, 34, '1566896102.jpg', '{\"ar\":\"كريمة بودرة قليلة الدسم\",\"en\":\"Cream low-fat powder\"}', '6', '1', '1', '1', '25', '2019-08-27 08:55:02', '2019-08-27 05:55:02'),
(206, 37, '1566895319.jpg', '{\"ar\":\"شاي بدون سكر\",\"en\":\"Tea without sugar\"}', '236', '0', '0', '0', '1', '2019-08-27 08:41:59', '2019-08-27 05:41:59'),
(207, 37, '1566895235.jpg', '{\"ar\":\"شراب بنكهة الفواكه\",\"en\":\"Fruit flavored syrup\"}', '192', '3', '2', '2', '264', '2019-08-27 08:40:35', '2019-08-27 05:40:35'),
(208, 37, '1566895117.jpg', '{\"ar\":\"شراب عنب\",\"en\":\"Grape syrup\"}', '200', '0', '0', '0', '112', '2019-08-27 08:38:37', '2019-08-27 05:38:37'),
(209, 37, '1566895044.jpg', '{\"ar\":\"عصير أناناس\",\"en\":\"Pineapple juice\"}', '200', '1', '1', '1', '140', '2019-08-27 08:37:24', '2019-08-27 05:37:24'),
(210, 37, '1566894981.jpg', '{\"ar\":\"عصير تفاح غير محلى\",\"en\":\"Unsweetened apple juice\"}', '200', '1', '1', '1', '116', '2019-08-27 08:36:21', '2019-08-27 05:36:21'),
(211, 37, '1566894910.jpg', '{\"ar\":\"عصير جزر\",\"en\":\"Carrot juice\"}', '200', '1', '2', '2', '98', '2019-08-27 08:35:10', '2019-08-27 05:35:10'),
(212, 37, '1566894854.jpg', '{\"ar\":\"عصير جوافة\",\"en\":\"guava juice\"}', '200', '0', '0', '0', '87', '2019-08-27 08:34:14', '2019-08-27 05:34:14'),
(213, 37, '1566894759.jpg', '{\"ar\":\"عصير طماطم\",\"en\":\"tomato juice\"}', '200', '1', '2', '2', '41', '2019-08-27 08:32:39', '2019-08-27 05:32:39'),
(214, 20, '1566894694.jpg', '{\"ar\":\"البازلا الخضراء\",\"en\":\"Green Peas\"}', '160', '1', '8', '8', '134', '2019-08-27 08:31:34', '2019-08-27 05:31:34'),
(215, 20, '1566894587.jpg', '{\"ar\":\"البطاطا الحلوة\",\"en\":\"sweet potato\"}', '130', '1', '2', '2', '111', '2019-08-27 08:29:47', '2019-08-27 05:29:47'),
(216, 20, '1566894485.jpg', '{\"ar\":\"البقدونس\",\"en\":\"parsley\"}', '10', '1', '1', '1', '4', '2019-08-27 08:28:05', '2019-08-27 05:28:05'),
(217, 20, '1566894416.jpg', '{\"ar\":\"الترمس\",\"en\":\"Thermoses\"}', '166', '4', '25', '25', '125', '2019-08-27 08:26:56', '2019-08-27 05:26:56'),
(218, 20, '1566894341.jpg', '{\"ar\":\"الجرجير\",\"en\":\"Watercress\"}', '34', '0', '1', '1', '3', '2019-08-27 08:25:41', '2019-08-27 05:25:41'),
(219, 20, '1566894278.jpg', '{\"ar\":\"السبانخ\",\"en\":\"Spinach\"}', '30', '1', '1', '1', '6', '2019-08-27 08:24:38', '2019-08-27 05:24:38'),
(220, 20, '1566894203.jpg', '{\"ar\":\"الفاصولياء\",\"en\":\"Beans\"}', '177', '15', '1', '1', '224.8', '2019-08-27 08:23:23', '2019-08-27 05:23:23'),
(221, 20, '1566894096.jpg', '{\"ar\":\"القرنبيط\",\"en\":\"Cauliflower\"}', '100', '1', '2', '2', '25', '2019-08-27 08:21:36', '2019-08-27 05:21:36'),
(222, 20, '1566894005.jpg', '{\"ar\":\"بطاطس\",\"en\":\"Potatos\"}', '200', '1', '4', '4', '164', '2019-08-27 08:20:05', '2019-08-27 05:20:05'),
(228, 19, '1567585159.jpg', '{\"ar\":\"اسماس\",\"en\":\"asmaa\"}', '1', '0', '0', '0', '0', '2019-09-04 08:19:20', '2019-09-04 05:19:20'),
(232, 19, '1567585800.png', '{\"ar\":\"اسماس\",\"en\":\"asmaa\"}', '20.25', '20.365', '20.258784', '20.258784', '2.019', '2019-09-04 08:30:01', '2019-09-04 05:30:01');

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE `profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `acount_status_id` int(10) UNSIGNED DEFAULT NULL,
  `type_sugers_id` int(10) UNSIGNED DEFAULT NULL,
  `phone` varchar(29) CHARACTER SET utf8 NOT NULL,
  `age` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `gender` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `height` int(11) NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` int(11) NOT NULL,
  `date_injury` date DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `acount_status_id`, `type_sugers_id`, `phone`, `age`, `user_id`, `gender`, `height`, `image`, `weight`, `date_injury`, `created_at`, `updated_at`) VALUES
(93, NULL, NULL, '01021111', 111, 127, '1111', 51, '1566727791.png', 201, NULL, '2019-08-25 06:34:35', '2019-08-25 07:09:51'),
(94, 10, 14, '01141751523', 66, 128, '1', 167, NULL, 100, '2016-02-28', '2019-08-25 06:59:01', '2019-08-27 12:03:11'),
(95, 9, 13, '01066666666', 55, 129, '1', 154, NULL, 100, '2019-01-01', '2019-08-25 07:37:11', '2019-08-25 07:37:11'),
(96, 7, 13, '05556989999', 25, 130, '1', 186, NULL, 97, '2019-01-01', '2019-08-25 12:30:41', '2019-08-25 12:30:41'),
(99, 11, 16, '01001113433', 33, 133, '1', 174, 'uploads/1566820836febdac.png', 83, '2019-01-01', '2019-08-26 08:36:36', '2019-08-26 09:00:36'),
(100, 7, 13, '05788889980', 24, 134, '1', 182, NULL, 95, '2019-01-01', '2019-08-26 10:07:41', '2019-08-26 10:07:41'),
(101, 7, 13, '09088599998', 25, 135, '2', 167, NULL, 58, '2009-08-01', '2019-08-29 04:05:58', '2019-08-29 04:05:58'),
(102, 7, 13, '01555448889', 25, 136, '2', 150, NULL, 95, '2019-01-01', '2019-09-02 13:33:34', '2019-09-02 13:33:34'),
(103, 7, 16, '12345678910', 55, 137, '1', 150, 'uploads/1567465544fcdbae.png', 100, '2019-01-01', '2019-09-02 19:43:36', '2019-09-02 20:05:54'),
(104, 7, 13, '0055898998', 23, 138, '1', 184, NULL, 96, '2019-01-01', '2019-09-02 20:01:46', '2019-09-02 20:01:46'),
(105, 10, 13, '01023966383', 18, 139, '1', 150, NULL, 120, '2019-01-01', '2019-09-03 13:38:50', '2019-09-03 13:38:50');

-- --------------------------------------------------------

--
-- Table structure for table `readings`
--

CREATE TABLE `readings` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `measurement_id` int(10) UNSIGNED NOT NULL,
  `time` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `count` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `readings`
--

INSERT INTO `readings` (`id`, `user_id`, `measurement_id`, `time`, `count`, `created_at`, `updated_at`) VALUES
(145, 129, 11, '2019-08-25 14:33:00', '150 ملغم / حل', '2019-08-25 09:33:23', '2019-08-25 09:33:23'),
(146, 129, 11, '2019-08-25 14:33:00', '150 ملغم / حل', '2019-08-25 09:35:59', '2019-08-25 09:35:59'),
(147, 129, 11, '2019-08-25 15:54:00', '231 ملغم / حل', '2019-08-25 10:55:04', '2019-08-25 10:55:04'),
(148, 130, 11, '2019-08-25 17:32:00', '232 ملغم / حل', '2019-08-25 12:32:12', '2019-08-25 12:32:12'),
(149, 130, 11, '2019-08-25 17:32:00', '121 ملغم / حل', '2019-08-25 12:32:29', '2019-08-25 12:32:29'),
(150, 130, 11, '2019-08-25 17:32:00', '61 ملغم / حل', '2019-08-25 12:32:35', '2019-08-25 12:32:35'),
(151, 133, 11, '2019-08-26 15:05:00', '150 ملغم / حل', '2019-08-26 10:05:24', '2019-08-26 10:05:24'),
(152, 133, 11, '2019-08-26 15:05:00', '150 ملغم / حل', '2019-08-26 10:05:28', '2019-08-26 10:05:28'),
(153, 135, 11, '2019-08-29 07:22:00', '150 ملغم / حل', '2019-08-29 04:22:07', '2019-08-29 04:22:07'),
(154, 135, 11, '2019-08-29 07:22:00', '150 ملغم / حل', '2019-08-29 04:22:23', '2019-08-29 04:22:23'),
(155, 135, 11, '2019-08-29 07:22:00', '231 ملغم / حل', '2019-08-29 04:23:48', '2019-08-29 04:23:48'),
(156, 137, 14, '2019-09-02 16:06:00', '146 ملغم / حل', '2019-09-02 20:08:25', '2019-09-02 20:08:25'),
(157, 135, 11, '2019-09-19 09:55:00', '299 ملغم / حل', '2019-09-19 06:59:15', '2019-09-19 06:59:15'),
(158, 135, 11, '2019-09-19 09:55:00', '299 ملغم / حل', '2019-09-19 06:59:26', '2019-09-19 06:59:26');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` smallint(6) DEFAULT NULL,
  `module` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `orderby` smallint(6) DEFAULT NULL,
  `updated_at` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `slug`, `name`, `value`, `type`, `module`, `orderby`, `updated_at`) VALUES
(1, 'FaceBook/Cicrapplication', 'Twitter/Cicrapplicat', 'Cicr_Email@email.com', 0, '+0012345689990', 0, '2019-08-29 08:11:22');

-- --------------------------------------------------------

--
-- Table structure for table `static_pages`
--

CREATE TABLE `static_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `text` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `static_pages`
--

INSERT INTO `static_pages` (`id`, `name`, `title`, `text`, `image`, `created_at`, `updated_at`) VALUES
(3, '{\"ar\":\"الشروط والاحكام\",\"en\":\" Terms and conditions\"}', '{\"ar\":\"مقال عن الاحكام\",\"en\":\"An article on medicine\"}', '{\"ar\":\"ستخدم الدواء حسب الإرشادات وفي الوقت المناسب وأكمل المدة اللازمة والمحددة لاستخدامه ولا توقفه عند الشعور بتحسن الأعراض.\\r\\nعند استخدام الأدوية اللاوصفية, اتبع التعليمات المكتوبة على ملصق علبة الدواء إلا إذا أرشدك مقدم الرعاية الصحية إلى غير ذلك.\\r\\nعندما تشعر أن الدواء لا يعمل بالشكل المطلوب, ابلغ مقدم الرعاية الصحية بذلك.\\r\\nيجب عليك ألا تخلط الأدوية مع بعضها في علبة واحدة. ويفضل حفظ الأدوية محكمة الغلق في علبتها الأصلية عند عدم استخدامها.\\r\\nلا تزل الملصق من علبة الدواء لأن إرشادات الاستخدام ومعلومات أخرى مهمة مكتوبة عليه.\\r\\nلتجنب الأخطاء, لاتأخذ الدواء في الظلام, واقرأ دائما الإرشادات قبل تناول الدواء واضعا في الحسبان مدة صلاحيته.\",\"en\":\"Use the medicine as directed and in a timely manner.\\r\\nWhen using over-the-counter medications, follow the instructions\\r\\nWhen you feel that the medicine is not working as required.\\r\\nYou should not confuse medicines and prefer to save medicines.\\r\\nDo not remove the label.\\r\\nAnd mistakes, do not take medicine in the dark, always vomit instructions\"}', '1565074485.jpg', NULL, '2019-08-06 03:54:45'),
(4, '{\"ar\":\"عن التطبيق\",\"en\":\"about_us\"}', '{\"ar\":\"عن التطبيق\",\"en\":\"An article on medicine\"}', '{\"ar\":\"هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.\",\"en\":\"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s\"}', '1567064421.png', '2019-08-01 21:00:00', '2019-08-29 04:40:21'),
(5, '{\"ar\":\"تغذيه السكري\",\"en\":\"diabetes feds\"}', '{\"ar\":\"مقال عن الاحكام\",\"en\":\"An article on medicine\"}', '{\"ar\":\"ستخدم الدواء حسب الإرشادات وفي الوقت المناسب وأكمل المدة اللازمة والمحددة لاستخدامه ولا توقفه عند الشعور بتحسن الأعراض.\\r\\nعند استخدام الأدوية اللاوصفية, اتبع التعليمات المكتوبة على ملصق علبة الدواء إلا إذا أرشدك مقدم الرعاية الصحية إلى غير ذلك.\\r\\nعندما تشعر أن الدواء لا يعمل بالشكل المطلوب, ابلغ مقدم الرعاية الصحية بذلك.\\r\\nيجب عليك ألا تخلط الأدوية مع بعضها في علبة واحدة. ويفضل حفظ الأدوية محكمة الغلق في علبتها الأصلية عند عدم استخدامها.\\r\\nلا تزل الملصق من علبة الدواء لأن إرشادات الاستخدام ومعلومات أخرى مهمة مكتوبة عليه.\\r\\nلتجنب الأخطاء, لاتأخذ الدواء في الظلام, واقرأ دائما الإرشادات قبل تناول الدواء واضعا في الحسبان مدة صلاحيته.\",\"en\":\"Use the medicine as directed and in a timely manner.\\r\\nWhen using over-the-counter medications, follow the instructions\\r\\nWhen you feel that the medicine is not working as required.\\r\\nYou should not confuse medicines and prefer to save medicines.\\r\\nDo not remove the label.\\r\\nAnd mistakes, do not take medicine in the dark, always vomit instructions\"}', '1565074485.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `type_sugers`
--

CREATE TABLE `type_sugers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `type_sugers`
--

INSERT INTO `type_sugers` (`id`, `name`, `created_at`, `updated_at`) VALUES
(13, '{\"ar\":\"مستوي اول\",\"en\":\"First level\"}', '2019-07-21 07:16:19', '2019-08-25 05:48:56'),
(14, '{\"ar\":\"مستوي ثاني\",\"en\":\"Second level\"}', '2019-07-28 10:18:36', '2019-08-25 05:52:01'),
(15, '{\"ar\":\"مستوي ثالث\",\"en\":\"Third level\"}', '2019-07-28 10:19:07', '2019-08-25 05:56:43'),
(16, '{\"ar\":\"بدون\",\"en\":\"without\"}', '2019-07-28 10:19:54', '2019-08-25 05:58:33');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `name`, `password`, `remember_token`, `created_at`, `updated_at`, `code`) VALUES
(127, 'asmaa@abozied.com', 'image1', '$2y$10$Woz3HAVB1khvKO.joBmV2.0oiADJCPX6llL.LSWtOWX6RL7aJ1MYG', NULL, '2019-08-25 06:34:35', '2019-08-25 07:09:51', NULL),
(128, 'ahmedomer@maakeweb.com', 'ahmed taha', '$2y$10$3/O2CvnpPyyYerEOH5kihemhAnr9HVcUcwR8coBszSYRuzj5Kh3bq', 'QDEC3FtlvtA2jDdkAbUHShPaJrtIZ83To26ivPPdoPutt6VExBeR8KS7h0Y0', '2019-08-25 06:59:01', '2019-08-29 03:16:12', NULL),
(129, 'mohamed@gmail.com', 'mohamed', '$2y$10$teSPlfSRTCmetZwdQKXW7eJuLRoOMgt.odjVwc4oPg0kb6nPUgx5S', 'v5QQXDRwuY1gCcKSR3VzwPjI5ZDkNM9dNmVNsRRdi4t76FehmCi5f4qRHkMw', '2019-08-25 07:37:11', '2019-08-25 07:37:11', NULL),
(130, 'Gm@jaadara.com', 'a7medalfy', '$2y$10$OcNeMUKI6QpIO5G0hpzzj.kNsuiXn6AooaDQ.o0llL8WWiT7b8aWS', 'alx73DtaK1RGtk3DCnW7DTd1m5BFUfgVR0EG5bbSSrTtMOGryDkhymKm6psN', '2019-08-25 12:30:41', '2019-08-25 12:31:39', NULL),
(131, 'jaadar@gmail.com', 'nada', '$2y$10$QGRDPH8xVenQ35xuW7msMeEm5XhI0HXR/xGybizw0YbXdypupBqpm', 'WpnfNEOCqWJ6PfCUr2f3AQMxOvFMDrMtyR6FgiS5Ks2PZN1Uygob7P5cKNAP', '2019-08-26 05:00:36', '2019-08-26 05:00:36', NULL),
(132, 'jaad@gmail.com', 'nada', '$2y$10$146hCGDv0ZizP2r.E0sGVO9nRHZaDI9YkQZQqqEt7yjO5UECDa0Ou', '6fV2YPkveB3ooN9x0Am74LhZuT7XExsoD9BEatr2KEegAGXhKcStG8HpUsnW', '2019-08-26 05:01:03', '2019-08-26 05:01:03', NULL),
(133, 'm@gmail.com', 'محمد', '$2y$10$owm/n7gyea1epf0yr7zkr.IyIld3mlc5NEdmEKYDcnFI7RAsNGhCi', 'uELHk7ZjeWPuwCbX5n1kHc3WqQ1DQqcecAUE4E5ixDV9yowNeWO9WBr0U3Vl', '2019-08-26 08:36:36', '2019-08-26 09:00:36', NULL),
(134, 'Gm1@jaadara.com', 'ahmed el alfy', '$2y$10$wloLkp6lMq4lFeNKOdQhu.rcyE9UNc7F3pfXN40NYiKLDqMi9Rd8W', '6O3UG1ED3kkp2D0FATyvTNH0d3kTl25T3E2RMwNWanCKRcYrPBSbbrgZbNTJ', '2019-08-26 10:07:41', '2019-08-26 10:07:41', NULL),
(135, 'jaa@jaadara.com', 'تجربه التطبيق', '$2y$10$tFQ.qYqbE.v8kmriOFKDW.Bh9IzwvRJcWk9LoWTjXyfu.USlOCrDu', 'ydn2DLpqf9XtejVXYs5FAKCGj8opS47RPLyEgcIdF3Myhu4TIe0gePSdNw87', '2019-08-29 04:05:58', '2019-08-29 04:05:58', NULL),
(136, 'the@was.com', 'ahmed alfy', '$2y$10$qluqAO/VkS2Ddv6VlGE2c.amZ1e8F7gEl8xVOW3ZQHvlEzNHddnIy', 'yhO18Rf2hxYPYHRxo7R21e1XKfgmSuqI19ZtXMgxQkJHTZYuctnjRgUeW0AT', '2019-09-02 13:33:34', '2019-09-02 13:33:34', NULL),
(137, 'mohamed@mohamed.com', 'mohamed', '$2y$10$5sTGdPg/6IzQOmzx5uAXGu1W4gxZ1My0lYmBCV.rBFdNn6ZQoJTqq', 'VI88ZvjCRIWrkpvSIBjRf3z8EXA3pAzuLHD6yol60Bp4gO8Fx4aINyEULBV4', '2019-09-02 19:43:36', '2019-09-02 20:05:44', NULL),
(138, 'sales@jaadara.com', 'احمد الالفي', '$2y$10$5CZb4o1J9.zNElHnSm9vqutVnE..h/qosf5FF47cup7l4QzqXfw9u', 'xNs2AVawZWoAoqgji2cjgxx13SuHQaL20YhsAnizitEm65oyQdm0gv51Hwqg', '2019-09-02 20:01:46', '2019-09-02 20:01:46', NULL),
(139, 'ahmefgomaa@gamil.com', 'ahmef', '$2y$10$ZsebUV87TdHUsqGECPmqmuJh9eoPvPqDZgWjUnet15mcJZxNuGxf6', 'Cy4cdbKVWUZz6FE2qxIU8busMStUaWchWyrvNo3LgX7YGA96xLx3VnMSXAsz', '2019-09-03 13:38:50', '2019-09-03 13:38:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_product`
--

CREATE TABLE `user_product` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_product`
--

INSERT INTO `user_product` (`id`, `user_id`, `product_id`, `created_at`, `updated_at`) VALUES
(154, 129, 187, NULL, NULL),
(159, 128, 179, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_status`
--
ALTER TABLE `account_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `advertisements`
--
ALTER TABLE `advertisements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog_user`
--
ALTER TABLE `blog_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blog_user_blog_id_foreign` (`blog_id`),
  ADD KEY `blog_user_user_id_foreign` (`user_id`);

--
-- Indexes for table `catogeries`
--
ALTER TABLE `catogeries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_blog_id_foreign` (`blog_id`);

--
-- Indexes for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD PRIMARY KEY (`id`),
  ADD KEY `contact_us_user_id_foreign` (`user_id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `measurement_times`
--
ALTER TABLE `measurement_times`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medicines`
--
ALTER TABLE `medicines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messagers`
--
ALTER TABLE `messagers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_catogery_id_foreign` (`catogery_id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_acount_status_id_foreign` (`acount_status_id`),
  ADD KEY `profiles_type_sugers_id_foreign` (`type_sugers_id`);

--
-- Indexes for table `readings`
--
ALTER TABLE `readings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `readings_user_id_foreign` (`user_id`),
  ADD KEY `readings_measurement_id_foreign` (`measurement_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `type_sugers`
--
ALTER TABLE `type_sugers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_product`
--
ALTER TABLE `user_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_product_product_id_foreign` (`product_id`),
  ADD KEY `user_product_user_id_foreign` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_status`
--
ALTER TABLE `account_status`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `advertisements`
--
ALTER TABLE `advertisements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;

--
-- AUTO_INCREMENT for table `blog_user`
--
ALTER TABLE `blog_user`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `catogeries`
--
ALTER TABLE `catogeries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;

--
-- AUTO_INCREMENT for table `contact_us`
--
ALTER TABLE `contact_us`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `measurement_times`
--
ALTER TABLE `measurement_times`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `medicines`
--
ALTER TABLE `medicines`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `messagers`
--
ALTER TABLE `messagers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=234;

--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=106;

--
-- AUTO_INCREMENT for table `readings`
--
ALTER TABLE `readings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=159;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `type_sugers`
--
ALTER TABLE `type_sugers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=140;

--
-- AUTO_INCREMENT for table `user_product`
--
ALTER TABLE `user_product`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=161;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_blog_id_foreign` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contact_us`
--
ALTER TABLE `contact_us`
  ADD CONSTRAINT `contact_us_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_catogery_id_foreign` FOREIGN KEY (`catogery_id`) REFERENCES `catogeries` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_acount_status_id_foreign` FOREIGN KEY (`acount_status_id`) REFERENCES `account_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `profiles_type_sugers_id_foreign` FOREIGN KEY (`type_sugers_id`) REFERENCES `type_sugers` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `readings`
--
ALTER TABLE `readings`
  ADD CONSTRAINT `readings_measurement_id_foreign` FOREIGN KEY (`measurement_id`) REFERENCES `measurement_times` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `readings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
