<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->increments('id');
			$table->string('email');
			$table->text('name');

			$table->string('password');
			$table->string('remember_token');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('users');
	}
}