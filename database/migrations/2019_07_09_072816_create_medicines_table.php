<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMedicinesTable extends Migration {

	public function up()
	{
		Schema::create('medicines', function(Blueprint $table) {
			$table->increments('id');
			$table->string('image');
			$table->text('name');
			$table->text('text');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('medicines');
	}
}