<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAccountStatusTable extends Migration {

	public function up()
	{
		Schema::create('account_status', function(Blueprint $table) {
			$table->increments('id');
			$table->text('name');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('account_status');
	}
}