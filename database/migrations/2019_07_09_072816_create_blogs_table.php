<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBlogsTable extends Migration {

	public function up()
	{
		Schema::create('blogs', function(Blueprint $table) {
			$table->increments('id');
			$table->string('image');
			$table->text('title');
			$table->text('text');
            $table->string('type');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('blogs');
	}
}